@php
    $settings=\App\Facade\DbSettings::GetDbSetting();
    $userLogged=\App\Facade\OrganizationInfo::GetUserInfoWithoutParameter();
    $RegisteredEmail=$userLogged->RegisteredEmail;
    $UID=$userLogged->UserId;
@endphp

@section('CssFiles')
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/vendors.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/bordered-layout.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/pages/app-chat.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/pages/app-chat-list.css">
    <!-- END: Page CSS-->
    <!-- END: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/plugins/extensions/ext-component-sweet-alerts.css">
    <!-- BEGIN: Custom CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/assets/css/style.css">
    <!-- END: Custom CSS-->

@stop
@extends('Layouts.dashboardMainLayout')

@section('MainContent')



    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="app-user-view-account">
                    <div class="row">
                        <!-- User Content -->
                        <div class="col-xl-12 col-lg-12 col-md-12 order-0 order-md-1">
                            <!-- User Pills -->
                            <ul class="nav nav-pills mb-2">
                                <li class="nav-item">
                                    <a class="nav-link active" href="/AdminPanel/Support/RequestList">
                                        <i data-feather="user" class="font-medium-3 me-50"></i>
                                        <span class="fw-bold">User Request list</span></a>
                                </li>
                            </ul>
                            <!--/ User Pills -->

                            <div class="content-right">
                                <div class="content-wrapper container-xxl ">
                                    <div class="content-header row">
                                    </div>
                                    <div class="content-body">
                                        <div class="body-content-overlay"></div>
                                        <!-- Main chat area -->
                                        <section class="chat-app-window">
                                            <!-- To load Conversation -->
                                            <div class="start-chat-area">
                                                <div class="mb-1 start-chat-icon">
                                                    <i data-feather="message-square"></i>
                                                </div>
                                                <h4 class="sidebar-toggle start-chat-text">{{$Rows[0]['Title']}}</h4>
                                            </div>
                                            <!--/ To load Conversation -->

                                            <!-- Active Chat -->
                                            <div class="active-chat ">
                                                <!-- Chat Header -->
                                                <div class="chat-navbar">
                                                    <header class="chat-header">
                                                        <div class="d-flex align-items-center">
                                                            <div class="sidebar-toggle d-block d-lg-none me-1">
                                                                <i data-feather="menu" class="font-medium-5"></i>
                                                            </div>
                                                            <div class="avatar avatar-border user-profile-toggle m-0 me-1">
                                                                <img src="/Themes/Admin/app-assets/images/pages/calendar-illustration.png"
                                                                     alt="avatar" height="36" width="36"/>
                                                                <span class="avatar-status-busy"></span>
                                                            </div>
                                                            <h6 class="mb-0">Support's Team</h6>
                                                        </div>
                                                        <div class="d-flex align-items-center">

                                                        </div>
                                                    </header>
                                                </div>
                                                <!--/ Chat Header -->

                                                <!-- User Chat messages -->
                                                <div class="user-chats">
                                                    <div class="chats">
                                                        @foreach($Rows as $row)
                                                            <div class="divider">
                                                                <div class="divider-text">{{$row->created_at}}</div>
                                                            </div>
                                                            @if($row->itself)
                                                                <div class="chat">
                                                                    <div class="chat-avatar">
                                                <span class="avatar box-shadow-1 cursor-pointer">
                                                    <img src="/{{$row->Image}}"
                                                         alt="avatar" height="36" width="36"/>
                                                </span>
                                                                    </div>
                                                                    <div class="chat-body">
                                                                        <div class="chat-content">
                                                                            <p>{!! $row->Comments !!}</p></div>
                                                                        @unless(is_null($row->attachments))
                                                                            <div class="chat-content"><p><a class="a-class"
                                                                                            href="/{{$row->attachments}}"
                                                                                            target="_blank"><i data-feather='link'></i></a>
                                                                                </p></div>
                                                                        @endunless
                                                                    </div>
                                                                </div>
                                                            @else
                                                                <div class="chat chat-left">
                                                                    <div class="chat-avatar">
                                                <span class="avatar box-shadow-1 cursor-pointer">
                                                    <img src="/Themes/Admin/app-assets/images/pages/calendar-illustration.png"
                                                         alt="avatar" height="36" width="36"/>
                                                </span>
                                                                    </div>
                                                                    <div class="chat-body">
                                                                        <div class="chat-content">
                                                                            <p>{!! $row->Comments !!}</p></div>
                                                                        @unless(is_null($row->attachments))
                                                                            <div class="chat-content"><p><a class="a-class"
                                                                                                            href="/{{$row->attachments}}"
                                                                                                            target="_blank"><i data-feather='link'></i></a>
                                                                                </p></div>
                                                                        @endunless
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <!-- User Chat messages -->

                                                <!-- Submit Chat form -->
                                                <form class="chat-app-form" action="#"
                                                      method="post" enctype="multipart/form-data">

                                                    <input type="hidden" name="iptID" id="iptID" value="{{\App\MyClasses\CmsFunctions::EncodedText($id)}}">
                                                    @if($itself)
                                                        <input type="hidden" name="iptItself" id="iptItself" value="{{\App\MyClasses\CmsFunctions::EncodedText(1)}}">
                                                    @else
                                                        <input type="hidden" name="iptItself" id="iptItself" value="{{\App\MyClasses\CmsFunctions::EncodedText(0)}}">
                                                    @endif
                                                    <div class="input-group input-group-merge me-1 form-send-message">
                                                        <span class="speech-to-text input-group-text"><i
                                                                    data-feather="hexagon"
                                                                    class="cursor-pointer"></i></span>
                                                        <input type="text" class="form-control message"
                                                               placeholder="Type your message " id="iptContext" name="iptContext"/>
                                                        <span class="input-group-text">
                                            <label for="attach-doc" class="attachment-icon form-label mb-0">

                                                </label></span>
                                                    </div>
                                                    <button id="confirm-message" type="button" class="btn btn-primary send "
                                                         >
                                                        <i data-feather="send" class="d-lg-none"></i>
                                                        <span class="d-none d-lg-block">Send</span>
                                                    </button>
                                                </form>
                                                <!--/ Submit Chat form -->
                                            </div>
                                            <!--/ Active Chat -->
                                        </section>
                                        <!--/ Main chat area -->


                                    </div>
                                    <!--/ User Chat profile right area -->

                                </div>
                            </div>
                        </div>


                    </div>
                    <!--/ User Content -->
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->
@stop

@section('JsFiles')
    <!-- BEGIN: Vendor JS-->
    <script src="/Themes/Admin/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <!-- END: Page Vendor JS-->
    <!-- BEGIN: Page Vendor JS-->
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/polyfill.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/Themes/Admin/app-assets/js/core/app-menu.js"></script>
    <script src="/Themes/Admin/app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="/Themes/Admin/app-assets/js/scripts/pages/app-chat.js"></script>
    <!-- END: Page JS-->

    <script>
        $(function () {
            var confirmMessage = $('#confirm-message');
            if (confirmMessage.length) {
                confirmMessage.on('click', function () {
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "Are you sure for send message to support team?",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Yes, of course!',
                        customClass: {
                            confirmButton: 'btn btn-primary',
                            cancelButton: 'btn btn-outline-danger ms-1'
                        },
                        buttonsStyling: false
                    }).then(function (result) {
                        if (result.value) {
                            var iptItself = $('#iptItself').val();
                            var iptID = $('#iptID').val();
                            var iptContext = $('#iptContext').val();
                            var token = $('meta[name="csrf-token"]').attr('content');
                            $.ajax({
                                url: '/AdminPanel/Support/SubmitResponse',
                                data: {_token: token, id: iptID,iptItself:iptItself , iptContext: iptContext},
                                type: 'POST',
                                dataType: 'JSON',
                                success: function (data) {
                                    if (data.state == 1) {
                                        toastr['success'](data.message, 'Success', {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            progressBar: true  });
                                        location.reload();
                                    } else {
                                        toastr['error'](data.message, 'Error', {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            progressBar: true  });
                                    }
                                }
                            });
                        }
                    });
                });
            }
        });
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>
@stop
