<?php

namespace App\Http\Controllers\Dashboard;

use App\DbModels\Dashboard\Fund\UserLevel;
use App\DbModels\Dashboard\Package\Package;
use App\DbModels\Dashboard\Package\PackageUser;
use App\DbModels\Dashboard\Package\PackageUserTermination;
use App\DbModels\Dashboard\Wallet\Wallet;
use App\MyClasses\CmsFunctions;
use Carbon\Carbon;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class PackageController extends Controller
{
    public function PackageList()
    {
        $UID=CmsFunctions::GetUID();
        $rows = Package::all();
        return view('Dashboard.Package.PackageList', compact('rows','UID'));
    }

    public function UserPackages()
    {
        $UID=CmsFunctions::GetUID();
        $rows = PackageUser::where('package_users.user_id', $UID)
            ->join('packages', 'packages.id', '=', 'package_users.package_id')
            ->select('package_users.*', 'packages.Name', 'packages.MinPrice', 'packages.MaxPrice')
            ->orderBy('package_users.id', 'DESC')->get();
        return view('Dashboard.Package.UserPackages', compact('rows','UID'));
    }

    public function UserPackageList($id)
    {
        $UID=CmsFunctions::DecodedText($id);
        $rows = PackageUser::where('package_users.user_id', $UID)
            ->join('packages', 'packages.id', '=', 'package_users.package_id')
            ->select('package_users.*', 'packages.Name', 'packages.MinPrice', 'packages.MaxPrice')
            ->orderBy('package_users.id', 'DESC')->get();
        return view('Dashboard.Package.UserPackages', compact('rows','UID'));
    }

    public function TotalUserPackages()
    {
        $rows = PackageUser::join('packages', 'packages.id', '=', 'package_users.package_id')
            ->join('users', 'users.id', '=', 'package_users.user_id')
            ->select('package_users.*', 'packages.Name', 'packages.MinPrice', 'packages.MaxPrice', 'users.email', 'users.first_name', 'users.last_name')
            ->orderBy('package_users.id', 'DESC')->paginate(100);
        return view('Dashboard.Package.TotalUserPackages', compact('rows'));
    }

    public function TerminationList()
    {
        $rows = PackageUserTermination::join('package_users', 'package_users.id', '=', 'user_package_termination.package_user_id')
            ->join('packages', 'packages.id', '=', 'package_users.package_id')
            ->join('users', 'users.id', '=', 'package_users.user_id')
            ->select('package_users.*', 'packages.Name', 'packages.MinPrice', 'packages.MaxPrice', 'users.email', 'users.first_name', 'users.last_name')
            ->orderBy('user_package_termination.id', 'DESC')->paginate(100);
        return view('Dashboard.Package.TerminationList', compact('rows'));
    }

    public function GetUserPackageInfo(Request $request)
    {
        try {
            $request->validate([
                'id' => 'required'
            ]);
            $RID = CmsFunctions::DecodedText($request->id);
            $row = PackageUserTermination::where('user_package_termination.package_user_id', $RID)
                ->join('package_users', 'package_users.id', '=', 'user_package_termination.package_user_id')
                ->join('packages', 'packages.id', '=', 'package_users.package_id')
                ->join('users', 'users.id', '=', 'package_users.user_id')
                ->select('package_users.*', 'packages.Name', 'packages.MinPrice', 'packages.MaxPrice', 'users.email', 'users.first_name', 'users.last_name')
                ->first();
            $message = array(
                'state' => 1,
                'iptKey' => CmsFunctions::EncodedText($RID),
                'message' => $row
            );
            return response()->json($message);
        } catch (\Exception $e) {
            $message = array(
                'state' => 0,
                'message' => $e->getMessage()
            );
            return response()->json($message);
        }
    }

    public function addUserPackage(Request $request)
    {
        try {
            $request->validate([
                'id' => 'required',
                'iptPrice' => 'required'
            ]);
            DB::beginTransaction();
            $RID = $request->id;
            $CarbonNow=Carbon::now();
            $CurrentUser= Sentinel::getUser();
            $UID = $CurrentUser['id'];
            $UserAsset = CmsFunctions::GetUserWalletAssets($UID);
            $Price = $request->iptPrice;
            $PackageInfo = Package::where('id', $RID)->first();
            $UserPackage = PackageUser::where('user_id', $UID)->orderBy('id', 'DESC')->select('id')->first();
            if (($PackageInfo->extra == 1) && (empty($UserPackage))) {
                DB::rollBack();
                $message = array(
                    'state' => 0,
                    'message' => 'pre requirement: must be purchase one of the package A,B,C or D '
                );
                return response()->json($message);
            }

            if (($Price >= $PackageInfo->MinPrice) && ($Price <= $PackageInfo->MaxPrice)) {
                if ($Price <= $UserAsset) {
                    $UserLevels=UserLevel::where('child_user_id',$UID)->select('parent_user_id','LevelID')->orderBy('LevelID')->get();
                    foreach($UserLevels as $ulRow){
                        $Level=$ulRow->LevelID;
                        $GetProfitLevel=CmsFunctions::ProfitLevels($Level);
                        $LevelProfit=$Price*($GetProfitLevel / 100);
                        Wallet::insert([
                            'created_at' => $CarbonNow,
                            'State' => 7,
                            'Comments' => 'Profit from buy ' . $PackageInfo->Name.' by=>'.$CurrentUser['email'],
                            'Price' => $LevelProfit,
                            'user_id' => $ulRow->parent_user_id,
                        ]);
                    }
                    PackageUser::insert([
                        'package_id' => $RID,
                        'user_id' => $UID,
                        'Price' => $Price,
                        'DailyProfitPercent' => $PackageInfo->DailyProfitPercent,
                        'expire_date' => Carbon::now()->addYear(),
                        'created_at' => $CarbonNow,
                    ]);
                    Wallet::insert([
                        'created_at' => $CarbonNow,
                        'State' => 5,
                        'Comments' => 'Buy ' . $PackageInfo->Name,
                        'Price' => -($Price),
                        'user_id' => $UID,
                    ]);
                } else {
                    DB::rollBack();
                    $message = array(
                        'state' => 0,
                        'message' => 'error at insufficient user assets.'
                    );
                    return response()->json($message);
                }
            } else {
                DB::rollBack();
                $message = array(
                    'state' => 0,
                    'message' => 'error at package price limitation.'
                );
                return response()->json($message);

            }
            DB::commit();
            $message = array(
                'state' => 1,
                'message' => config('msg.buySuccess')
            );
            return response()->json($message);
        } catch (\Exception $e) {
            DB::rollBack();
            $message = array(
                'state' => 0,
                'message' => $e->getMessage()
            );
            return response()->json($message);
        }
    }

    public function UserPackageTermination(Request $request)
    {
        try {
            $request->validate([
                'id' => 'required'
            ]);
            $RID = CmsFunctions::DecodedText($request->id);
            PackageUserTermination::insert([
                'package_user_id' => $RID
            ]);
            $message = array(
                'state' => 1,
                'message' => config('msg.success')
            );
            return response()->json($message);
        } catch (\Exception $e) {
            $message = array(
                'state' => 0,
                'message' => $e->getMessage()
            );
            return response()->json($message);
        }
    }

    public function RemovePackageByAdmin(Request $request)
    {
        try {
            $request->validate([
                'id' => 'required'
            ]);
           PackageUser::where('id',CmsFunctions::DecodedText($request->id))->delete();
            $message = array(
                'state' => 1,
                'message' => config('msg.success')
            );
            return response()->json($message);
        } catch (\Exception $e) {
            $message = array(
                'state' => 0,
                'message' => $e->getMessage()
            );
            return response()->json($message);
        }
    }

    public function RecheckTermination(Request $request)
    {
        try {
            $request->validate([
                'iptDeductionPrice' => 'required',
                'iptKey' => 'required'
            ]);
            $DeductionPrice=$request->iptDeductionPrice;
            $RID = CmsFunctions::DecodedText($request->iptKey);
            $row = PackageUserTermination::where('user_package_termination.package_user_id', $RID)
                ->join('package_users', 'package_users.id', '=', 'user_package_termination.package_user_id')
                ->select('package_users.*')
                ->first();
            $PackageUserID = $row->id;
            $PackageUserPrice=$row->Price;
            PackageUser::where('id', $PackageUserID)->update(['Disable'=> 1]);
            Wallet::insert([
                'State' => 6,
                'Comments' => 'Termination users package',
                'ReferenceID' => $PackageUserID,
                'Price' => ($PackageUserPrice - $DeductionPrice),
                'user_id' => $row->user_id,
            ]);
            Session::flash('termination_package_successfully', config('msg.update'));
            return back();
        } catch (\Exception $e) {
            Session::flash('error_at_termination_package', $e->getMessage());
            return back();
        }
    }

    public function UserLevel()
    {
        $UID=CmsFunctions::GetUID();

        return view('Dashboard.Level.UserLevel', compact('UID'));
    }

    public function UserSubsetList($id)
    {
        $UID=CmsFunctions::DecodedText($id);
        return view('Dashboard.Level.UserLevel', compact('UID'));
    }

    public function UpLine()
    {
        $UID=CmsFunctions::GetUID();
        $Rows=UserLevel::where([
            ['user_levels.child_user_id','=',$UID],
            ['user_levels.LevelID','<=',3]
        ])->join('users','users.id','=','user_levels.parent_user_id')
            ->select('user_levels.*','users.email','users.first_name','users.last_name')
            ->orderBy('user_levels.LevelID','DESC')
            ->get();
        return view('Dashboard.Level.UpLine', compact('Rows'));
    }

    public function TotalUserProfit(string $txt)
    {
        try {
            $UID=CmsFunctions::GetUID();
            $string=CmsFunctions::DecodedText($txt);
            $strArr=explode('-',$string);
            $UID=$strArr[0];
            $RID=$strArr[1];
            $rows=Wallet::where([
                ['user_id','=',$UID],
                ['State','=',8],
                ['ReferenceID','=',$RID]
            ])->orderBy('id','DESC')->paginate(100);
            return view('Dashboard.Package.TotalUserProfit', compact('rows','UID'));
        }   catch (\Exception $e){
            return back();
        }
    }
}
