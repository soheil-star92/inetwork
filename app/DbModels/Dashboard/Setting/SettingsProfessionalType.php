<?php

namespace App\DbModels\Dashboard\Setting;

use Illuminate\Database\Eloquent\Model;

class SettingsProfessionalType extends Model
{
	protected $table='setting_professional_types';
	public $timestamps=false;
}
