@extends('Layouts.dashboardMainLayout')
@section('BreadCrumbLevel')
    <li class="active">لیست اخبار اسلایدری</li>
@Stop
@section('BreadCrumbURL','/news/newsLists')
@section('BreadCrumbTitle','لیست اخبار')
@section('MainContent')
    <div class="row">
        <section class="col-lg-12 col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <i class="fa fa-info-circle"></i>
                    <h3 class="box-title">
                        {{ \App\Facade\DbSettings::GetDbSetting()->Title }}
                    </h3>
                    <!-- tools box -->
                    <div class="pull-left box-tools">
                        <button type="button" class="btn bg-info btn-sm" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /. tools -->
                </div>

                <div class="box-body">
                    <div class="content">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">لیست کلی اسلایدر اخبار</h3>
                            </div>
                            <div class="box-header with-border">
                                @include('Dashboard.toolbars.newsToolbar')
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table id="keyWordDataTable"
                                           class="table table-bordered table-striped table-responsive table-hover">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>کد خبر</th>
                                            <th>عنوان خبر</th>
                                            <th>طبقه بندی خبر</th>
                                            <th>استان</th>
                                            <th>کاربر ارسالی</th>
                                            <th>تاریخ ارسال</th>
                                            <th>بازدید</th>
                                            <th>وضعیت خبر</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($news as $key=>$n)
                                            <tr>
                                                <td>{{ ++$key }}</td>
                                                <td>{{ $n['id'] }}</td>
                                                <td>{{ $n['Title'] }}</td>
                                                <td>{{ $n->newsCategoryTitle }}</td>
                                                <td>{{ $n['provinceTitle'] }}</td>
                                                <td>{{ $n['first_name'].' '.$n['last_name'] }}</td>
                                                <td>
                                                    @if(\App\Facade\OrganizationInfo::GetAccessFromProfessionalSettings('show_date_in_human_function'))
                                                        {{\App\MyClasses\CmsFunctions::GetDateForHumans($n['created_at'])}}
                                                    @else
                                                        {{ \App\MyClasses\CmsFunctions::GetGregorianDateAndConvertToJalaliDate($n['created_at']) }}
                                                    @endif
                                                </td>
                                                <td>{{ $n['View'] }}</td>
                                                <td>{{ $n['newsStateTitle'] }}</td>
                                                <td>
                                                    <div class="input-group">
                                                        <button type="button"
                                                                class="btn bg-light-blue-active dropdown-toggle "
                                                                data-toggle="dropdown"
                                                                style="font-size: 9px !important ">
                                                            <span class="fa fa-caret-down"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a class="btn bg-maroon btn-xs" data-id="{{ $n->id }}"
                                                                   title="حذف رکورد"><span class="fa fa-trash"></span>حذف</a>
                                                            </li>
                                                            @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.news.allow.update']))
                                                                <li><a class="btn bg-green btn-xs"
                                                                       href="/news/updateNewsForm/{{ $n->id  }}"
                                                                       title="ویرایش رکورد"><span
                                                                                class="fa fa-edit"></span>ویرایش</a>
                                                                </li>
                                                            @endif
                                                            @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.news.allow.on.slider']))
                                                                <li><a class="btn bg-yellow-gradient btn-xs"
                                                                       data-id="{{ $n->id }}"
                                                                       title="تغییر وضعیت اسلایدری"><span
                                                                                class="fa fa-sliders"></span>اسلایدر</a>
                                                                </li>
                                                            @endif
                                                            @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.news.allow.show.news.comments']))
                                                                <li><a class="btn bg-black-gradient btn-xs"
                                                                       href="/news/showNewsComments/{{ $n->id }}"
                                                                       title="نمایش نظرات بازدیدکنندگان"><span
                                                                                class="fa fa-user"></span>نظرات</a>
                                                                </li>
                                                            @endif
                                                            @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.news.allow.show.news.changes']))
                                                                <li><a class="btn bg-fuchsia btn-xs"
                                                                       data-target="#news_changes_show_modal" data-id="{{ $n->id }}"
                                                                       data-toggle="modal"
                                                                       title="گزارش گردش خبر"><span
                                                                                class="fa fa-clipboard"></span>گردش خبر</a>
                                                                </li>
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('JsFiles')
    <script src="{{ URL::asset('assets/login/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            $('.bg-maroon').click(function () {
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                var ele = $(this).parent().parent().parent().parent().parent();
                swal({
                    title: "",
                    text: "آیا از حذف این رکورد مطمئن هستید؟",
                    icon: "warning",
                    buttons: ["خیر", "بله"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '/news/removeNews',
                            data: {_token: token, id: id},
                            type: 'POST',
                            dataType: 'JSON',
                            success: function (data) {
                                if (data.state == 1) {
                                    ele.fadeOut().remove();
                                    swal("", data.message, "success")
                                } else {
                                    swal(data.message, {
                                        icon: "warning",
                                        dangerMode: true
                                    });
                                }
                            }
                        });
                    }
                });
            });

            $('.bg-yellow-gradient').click(function () {
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                swal({
                    title: "",
                    text: "آیا از تغییر وضعیت اسلایدری این رکورد مطمئن هستید؟",
                    icon: "warning",
                    buttons: ["خیر", "بله"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '/news/changeSliderState',
                            data: {_token: token, id: id},
                            type: 'POST',
                            dataType: 'JSON',
                            success: function (data) {
                                if (data.state == 1) {
                                    swal("",data.message,"success");
                                    location.reload();
                                } else {
                                    swal(data.message,{
                                        icon:"warning",
                                        dangerMode:true,
                                    });
                                }
                            }
                        });
                    }
                });
            });

            $('.bg-fuchsia').click(function () {
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/news/logNewsChangesShow',
                    data: {_token: token, id: id},
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.state == 1) {
                            $('#ShowLogNewsChange').empty();
                            $('#ShowLogNewsChange').append(data.message);
                            $('#ShowNewsTitle').empty();
                            $('#ShowNewsTitle').text(data.newsTitle);
                        } else {
                            swal(data.message, {
                                icon: "warning",
                                dangerMode: true,
                            });
                        }
                    }
                });
            });

        })
    </script>
@endsection
