@php
    use \App\MyClasses\CmsFunctions;
    use \App\MyClasses\CmsThemes;
@endphp
@extends('Themes.iNetwork.MainLayout')
@section('Content')
    <!-- INNER PAGE BANNER -->
    <div class="wt-bnr-inr overlay-wraper" style="background-image:url(/Themes/HomePage/images/banner/about-banner.jpg);">
        <div class="overlay-main bg-black opacity-07"></div>
        <div class="container">
            <div class="wt-bnr-inr-entry">
                <h1 class="text-white">About Us</h1>
            </div>
        </div>
    </div>
    <!-- INNER PAGE BANNER END -->

    <!-- BREADCRUMB ROW -->
    <div class="bg-gray-light p-tb20">
        <div class="container">
            <ul class="wt-breadcrumb breadcrumb-style-2">
                <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
                <li>About</li>
            </ul>
        </div>
    </div>
    <!-- BREADCRUMB  ROW END -->

    <!-- ABOUT COMPANY SECTION START -->
    <div class="section-full p-tb100">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="section-head text-left">
                        <span class="wt-title-subline text-gray-dark font-16 m-b15">What is Invest Chain Network</span>
                        <h2 class="text-uppercase">A New Kind of Investing </h2>
                        <div class="wt-separator-outer"><div class="wt-separator bg-primary"></div></div>
                        <p><strong>Invest Chain Network with registration 13047680 Company number, has an experienced team in the field of trading and mining cryptocurrencies. which has reached to 100% monthly profit return in the humane and bots way, and to guarantee the principal and profit of investors, are locked 100,000 £ as initial investment and 10,000,000 £ as real estate to Companies for England and wales.</strong></p>
                        <p>
                            We provide 12 to 16 percent monthly and 144 to 172 percent annually profit for people who do not have the knowledge of trading and mining and entrust their investment to us. Which as the work progresses will pay higher profits.
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="wt-media">
                        <img src="/Themes/HomePage/images/gallery/pic3.jpg" alt="" class="img-responsive"/>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="section-head text-left">
                        <p>
                            We provide 12 to 16 percent monthly and 144 to 172 percent annually profit for people who don’t have the knowledge of trading and mining and entrust their investment to us. Which will pay higher profits as the work progresses.
                        </p>
                        <p>The investment period is annually, and it is possible to termination after 2 months. Which is guaranteed by the repayment of your initial investment by the Companies for England and wales. Sub-collection is completely optional and if you wish you can get up to 21 levels of commissions.</p>
                        <p>In addition, you will be given prizes including: Phone - laptop and car for team building, if need be, you can get it in $USD</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ABOUT COMPANY SECTION END -->

    <!-- WHY CHOOSE US SECTION START  -->
    <div class="section-full  p-t80 p-b80 bg-gray">
        <div class="container">
            <!-- TITLE START-->
            <div class="section-head text-center">
                <h2 class="text-uppercase">Why Choose Invest Chain Network</h2>
                <div class="wt-separator-outer"><div class="wt-separator bg-primary"></div></div>
            </div>
            <!-- TITLE END-->
            <div class="section-content no-col-gap">
                <div class="row">

                    <!-- COLUMNS 1 -->
                    <div class="col-md-4 col-sm-6 animate_line">
                        <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                            <img src="/assets/docs/0001.jpg" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 animate_line">
                        <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                            <img src="/assets/docs/0002.jpg" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 animate_line">
                        <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                            <img src="/assets/docs/0003.jpg" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 animate_line">
                        <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                            <img src="/assets/docs/0004.jpg" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 animate_line">
                        <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                            <img src="/assets/docs/0005.jpg" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 animate_line">
                        <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                            <img src="/assets/docs/0006.jpg" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 animate_line">
                        <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                            <img src="/assets/docs/0007.jpg" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 animate_line">
                        <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                            <img src="/assets/docs/0008.jpg" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 animate_line">
                        <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                            <img src="/assets/docs/0009.jpg" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 animate_line">
                        <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                            <img src="/assets/docs/0010.jpg" alt=""></a>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <!-- WHY CHOOSE US SECTION END -->



@stop
