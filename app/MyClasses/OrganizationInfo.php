<?php

namespace App\MyClasses;

use App\DbModels\Dashboard\Message\Message;
use App\DbModels\Dashboard\News\NewsCategory;
use App\DbModels\Dashboard\News\NewsComment;
use App\DbModels\Dashboard\News\Province;
use App\DbModels\Dashboard\Setting\City;
use App\DbModels\Dashboard\Setting\Link;
use App\DbModels\Dashboard\Setting\Section;
use App\DbModels\Dashboard\Setting\SettingsProfessional;
use App\DbModels\Dashboard\Setting\SocialMedia;
use App\DbModels\Dashboard\Users\User;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

class OrganizationInfo
{
	protected  $userId=null;

	public function SetUsers($UserId)
	{
		$this->userId=$UserId;
	}

	public function GetUser()
	{
		return $this->userId;
	}

	public function GetUserInfoWithoutParameter()
	{
		$info=User::where('users.id',$this->GetUserRoleAccess()->id)
				->leftJoin('info_users','info_users.user_id','=','users.id')
				->leftJoin('role_users','role_users.user_id','=','users.id')
				->leftJoin('roles','roles.id','=','role_users.role_id')
				->select(
						'users.id AS UserId',
						'users.first_name',
						'users.last_name',
						'users.email',
                        'users.created_at',
						'info_users.ReferralCode',
						'info_users.ShowReferralCode',
						'info_users.RegisteredEmail',
						'info_users.Image',
						'info_users.Phone',
						'info_users.country_id',
						'info_users.id_i',
						'info_users.Wallet',
						'info_users.City',
						'info_users.confirmWallet',
						'roles.name AS RoleName'
				)
				->first();
		return $info;
	}

    public function GetUserRoleAccess()
    {
        $user=Sentinel::getUser();
        return $user;
    }

	public function GetNewsCategories()
	{
		$newsCategories=NewsCategory::orderBy('id','ASC')
				->get();
		return $newsCategories;
	}

	public function GetLinks()
	{
		$links=Link::where('Disabled',0)
				->orderBy('id','DESC')
				->get();
		return $links;
	}

	public function GetSocialMedia()
	{
		$social=SocialMedia::first();
		return $social;
	}

	public function GetProvinces()
	{
		$province=Province::where('Disabled',0)
				->orderBy('id')
				->get();
		return $province;
	}

    public function GetDisableProvinces()
    {
        $provinces=Province::where([
            ['Disabled','=',1],
            ['id','!=',1],
        ])
            ->orderBy('id')
            ->get();
        return $provinces;
	}

//	public function GetUnreadMessage()
//	{
//		$user=Sentinel::getUser();
//		$unreadMessage=Message::where([
//				['messages.to_trash','=',0],
//				['messages.Show','=',0],
//				['messages.to_user_id','=',$user['id']],
//		])
//				->join('users','users.id','=','messages.from_user_id')
//				->leftJoin('info_users','info_users.user_id','=','users.id')
//				->select(
//						'messages.*','users.first_name','users.last_name','info_users.Image'
//				)
//				->get();
//		return $unreadMessage;
//	}

	public function GetUnActiveNewsComment()
	{
		$unactiveNewsComment=NewsComment::where('State',0)
            ->select('Message','Name','created_at')
            ->orderBy('id','DESC')
            ->get();
		return $unactiveNewsComment;
	}

    public function GetCityFromProvince($provinceId)
    {
        $cities=City::where('province_id',$provinceId)
            ->orderBy('id')
            ->get();
        return $cities;
	}

    public function GetAccessFromProfessionalSettings($title)
    {
        $Permission=SettingsProfessional::where('Title',$title)
            ->select('State')
            ->first();
        return $Permission->State;
    }

}
