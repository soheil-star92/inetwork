@extends('Layouts.dashboardMainLayout')
@section('BreadCrumbURL','/comments/showNewsCommentsLists')
@section('BreadCrumbTitle','لیست نظرات اخبار')
@section('CssFiles')
    <link rel="stylesheet" href="{{ asset('assets/login/dist/css/persian-datepicker-0.4.5.min.css') }}"/>
    <link rel="stylesheet"
          href="{{ asset('assets/login/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}"/>
    <link rel="stylesheet"
          href="{{ asset('assets/login/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}"/>
    <link rel="stylesheet"
          href="{{ asset('assets/login/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"/>
@stop
@section('MainContent')
    <div class="row">
        <section class="col-lg-12 col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <i class="fa fa-info-circle"></i>
                    <h3 class="box-title">
                        {{ \App\Facade\DbSettings::GetDbSetting()->Title }}
                    </h3>
                    <!-- tools box -->
                    <div class="pull-left box-tools">
                        <button type="button" class="btn bg-info btn-sm" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /. tools -->
                </div>

                <div class="box-body">
                    <div class="content">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">نظرات کاربران(اخبار)</h3>
                            </div>
                            @include('Dashboard.toolbars.CommentsToolbar')
                            <div class="box-header with-border">
                                <div class="modal modal-info fade" id="modal-show-ip-location">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">نمایش مکانIp</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    <label for="country">country</label>
                                                    <input type="text" name="country" id="country" class="form-control"
                                                           disabled>
                                                </p>
                                                <p>
                                                    <label for="countryCode">countryCode</label>
                                                    <input type="text" name="countryCode" id="countryCode"
                                                           class="form-control" disabled>
                                                </p>
                                                <p>
                                                    <label for="region">region</label>
                                                    <input type="text" name="region" id="region" class="form-control"
                                                           disabled>
                                                </p>
                                                <p>
                                                    <label for="regionName">regionName</label>
                                                    <input type="text" name="regionName" id="regionName"
                                                           class="form-control" disabled>
                                                </p>
                                                <p>
                                                    <label for="city">city</label>
                                                    <input type="text" name="city" id="city" class="form-control"
                                                           disabled>
                                                </p>
                                                <p>
                                                    <label for="query">query</label>
                                                    <input type="text" name="query" id="query" class="form-control"
                                                           disabled>
                                                </p>
                                                <p>
                                                    <label for="isp">isp</label>
                                                    <input type="text" name="isp" id="isp" class="form-control"
                                                           disabled>
                                                </p>
                                                <p>
                                                    <label for="isp">timezone</label>
                                                    <input type="text" name="timezone" id="timezone"
                                                           class="form-control" disabled>
                                                </p>
                                                <p>
                                                    <label for="lon">lon</label>
                                                    <input type="text" name="lon" id="lon" class="form-control"
                                                           disabled>
                                                </p>
                                                <p>
                                                    <label for="lat">lat</label>
                                                    <input type="text" name="lat" id="lat" class="form-control"
                                                           disabled>
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-outline pull-left"
                                                        data-dismiss="modal">خروج
                                                </button>
                                            </div>
                                        </div>
                                        </form>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.comments.show.news.comments']))
                                    <div class="modal modal-info fade" id="modal-show-news-comment">
                                        <div class="modal-dialog">
                                            <form action="/comments/NewsCommentUpdate" id="UpdateForm" method="post">
                                                {{csrf_field()}}
                                                <input type="hidden" name="NCID" id="NCID">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">نمایش نظرات</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>
                                                        <label for="ShowDate">تاریخ ارسال</label>
                                                        <input type="text" name="ShowDate" id="ShowDate"
                                                               class="form-control text-center">
                                                    </p>
                                                    <p>
                                                        <label for="ShowName">نویسنده</label>
                                                        <input type="text" name="ShowName" id="ShowName"
                                                               class="form-control">
                                                    </p>
                                                    <p>
                                                        <label for="ShowEmail">پست الکترونیکی</label>
                                                        <input type="text" name="ShowEmail" id="ShowEmail"
                                                               class="form-control text-left">
                                                    </p>
                                                    <p>
                                                        <label for="ShowMessage">متن نظر</label>
                                                        <textarea name="ShowMessage" id="mytextarea"
                                                                  class="form-control" cols="30" rows="10"></textarea>
                                                    </p>
                                                    <p>
                                                        <label for="ShowWebsite">Website</label>
                                                        <input type="text" name="ShowWebsite" id="ShowWebsite"
                                                               class="form-control text-left">
                                                    </p>
                                                    <p>
                                                        <label for="ShowIP">IP</label>
                                                        <input type="text" name="ShowIP" id="ShowIP"
                                                               class="form-control text-left" disabled>
                                                    </p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-outline pull-left"
                                                            data-dismiss="modal">خروج
                                                    </button>
                                                    @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.comments.allow.update.news.comment']))
                                                        <input type="submit" class="btn btn-outline" value="به روزرسانی">
                                                    @endif
                                                </div>
                                            </div>
                                            </form>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                @endif
                                <!-- /.modal-dialog -->
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="keyWordDataTable"
                                       class="table table-bordered table-striped table-responsive table-hover">
                                    <thead>
                                    <tr>
                                        <th>ردیف</th>
                                        <th>عنوان خبر</th>
                                        <th>نام بازدید کننده</th>
                                        <th>پست الکترونیکی</th>
                                        <th>تاریخ ارسال</th>
                                        <th>وضعیت</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($newsComments as $key=>$n)
                                        <tr @if($n->State == 0) style="background-color: rgba(200,0,0,0.3)" @endif>
                                            <td>{{ ++$key }}</td>
                                            <td><a @if(\App\Facade\OrganizationInfo::GetAccessFromProfessionalSettings('show_url_post_as_text')) href="/ShowNews/{{str_replace(' ','_',$n->Title)}}" @else href="/ShowNews/{{$n->id}}" @endif
                                                   target="_blank">{{ $n['Title'] }}</a></td>
                                            <td>{{ $n['Name'] }}</td>
                                            <td>{{ $n['Email'] }}</td>
                                            <td>
                                                @if(\App\Facade\OrganizationInfo::GetAccessFromProfessionalSettings('show_date_in_human_function'))
                                                    {{\App\MyClasses\CmsFunctions::GetDateForHumans($n['created_at'])}}
                                                @else
                                                    {{ \App\MyClasses\CmsFunctions::GetGregorianDateAndConvertToJalaliDate($n['created_at']) }}
                                                @endif
                                            </td>
                                            <td>{{ ($n['State'] == 0) ? 'غیرفعال' : 'فعال' }}</td>
                                            <td>
                                                <div class="input-group">
                                                    <button type="button"
                                                            class="btn bg-light-blue-active dropdown-toggle "
                                                            data-toggle="dropdown"
                                                            style="font-size: 9px !important ">
                                                        <span class="fa fa-caret-down"></span></button>
                                                    <ul class="dropdown-menu">
                                                        @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.comments.remove.comment']))
                                                            <li><a class="btn bg-maroon btn-xs"
                                                                   data-id="{{ $n->id }}"
                                                                   title="حذف رکورد"><span
                                                                            class="fa fa-trash"></span>حذف</a>
                                                            </li>
                                                        @endif
                                                        @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.comments.allow.change.state']))
                                                            <li><a class="btn bg-yellow-gradient btn-xs"
                                                                   data-id="{{ $n->id }}"
                                                                   title="تغییر وضعیت خبر"><span
                                                                            class="fa fa-ban"></span>تغییر وضعیت</a>
                                                            </li>
                                                        @endif
                                                        @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.comments.show.news.comments']))
                                                            <li><a class="btn bg-green btn-xs"
                                                                   data-id="{{ $n->id }}" data-toggle="modal"
                                                                   data-target="#modal-show-news-comment"
                                                                   title="نمایش نظر"><span
                                                                            class="fa fa-search"></span>نمایش</a>
                                                            </li>
                                                        @endif
                                                        <li><a class="btn bg-black-gradient btn-xs"
                                                               data-id="{{ $n->IP }}" data-toggle="modal"
                                                               data-target="#modal-show-ip-location"
                                                               title="نمایش مکان Ip"><span
                                                                        class="fa fa-location-arrow"></span>مکانIp</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                            {{ $newsComments->links() }}
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection

@section('JsFiles')
    <script src="{{ URL::asset('assets/login/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/login/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ URL::asset('assets/login/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ URL::asset('assets/login/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ URL::asset('assets/login/dist/js/persian-date-0.1.8.min.js') }}"></script>
    <script src="{{ URL::asset('assets/login/dist/js/persian-datepicker-0.4.5.min.js') }}"></script>
    <!-- TinyMCE Editor -->
    <script src="{{ URL::asset('assets/login/bower_components/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ URL::asset('assets/login/bower_components/tinymce/myStyle/CustomizeFunctions.js') }}"></script>
    <script>
        $(document).ready(function(){
            @if(Session::has('send-email-for-comment-news-success'))
            swal("","ارسال ایمیل با موفقیت صورت گرفت.", "success");
            @endif
            @if(Session::has('news_comment_update_success'))
            swal("","رکورد مورد نظر با موفقیت ویرایش گردید", "success");
            @endif
        });
    </script>
    <script>
        $(function () {
            $('.bg-maroon').click(function () {
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                var ele = $(this).parent().parent().parent().parent().parent();
                swal({
                    title: "",
                    text: "آیا از حذف این رکورد مطمئن هستید؟",
                    icon: "warning",
                    buttons: ["خیر", "بله"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '/comments/removeNewsComment',
                            data: {_token: token, id: id},
                            type: 'POST',
                            dataType: 'JSON',
                            success: function (data) {
                                if (data.state == 1) {
                                    ele.fadeOut().remove();
                                    swal("", data.message, "success");
                                } else {
                                    swal(data.message, {
                                        icon: 'warning',
                                        dangerMode: true
                                    });
                                }
                            }
                        });
                    }
                });
            });
            $('.bg-yellow-gradient').click(function () {
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                swal({
                    title: "",
                    text: "آیا از تغییر وضعیت این رکورد مطمئن هستید؟",
                    icon: "warning",
                    buttons: ["خیر", "بله"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '/comments/changeStateNewsComment',
                            data: {_token: token, id: id},
                            type: 'POST',
                            dataType: 'JSON',
                            success: function (data) {
                                if (data.state == 1) {
                                    swal("", data.message, "success");
                                    location.reload();
                                } else {
                                    swal(data.message, {
                                        icon: 'warning',
                                        dangerMode: true
                                    });
                                }
                            }
                        });
                    }
                });
            });

            $('.bg-green').click(function () {
                $('.ShowModalLoadingArea').css({'display':'block','position':'sticky',});
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/comments/showNewsComment',
                    data: {_token: token, id: id, Type: 1},
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.state == 1) {
                            $('#ShowDate').val(data.ShowDate);
                            $('#NCID').val(data.NCID);
                            $('#ShowName').val(data.message.Name);
                            $('#ShowEmail').val(data.message.Email);
                            tinymce.get('mytextarea').setContent(data.message.Message);
                            $('#ShowIP').val(data.message.IP);
                            $('#ShowWebsite').val(data.message.Website);
                            $('.ShowModalLoadingArea').css('display','none');
                        } else {
                            swal(data.message, {
                                icon: 'warning',
                                dangerMode: true
                            });
                        }
                    }
                });
            });


            $('.bg-black-gradient').click(function () {
                var IP = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/showIpLocation',
                    data: {_token: token, IP: IP},
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.state == 1) {
                            $('#country').val(data.message.country);
                            $('#countryCode').val(data.message.countryCode);
                            $('#region').val(data.message.region);
                            $('#regionName').val(data.message.regionName);
                            $('#city').val(data.message.city);
                            $('#query').val(data.message.query);
                            $('#isp').val(data.message.isp);
                            $('#timezone').val(data.message.timezone);
                            $('#lon').val(data.message.lon);
                            $('#lat').val(data.message.lat);
                        } else {
                            swal(data.message, {
                                icon: 'warning',
                                dangerMode: true
                            });
                        }
                    }
                });
            });

            $('#ShowDate').persianDatepicker({
                format: 'YYYY/M/D h:m:s',
                observer: true,
                timePicker: {
                    enabled: true
                },
            });
            $('#sendMail').one('submit',function(){
                $(this).find('button[type="submit"]').attr('disabled','disabled');
            });
            $('#UpdateForm').one('submit',function(){
                $(this).find('input[type="submit"]').attr('disabled','disabled');
            });
        })
    </script>
@endsection
