<?php

namespace App\DbModels\Dashboard\Users;

use Illuminate\Database\Eloquent\Model;

class LogPassword extends Model
{
    protected $table='log_password';
}
