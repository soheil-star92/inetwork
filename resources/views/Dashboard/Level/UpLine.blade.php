@php
    use App\DbModels\Dashboard\Fund\UserLevel;
    $settings=\App\Facade\DbSettings::GetDbSetting();
    $userLogged=\App\Facade\OrganizationInfo::GetUserInfoWithoutParameter();
    $RegisteredEmail=$userLogged->RegisteredEmail;
    $UID=$userLogged->UserId;
@endphp

@section('CssFiles')

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/extensions/jstree.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/bordered-layout.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/css/plugins/extensions/ext-component-tree.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/assets/css/style.css">
    <!-- END: Custom CSS-->


@stop
@extends('Layouts.dashboardMainLayout')

@section('MainContent')
    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">User Up Line</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/index">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#">Branches</a>
                                    </li>
                                    <li class="breadcrumb-item active">UpLine
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Tree section -->
                <section class="basic-custom-icons-tree">
                    <div class="row">
                        <!-- Basic Tree -->
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Branches</h4>
                                </div>
                                <div class="card-body">
                                    <div id="jstree-basic">
                                        <ul>
                                            <li data-jstree='{"icon" : "far fa-folder"}'> . . .</li>
                                            @foreach($Rows as $row)
                                                <li data-jstree='{"icon" : "far fa-folder"}'>
                                                    Level {{$row->LevelID}} | {{$row->first_name.' '.$row->last_name.' ( '.$row->email.' ) '}}
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Basic Tree -->

                    </div>
                </section>


            </div>
        </div>
    </div>
    <!-- END: Content-->

@stop

@section('JsFiles')

    <!-- BEGIN: Vendor JS-->
    <script src="/Themes/Admin/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/jstree.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/Themes/Admin/app-assets/js/core/app-menu.js"></script>
    <script src="/Themes/Admin/app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="/Themes/Admin/app-assets/js/scripts/extensions/ext-component-tree.js"></script>
    <!-- END: Page JS-->

    <script>
        $(window).on('load', function () {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>
@stop
