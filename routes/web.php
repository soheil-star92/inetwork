<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\DbModels\Dashboard\Lang;
use App\DbModels\Dashboard\Users\PermissionDescription;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;


Route::get('Test', 'TestController@index');
Route::post('Test', 'TestController@insert');
Route::get('Test/{id}', 'TestController@show');
Route::put('Test/{id}', 'TestController@update');
Route::delete('Test/{id}', 'TestController@delete');
Route::get('/PV', 'TestController@PV');

Route::get('/', 'ViewBaseController@MainPage');
Route::get('/About', 'ViewBaseController@AboutUs');
Route::get('/Gifts', 'ViewBaseController@Gifts');
Route::get('/ProfitPackages', 'ViewBaseController@ProfitPackages');
Route::get('/Leaders', 'ViewBaseController@Leaders');
Route::get('/Blog', 'ViewBaseController@Blog');
Route::get('/NewsCategory/{id}', 'ViewBaseController@NewsCategory');
Route::get('/ShowNews/{id}', 'ViewBaseController@ShowNews');
Route::get('/Pages/{id}', 'ViewBaseController@ShowPages');
Route::get('/tag/{Title}', 'ViewBaseController@tag');
Route::get('/Contact', 'ViewBaseController@ContactUs');
Route::post('/ContactUsSubmit', 'ViewBaseController@ContactUsSubmit');
Route::post('/NewsCommentSubmit', 'ViewBaseController@NewsCommentSubmit');
Route::post('/UserRegisterSubmit', 'ViewBaseController@UserRegisterSubmit');
Route::get('/UserRegister', 'ViewBaseController@UserRegister');

Route::post('/ChangeLanguage', function (Request $request) {
    Session(['DefaultLanguage' => $request->ChangeLanguage]);
    return redirect('/');
});
Route::get('/login', 'Center\CenterController@Login');
Route::get('/ForgetPassword', 'Center\CenterController@ForgetPassword');
Route::post('/SendForgetPassword', 'Center\CenterController@SendForgetPassword');
Route::post('/authentication', 'Center\CenterController@authentication');
Route::get('/CheckDailyProfit', 'Center\CenterController@CheckDailyProfit');
Route::get('/Verification/Email/Confirmation/{id}', 'Dashboard\UserController@ConfirmationEmail');

/* TODO Start Dashboard Page Route */
Route::middleware('CheckAuthorized')->group(function () {

    Route::prefix('/AdminPanel')->group(function () {

        Route::post('/SendUserEmailVerification', 'Dashboard\UserController@SendUserEmailVerification');

        Route::middleware('UserRole:sub.system.notifications')->group(function () {
            Route::get('/ContactUsList', 'Dashboard\GeneralController@ContactUsList');
        });

        Route::prefix('/Users')->middleware('UserRole:sub.system.users')->group(function () {
            Route::get('/UsersList', 'Dashboard\UserController@UsersList2');
            Route::get('/UserPackageList/{id}', 'Dashboard\PackageController@UserPackageList');
            Route::get('/UserSubsetList/{id}', 'Dashboard\PackageController@UserSubsetList');
            Route::get('/UserWalletList/{id}', 'Dashboard\WalletController@UserWalletList');
            Route::post('/GetUserInfo', 'Dashboard\UserController@GetUserInfo');
            Route::post('/UpdateInfo', 'Dashboard\UserController@UpdateInfo');
            Route::post('removeUser', 'Dashboard\UserController@removeUser');
            Route::post('GetUserFinancialInfo', 'Dashboard\UserController@GetUserFinancialInfo');
            Route::post('UserAssetManualIncrease', 'Dashboard\UserController@UserAssetManualIncrease');
            Route::post('AddUser', 'Dashboard\UserController@AddUserNew');
            //Route::post('addUser', 'Dashboard\UserController@addUser');
        });


        Route::prefix('/Wallet')->middleware('UserRole:sub.system.user.wallet')->group(function () {
            Route::get('/UserAssets', 'Dashboard\WalletController@UserAssets');
            Route::get('/UserTransfer', 'Dashboard\WalletController@UserTransfer');
            Route::get('/UserTransactions', 'Dashboard\WalletController@UserTransactions');
            Route::get('/UserProfits', 'Dashboard\WalletController@UserProfits');

            Route::post('/addUserTransfer', 'Dashboard\WalletController@addUserTransfer');
            Route::post('/addUserDeposit', 'Dashboard\WalletController@addUserDeposit');
            Route::post('/getUserDeposit', 'Dashboard\WalletController@getUserDeposit');

            Route::get('/UserWithdraw', 'Dashboard\WalletController@UserWithdraw');
            Route::post('/getUserByEmail', 'Dashboard\WalletController@getUserByEmail');
            Route::post('/addUserWithdraw', 'Dashboard\WalletController@addUserWithdraw');
            Route::post('/getUserWithdraw', 'Dashboard\WalletController@getUserWithdraw');


            Route::middleware('UserRole:sub.system.deposit.admin')->group(function () {
                Route::get('/DepositList', 'Dashboard\WalletController@DepositList');
                Route::get('/TransferList', 'Dashboard\WalletController@TransferList');
                Route::post('/UpdateDepositRow', 'Dashboard\WalletController@UpdateDepositRow');
                Route::get('/WithdrawList', 'Dashboard\WalletController@WithdrawList');
                Route::post('/UpdateUserWithdraw', 'Dashboard\WalletController@UpdateUserWithdraw');
                Route::post('/RemoveWalletByAdmin', 'Dashboard\WalletController@RemoveWalletByAdmin');
            });
        });

        Route::prefix('/Package')->middleware('UserRole:sub.system.user.package')->group(function () {
            Route::get('/PackageList', 'Dashboard\PackageController@PackageList');
            Route::get('/TotalUserProfit/{txt}', 'Dashboard\PackageController@TotalUserProfit');
            Route::post('/addUserPackage', 'Dashboard\PackageController@addUserPackage');
            Route::get('/UserPackages', 'Dashboard\PackageController@UserPackages');
            Route::post('/UserPackageTermination', 'Dashboard\PackageController@UserPackageTermination');
            Route::post('/GetUserPackageInfo', 'Dashboard\PackageController@GetUserPackageInfo');
            Route::middleware('UserRole:sub.system.package.admin')->group(function () {
                Route::get('/TotalUserPackages', 'Dashboard\PackageController@TotalUserPackages');
                Route::get('/TerminationList', 'Dashboard\PackageController@TerminationList');
                Route::post('/RecheckTermination', 'Dashboard\PackageController@RecheckTermination');
                Route::post('/RemovePackageByAdmin', 'Dashboard\PackageController@RemovePackageByAdmin');

            });
        });

        Route::prefix('/Support')->middleware('UserRole:sub.system.user.support')->group(function () {
            Route::get('/RequestList', 'Dashboard\SupportController@RequestList');
            Route::get('/UserNotification', 'Dashboard\SupportController@UserNotification');
            Route::post('/addNewRequest', 'Dashboard\SupportController@addNewRequest');
            Route::post('/SubmitResponse', 'Dashboard\SupportController@SubmitResponse');
            Route::post('/GetNotificationInfo', 'Dashboard\SupportController@GetNotificationInfo');
            Route::get('/ShowDetails/{id}', 'Dashboard\SupportController@ShowDetails');

            Route::middleware('UserRole:sub.system.support.admin')->group(function () {
                Route::get('/TotalRequestList', 'Dashboard\SupportController@TotalRequestList');
                Route::get('/TotalNotification', 'Dashboard\SupportController@TotalNotification');
                Route::post('/RemoveSupportRow', 'Dashboard\SupportController@RemoveSupportRow');
                Route::post('/addNotification', 'Dashboard\SupportController@addNotification');
                Route::post('/RemoveUserNotification', 'Dashboard\SupportController@RemoveUserNotification');

            });
        });

        Route::prefix('/Branches')->middleware('UserRole:sub.system.user.branch')->group(function () {
            Route::get('/UserLevel', 'Dashboard\PackageController@UserLevel');
            Route::get('/UpLine', 'Dashboard\PackageController@UpLine');
        });
        Route::get('/UpLine', 'Dashboard\PackageController@UpLine');

        Route::post('UpdateUserInfo', 'Dashboard\UserController@UpdateUserInfo');
    });

    Route::get('/index', 'Center\CenterController@mainPage');

    /****** TODO News Start Routes*******/

    Route::prefix('news')->middleware('UserRole:sub.system.news')->group(function () {
        Route::get('addNewsForm', 'Dashboard\NewsController@addNewsForm')->middleware('UserRole:sub.system.news.add.news');
        Route::post('getCityByProvince', 'Dashboard\NewsController@getCityByProvince');
        Route::post('GetNewsCategoryChildesByAjax', 'Dashboard\NewsController@GetNewsCategoryChildesByAjax');

        Route::middleware('UserRole:sub.system.news.add.keyword')->group(function () {
            Route::get('keywords', 'Dashboard\NewsController@keywords');
            Route::post('addKeyword', 'Dashboard\NewsController@addKeyword');
            Route::post('removeKeyword', 'Dashboard\NewsController@removeKeyword');
            Route::post('keywordShow', 'Dashboard\NewsController@keywordShow');
            Route::post('keywordsEdit', 'Dashboard\NewsController@keywordsEdit');
            Route::post('SearchInKeywords', 'Dashboard\NewsController@SearchInKeywords');
            Route::get('topKeywordsStatic', 'Dashboard\NewsController@topKeywordsStatic');
        });
        Route::post('addNews', 'Dashboard\NewsController@addNews');
        Route::middleware('UserRole:sub.system.news.lists')->group(function () {
            Route::get('newsLists', 'Dashboard\NewsController@newsLists');
            Route::post('SearchInNews', 'Dashboard\NewsController@SearchInNews');

            Route::middleware('UserRole:sub.system.news.allow.show.news.changes')->group(function () {
                Route::post('logNewsChangesShow', 'Dashboard\NewsController@logNewsChangesShow');
            });
        });
        Route::middleware('UserRole:sub.system.news.allow.note.news.lists')->group(function () {
            Route::get('/noteNewsLists', 'Dashboard\NewsController@noteNewsLists');
        });
        Route::middleware('UserRole:sub.system.news.allow.remove.news')->group(function () {
            Route::post('removeNews', 'Dashboard\NewsController@removeNews');
        });

        Route::middleware('UserRole:sub.system.news.allow.update')->group(function () {
            Route::get('updateNewsForm/{id}', 'Dashboard\NewsController@updateNewsForm');
            Route::post('NewsAddInOtherLanguage', 'Dashboard\NewsController@NewsAddInOtherLanguage');
            Route::post('NewsUpdateInOtherLanguage', 'Dashboard\NewsController@NewsUpdateInOtherLanguage');
            Route::post('updateNews', 'Dashboard\NewsController@updateNews');
        });
        Route::middleware('UserRole:sub.system.news.allow.update.my.news')->group(function () {
            Route::middleware('UserRole:sub.system.news.enable.update.news.button')->group(function () {
                Route::get('updateMyNewsForm/{id}', 'Dashboard\NewsController@updateNewsForm');
                Route::post('updateMyNews', 'Dashboard\NewsController@updateNews');
            });
        });

        Route::middleware('UserRole:sub.system.news.allow.on.slider')->group(function () {
            Route::post('changeSliderState', 'Dashboard\NewsController@changeSliderState');
        });

        Route::middleware('UserRole:sub.system.news.slider.news')->group(function () {
            Route::get('sliderNews', 'Dashboard\NewsController@sliderNews');
        });


        Route::middleware('UserRole:sub.system.news.allow.show.news.comments')->group(function () {
            Route::get('showNewsComments/{id}', 'Dashboard\NewsController@showNewsComments');
            Route::post('showNewsCommentAjax', 'Dashboard\NewsController@showNewsCommentAjax');
            Route::post('changeNewsCommentState', 'Dashboard\NewsController@changeNewsCommentState');
        });

        Route::middleware('UserRole:sub.system.news.my.news')->group(function () {
            Route::get('myNews', 'Dashboard\NewsController@myNews');
        });

        Route::middleware('UserRole:sub.system.news.notes')->group(function () {
            Route::get('noteLists', 'Dashboard\NewsController@noteLists');
            Route::post('addNote', 'Dashboard\NewsController@addNote');
            Route::post('removeNote', 'Dashboard\NewsController@removeNote');
            Route::post('showNews', 'Dashboard\NewsController@showNews');
            Route::post('editNote', 'Dashboard\NewsController@editNote');
        });


    });

    /****** TODO News End Routes*******/

    /****** TODO Notifications Start Routes*******/

    Route::prefix('Notifications')->middleware('UserRole:sub.system.notifications')->group(function () {
        Route::get('ContactUsRequests', 'Dashboard\NotificationController@ContactUsRequests');
        Route::post('ContactUsShow', 'Dashboard\NotificationController@ContactUsShow');
        Route::post('ContactUsRemove', 'Dashboard\NotificationController@ContactUsRemove');
    });

    /****** TODO Notifications End Routes*******/

    /****** TODO Users Start Routes*******/


    Route::prefix('users')->middleware('UserRole:sub.system.users')->group(function () {
        Route::get('roles', 'Dashboard\UserController@roles');
        Route::post('removeRole', 'Dashboard\UserController@removeRole');
        Route::post('addRole', 'Dashboard\UserController@addRole');
        Route::post('showPermissions', 'Dashboard\UserController@showPermissions');
        Route::post('changePermissions', 'Dashboard\UserController@changePermissions');
        Route::get('usersList', 'Dashboard\UserController@usersList');
        Route::post('addUser', 'Dashboard\UserController@addUser');
        Route::post('removeUser', 'Dashboard\UserController@removeUser');
        Route::get('showUser/{id}', 'Dashboard\UserController@showUser');
        Route::post('updateUser', 'Dashboard\UserController@updateUser');
        Route::post('ChangeUserStateBlocking', 'Dashboard\UserController@ChangeUserStateBlocking');
    });

    /****** TODO Users End Routes*******/


    /****** TODO Profile Start Routes*******/

    Route::prefix('profile')->middleware('UserRole:sub.system.profile')->group(function () {
        Route::get('profile', 'Dashboard\ProfileController@profile');
        Route::post('updateProfile', 'Dashboard\ProfileController@updateProfile');
    });

    /****** TODO Profile End Routes*******/

    /****** TODO Settings Start Routes*******/

    Route::prefix('settings')->middleware('UserRole:sub.system.settings')->group(function () {
        Route::middleware('UserRole:sub.system.settings.base')->group(function () {
            Route::get('professionalSettings', 'Dashboard\SettingController@professionalSettings');
            Route::post('UpdateProfessionalSetting', 'Dashboard\SettingController@UpdateProfessionalSetting');

            Route::get('setting', 'Dashboard\SettingController@setting');
            Route::post('updateSetting', 'Dashboard\SettingController@updateSetting');

            Route::get('newsCategory', 'Dashboard\SettingController@newsCategory');
            Route::post('removeNewsCategory', 'Dashboard\SettingController@removeNewsCategory');
            Route::post('addNewsCategory', 'Dashboard\SettingController@addNewsCategory');
            Route::post('showNewsCategory', 'Dashboard\SettingController@showNewsCategory');
            Route::post('editNewsCategory', 'Dashboard\SettingController@editNewsCategory');
            Route::post('editNewsCategoryChild', 'Dashboard\SettingController@editNewsCategoryChild');
            Route::post('NewsCategoryChildUpdate', 'Dashboard\SettingController@NewsCategoryChildUpdate');
            Route::get('socialMedia', 'Dashboard\SettingController@socialMedia');
            Route::post('updateSocialMedia', 'Dashboard\SettingController@updateSocialMedia');
            Route::get('links', 'Dashboard\SettingController@links');
            Route::post('addLink', 'Dashboard\SettingController@addLink');
            Route::post('changeStateLink', 'Dashboard\SettingController@changeStateLink');
            Route::post('removeLink', 'Dashboard\SettingController@removeLink');
            Route::get('provinces', 'Dashboard\SettingController@provinces');
            Route::post('addProvince', 'Dashboard\SettingController@addProvince');
            Route::post('removeProvince', 'Dashboard\SettingController@removeProvince');
            Route::post('showProvince', 'Dashboard\SettingController@showProvince');
            Route::post('editProvince', 'Dashboard\SettingController@editProvince');
            Route::post('addCity', 'Dashboard\SettingController@addCity');
            Route::post('ProvinceCitiesShow', 'Dashboard\SettingController@ProvinceCitiesShow');
            Route::post('editCity', 'Dashboard\SettingController@editCity');
            Route::post('cityRemove', 'Dashboard\SettingController@cityRemove');
            Route::get('CenterSliders', 'Dashboard\SettingController@CenterSliders');
            Route::post('CenterSliderAdd', 'Dashboard\SettingController@CenterSliderAdd');
            Route::post('CenterSliderRemove', 'Dashboard\SettingController@CenterSliderRemove');
            Route::post('CenterSliderChangeState', 'Dashboard\SettingController@CenterSliderChangeState');
            Route::post('CenterSliderShow', 'Dashboard\SettingController@CenterSliderShow');
            Route::post('CenterSliderEdit', 'Dashboard\SettingController@CenterSliderEdit');
            Route::post('NewsCategoryChangeState', 'Dashboard\SettingController@NewsCategoryChangeState');
        });
    });

    /****** TODO Settings End Routes*******/

    /****** TODO FileManager Start Routes*******/

    Route::prefix('FileManager')->middleware('UserRole:sub.system.file.manager')->group(function () {
        Route::get('Public', 'Dashboard\FileManagerController@PublicFiles');
    });

    /****** TODO FileManager End Routes*******/

    /****** TODO Comments Start Routes*******/

    Route::prefix('comments')->middleware('UserRole:sub.system.comments')->group(function () {
        Route::get('showNewsCommentsLists', 'Dashboard\CommentsController@showNewsCommentsLists');
        Route::post('showInfoForSendEmail', 'Dashboard\CommentsController@showInfoForSendEmail');
        Route::post('SendEmail', 'Dashboard\CommentsController@SendEmail');
        Route::middleware('UserRole:sub.system.comments.remove.comment')->group(function () {
            Route::post('removeNewsComment', 'Dashboard\CommentsController@removeNewsComment');
        });
        Route::middleware('UserRole:sub.system.comments.allow.change.state')->group(function () {
            Route::post('changeStateNewsComment', 'Dashboard\CommentsController@changeStateNewsComment');
        });
        Route::middleware('UserRole:sub.system.comments.show.news.comments')->group(function () {
            Route::post('showNewsComment', 'Dashboard\CommentsController@showNewsComment');
        });
        Route::middleware('UserRole:sub.system.comments.allow.update.news.comment')->group(function () {
            Route::post('NewsCommentUpdate', 'Dashboard\CommentsController@NewsCommentUpdate');
        });
    });

    /****** TODO Comments End Routes*******/

    /****** TODO Message Start Routes*******/

    Route::prefix('message')->middleware('UserRole:sub.system.message')->group(function () {
        Route::get('showReceiveMessageLists', 'Dashboard\MessageController@showReceiveMessageLists');
        Route::get('showSendMessageLists', 'Dashboard\MessageController@showSendMessageLists');
        Route::get('trashMessageLists', 'Dashboard\MessageController@trashMessageLists');
        Route::get('compose', 'Dashboard\MessageController@compose');
        Route::post('sendMessage', 'Dashboard\MessageController@sendMessage');
        Route::get('readMessage/{id}', 'Dashboard\MessageController@readMessage');
        Route::post('sendToTrashByTo', 'Dashboard\MessageController@sendToTrashByTo');
        Route::post('sendToTrashByFrom', 'Dashboard\MessageController@sendToTrashByFrom');
        Route::post('sendToTrash', 'Dashboard\MessageController@sendToTrash');
        Route::post('removeMessage', 'Dashboard\MessageController@removeMessage');
    });

    /****** TODO Message End Routes*******/

    /****** TODO Exclusive Pages Start Routes*******/

    Route::prefix('ExclusivePages')->middleware('UserRole:sub.system.exclusive.pages')->group(function () {
        Route::get('ExclusivePagesLists', 'Dashboard\ExclusivePagesController@ExclusivePagesLists');
        Route::post('ExclusivePageAdd', 'Dashboard\ExclusivePagesController@ExclusivePageAdd');
        Route::post('ExclusivePagesRemove', 'Dashboard\ExclusivePagesController@ExclusivePagesRemove');
        Route::post('ExclusivePageChangeState', 'Dashboard\ExclusivePagesController@ExclusivePageChangeState');
        Route::post('ExclusivePageShow', 'Dashboard\ExclusivePagesController@ExclusivePageShow');
        Route::post('ExclusivePageUpdate', 'Dashboard\ExclusivePagesController@ExclusivePageUpdate');
    });

    /****** TODO Exclusive Pages End Routes*******/

});
/* TODO End Dashboard Page Route */


/* TODO Sentinel Start Routes */
Route::get('/createUser', 'SentinelController@createUser');
Route::get('/activationUser/{id}', 'SentinelController@activationUser');
Route::get('/checkAuthorized', 'SentinelController@checkAuthorized');
Route::get('/logout', 'SentinelController@logout');
Route::get('/getUser', 'SentinelController@getUser');
Route::get('/addPermissionToRole/{ActionName}/{ActionDescription}/{ActionType}', 'SentinelController@addPermissionToRole');
Route::get('/RemovePermission/{ActionName}', 'SentinelController@RemovePermission');
Route::get('/attachUserToRole', 'SentinelController@assignUserToRole');
Route::get('/detachUserToRole', 'SentinelController@detachUserToRole');
Route::post('/PublicRegistration', 'ViewBaseController@PublicRegistration');
/* TODO Sentinel End Routes */
