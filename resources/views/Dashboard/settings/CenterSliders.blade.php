@extends('Layouts.dashboardMainLayout')
@section('BreadCrumbLevel')
    <li class="active">اسلایدرهای مرکزی</li>
@Stop
@section('BreadCrumbURL','/settings/setting')
@section('BreadCrumbTitle','تنظیمات پایه ای سیستم')
@section('CssFiles')
    <link rel="stylesheet" type="text/css"
          href="{{ URL::asset('assets/login/bower_components//fancyBox/jquery.fancybox.min.css') }}">
    <style type="text/css">
        #image-preview {
            width: 200px;
            height: 200px;
            position: relative;
            overflow: hidden;
            background-color: #ffffff;
            color: #ecf0f1;
        }
        #image-preview-edit {
            width: 200px;
            height: 200px;
            position: relative;
            overflow: hidden;
            background-color: #ffffff;
            color: #ecf0f1;
        }

        #image-preview input {
            line-height: 200px;
            font-size: 200px;
            position: absolute;
            opacity: 0;
            z-index: 10;
        }

        #image-preview-edit input {
            line-height: 200px;
            font-size: 200px;
            position: absolute;
            opacity: 0;
            z-index: 10;
        }

        #image-preview label {
            position: absolute;
            z-index: 5;
            opacity: 0.8;
            cursor: pointer;
            background-color: #bdc3c7;
            width: 200px;
            height: 50px;
            font-size: 20px;
            line-height: 50px;
            text-transform: uppercase;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: auto;
            text-align: center;
        }

        #image-preview-edit label {
            position: absolute;
            z-index: 5;
            opacity: 0.8;
            cursor: pointer;
            background-color: #bdc3c7;
            width: 200px;
            height: 50px;
            font-size: 20px;
            line-height: 50px;
            text-transform: uppercase;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: auto;
            text-align: center;
        }
    </style>
@endsection

@section('MainContent')
    <div class="row">
        <section class="col-lg-12 col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <i class="fa fa-info-circle"></i>
                    <h3 class="box-title">
                        {{ \App\Facade\DbSettings::GetDbSetting()->Title }}
                    </h3>
                    <!-- tools box -->
                    <div class="pull-left box-tools">
                        <button type="button" class="btn bg-info btn-sm" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /. tools -->
                </div>
                <div class="modal modal-info fade" id="modal-slider-add">
                    <div class="modal-dialog">
                        <form action="/settings/CenterSliderAdd" method="post" id="SliderAdd" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">افزودن گرافیک</h4>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        <label for="Title">عنوان</label>
                                        <input type="text" name="Title" id="Title" class="form-control" required>
                                    </p>
                                    <p>
                                    <div id="image-preview">
                                        <label for="image-upload" id="image-label">عکس جدید</label>
                                        <input type="file" name="fileupload" id="image-upload"/>
                                    </div>
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">خروج
                                    </button>
                                    <input type="submit" class="btn btn-outline" value="ثبت">
                                </div>
                            </div>
                        </form>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

                <div class="modal modal-info fade" id="modal-slider-edit">
                    <div class="modal-dialog">
                        <form action="/settings/CenterSliderEdit" method="post" id="sliderEdit" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="SID" id="SID" value="">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">ویرایش گرافیک</h4>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        <label for="ShowTitle">عنوان</label>
                                        <input type="text" name="ShowTitle" id="ShowTitle" class="form-control" required>
                                    </p>
                                    <p>
                                        <label for="ShowOrder">ترتیب</label>
                                        <input type="number" name="ShowOrder" id="ShowOrder" class="form-control" required>
                                    </p>
                                    <p>
                                    <div id="image-preview-edit">
                                        <label for="image-upload-edit" id="image-label-edit">عکس جدید</label>
                                        <input type="file" name="fileupload" id="image-upload-edit"/>
                                    </div>
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">خروج
                                    </button>
                                    <input type="submit" class="btn btn-outline" value="ویرایش">
                                </div>
                            </div>
                        </form>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <div class="box-body">
                    <div class="content">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">اسلایدرهای مرکزی</h3>
                            </div>
                            <div class="box-header with-border">
                                <a class="btn btn-app" data-toggle="modal" data-target="#modal-slider-add">
                                    <i class="fa fa-plus"></i>جدید
                                </a>
                                @includeIf('Dashboard.toolbars.settingToolbar')
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table id="keyWordDataTable"
                                           class="table table-bordered table-striped table-responsive table-hover">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>عنوان</th>
                                            <th>گرافیک</th>
                                            <th>ترتیب</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($sliders as $key=>$n)
                                            <tr @if($n->State == 1) style="background-color: rgba(200,0,0,0.3)" @endif>
                                                <td>{{ ++$key }}</td>
                                                <td>{{ $n['Title'] }}</td>
                                                @php
                                                    $smallPath=str_replace('/Sliders/','/Sliders/small/',$n['Image']);
                                                    $imgOutput=(file_exists($smallPath) == 1 ? $smallPath : $n['Image']);
                                                @endphp
                                                <td>
                                                    <a data-fancybox="images" data-caption="{{ $n['Title'] }}"
                                                       href="/{{ $n['Image'] }}">
                                                        <img height="60" width="60" src="/{{ $imgOutput }}"
                                                             alt="گرافیک">
                                                    </a>
                                                </td>
                                                <td>{{ $n['Order'] }}</td>
                                                <td>
                                                    <div class="input-group">
                                                        <button type="button"
                                                                class="btn bg-light-blue-active dropdown-toggle "
                                                                data-toggle="dropdown"
                                                                style="font-size: 9px !important ">
                                                            <span class="fa fa-caret-down"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a class="btn bg-maroon btn-xs" data-id="{{ $n->id }}"
                                                                   title="حذف رکورد"><span class="fa fa-trash"></span>حذف</a>
                                                            </li>
                                                            <li><a class="btn bg-yellow-gradient btn-xs"
                                                                   data-id="{{ $n->id }}"
                                                                   title="غیرفعال کردن"><span class="fa fa-ban"></span>فعال/غیرفعال</a>
                                                            </li>
                                                            <li><a class="btn bg-green btn-xs" data-id="{{ $n->id }}" data-toggle="modal" data-target="#modal-slider-edit"
                                                                   title="ویرایش"><span class="fa fa-edit"></span>ویرایش</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('JsFiles')
    <script src="{{ URL::asset('assets/login/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/login/bower_components/fancyBox/jquery.fancybox.min.js') }}"></script>
    <script src="{{ URL::asset('assets/login/bower_components/imagePreview/jquery.uploadPreview.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            @if(Session::has('center_slider_add_success'))
            swal("", "رکورد جدید با موفقیت اضافه گردید", "success");
            @endif
            @if(Session::has('center_slider_edit_success'))
            swal("", "رکورد موردنظر با موفقیت ویرایش گردید", "success");
            @endif
        });
    </script>
    <script>
        $(function () {
            $('.bg-maroon').click(function () {
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                var ele = $(this).parent().parent().parent().parent().parent();
                swal({
                    title: "",
                    text: "آیا از حذف این رکورد مطمئن هستید؟",
                    icon: "warning",
                    buttons: ["خیر", "بله"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '/settings/CenterSliderRemove',
                            data: {_token: token, id: id},
                            type: 'POST',
                            dataType: 'JSON',
                            success: function (data) {
                                if (data.state == 1) {
                                    ele.fadeOut().remove();
                                    swal("", data.message, "success");
                                } else {
                                    swal(data.message, {
                                        icon: 'warning',
                                        dangerMode: true
                                    });
                                }
                            }
                        });
                    }
                });
            });

            $('.bg-yellow-gradient').click(function () {
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                swal({
                    title: "",
                    text: "آیا از تغییر وضعیت این رکورد مطمئن هستید؟",
                    icon: "warning",
                    buttons: ["خیر", "بله"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '/settings/CenterSliderChangeState',
                            data: {_token: token, id: id},
                            type: 'POST',
                            dataType: 'JSON',
                            success: function (data) {
                                if (data.state == 1) {
                                    swal("", data.message, "success");
                                    location.reload();
                                } else {
                                    swal(data.message, {
                                        icon: 'warning',
                                        dangerMode: true
                                    });
                                }
                            }
                        });
                    }
                });
            });

            $('.bg-green').click(function () {
                $('.ShowModalLoadingArea').css({'display':'block','position':'sticky',});
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/settings/CenterSliderShow',
                    data: {_token: token, id: id},
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.state == 1) {
                            $('#ShowTitle').val(data.message.Title);
                            $('#ShowOrder').val(data.message.Order);
                            $('#SID').val(id);
                            $('.ShowModalLoadingArea').css('display','none');
                        } else {
                            swal(data.message, {
                                icon: 'warning',
                                dangerMode: true
                            });
                        }
                    }
                });
            });

            $.uploadPreview({
                input_field: "#image-upload,#image-upload-edit",   // Default: .image-upload
                preview_box: "#image-preview,#image-preview-edit",  // Default: .image-preview
                label_field: "#image-label,#image-label-edit",    // Default: .image-label
                label_default: "آپلود عکس جدید",   // Default: Choose File
                label_selected: "تغییر عکس",  // Default: Change File
                no_label: false                 // Default: false
            });

            $('[data-fancybox="images"]').fancybox({
                buttons: [
                    'slideShow',
                    'fullScreen',
                    'thumbs',
                    'download',
                    'zoom',
                    'close'
                ],
            });
            $('#SliderAdd').on('submit', function () {
                $(this).find('input[type="submit"]').attr('disabled', 'disabled')
            });
            $('#sliderEdit').on('submit', function () {
                $(this).find('input[type="submit"]').attr('disabled', 'disabled')
            });

        })
    </script>
@endsection
