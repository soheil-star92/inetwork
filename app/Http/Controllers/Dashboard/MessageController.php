<?php

namespace App\Http\Controllers\Dashboard;

use App\DbModels\Dashboard\Message\Message;
use App\DbModels\Dashboard\Users\RoleAdministrator;
use App\DbModels\Dashboard\Users\User;
use App\Facade\OrganizationInfo;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Soheili\ImageWorker\ImageWorker;

class MessageController extends Controller
{
	public function showReceiveMessageLists()
	{
		$user = Sentinel::getUser();
		$messages = Message::where([
				['messages.to_trash', '=', 0],
				['to_user_id', '=', $user['id']]
		])
				->join('users', 'users.id', '=', 'messages.from_user_id')
				->select(
						'messages.*', 'users.first_name', 'users.last_name'
				)
				->orderBy('id', 'DESC')
				->get();
		return view('Dashboard.message.showReceiveMessageLists', compact('messages'));
	}

	public function showSendMessageLists()
	{
		$user = Sentinel::getUser();
		$messages = Message::where([
				['messages.from_trash', '=', 0],
				['from_user_id', '=', $user['id']]
		])
				->join('users', 'users.id', '=', 'messages.to_user_id')
				->select(
						'messages.*', 'users.first_name', 'users.last_name'
				)
				->orderBy('id', 'DESC')
				->get();
		return view('Dashboard.message.showSendMessageLists', compact('messages'));
	}

	public function compose()
	{
		$users = User::all();
		if (OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.message.send.message.just.to.administrators'])) {
			$roles = RoleAdministrator::pluck('role_id');
			$users = User::whereIn('role_users.role_id', json_decode($roles))
					->join('role_users', 'role_users.user_id', '=', 'users.id')
					->select('users.*')
					->get();
			unset($roles);
		}
		return view('Dashboard.message.compose', compact('users'));
	}

	public function sendMessage(Request $request)
	{
		$request->validate([
				'Subject' => 'required',
				'Message' => 'required',
				'sctUserId' => 'required',
				'Attachment' => 'file|mimes:pdf,zip,rar,doc,docx,png,jpg,jpeg|max:10000'
		]);
		$user = Sentinel::getUser();
		if ($request->hasFile('Attachment')) {
			$IW = new ImageWorker($request, 'Attachment', 'otherFiles/messageAttachments/');
			$IW->Move();
		}
		foreach ($request->sctUserId AS $row) {
			$sendMessage = new Message();
			$sendMessage->to_user_id = $row;
			$sendMessage->from_user_id = $user['id'];
			$sendMessage->Subject = $request->Subject;
			$sendMessage->Message = $request->Message;
			$sendMessage->Attachment = isset($IW->FullPath)? $IW->FullPath : null;
			$sendMessage->Show = 0;
			$sendMessage->from_trash = 0;
			$sendMessage->to_trash = 0;
			$sendMessage->save();
		}
		unset($IW);
		Session::flash('send-message-success');
		return back();
	}

	public
	function readMessage($id)
	{
		Message::where('id', $id)
				->update([
						'Show' => 1
				]);
		$message = Message::where('messages.id', $id)
				->join('users', 'users.id', '=', 'messages.from_user_id')
				->select(
						'messages.*', 'users.first_name', 'users.last_name'
				)
				->first();
		return view('Dashboard.message.readMessage', compact('message'));
	}

	public
	function trashMessageLists()
	{
		$messages = Message::where('messages.to_trash', 1)
				->join('users', 'users.id', '=', 'messages.from_user_id')
				->select(
						'messages.*', 'users.first_name', 'users.last_name'
				)
				->get();
		return view('Dashboard.message.trashMessageLists', compact('messages'));
	}

	public
	function sendToTrashByTo(Request $request)
	{
		$msgSelectedId = $request->msgId;
		if ($msgSelectedId) {
			foreach ($msgSelectedId AS $msID) {
				Message::where('id', $msID)
						->update([
								'to_trash' => 1
						]);
			}
			Session::flash('send-message-to-trash-by-to-success');
		}
		return back();
	}

	public
	function sendToTrashByFrom(Request $request)
	{
		$msgSelectedId = $request->msgId;
		if ($msgSelectedId) {
			foreach ($msgSelectedId AS $msID) {
				Message::where('id', $msID)
						->update([
								'from_trash' => 1
						]);
			}
			Session::flash('send-message-to-trash-by-from-success');
		}
		return back();
	}

	public
	function sendToTrash(Request $request)
	{
		$user = Sentinel::getUser();
		$msgId = Session::pull('msgId');
		$msgUsers = Message::where('id', $msgId)
				->select('from_user_id', 'to_user_id')
				->first();
		if ($user['id'] == $msgUsers['from_user_id']) {
			Message::where('id', $msgId)
					->update([
							'from_trash' => 1
					]);
		} elseif ($user['id'] == $msgUsers['to_user_id']) {
			Message::where('id', $msgId)
					->update([
							'to_trash' => 1
					]);
		}
		return response()->redirectTo('/message/showReceiveMessageLists');
	}

	public
	function removeMessage(Request $request)
	{
		$msgSelectedId = $request->msgId;
		if ($msgSelectedId) {
			foreach ($msgSelectedId AS $msID) {
				$msg = Message::where('id', $msID)
						->select('Attachment')
						->first();
				if (file_exists(public_path($msg['Attachment'])) && !is_null($msg['Attachment'])) {
					unlink(public_path($msg['Attachment']));
				}
				Message::where('id', $msID)
						->delete();
			}
			Session::flash('remove-message-success');
		}
		return back();
	}
}
