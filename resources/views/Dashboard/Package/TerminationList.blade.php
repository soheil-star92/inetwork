@php
    $settings=\App\Facade\DbSettings::GetDbSetting();
    $userLogged=\App\Facade\OrganizationInfo::GetUserInfoWithoutParameter();
    $RegisteredEmail=$userLogged->RegisteredEmail;

@endphp

@section('CssFiles')
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/forms/select/select2.min.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/animate/animate.min.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/extensions/sweetalert2.min.css">
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/vendors/css/tables/datatable/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/vendors/css/tables/datatable/responsive.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/vendors/css/tables/datatable/buttons.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/vendors/css/tables/datatable/rowGroup.bootstrap5.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/bordered-layout.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/plugins/forms/form-validation.css">
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/css/plugins/extensions/ext-component-sweet-alerts.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/pages/modal-create-app.css">
    <!-- END: Page CSS-->
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/css/plugins/extensions/ext-component-sweet-alerts.css">

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/assets/css/style.css">
    <!-- END: Custom CSS-->


@stop
@extends('Layouts.dashboardMainLayout')

@section('MainContent')

    <div class="modal fade" id="showRowModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-edit-user">
            <div class="modal-content">
                <div class="modal-header bg-transparent">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body pb-5 px-sm-5 pt-50">
                    <div class="text-center mb-2">
                        <h1 class="mb-1">Show Record</h1>
                    </div>
                    <form id="editUserForm" class="row gy-1 pt-75" action="/AdminPanel/Package/RecheckTermination"
                          method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="iptKey" id="iptKey">
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptFullname">Fullname</label>
                            <input type="text" id="iptFullname" name="iptFullname"
                                   class="form-control" placeholder="0" disabled />
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptEmail">Email</label>
                            <input type="text" id="iptEmail" name="iptEmail"
                                   class="form-control" placeholder="0" disabled/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptPackageName">Package Name</label>
                            <input type="text" id="iptPackageName" name="iptPackageName"
                                   class="form-control"
                                   disabled/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptPrice">Price($)</label>
                            <input type="text" id="iptPrice" name="iptPrice"
                                   class="form-control" placeholder="0" disabled/>
                        </div>

                       <div class="col-12 col-md-6">
                            <label class="form-label" for="iptDailyProfitPercent">Daily Profit Percent</label>
                            <input type="text" id="iptDailyProfitPercent" name="iptDailyProfitPercent"
                                   class="form-control" placeholder="0" disabled/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptCreatedAt">Created at</label>
                            <input type="text" id="iptCreatedAt" name="iptCreatedAt"
                                   class="form-control" placeholder="0" disabled/>
                        </div>

                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptDeductionPrice">Deduction Price($)</label>
                            <input type="number" id="iptDeductionPrice" name="iptDeductionPrice"
                                   class="form-control" placeholder="0" required/>
                        </div>


                        <div class="col-12 text-center mt-2 pt-50">
                            <button type="submit" class="btn btn-primary me-1">Submit</button>
                            <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal"
                                    aria-label="Close">
                                Discard
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="app-user-view-account">
                    <div class="row">
                        <!-- User Content -->
                        <div class="col-xl-12 col-lg-12 col-md-12 order-0 order-md-1">
                            <!-- User Pills -->
                            <ul class="nav nav-pills mb-2">
                                <li class="nav-item">
                                    <a class="nav-link active" href="/AdminPanel/Package/UserPackages">
                                        <i data-feather="user" class="font-medium-3 me-50"></i>
                                        <span class="fw-bold">User's Packages</span></a>
                                </li>
                            </ul>
                            <!--/ User Pills -->

                            <!-- Project table -->
                            <div class="card">
                                <h4 class="card-header">User's Package List</h4>
                                <div class="table-responsive">
                                    <ul>
                                    <li>the termination any packages isn't possible before 2 months</li>
                                    <li>be removed all profits deposited to wallet , if termination after 2 to 6 months.</li>
                                    <li>the termination after 6 months,to be deducted 60 percent of all profit deposited to user wallet.</li>
                                    <li> pre requirement for extra package: must be purchase one of the package A,B,C or D </li>
                                    </ul>
                                </div>
                                <div class="table-responsive">
                                    <table id="keyWordDataTable" class="table datatable-project">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Email</th>
                                            <th>FullName</th>
                                            <th>Name</th>
                                            <th>Daily PercentProfit</th>
                                            <th>User Price($)</th>
                                            <th class="text-nowrap">Created</th>
                                            <th class="text-nowrap">Expire</th>
                                            <th>Total Profit Income</th>
                                            <th>Status</th>
                                            <th>Operation</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($rows as $key=>$row)
                                            <tr>
                                                <td>{{++$key}}</td>
                                                <td>{{$row->email}}</td>
                                                <td>{{$row->first_name.' '.$row->last_name}}</td>
                                                <td>{{$row->Name}}</td>
                                                <td>{{$row->DailyProfitPercent}}</td>
                                                <td>{{$row->Price}}$</td>
                                                <td>{{$row->created_at}}</td>
                                                <td>{{$row->expire_date}}</td>
                                                <td>${{\App\MyClasses\CmsFunctions::TotalUserPackageProfit($row->id,$row->user_id)}}</td>
                                                <td>{!! \App\MyClasses\CmsFunctions::UserPackageStatus($row->Disable) !!}</td>
                                                <td>
                                                    @if($row->Disable == 0)
                                                            <div class="dropdown">
                                                                <button type="button"
                                                                        class="btn btn-sm dropdown-toggle hide-arrow py-0 waves-effect waves-float waves-light"
                                                                        data-bs-toggle="dropdown" aria-expanded="false">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14"
                                                                         height="14" viewBox="0 0 24 24" fill="none"
                                                                         stroke="currentColor" stroke-width="2"
                                                                         stroke-linecap="round" stroke-linejoin="round"
                                                                         class="feather feather-more-vertical">
                                                                        <circle cx="12" cy="12" r="1"></circle>
                                                                        <circle cx="12" cy="5" r="1"></circle>
                                                                        <circle cx="12" cy="19" r="1"></circle>
                                                                    </svg>
                                                                </button>

                                                                <div class="dropdown-menu dropdown-menu-end" style="">
                                                                    <a data-bs-toggle="modal" data-id="{{\App\MyClasses\CmsFunctions::EncodedText($row->id)}}"
                                                                       data-bs-target="#showRowModal" class="dropdown-item aShowRow"

                                                                    >
                                                                        <i data-feather='slash'></i>
                                                                        <span>Recheck Termination</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /Project table -->

                        </div>
                        <!--/ User Content -->
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->
@stop

@section('JsFiles')
    <!-- BEGIN: Vendor JS-->
    <script src="/Themes/Admin/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="/Themes/Admin/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/forms/cleave/cleave.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/forms/cleave/addons/cleave-phone.us.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/moment.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/dataTables.bootstrap5.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/responsive.bootstrap5.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/jszip.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/polyfill.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/Themes/Admin/app-assets/js/core/app-menu.js"></script>
    <script src="/Themes/Admin/app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-add-new-cc.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/page-pricing.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-add-new-address.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-create-app.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-two-factor-auth.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-edit-user.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-share-project.js"></script>
    <!-- END: Page JS-->


    <!-- BEGIN: Page Vendor JS-->
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/polyfill.min.js"></script>
    <!-- END: Page Vendor JS-->


    <script>
        $(document).ready(function () {
            $('body').on('click', '.aShowRow', function () {
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/AdminPanel/Package/GetUserPackageInfo',
                    data: {_token: token, id: id},
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.state == 1) {
                            $('#iptEmail').val(data.message.email);
                            $('#iptFullname').val(data.message.first_name+' '+data.message.last_name);
                            $('#iptPackageName').val(data.message.Name);
                            $('#iptKey').val(data.iptKey);
                            $('#iptPrice').val(data.message.Price);
                            $('#iptCreatedAt').val(data.message.created_at);
                            $('#iptDailyProfitPercent').val(data.message.DailyProfitPercent);
                        } else {
                            toastr['error'](data.message, 'Error', {
                                closeButton: true,
                                tapToDismiss: false,
                                progressBar: true  });
                        }
                    }
                });

            });

            $('#showRowModal').one('submit', function () {
                $(this).find('button[type="submit"]').attr('disabled', 'disabled');
            });
            @if(Session::has('error_at_termination_package'))
                toastr['error']('{{Session::get('error_at_termination_package')}}', 'Error', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true
            });
            @endif
                    @if(Session::has('termination_package_successfully'))
                toastr['success']('{{Session::get('termination_package_successfully')}}', 'Success', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true
            });
            @endif

            $('#keyWordDataTable').DataTable({
                "language": {
                    "lengthMenu": "نمایش _MENU_ رکورد در صفحه",
                    "zeroRecords": "Info Empty",
                    "info": "نمایش updateNewsFormصفحه _PAGE_ از _PAGES_",
                    "infoEmpty": "Info Empty",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "search": "Search: "
                },
                "paging": false,
                "info": false,
                stateSave: false
                // scrollY:        300,
                // scrollCollapse: true,
                // fixedColumns:   true
            });

        });

        $(window).on('load', function () {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>
@stop
