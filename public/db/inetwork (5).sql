-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 01, 2022 at 08:48 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inetwork`
--

-- --------------------------------------------------------

--
-- Table structure for table `activations`
--

DROP TABLE IF EXISTS `activations`;
CREATE TABLE IF NOT EXISTS `activations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iso` char(2) COLLATE utf8mb4_persian_ci NOT NULL,
  `name` varchar(80) COLLATE utf8mb4_persian_ci NOT NULL,
  `nicename` varchar(80) COLLATE utf8mb4_persian_ci NOT NULL,
  `iso3` char(3) COLLATE utf8mb4_persian_ci DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 225),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 850),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 856),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340),
(235, 'WF', 'WALLIS AND FUTUNinetworkA', 'Wallis and Futuna', 'WLF', 876, 681),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263);

-- --------------------------------------------------------

--
-- Table structure for table `exclusive_page_lists`
--

DROP TABLE IF EXISTS `exclusive_page_lists`;
CREATE TABLE IF NOT EXISTS `exclusive_page_lists` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Title` varchar(50) COLLATE utf8mb4_persian_ci NOT NULL,
  `Content` longtext COLLATE utf8mb4_persian_ci NOT NULL,
  `View` int(10) NOT NULL DEFAULT '0',
  `State` tinyint(2) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `exclusive_page_lists`
--

INSERT INTO `exclusive_page_lists` (`id`, `Title`, `Content`, `View`, `State`, `created_at`, `updated_at`) VALUES
(24, 'همدان 118', '<p>متن2</p>\r\n<p><strong>متنییی1</strong></p>\r\n<p>تنون</p>\r\n<p style=\"text-align: center;\"><em>یشسی</em></p>\r\n<p>سشی</p>', 9, 1, '2019-01-22 19:34:52', '2019-07-23 18:24:34'),
(25, 'تستی', '<p>fgdf</p>\r\n<p>gdfg</p>', 0, 0, '2019-01-22 21:05:06', '2019-01-22 21:05:33');

-- --------------------------------------------------------

--
-- Table structure for table `info_users`
--

DROP TABLE IF EXISTS `info_users`;
CREATE TABLE IF NOT EXISTS `info_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `id_i` varchar(12) COLLATE utf8mb4_persian_ci DEFAULT NULL,
  `Address` text COLLATE utf8mb4_persian_ci,
  `Phone` varchar(15) COLLATE utf8mb4_persian_ci DEFAULT NULL,
  `Image` text COLLATE utf8mb4_persian_ci,
  `Blocked` tinyint(1) NOT NULL DEFAULT '0',
  `ReferralCode` int(10) UNSIGNED NOT NULL,
  `ShowReferralCode` tinyint(3) NOT NULL DEFAULT '0',
  `HeadCode` int(11) NOT NULL DEFAULT '0',
  `country_id` int(11) NOT NULL DEFAULT '0',
  `RegisteredEmail` tinyint(3) NOT NULL DEFAULT '0',
  `Wallet` text COLLATE utf8mb4_persian_ci,
  KEY `FK_info_users_users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `info_users`
--

INSERT INTO `info_users` (`user_id`, `id_i`, `Address`, `Phone`, `Image`, `Blocked`, `ReferralCode`, `ShowReferralCode`, `HeadCode`, `country_id`, `RegisteredEmail`, `Wallet`) VALUES
(1, '', NULL, NULL, 'assets/no-image.png', 0, 123000, 0, 0, 10, 1, 'sdfghsfghdsfghdghjdtghjdghj'),
(15, NULL, NULL, '09392980663', 'assets/no-image.png', 0, 123016, 0, 123000, 101, 0, 'xyzkerim'),
(16, NULL, NULL, '09392980667', 'assets/no-image.png', 0, 123040, 0, 123016, 4, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `keywords`
--

DROP TABLE IF EXISTS `keywords`;
CREATE TABLE IF NOT EXISTS `keywords` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Title` varchar(100) COLLATE utf8mb4_persian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2340 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `keywords`
--

INSERT INTO `keywords` (`id`, `Title`) VALUES
(1, 'کلمه_کلیدی#'),
(2334, 'تستی#'),
(2335, 'خالی#'),
(2339, 'لیبل#');

-- --------------------------------------------------------

--
-- Stand-in structure for view `keywords_use_in_news_count_view`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `keywords_use_in_news_count_view`;
CREATE TABLE IF NOT EXISTS `keywords_use_in_news_count_view` (
`id` bigint(20) unsigned
,`Title` varchar(100)
,`countKeyword` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `lang`
--

DROP TABLE IF EXISTS `lang`;
CREATE TABLE IF NOT EXISTS `lang` (
  `id` tinyint(1) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Title` varchar(3) COLLATE utf8mb4_persian_ci NOT NULL,
  `Flag` varchar(100) COLLATE utf8mb4_persian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `lang`
--

INSERT INTO `lang` (`id`, `Title`, `Flag`) VALUES
(1, 'fa', '/otherFiles/Languages/fa.png'),
(2, 'en', '/otherFiles/Languages/en.png');

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

DROP TABLE IF EXISTS `links`;
CREATE TABLE IF NOT EXISTS `links` (
  `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Title` varchar(50) COLLATE utf8mb4_persian_ci NOT NULL,
  `Link` varchar(100) COLLATE utf8mb4_persian_ci NOT NULL,
  `Disabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `links`
--

INSERT INTO `links` (`id`, `Title`, `Link`, `Disabled`) VALUES
(5, 'توسعه پرداز ریتا', 'http://RitaCo.Net', 0),
(6, 'گوگل', 'http://google.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `replay_message_id` int(10) UNSIGNED DEFAULT NULL,
  `from_user_id` int(10) UNSIGNED NOT NULL,
  `to_user_id` int(10) UNSIGNED NOT NULL,
  `Subject` varchar(70) COLLATE utf8mb4_persian_ci NOT NULL,
  `Message` text COLLATE utf8mb4_persian_ci NOT NULL,
  `Attachment` text COLLATE utf8mb4_persian_ci,
  `Show` tinyint(1) NOT NULL,
  `to_trash` tinyint(1) NOT NULL,
  `from_trash` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_messages_users` (`from_user_id`),
  KEY `FK_messages_users_2` (`to_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_07_02_230147_migration_cartalyst_sentinel', 1);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `Title` varchar(150) COLLATE utf8mb4_persian_ci NOT NULL,
  `Bottom_Title` varchar(150) COLLATE utf8mb4_persian_ci DEFAULT NULL,
  `Top_Title` varchar(150) COLLATE utf8mb4_persian_ci DEFAULT NULL,
  `Image` text COLLATE utf8mb4_persian_ci NOT NULL,
  `Content` longtext COLLATE utf8mb4_persian_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `news_category_id` tinyint(3) UNSIGNED NOT NULL,
  `news_category_child_id` tinyint(3) DEFAULT NULL,
  `View` bigint(20) NOT NULL DEFAULT '0',
  `Slider` tinyint(1) NOT NULL DEFAULT '0',
  `news_state_id` tinyint(1) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_news_news_categories` (`news_category_id`),
  KEY `FK_news_news_states` (`news_state_id`),
  KEY `FK_news_users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `Title`, `Bottom_Title`, `Top_Title`, `Image`, `Content`, `user_id`, `news_category_id`, `news_category_child_id`, `View`, `Slider`, `news_state_id`, `created_at`, `updated_at`) VALUES
(2, 'سرو غذا و نوشیدنی در رستوران', NULL, NULL, 'assets/no-image.png', '<p dir=\"rtl\" style=\"font-family: \'Times New Roman\'; font-size: medium; line-height: 24px;\" align=\"justify\"><span style=\"color: #000066; font-family: tahoma; font-size: small;\"><span lang=\"fa\">منوهایی که سازمان و یا تشکیلات ارایه میدهند و تنوع در انتخاب منوها می بایست با نیازمندیهای افراد تطابق داشته باشد. در هتل ها و رستورانهایی که در سطح پایین تری قرار دارند، حق انتخاب مشتری به دلایل</span>&nbsp;<span lang=\"fa\">متعددی تقریباً محدود است. دلیل اول قیمت هاست. به عنوان نمونه اگر قرار باشد فرد در یک وعده غذایی به طور کامل از دسرها و ... استفاده کند و قیمت ها هم محدود باشند، برخی از غذاها و دسر ها به دلیل بالا بودن قیمت ها از منو حذف می شوند. در مرتبه بعد، مدت زمانی است که فرد برای خوردن غذای خود صرف میکند. این زمان معمولاً در این دسته از رستورانها کمتر از نیم ساعت است و به ندرت افزایش می یابد. با در نظر گرفتن بازه ی زمانی موجود نسبت به رستورانهای دیگر مشتری های کمتری مراجعه می کنند. در مرتبه سوم احتمال دارد مشتری هنگامی که با منوی کامل و گسترده روبرو می شود، احساس خوبی نداشته باشد و ترجیح دهد منوی غذایش به صورت محدودتری ارائه شود اما همواره از چندین انتخاب برخوردار باشد. در رستورانهای مدرن و مجهز، قطعاً منوی غذایی که ارائه میشود گزینه های بیشتری در اختیار مشتری قرار می دهد و به تناسب غذاها مجلل تر هستند. اغلب حداقل زمانی که فرد در این دسته از رستورانها برای یک وعده غذایی صرف میکند، یک ساعت و نیم است و گاه ممکن است سه یا چهار ساعت زمان ببرد که البته به تعداد میهمانان و نوع غذا بستگی دارد. مسلماً زمانی که فرد برای خواندن منو و انتخاب غذا اختصاص میدهد نسبت به رستورانهای کوچکتر بسیار بیشتر است. مشتریانی که در این گروه از رستورانهارفت و آمد میکنند، انتظار منوی وسیع تری را دارند که شامل انواع سالادها، دسرها، نوشیدنیها، غذاهای اصلی باشد. بدین ترتیب منوی ارائه شونده در یک مجموعه به عوامل متعددی بستگی دارد</span></span></p>\r\n<p dir=\"rtl\" style=\"font-family: \'Times New Roman\'; font-size: medium; line-height: 16px;\" align=\"justify\"><span lang=\"fa\"><span style=\"color: #000066; font-family: tahoma; font-size: small;\">- هزینه ای که مشتری می خواهد در برابر آن بپردازد</span></span></p>\r\n<p dir=\"rtl\" style=\"font-family: \'Times New Roman\'; font-size: medium; line-height: 16px;\" align=\"justify\"><span lang=\"fa\"><span style=\"color: #000066; font-family: tahoma; font-size: small;\">- مدت زمانی که برای میل کردن غذا یا نوشیدنی صرف می کند.</span></span></p>\r\n<p dir=\"rtl\" style=\"font-family: \'Times New Roman\'; font-size: medium; line-height: 16px;\" align=\"justify\"><span lang=\"fa\"><span style=\"color: #000066; font-family: tahoma; font-size: small;\">- نوع رستوران و فردی که به رستوران می رود.</span></span></p>\r\n<p dir=\"rtl\" style=\"font-family: \'Times New Roman\'; font-size: medium;\" align=\"justify\"><span lang=\"fa\"><span style=\"color: #000066; font-family: tahoma; font-size: small;\">- البته عوامل دیگری هم وجود دارند که در سطوح بعدی قرار میگیرند بعنوان نمونه محصولات و تسهیلاتی که در رستوران قابل دسترسی میباشد.</span></span></p>\r\n<p dir=\"rtl\" style=\"font-family: \'Times New Roman\'; font-size: medium; text-align: -webkit-center; line-height: 24px;\"><strong><span style=\"color: #000066; font-family: tahoma; font-size: large;\">انواع سرویس دهی</span></strong></p>\r\n<p dir=\"rtl\" style=\"font-family: \'Times New Roman\'; font-size: medium; text-align: -webkit-center; line-height: 24px;\"><img src=\"http://www.restoran.ir/images/pic2%20.gif\" width=\"110\" height=\"97\" border=\"0\" /></p>\r\n<p dir=\"rtl\" style=\"font-family: \'Times New Roman\'; font-size: medium; line-height: 24px;\" align=\"justify\"><span lang=\"fa\"><span style=\"color: #000066; font-family: tahoma; font-size: small;\">به طور کلی هر چند هزینه صرف شده برای یک وعده غذایی بالاتر باشد، مشتری انتظار در یافت خدمات بیشتری دارد. به عنوان نمونه در کافه تریاها و سلف سرویس ها، از آنجائیکه کلیه مراحل انتخاب، برداشتن غذا و سرو، آوردن آن به میز و در انتها برداشتن ظرف را خود فرد انجام می دهد، هزینه ها به مراتب پایین تر هستند. هر زمان که افزایش هزینه مد نظر باشد در درجه اول افزایش خدمت رسانی نباید فراموش شود. لازم است هر رستورانی در ابتدا درجه خدمات خود را مطابق با استانداردها تعیین کند و مناسب با آن استانداردها تعیین و مناسب با آن استانداردها و درجه ی سرویس دهی با مشتربان رفتار کند. بدین ترتیب چنانچه رستورانی ارائه دهنده غذا و&nbsp;&nbsp; نوشیدنی هایی در&nbsp; سطح رسمی است، لازم است ابعاد دیگر هم با این مورد همخوانی داشته باشند؛ لباس کارمندان، ادب و نزاکت پرسنل در مقابل میهمانان و ... بنابراین نه تنها خدمات رسانی بلکه آموزش پرسنل در رستوران بسیار حائز اهمیت است و نیز اینکه در درجه اول آنچه مورد توجه و نیاز مشتری است در نظر گرفته شود.</span></span></p>\r\n<p dir=\"rtl\" style=\"font-family: \'Times New Roman\'; font-size: medium; text-align: -webkit-center;\"><strong><span lang=\"fa\"><span style=\"color: #000066; font-family: tahoma; font-size: large;\">طراحی داخلی رستوران</span></span></strong></p>\r\n<p dir=\"rtl\" style=\"font-family: \'Times New Roman\'; font-size: medium; line-height: 24px;\" align=\"justify\"><span lang=\"fa\"><span style=\"color: #000066; font-family: tahoma; font-size: small;\">طرح داخلی رستوران اولین چیزی است که مشتری با آن ارتباط برقرار میکند و میتواند اثر مثبت و یا منفی روی فرد داشته باشد. باید بدانیم که این تاثیر بسیار مهم است. گاه ممکن است فردی بدون اینکه به دنبال رستوران خاصی باشد، هنگام عبور از مقابل آن تحت تاثیر طراحی آن قرار بگیرد و برآن شود تا در این محل غذا میل کند. طراحی داخلی یک رستوران تحت تاثیر عوامل متعدی است؛ اندازه و شکل سالن پذیرایی، مبلمان، چیدمان داخلی، ترکیب رنگ ها، نورپردازی و ... کافه تریایی که در یک کارخانه و یا مکانهای مشابه واقع شده است فضای وسیعی دارد، میزها و صندلی ها با اندازه و طرح استاندارد، رنگ آمیزی و نورپردازی ملایم و مطابق با آن مکان طراحی&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; شده اند. در رستورانهای مجلل اگر چه فضاسازی به نحوی است که مشتری تمایل دارد زمان بیشتری را در رستوران سپری کند، طراحی داخلی بسیار راحت و دلپسند است. عناصر دیگری هم از قبیل تابلوها، عکسها و ... در رستورانها هستند که توجه فرد را به خود جلب میکند. میزها در فاصله بیشتری از یکدیگر قرار دارند، نوع صندلی ها و طرح قرار گرفتن آنها به نحوی است که فرد بتواند ساعات بیشتری را در رستوران سپری کند و در عین حال احساس آرامش خود را از دست ندهد. طراحی داخلی یک رستوران آنقدر مهم است که نتوان به سادگی از کنار آن گذشت در بیشتر موارد می بایست با یک طراح حرفه ای مشورت کرد.</span></span></p>\r\n<p dir=\"rtl\" style=\"font-family: \'Times New Roman\'; font-size: medium; text-align: -webkit-center;\"><strong><span lang=\"fa\"><span style=\"color: #000066; font-family: tahoma; font-size: large;\">جو رستوران</span></span></strong></p>\r\n<p dir=\"rtl\" style=\"font-family: \'Times New Roman\'; font-size: medium; line-height: 24px;\" align=\"justify\"><span lang=\"fa\"><span style=\"color: #000066; font-family: tahoma; font-size: small;\">توضیح و تشریح جو مسایل مربوط به جو رستوران اندکی دشوار است و اغلب از آن به عنوان فاکتوری ناملموس یاد میشود که شاید جزء مواری باشد که ناخواسته بهیشترین تاثیر را روی مشتری میگذارد. همهی رستورانها جو مشابه و یکسانی ندارند، برخی از مدیران و مسئولان رستورانم تلاش میکنند عمداً جو متفاوتی را ایجاد کنند. به عنوان نمونه رستورانهای مجلل اغلب جو رسمی دارند و این حس را در نحوه پوشش کارمندان و پرسنل، دکوراسیون، خدمات رسانی، افرادی که در این رستوران رفت و آمد میکنند و ... القاء میکند. برخی از رستورانهایی که در سطح پایین تری قرار دارند سعی دارند تا جو دوستانه و راحتری را بجود بیاورند. از عوامل قابل ذکر، محل قرار گرفتن رستوران است. در محل یابی یک رستوران نه تنها بازار فعلی بلکه بازار آتی آن می بایست مورد توجه و بررسی قرار بگیرد. مثلاً رستورانهایی وجود دارند که در میان کارخانه جات و شرکت های یزرگ تجاری قرار گرفته اند، چرا که در ساعات نهار و شام و ... کلیه ی کارکنان و پرسنل برای تهیه غذای خود به این مکان مراجعه میکنند و این امر به شدت بر درآمد رستوران تاثیر می گذارد. نحوه دسترسی به رستوران یعنی اینکه داخل یا خارج شهر باشد، نزدیک به وسایل حمل و نقل شهری باشد یا خیر، از اهمیت ویژه ای برخوردار است.لازم به ذکر است در بوجود آمدن جو مناسب برای رستوران نحوه پوشش پرسنل، تعداد کارکنان، قدرت و مهارت افراد در خصوص مسئولیت هایشان، سن، ملیت و .... بی تاثیر نمی باشد.</span></span></p>\r\n<p dir=\"rtl\" style=\"font-family: \'Times New Roman\'; font-size: medium;\" align=\"justify\"><strong><span lang=\"fa\"><span style=\"color: #800080; font-family: tahoma;\">منبع:</span>&nbsp;</span><span style=\"color: #000066; font-family: verdana; font-size: small;\">food &amp; beverage services</span></strong></p>', 1, 17, NULL, 53, 0, 4, '2019-06-18 20:13:24', '2019-12-12 16:45:59'),
(3, 'خبرگزاری ایرنا', NULL, 'روتیتر تستی', 'assets/no-image.png', '<p>رای استفاده از تمامی امکانات ما کافی است در سامانه ثروت آفرینی مدرن ثبت نام کرده و سپس از مزایای سیستم استفاده نماییدf</p>', 1, 34, NULL, 16, 0, 4, '2019-07-31 20:12:10', '2020-02-25 12:37:08'),
(4, 'dfgsdfg', NULL, 'dfsdfgs', 'assets/no-image.png', '<p>sdfgsdf</p>', 1, 17, NULL, 3, 0, 4, '2019-07-31 20:12:22', '2019-10-03 18:21:03');

-- --------------------------------------------------------

--
-- Table structure for table `news_categories`
--

DROP TABLE IF EXISTS `news_categories`;
CREATE TABLE IF NOT EXISTS `news_categories` (
  `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `PersianTitle` varchar(30) COLLATE utf8mb4_persian_ci NOT NULL,
  `EnglishTitle` varchar(30) COLLATE utf8mb4_persian_ci DEFAULT NULL,
  `Logo` varchar(255) COLLATE utf8mb4_persian_ci DEFAULT NULL,
  `State` tinyint(1) DEFAULT '0',
  `parent_id` tinyint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `news_categories`
--

INSERT INTO `news_categories` (`id`, `PersianTitle`, `EnglishTitle`, `Logo`, `State`, `parent_id`) VALUES
(17, 'اخبار', 'News', '(NULL)', 0, NULL),
(26, 'خدمات شرکت', 'Services', NULL, 0, NULL),
(27, 'سیویل', NULL, NULL, 0, 26),
(28, 'پایپینگ', NULL, NULL, 0, 26),
(29, 'استراکچر', NULL, NULL, 0, 26),
(30, 'رنگ و سندبلاست', NULL, NULL, 0, 26),
(31, 'ساخت و نصب ساپورت', NULL, NULL, 0, 26),
(32, 'برق و ابزار دقیق', NULL, NULL, 0, 26),
(33, 'تعمیر و نگهداری', NULL, NULL, 0, 26),
(34, 'تصاویر پروژه ها', NULL, NULL, 0, NULL),
(35, 'سیویل', 'ball bearings', NULL, 0, 17);

-- --------------------------------------------------------

--
-- Table structure for table `news_comments`
--

DROP TABLE IF EXISTS `news_comments`;
CREATE TABLE IF NOT EXISTS `news_comments` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `news_id` int(10) UNSIGNED NOT NULL,
  `Name` varchar(70) COLLATE utf8mb4_persian_ci NOT NULL,
  `Email` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL,
  `Website` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL,
  `IP` varchar(30) COLLATE utf8mb4_persian_ci NOT NULL,
  `Message` text COLLATE utf8mb4_persian_ci NOT NULL,
  `State` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `Seen` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_news_comments_news` (`news_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `news_keywords`
--

DROP TABLE IF EXISTS `news_keywords`;
CREATE TABLE IF NOT EXISTS `news_keywords` (
  `keyword_id` bigint(20) UNSIGNED NOT NULL,
  `news_id` int(10) UNSIGNED NOT NULL,
  KEY `FK_news_keywords_news` (`news_id`),
  KEY `FK_news_keywords_keywords` (`keyword_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `news_keywords`
--

INSERT INTO `news_keywords` (`keyword_id`, `news_id`) VALUES
(1, 2),
(2334, 4),
(1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `news_states`
--

DROP TABLE IF EXISTS `news_states`;
CREATE TABLE IF NOT EXISTS `news_states` (
  `id` tinyint(1) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Title` varchar(20) COLLATE utf8mb4_persian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `news_states`
--

INSERT INTO `news_states` (`id`, `Title`) VALUES
(1, 'در دست بررسی'),
(2, 'عدم تایید'),
(3, 'غیرفعال'),
(4, 'فعال');

-- --------------------------------------------------------

--
-- Table structure for table `notifications_contact_us`
--

DROP TABLE IF EXISTS `notifications_contact_us`;
CREATE TABLE IF NOT EXISTS `notifications_contact_us` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `FullName` varchar(100) COLLATE utf8mb4_persian_ci NOT NULL,
  `Email` varchar(50) COLLATE utf8mb4_persian_ci DEFAULT NULL,
  `Phone` varchar(15) COLLATE utf8mb4_persian_ci DEFAULT NULL,
  `Subject` varchar(40) COLLATE utf8mb4_persian_ci DEFAULT NULL,
  `Message` varchar(255) COLLATE utf8mb4_persian_ci NOT NULL,
  `IP` varchar(20) COLLATE utf8mb4_persian_ci DEFAULT NULL,
  `State` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `notifications_contact_us`
--

INSERT INTO `notifications_contact_us` (`id`, `FullName`, `Email`, `Phone`, `Subject`, `Message`, `IP`, `State`, `created_at`, `updated_at`) VALUES
(6, 'mohamamd', 'm.s@gmail.com', NULL, NULL, 'asfdsd', '127.0.0.1', 0, '2021-12-31 21:31:53', '2022-01-01 23:17:00');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

DROP TABLE IF EXISTS `packages`;
CREATE TABLE IF NOT EXISTS `packages` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) COLLATE utf8mb4_persian_ci NOT NULL,
  `MinPrice` decimal(20,2) NOT NULL DEFAULT '0.00',
  `MaxPrice` decimal(20,2) NOT NULL DEFAULT '0.00',
  `DailyProfitPercent` decimal(20,3) NOT NULL DEFAULT '0.000',
  `extra` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `Name`, `MinPrice`, `MaxPrice`, `DailyProfitPercent`, `extra`, `created_at`, `updated_at`) VALUES
(1, 'Package A', '50.00', '499.00', '0.400', 0, '2021-12-29 00:56:40', '2021-12-29 03:10:43'),
(2, 'Package B', '500.00', '1999.00', '0.430', 0, '2021-12-29 00:57:08', '2021-12-29 03:10:47'),
(3, 'Package C', '2000.00', '9999.00', '0.470', 0, '2021-12-29 00:57:25', '2021-12-29 03:10:50'),
(4, 'Package D', '10000.00', '1000000.00', '0.530', 0, '2021-12-29 00:57:55', '2021-12-29 03:10:56'),
(5, 'Extra Package', '20.00', '49.00', '0.400', 1, '2021-12-29 00:59:16', '2021-12-29 03:10:59');

-- --------------------------------------------------------

--
-- Table structure for table `package_users`
--

DROP TABLE IF EXISTS `package_users`;
CREATE TABLE IF NOT EXISTS `package_users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `package_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `Price` decimal(20,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `expire_date` datetime NOT NULL,
  `DailyProfitPercent` decimal(20,3) NOT NULL DEFAULT '0.000',
  `Disable` int(5) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_package_users_packages` (`package_id`),
  KEY `FK_package_users_users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `package_users`
--

INSERT INTO `package_users` (`id`, `package_id`, `user_id`, `Price`, `expire_date`, `DailyProfitPercent`, `Disable`, `created_at`, `updated_at`) VALUES
(4, 1, 1, '84.00', '2022-05-01 00:00:00', '0.400', 0, '2021-12-29 03:11:36', '2022-01-01 23:02:43'),
(5, 1, 15, '50.00', '2022-09-29 04:24:28', '0.400', 1, '2021-09-29 04:24:28', '2022-01-01 22:25:40'),
(6, 5, 15, '20.00', '2022-12-29 23:53:33', '0.400', 0, '2021-12-29 23:53:33', '2021-12-29 23:53:33'),
(7, 1, 16, '95.00', '2022-12-30 22:14:45', '0.400', 0, '2021-12-30 22:14:45', '2021-12-30 22:14:45');

-- --------------------------------------------------------

--
-- Table structure for table `permission_descriptions`
--

DROP TABLE IF EXISTS `permission_descriptions`;
CREATE TABLE IF NOT EXISTS `permission_descriptions` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `permission_name` varchar(100) COLLATE utf32_persian_ci NOT NULL,
  `title` varchar(100) COLLATE utf32_persian_ci NOT NULL,
  `role_type_id` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_permission_descriptions_permission_types` (`role_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf32 COLLATE=utf32_persian_ci;

--
-- Dumping data for table `permission_descriptions`
--

INSERT INTO `permission_descriptions` (`id`, `permission_name`, `title`, `role_type_id`) VALUES
(1, 'sub.system.users', 'دسترسی به زیر سیستم پرسنل و کاربری ها', 1),
(2, 'sub.system.settings', 'دسترسی به زیر سیستم تنظیمات', 1),
(3, 'sub.system.profile', 'دسترسی به زیر سیستم پروفایل', 1),
(4, 'sub.system.settings.base', 'دسترسی تنظیمات پایه سایت', 2),
(5, 'sub.system.notifications', 'دسترسی به زیر سیستم اعلانات', 1),
(6, 'sub.system.news', 'دسترسی به زیر سیستم خبری', 1),
(7, 'sub.system.news.add.news', 'دسترسی به ارسال خبر', 3),
(8, 'sub.system.news.lists', 'دسترسی به لیست اخبار', 3),
(9, 'sub.system.news.my.news', 'دسترسی به اخبار ارسالی من', 3),
(10, 'sub.system.news.add.keyword', 'دسترسی به کلمات کلیدی', 3),
(11, 'sub.system.news.show.user.views', 'نمایش تعداد بازدید خبر', 3),
(12, 'sub.system.news.allow.remove.news', 'دسترسی به حذف خبر', 3),
(13, 'sub.system.news.allow.update', 'دسترسی به ویرایش خبر', 3),
(14, 'sub.system.news.allow.on.slider', 'دسترسی به تبدیل خبر به اسلایدر', 3),
(15, 'sub.system.news.access.to.edit.other.user.news', 'دسترسی به ویرایش اخبار غیرکاربر', 3),
(16, 'sub.system.news.access.to.edit.enabled.news', 'دسترسی به ویرایش اخبار فعال', 3),
(17, 'sub.system.news.allow.change.news.state', 'اجازه به انجام تغییر وضعیت خبر', 3),
(18, 'sub.system.news.enable.update.news.button', 'فعال کردن دکمه ویرایش خبر', 3),
(19, 'sub.system.news.allow.update.my.news', 'ویرایش و نمایش خبرهای ارسالی کاربر', 3),
(20, 'sub.system.file.manager', 'دسترسی به مدیریت فایل', 1),
(21, 'sub.system.comments', 'دسترسی به زیر سیستم نظرات کاربران', 1),
(22, 'sub.system.comments.remove.comment', 'دسترسی به حذف نظرات کاربران', 4),
(23, 'sub.system.comments.allow.change.state', 'دسترسی به تغییر وضعیت نظرات کاربران', 4),
(24, 'sub.system.comments.show.news.comments', 'دسترسی به نمایش نظر در زیر سیستم نظرات', 4),
(25, 'sub.system.message', 'دسترسی به زیر سیستم پیام ها', 1),
(26, 'sub.system.message.show.trash.icon', 'دسترسی به پیام های حذف شده', 5),
(27, 'sub.system.message.send.message.just.to.administrators', 'ارسال پیام فقط به مدیرها', 5),
(30, 'sub.system.comments.allow.update.news.comment', 'دسترسی به ویرایش نظرات', 4),
(32, 'sub.system.exclusive.pages', 'دسترسی به زیر سیستم صفحات اختصاصی', 1),
(33, 'sub.system.deposit.admin', 'Permission to Deposit Lists', 1),
(34, 'sub.system.package.admin', 'Permission to Total user packets Lists', 1),
(35, 'sub.system.support.admin', 'allow access to support system', 1);

-- --------------------------------------------------------

--
-- Table structure for table `permission_types`
--

DROP TABLE IF EXISTS `permission_types`;
CREATE TABLE IF NOT EXISTS `permission_types` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `Title` varchar(50) COLLATE utf32_persian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf32 COLLATE=utf32_persian_ci;

--
-- Dumping data for table `permission_types`
--

INSERT INTO `permission_types` (`id`, `Title`) VALUES
(1, 'زیر سیستم ها'),
(2, 'زیر سیستم تنظیمات'),
(3, 'زیر سیستم اخبار'),
(4, 'زیر سیستم نظرات'),
(5, 'زیر سیستم پیام');

-- --------------------------------------------------------

--
-- Table structure for table `persistences`
--

DROP TABLE IF EXISTS `persistences`;
CREATE TABLE IF NOT EXISTS `persistences` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `persistences_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `persistences`
--

INSERT INTO `persistences` (`id`, `user_id`, `code`, `created_at`, `updated_at`) VALUES
(1, 1, 'MMak0M5THnO7WdWPVRGotmuycSqK1vpA', '2018-09-24 08:54:42', '2018-09-24 08:54:42'),
(2, 1, 'KdsJ5Kk5KGHtEyRWZ3a5uRQteQpMYoPb', '2018-09-24 08:56:58', '2018-09-24 08:56:58'),
(5, 1, 'FPcz7V2xa4TtNG4ZpCvODB73CXrrK7Ow', '2018-09-24 10:07:24', '2018-09-24 10:07:24'),
(7, 1, 't4DRrfuTwXv9I7SlgOX1wtgXaJuKa2If', '2018-09-24 10:08:34', '2018-09-24 10:08:34'),
(8, 1, 'BYjE7kaZsN7kUemrLmtQlBWjeGrHCoCd', '2018-09-25 05:16:01', '2018-09-25 05:16:01'),
(9, 1, 'kleY7fNkTaVJBLneEI98J3LqpgioKERC', '2018-09-25 07:55:59', '2018-09-25 07:55:59'),
(11, 1, 'Vc6cHvsNhSxgydaSlq1QR3TsNCMTLTfp', '2019-01-18 07:46:49', '2019-01-18 07:46:49'),
(12, 1, 'gNK9R3phppiVh9Kh8ftFPJOigpy7NwZJ', '2019-01-18 07:47:40', '2019-01-18 07:47:40'),
(13, 1, 'peKi5rgJ3KidmwynhzBQDdNCtfjLh0J0', '2019-01-18 08:02:28', '2019-01-18 08:02:28'),
(16, 1, '2cwV6P2I8qceyY7gySy6rp5c8f1vgZu1', '2019-01-18 08:51:26', '2019-01-18 08:51:26'),
(17, 1, '1hbwD7OvultaodzGiGyDJCVW71pWWrgu', '2019-01-18 16:19:34', '2019-01-18 16:19:34'),
(18, 1, 'qKxY7lJqFYB6m5lJeEUP6iZytSk6OIn2', '2019-01-19 04:56:53', '2019-01-19 04:56:53'),
(19, 1, 'FJnsOjH7C6GaH6duBXwkbEtJZ4teEwYe', '2019-01-19 18:02:38', '2019-01-19 18:02:38'),
(20, 1, '0rVbdT2yobzCRWPmp5wrcKhIsKgPQaHR', '2019-01-20 06:07:28', '2019-01-20 06:07:28'),
(23, 1, 'MoEvfh0KmAcf1q53cLXba0mjozyiAvxs', '2019-01-21 09:40:00', '2019-01-21 09:40:00'),
(24, 1, 'kWouGkTD5C3bkzifgRdWeftNbLGYMFBa', '2019-01-22 14:23:21', '2019-01-22 14:23:21'),
(25, 1, '5pjE4Q3Q6MzUtkH7r0LDFspPXKfaVs1K', '2019-01-22 19:43:00', '2019-01-22 19:43:00'),
(26, 1, '4zocf0KKYfBupPGJtUvRILS07UcT0IOV', '2019-01-24 15:31:44', '2019-01-24 15:31:44'),
(27, 3, 'MxuKayz4Ut8HsvPkx2Oy3O5HAG4Xtv81', '2019-01-24 15:33:55', '2019-01-24 15:33:55'),
(28, 1, 'q8cOhXFrqBq3G2DSaRV0MTDdb9MjfAos', '2019-01-25 07:20:54', '2019-01-25 07:20:54'),
(29, 8, 'gIyCzdNtc2cG8qSdViMYEeN8k8n3nSys', '2019-01-25 07:26:15', '2019-01-25 07:26:15'),
(30, 1, 'gTJ3yoDfSLqmwE8Tn7AFjNhz9W7AmG0P', '2019-01-25 07:38:51', '2019-01-25 07:38:51'),
(33, 1, 'voArDFXJxAH9ocV5rm2Qfg3LFaQZCCRq', '2019-01-25 10:56:48', '2019-01-25 10:56:48'),
(34, 1, 'ARbwXbjId201XK27GZLXD1oNF7fLilYh', '2019-01-25 14:21:37', '2019-01-25 14:21:37'),
(35, 1, 'URItmqTtXfCvc34yqOYNdzKizoN9s1QW', '2019-01-26 17:59:24', '2019-01-26 17:59:24'),
(36, 1, 'RWHGtX87hxPB9uiq8Fxyl1WytUsMX0xO', '2019-01-29 17:20:19', '2019-01-29 17:20:19'),
(37, 1, 'mNSsewGp9kAlx9IGEfj7VylhYRMfO93h', '2019-01-31 06:15:11', '2019-01-31 06:15:11'),
(38, 1, 'qQZMQYNPUi4bwRaZZAluZgNvfT8hrqMS', '2019-01-31 13:36:40', '2019-01-31 13:36:40'),
(39, 1, '27G09nVnLT4o0YFpMUtafvLML6n3VJXZ', '2019-06-12 19:45:42', '2019-06-12 19:45:42'),
(40, 1, 'AgHvCuTFP6PuxVgaAopv1eOTTq3NpxiD', '2019-06-15 15:25:52', '2019-06-15 15:25:52'),
(42, 1, '5c03T8sz2BS0sLg6TuF24GahvxERUZ1O', '2019-06-16 17:11:09', '2019-06-16 17:11:09'),
(43, 1, 'XpBFKh72m5AyZzbRbBxzjiJG4tC7vGo1', '2019-06-18 23:33:46', '2019-06-18 23:33:46'),
(45, 1, 'xd7ySZkTrYvw0kq7ZyAq2AcJsPdwjuzn', '2019-06-19 00:01:26', '2019-06-19 00:01:26'),
(46, 1, 'tcFMk2pKSBDz1qfZU2f8NTGVcCcDCoX6', '2019-07-23 22:23:49', '2019-07-23 22:23:49'),
(47, 1, 'CTO3YYysXHCZMOmpY5xgqgAAJp9EYj04', '2019-07-23 18:11:09', '2019-07-23 18:11:09'),
(48, 1, 'FWJxK7QAw9OSu4TPqztinWsCMrLpEv9G', '2019-07-30 18:33:52', '2019-07-30 18:33:52'),
(49, 1, 'ZVMhON6OenQpUNSaISSPxfqIogS3M4SP', '2019-07-30 18:34:13', '2019-07-30 18:34:13'),
(50, 1, 'Lej3y9pdYq3M1gDLqkbxEIPlSiQYTDMS', '2019-07-31 15:37:19', '2019-07-31 15:37:19'),
(51, 1, 'gvNpLm3fUfCrwEn6bSyC1BEWifNj5xN5', '2019-10-03 15:38:25', '2019-10-03 15:38:25'),
(56, 2, 'OnrElMIzf5phXrKQOOJXw12SonsC70NB', '2019-12-12 19:09:55', '2019-12-12 19:09:55'),
(57, 1, 'xqH1R0gaJuSkGzlBgBEswwV7Cen2uDEy', '2019-12-13 07:26:15', '2019-12-13 07:26:15'),
(58, 1, 'lK0eLlBeVnTFhhoZea1mbCjvyQJLRxJW', '2020-02-24 21:01:19', '2020-02-24 21:01:19'),
(59, 1, 'G4OyKaVHeNUa1HUEtF7QI0CrNjJ18fu6', '2020-02-25 09:08:18', '2020-02-25 09:08:18'),
(60, 1, 'O9CUwr4el4bCkvjGVTqkVwa5nIVCXOyW', '2021-12-23 17:26:43', '2021-12-23 17:26:43'),
(63, 1, 'sB4mzIkHM14FJGBE7zzx5Sg83awK02dW', '2021-12-23 17:28:08', '2021-12-23 17:28:08'),
(64, 3, 'slTaix7VCddpFqn1P8XoZBVu0okdvl1Y', '2021-12-25 20:14:05', '2021-12-25 20:14:05'),
(65, 15, 'NtDp2xUyiprzslK3mu7htZ52jRpYyYyY', '2021-12-26 19:47:04', '2021-12-26 19:47:04'),
(67, 1, 'WM0VmAiWsaKJsnjWbVBRyKbIegk5ERWt', '2021-12-28 16:08:23', '2021-12-28 16:08:23'),
(68, 1, 'E523DDPtOnB6j9SJhKcGjb31qrRGJMay', '2021-12-28 18:15:24', '2021-12-28 18:15:24'),
(69, 1, 'vcWzrZUAN6mBTFQVXaNSR5MTKQ0ycVyg', '2021-12-28 23:29:24', '2021-12-28 23:29:24'),
(70, 15, 'FmXwXo2DrNhgaYps2BH0TU5pAUXH0rJq', '2021-12-29 00:47:28', '2021-12-29 00:47:28'),
(72, 16, 'BzLqUm7DvXLakggsIJfy53i6Cc9hl6Iz', '2021-12-30 18:43:37', '2021-12-30 18:43:37'),
(73, 1, 'YnnB4dcViCxZr1Puut4Hbs8744UgrIFT', '2021-12-31 07:14:23', '2021-12-31 07:14:23'),
(74, 1, 'Ls6F4v2JMGanBYUvOWyNu114aBhlTmps', '2021-12-31 08:54:10', '2021-12-31 08:54:10'),
(75, 1, 'yUDMaUchYhd7GWk6SPwgyjeCodNUVdre', '2021-12-31 08:54:34', '2021-12-31 08:54:34'),
(76, 15, 'hMnZVLTsMVkg5vUJJKIlxm4OKtSJ8BZi', '2021-12-31 11:54:16', '2021-12-31 11:54:16'),
(77, 1, 'Hc85QCIhlaSvJGbTpQlHbpqIPZkKJVcQ', '2022-01-01 13:58:20', '2022-01-01 13:58:20'),
(80, 15, '68l1LncFWv0pjsTvfChAviPtT9hKYEpy', '2022-01-01 18:02:00', '2022-01-01 18:02:00');

-- --------------------------------------------------------

--
-- Table structure for table `reminders`
--

DROP TABLE IF EXISTS `reminders`;
CREATE TABLE IF NOT EXISTS `reminders` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `slug`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'مدیر سیستم', 'Administrator', '{\"sub.system.users\":true,\"sub.system.settings\":true,\"sub.system.profile\":true,\"sub.system.settings.base\":true,\"sub.system.notifications\":true,\"sub.system.news\":true,\"sub.system.news.add.news\":true,\"sub.system.news.lists\":true,\"sub.system.news.my.news\":true,\"sub.system.news.add.keyword\":true,\"sub.system.news.show.user.views\":true,\"sub.system.news.allow.remove.news\":true,\"sub.system.news.allow.update\":true,\"sub.system.news.allow.on.slider\":true,\"sub.system.news.access.to.edit.other.user.news\":true,\"sub.system.news.access.to.edit.enabled.news\":true,\"sub.system.news.allow.change.news.state\":true,\"sub.system.news.enable.update.news.button\":true,\"sub.system.news.allow.update.my.news\":true,\"sub.system.file.manager\":true,\"sub.system.comments\":true,\"sub.system.comments.remove.comment\":true,\"sub.system.comments.allow.change.state\":true,\"sub.system.comments.show.news.comments\":true,\"sub.system.message\":true,\"sub.system.message.show.trash.icon\":true,\"sub.system.message.send.message.just.to.administrators\":false,\"sub.system.comments.allow.update.news.comment\":true,\"sub.system.exclusive.pages\":true,\"sub.system.deposit.admin\":true,\"sub.system.package.admin\":true,\"sub.system.support.admin\":true}', '2018-05-05 04:48:01', '2021-12-31 07:51:51'),
(2, 'مدیر سایت', 'مدیر سایت', '{\"sub.system.users\":true,\"sub.system.settings\":true,\"sub.system.profile\":true,\"sub.system.settings.base\":true,\"sub.system.notifications\":true,\"sub.system.news\":true,\"sub.system.news.add.news\":true,\"sub.system.news.lists\":true,\"sub.system.news.my.news\":true,\"sub.system.news.add.keyword\":true,\"sub.system.news.show.user.views\":true,\"sub.system.news.allow.remove.news\":true,\"sub.system.news.allow.update\":true,\"sub.system.news.allow.on.slider\":true,\"sub.system.news.access.to.edit.other.user.news\":true,\"sub.system.news.access.to.edit.enabled.news\":true,\"sub.system.news.allow.change.news.state\":true,\"sub.system.news.enable.update.news.button\":true,\"sub.system.news.allow.update.my.news\":true,\"sub.system.file.manager\":true,\"sub.system.comments\":true,\"sub.system.comments.remove.comment\":true,\"sub.system.comments.allow.change.state\":true,\"sub.system.comments.show.news.comments\":true,\"sub.system.message\":true,\"sub.system.message.show.trash.icon\":true,\"sub.system.message.send.message.just.to.administrators\":false,\"sub.system.comments.allow.update.news.comment\":true,\"sub.system.exclusive.pages\":true,\"sub.system.deposit.admin\":false,\"sub.system.package.admin\":false,\"sub.system.support.admin\":false}', '2019-01-18 08:23:35', '2021-12-31 07:51:51'),
(3, 'کاربر عادی', 'Unverified User', '{\"sub.system.users\":false,\"sub.system.settings\":false,\"sub.system.profile\":true,\"sub.system.settings.base\":true,\"sub.system.notifications\":false,\"sub.system.news\":false,\"sub.system.news.add.news\":true,\"sub.system.news.lists\":true,\"sub.system.news.my.news\":true,\"sub.system.news.add.keyword\":true,\"sub.system.news.show.user.views\":true,\"sub.system.news.allow.remove.news\":true,\"sub.system.news.allow.update\":true,\"sub.system.news.allow.on.slider\":true,\"sub.system.news.access.to.edit.other.user.news\":true,\"sub.system.news.access.to.edit.enabled.news\":true,\"sub.system.news.allow.change.news.state\":true,\"sub.system.news.enable.update.news.button\":true,\"sub.system.news.allow.update.my.news\":true,\"sub.system.file.manager\":false,\"sub.system.comments\":false,\"sub.system.comments.remove.comment\":true,\"sub.system.comments.allow.change.state\":true,\"sub.system.comments.show.news.comments\":true,\"sub.system.message\":false,\"sub.system.message.show.trash.icon\":true,\"sub.system.message.send.message.just.to.administrators\":false,\"sub.system.comments.allow.update.news.comment\":true,\"sub.system.exclusive.pages\":false,\"sub.system.deposit.admin\":false,\"sub.system.package.admin\":false,\"sub.system.support.admin\":false}', '2019-12-12 16:56:03', '2021-12-31 07:51:51');

-- --------------------------------------------------------

--
-- Table structure for table `role_administrators`
--

DROP TABLE IF EXISTS `role_administrators`;
CREATE TABLE IF NOT EXISTS `role_administrators` (
  `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_role_administrators_roles` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `role_administrators`
--

INSERT INTO `role_administrators` (`id`, `role_id`) VALUES
(1, 1),
(3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `role_users`
--

DROP TABLE IF EXISTS `role_users`;
CREATE TABLE IF NOT EXISTS `role_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FK_role_users_roles` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_users`
--

INSERT INTO `role_users` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, '2019-01-24 15:36:49'),
(15, 3, '2021-12-26 19:39:44', '2021-12-26 19:39:44'),
(16, 3, '2021-12-26 19:40:31', '2021-12-26 19:40:31');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` tinyint(1) UNSIGNED NOT NULL,
  `Title` varchar(80) COLLATE utf8mb4_persian_ci NOT NULL,
  `Description` text COLLATE utf8mb4_persian_ci NOT NULL,
  `Keywords` text COLLATE utf8mb4_persian_ci NOT NULL,
  `Phone` text COLLATE utf8mb4_persian_ci,
  `Email` text COLLATE utf8mb4_persian_ci,
  `Address` text COLLATE utf8mb4_persian_ci,
  `AboutUs` text COLLATE utf8mb4_persian_ci,
  `FavIcon` varchar(255) COLLATE utf8mb4_persian_ci DEFAULT NULL,
  `MainLogo` varchar(255) COLLATE utf8mb4_persian_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `Title`, `Description`, `Keywords`, `Phone`, `Email`, `Address`, `AboutUs`, `FavIcon`, `MainLogo`) VALUES
(1, 'ثروت آفرینی مدرن', 'ثروت آفرینی مدرن', 'ثروت آفرینی مدرن', '<p>09180182180</p>', 'info@test.com', '<p>بندرعباس ...</p>', '<p>شرکت محرک سازه دوار(Mohsco) با تکیه بر علم؛ دانش دانشگاهیان و تجربه متخصصین و نخبگان صنعت نفت و گاز و پتروشیمی کشور اقدام به ساخت تجهیزات و قطعات صنایع نفت و گاز و پتروشیمی نموده که در سال رونق اقتصادی و راستای تداوم صنایع و صرفه جویی ارزی جهت ساخت تجهیزات و قطعات مطابق با استاندارهای بین المللی APIاعلام آمادگی نموده است.</p>', '/Files/Settings/201901251548427460018.png', '/Files/Settings/201901251548427460025.png');

-- --------------------------------------------------------

--
-- Table structure for table `settings_professional`
--

DROP TABLE IF EXISTS `settings_professional`;
CREATE TABLE IF NOT EXISTS `settings_professional` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(100) COLLATE utf8mb4_persian_ci NOT NULL,
  `Description` varchar(100) COLLATE utf8mb4_persian_ci NOT NULL,
  `State` tinyint(1) NOT NULL DEFAULT '0',
  `setting_professional_type_id` tinyint(5) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_settings_professional_setting_professional_types` (`setting_professional_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `settings_professional`
--

INSERT INTO `settings_professional` (`id`, `Title`, `Description`, `State`, `setting_professional_type_id`) VALUES
(4, 'show_date_in_human_function', 'نمایش تاریخ ها بصورت کارکرد انسانی', 1, 1),
(5, 'show_url_post_as_text', 'نمایش آدرس URL بصورت متنی ', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_sliders`
--

DROP TABLE IF EXISTS `settings_sliders`;
CREATE TABLE IF NOT EXISTS `settings_sliders` (
  `id` tinyint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Title` varchar(50) COLLATE utf8mb4_persian_ci NOT NULL,
  `Image` text COLLATE utf8mb4_persian_ci NOT NULL,
  `Order` tinyint(5) NOT NULL,
  `State` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `settings_sliders`
--

INSERT INTO `settings_sliders` (`id`, `Title`, `Image`, `Order`, `State`) VALUES
(13, 'همدان 118', 'otherFiles/Settings/Sliders/201901191547913415025.png', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `setting_professional_types`
--

DROP TABLE IF EXISTS `setting_professional_types`;
CREATE TABLE IF NOT EXISTS `setting_professional_types` (
  `id` tinyint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Title` varchar(30) COLLATE utf8mb4_persian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `setting_professional_types`
--

INSERT INTO `setting_professional_types` (`id`, `Title`) VALUES
(1, 'تنظیمات عمومی');

-- --------------------------------------------------------

--
-- Table structure for table `social_media`
--

DROP TABLE IF EXISTS `social_media`;
CREATE TABLE IF NOT EXISTS `social_media` (
  `id` tinyint(1) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Instagram` varchar(150) COLLATE utf8mb4_persian_ci NOT NULL,
  `Telegram` varchar(150) COLLATE utf8mb4_persian_ci NOT NULL,
  `Facebook` varchar(150) COLLATE utf8mb4_persian_ci NOT NULL,
  `Twitter` varchar(150) COLLATE utf8mb4_persian_ci NOT NULL,
  `Soroush` varchar(150) COLLATE utf8mb4_persian_ci NOT NULL,
  `iGap` varchar(150) COLLATE utf8mb4_persian_ci NOT NULL,
  `Bale` varchar(150) COLLATE utf8mb4_persian_ci NOT NULL,
  `Youtube` varchar(150) COLLATE utf8mb4_persian_ci NOT NULL,
  `Aparat` varchar(150) COLLATE utf8mb4_persian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `social_media`
--

INSERT INTO `social_media` (`id`, `Instagram`, `Telegram`, `Facebook`, `Twitter`, `Soroush`, `iGap`, `Bale`, `Youtube`, `Aparat`) VALUES
(1, 'https://www.instagram.com/', 'https://t.me/#', '#', '#', '#', '#', '#', '#', '#');

-- --------------------------------------------------------

--
-- Table structure for table `support_details`
--

DROP TABLE IF EXISTS `support_details`;
CREATE TABLE IF NOT EXISTS `support_details` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `support_header_id` int(10) UNSIGNED NOT NULL,
  `itself` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` int(10) UNSIGNED NOT NULL,
  `Comments` text COLLATE utf8mb4_persian_ci NOT NULL,
  `attachments` text COLLATE utf8mb4_persian_ci,
  `Seen` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_support_details_support_headers` (`support_header_id`),
  KEY `FK_support_details_users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `support_details`
--

INSERT INTO `support_details` (`id`, `support_header_id`, `itself`, `user_id`, `Comments`, `attachments`, `Seen`, `created_at`, `updated_at`) VALUES
(5, 1, 1, 1, 'sftgjfghjdghsfgghsfghj', 'assets/attachment/202112311640947456018.png', 1, '2021-12-31 14:14:16', '2022-01-01 23:12:35'),
(6, 1, 1, 1, 'ABC', NULL, 1, '2021-12-31 15:14:32', '2022-01-01 23:12:35'),
(7, 2, 1, 15, 'its new problem by kerim', NULL, 1, '2021-12-31 15:24:40', '2021-12-31 15:25:47'),
(8, 2, 0, 1, 'it\'s noting OK!', NULL, 1, '2021-12-31 15:25:28', '2021-12-31 15:26:00'),
(9, 2, 1, 15, 'Ok baby', NULL, 1, '2021-12-31 15:25:47', '2021-12-31 15:25:47'),
(10, 2, 0, 1, '!@', NULL, 1, '2021-12-31 15:25:59', '2021-12-31 15:26:00');

-- --------------------------------------------------------

--
-- Table structure for table `support_headers`
--

DROP TABLE IF EXISTS `support_headers`;
CREATE TABLE IF NOT EXISTS `support_headers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `Title` varchar(250) COLLATE utf8mb4_persian_ci NOT NULL,
  `Language` varchar(10) COLLATE utf8mb4_persian_ci NOT NULL,
  `Status` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_support_headers_users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `support_headers`
--

INSERT INTO `support_headers` (`id`, `user_id`, `Title`, `Language`, `Status`, `created_at`, `updated_at`) VALUES
(1, 1, 'نظرسنجی سایت', 'fa', 0, '2021-12-31 14:14:16', '2021-12-31 14:14:16'),
(2, 15, 'new problem by kerim', 'en', 1, '2021-12-31 15:24:40', '2021-12-31 15:25:59');

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

DROP TABLE IF EXISTS `throttle`;
CREATE TABLE IF NOT EXISTS `throttle` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `permissions`, `last_login`, `first_name`, `last_name`, `created_at`, `updated_at`) VALUES
(1, 'soheili@manager.com', '$2y$10$wS2XlxUnOZc8pAdZRglhS.yF2FvJthXf5mmCo/k.MeRUW5yUNPStG', NULL, '2022-01-01 13:58:20', 'Admin', 'administratotr', '2021-12-18 19:40:31', '2021-12-26 19:40:31'),
(15, 'karim@yahoo.com', '$2y$10$s3ifYOF7vhPeLWt468NgRuvdnDDSbOAx4BuiX8tweCETtwtHa287K', NULL, '2022-01-01 18:02:00', 'karim', 'xyz', '2021-12-26 19:39:44', '2022-01-01 18:02:00'),
(16, 'reza@hass.ir', '$2y$10$zQdZxQ00z4HPLpJpYQZQueYp8Z7Ctt5s5IuGPzoUmi.bB.iFWjCA.', NULL, '2021-12-30 18:43:37', 'kam', 'mdas', '2021-12-26 19:40:31', '2021-12-30 18:43:37');

-- --------------------------------------------------------

--
-- Table structure for table `user_levels`
--

DROP TABLE IF EXISTS `user_levels`;
CREATE TABLE IF NOT EXISTS `user_levels` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_user_id` int(10) UNSIGNED NOT NULL,
  `child_user_id` int(10) UNSIGNED NOT NULL,
  `LevelID` tinyint(3) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `parent_user_id_child_user_id_LevelID` (`parent_user_id`,`child_user_id`,`LevelID`) USING BTREE,
  KEY `FK_user_levels_users` (`parent_user_id`) USING BTREE,
  KEY `FK_user_levels_users_2` (`child_user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=779 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `user_levels`
--

INSERT INTO `user_levels` (`id`, `parent_user_id`, `child_user_id`, `LevelID`, `created_at`, `updated_at`) VALUES
(776, 1, 15, 1, '2021-12-26 19:39:44', '2021-12-26 19:39:44'),
(777, 15, 16, 1, '2021-12-26 19:40:31', '2021-12-26 19:40:31'),
(778, 1, 16, 2, '2021-12-26 19:40:31', '2021-12-26 19:40:31');

-- --------------------------------------------------------

--
-- Table structure for table `user_package_termination`
--

DROP TABLE IF EXISTS `user_package_termination`;
CREATE TABLE IF NOT EXISTS `user_package_termination` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `package_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `Seen` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `package_user_id` (`package_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `user_package_termination`
--

INSERT INTO `user_package_termination` (`id`, `package_user_id`, `Seen`, `created_at`, `updated_at`) VALUES
(14, 5, 0, '2021-12-29 23:16:32', '2021-12-29 23:16:32'),
(18, 6, 0, '2021-12-29 23:54:08', '2021-12-29 23:54:08');

-- --------------------------------------------------------

--
-- Table structure for table `wallets`
--

DROP TABLE IF EXISTS `wallets`;
CREATE TABLE IF NOT EXISTS `wallets` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `Price` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `Comments` varchar(250) COLLATE utf8mb4_persian_ci DEFAULT NULL,
  `State` tinyint(5) NOT NULL DEFAULT '0',
  `ReferenceID` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_wallet_users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `wallets`
--

INSERT INTO `wallets` (`id`, `user_id`, `Price`, `Comments`, `State`, `ReferenceID`, `created_at`, `updated_at`) VALUES
(1, 15, '5.0000', NULL, 1, 3, '2021-12-28 20:18:47', '2021-12-28 20:18:47'),
(2, 1, '150.0000', NULL, 1, 6, '2021-12-28 20:28:23', '2021-12-28 20:28:23'),
(6, 15, '15.0000', NULL, 2, 0, '2021-12-28 22:13:55', '2021-12-28 22:13:55'),
(7, 1, '-15.0000', NULL, 3, 0, '2021-12-28 22:13:55', '2021-12-28 22:13:55'),
(10, 15, '50.0000', NULL, 2, 2, '2021-12-28 22:16:32', '2021-12-28 22:16:32'),
(11, 1, '-50.0000', NULL, 3, 2, '2021-12-28 22:16:32', '2021-12-28 22:16:32'),
(12, 15, '-5.0000', NULL, 4, 3, '2021-12-29 00:20:52', '2021-12-29 00:20:52'),
(16, 1, '-84.0000', 'Buy Package A', 5, NULL, '2021-12-29 03:11:36', '2021-12-29 03:11:36'),
(17, 15, '-50.0000', 'Buy Package A', 5, NULL, '2021-12-29 04:24:28', '2021-12-29 04:24:28'),
(18, 1, '456.0000', NULL, 1, 12, '2021-12-29 23:52:26', '2021-12-29 23:52:26'),
(19, 15, '157.0000', NULL, 2, 3, '2021-12-29 23:53:09', '2021-12-29 23:53:09'),
(20, 1, '-157.0000', NULL, 3, 3, '2021-12-29 23:53:09', '2021-12-29 23:53:09'),
(21, 15, '-20.0000', 'Buy Extra Package', 5, NULL, '2021-12-29 23:53:33', '2021-12-29 23:53:33'),
(23, 15, '4.0000', 'Termination users package', 6, 5, '2021-12-30 00:10:20', '2021-12-30 00:10:20'),
(24, 16, '150.0000', NULL, 1, 14, '2021-12-30 22:14:25', '2021-12-30 22:14:25'),
(25, 15, '4.7500', 'Profit from buy Package A by=>reza@hass.ir', 7, NULL, '2021-12-30 22:14:45', '2021-12-30 22:14:45'),
(26, 1, '1.9000', 'Profit from buy Package A by=>reza@hass.ir', 7, NULL, '2021-12-30 22:14:45', '2021-12-30 22:14:45'),
(27, 16, '-95.0000', 'Buy Package A', 5, NULL, '2021-12-30 22:14:45', '2021-12-30 22:14:45'),
(42, 1, '0.3360', 'daily Profit from  Package A', 8, 4, '2022-01-01 22:28:12', '2022-01-01 22:28:12'),
(43, 15, '0.0800', 'daily Profit from  Extra Package', 8, 6, '2022-01-01 22:28:12', '2022-01-01 22:28:12'),
(44, 16, '0.3800', 'daily Profit from  Package A', 8, 7, '2022-01-01 22:28:12', '2022-01-01 22:28:12');

-- --------------------------------------------------------

--
-- Table structure for table `wallet_deposits`
--

DROP TABLE IF EXISTS `wallet_deposits`;
CREATE TABLE IF NOT EXISTS `wallet_deposits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `Price` decimal(20,2) NOT NULL DEFAULT '0.00',
  `Comments` varchar(250) COLLATE utf8mb4_persian_ci DEFAULT '0',
  `TransferLink` text COLLATE utf8mb4_persian_ci,
  `AttachmentFile` text COLLATE utf8mb4_persian_ci,
  `Valid` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_wallet_deposits_users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `wallet_deposits`
--

INSERT INTO `wallet_deposits` (`id`, `user_id`, `Price`, `Comments`, `TransferLink`, `AttachmentFile`, `Valid`, `created_at`, `updated_at`) VALUES
(1, 15, '25.00', '0', NULL, NULL, 0, '2021-12-28 15:58:24', '2021-12-28 15:58:24'),
(2, 15, '20.00', '0', NULL, NULL, 1, '2021-12-28 16:07:10', '2021-12-28 16:07:12'),
(3, 15, '5.00', '0', '1200454e35678ughndxgh', 'assets/news/202112281640698482027.png', 1, '2021-12-28 17:04:42', '2021-12-28 20:19:24'),
(6, 1, '150.00', '0', 'ggg', NULL, 1, '2021-12-28 19:52:50', '2021-12-28 20:28:23'),
(9, 15, '15.00', 'deposit from transfer assets(soheili@manager.com)', 'deposit from transfer assets(soheili@manager.com)', NULL, 1, '2021-12-28 22:13:55', '2021-12-28 22:13:55'),
(11, 15, '50.00', 'deposit from transfer assets(soheili@manager.com)', 'deposit from transfer assets(soheili@manager.com)', NULL, 1, '2021-12-28 22:16:32', '2021-12-28 22:16:32'),
(12, 1, '456.00', '0', '1200454e35678ughndxgh', NULL, 1, '2021-12-29 23:52:09', '2021-12-29 23:52:26'),
(13, 15, '157.00', 'deposit from transfer assets(soheili@manager.com)', 'deposit from transfer assets(soheili@manager.com)', NULL, 1, '2021-12-29 23:53:09', '2021-12-29 23:53:09'),
(14, 16, '150.00', '0', 'ggg', NULL, 1, '2021-12-30 22:14:06', '2021-12-30 22:14:25');

-- --------------------------------------------------------

--
-- Table structure for table `wallet_transfers`
--

DROP TABLE IF EXISTS `wallet_transfers`;
CREATE TABLE IF NOT EXISTS `wallet_transfers` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `from_user_id` int(10) NOT NULL,
  `to_user_id` int(10) NOT NULL,
  `Price` decimal(20,2) NOT NULL DEFAULT '0.00',
  `Comments` text COLLATE utf8mb4_persian_ci,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `wallet_transfers`
--

INSERT INTO `wallet_transfers` (`id`, `from_user_id`, `to_user_id`, `Price`, `Comments`, `created_at`, `updated_at`) VALUES
(1, 1, 15, '15.00', 'deposit from transfer assets(soheili@manager.com)', '2021-12-28 22:13:55', '2021-12-28 22:13:55'),
(2, 1, 15, '50.00', 'deposit from transfer assets(soheili@manager.com)', '2021-12-28 22:16:32', '2021-12-28 22:16:32'),
(3, 1, 15, '157.00', 'deposit from transfer assets(soheili@manager.com)', '2021-12-29 23:53:09', '2021-12-29 23:53:09');

-- --------------------------------------------------------

--
-- Table structure for table `wallet_withdraws`
--

DROP TABLE IF EXISTS `wallet_withdraws`;
CREATE TABLE IF NOT EXISTS `wallet_withdraws` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `Price` decimal(20,2) NOT NULL DEFAULT '0.00',
  `WalletAddress` text COLLATE utf8mb4_persian_ci,
  `TrxFee` int(11) DEFAULT '0',
  `Valid` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_wallet_withdraws_users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;

--
-- Dumping data for table `wallet_withdraws`
--

INSERT INTO `wallet_withdraws` (`id`, `user_id`, `Price`, `WalletAddress`, `TrxFee`, `Valid`, `created_at`, `updated_at`) VALUES
(3, 15, '5.00', 'TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t', 1, 1, '2021-12-29 00:02:29', '2021-12-29 00:22:20'),
(4, 1, '5.00', 'TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t', 1, 0, '2021-12-29 00:03:53', '2021-12-29 00:03:53'),
(5, 1, '299.00', 'sdfghsfghdsfghdghjdtghjdghj', 1, 0, '2022-01-01 22:33:34', '2022-01-01 22:33:34');

-- --------------------------------------------------------

--
-- Structure for view `keywords_use_in_news_count_view`
--
DROP TABLE IF EXISTS `keywords_use_in_news_count_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`ritacone`@`localhost` SQL SECURITY DEFINER VIEW `keywords_use_in_news_count_view`  AS  select `k`.`id` AS `id`,`k`.`Title` AS `Title`,count(`nk`.`keyword_id`) AS `countKeyword` from (`keywords` `k` left join `news_keywords` `nk` on((`nk`.`keyword_id` = `k`.`id`))) group by `k`.`id`,`k`.`Title`,`nk`.`keyword_id` order by `k`.`id` ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `info_users`
--
ALTER TABLE `info_users`
  ADD CONSTRAINT `FK_info_users_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `FK_messages_users` FOREIGN KEY (`from_user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_messages_users_2` FOREIGN KEY (`to_user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `FK_news_news_categories` FOREIGN KEY (`news_category_id`) REFERENCES `news_categories` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_news_news_states` FOREIGN KEY (`news_state_id`) REFERENCES `news_states` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_news_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `news_comments`
--
ALTER TABLE `news_comments`
  ADD CONSTRAINT `FK_news_comments_news` FOREIGN KEY (`news_id`) REFERENCES `news` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `package_users`
--
ALTER TABLE `package_users`
  ADD CONSTRAINT `FK_package_users_packages` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_package_users_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `permission_descriptions`
--
ALTER TABLE `permission_descriptions`
  ADD CONSTRAINT `FK_permission_descriptions_permission_types` FOREIGN KEY (`role_type_id`) REFERENCES `permission_types` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `role_administrators`
--
ALTER TABLE `role_administrators`
  ADD CONSTRAINT `FK_role_administrators_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_users`
--
ALTER TABLE `role_users`
  ADD CONSTRAINT `FK_role_users_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_role_users_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `settings_professional`
--
ALTER TABLE `settings_professional`
  ADD CONSTRAINT `FK_settings_professional_setting_professional_types` FOREIGN KEY (`setting_professional_type_id`) REFERENCES `setting_professional_types` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `support_details`
--
ALTER TABLE `support_details`
  ADD CONSTRAINT `FK_support_details_support_headers` FOREIGN KEY (`support_header_id`) REFERENCES `support_headers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_support_details_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `support_headers`
--
ALTER TABLE `support_headers`
  ADD CONSTRAINT `FK_support_headers_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_levels`
--
ALTER TABLE `user_levels`
  ADD CONSTRAINT `FK_user_levels_users` FOREIGN KEY (`parent_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_user_levels_users_2` FOREIGN KEY (`child_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_package_termination`
--
ALTER TABLE `user_package_termination`
  ADD CONSTRAINT `FK_user_package_termination_package_users` FOREIGN KEY (`package_user_id`) REFERENCES `package_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wallets`
--
ALTER TABLE `wallets`
  ADD CONSTRAINT `FK_wallet_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wallet_deposits`
--
ALTER TABLE `wallet_deposits`
  ADD CONSTRAINT `FK_wallet_deposits_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wallet_withdraws`
--
ALTER TABLE `wallet_withdraws`
  ADD CONSTRAINT `FK_wallet_withdraws_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
