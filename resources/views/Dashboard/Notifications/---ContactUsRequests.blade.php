@extends('Layouts.dashboardMainLayout')
@section('BreadCrumbURL','/Notifications/ContactUsRequests')
@section('BreadCrumbTitle','درخواست های تماس با ما')
@section('CssFiles')
    <link rel="stylesheet" href="/assets/login/bower_components/iCheck/all.css">
@endsection
@section('MainContent')
    <div class="row">
        <section class="col-lg-12 col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <i class="fa fa-info-circle"></i>
                    <h3 class="box-title">
                        {{ \App\Facade\DbSettings::GetDbSetting()->Title }}
                    </h3>
                    <!-- tools box -->
                    <div class="pull-left box-tools">
                        <button type="button" class="btn bg-info btn-sm" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /. tools -->
                </div>

                <!-- modal -->
                <div class="modal modal-info fade" id="modal-record-show">
                    <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">نمایش اعلانات تماس با ما</h4>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        <label for="ShowSubject">موضوع</label>
                                        <input type="text" name="ShowSubject" id="ShowSubject"
                                               class="form-control">
                                    </p>
                                    <p>
                                        <label for="ShowFullName">ارسال کننده</label>
                                        <input type="text" name="ShowFullName" id="ShowFullName"
                                               class="form-control">
                                    </p>
                                    <p>
                                        <label for="ShowEmail">پست الکترونیکی</label>
                                        <input type="text" name="ShowEmail" id="ShowEmail"
                                               class="form-control text-left">
                                    </p>
                                    <p>
                                        <label for="ShowPhone">تلفن تماس</label>
                                        <input type="text" name="ShowPhone" id="ShowPhone"
                                               class="form-control text-left">
                                    </p>
                                    <p>
                                        <label for="ShowMessage">متن پیام</label>
                                        <textarea name="ShowMessage" id="ShowMessage" cols="5" rows="5" class="form-control"></textarea>
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">خروج
                                    </button>
                                </div>
                            </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <div class="box-body">
                    <div class="content">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">لیست تماس باما</h3>
                            </div>
                            <div class="box-header with-border">
                                @include('Dashboard.toolbars.NotificationToolbar')
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table id="keyWordDataTable" class="table table-bordered table-striped table-responsive table-hover">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>موضوع</th>
                                            <th>تاریخ ارسال</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($ContactUsRequests as $key=>$row)
                                            <tr @if($row['State'] == 1) style="background-color: #c3ff97" @endif>
                                                <td>{{ ++$key }}</td>
                                                <td>{{ is_null($row['Subject']) ? '[بدون موضوع]' : $row['Subject'] }}</td>
                                                <td>@if(\App\Facade\OrganizationInfo::GetAccessFromProfessionalSettings('show_date_in_human_function'))
                                                        {{\App\MyClasses\CmsFunctions::GetDateForHumans($row['created_at'])}}
                                                    @else
                                                        {{ \App\MyClasses\CmsFunctions::GetGregorianDateAndConvertToJalaliDate($row['created_at']) }}
                                                    @endif</td>
                                                <td>
                                                    <div class="input-group">
                                                        <button type="button"
                                                                class="btn bg-light-blue-active dropdown-toggle "
                                                                data-toggle="dropdown"
                                                                style="font-size: 9px !important ">
                                                            <span class="fa fa-caret-down"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a class="btn bg-maroon btn-xs" data-id="{{ $row['id'] }}" title="حذف رکورد"><span class="fa fa-trash"></span>حذف</a></li>
                                                            <li><a class="btn bg-olive btn-xs" data-id="{{ $row['id'] }}" data-target="#modal-record-show" data-toggle="modal" title="نمایش رکورد"><span class="fa fa-search"></span>نمایش</a></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{$ContactUsRequests->links()}}
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop

@section('JsFiles')
    <script src="/assets/login/bower_components/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function () {
            @if(Session::has('change_permission_success'))
            swal("", "سطوح دسترسی ویرایش گردید", "success");
            @endif
            @if(Session::has('add_role_success'))
            swal("", "سطح دسترسی با موفقیت ایجاد گردید", "success");
            @endif
        });
    </script>
    <script>
        $(function () {
            $('#addC').one('submit',function(){
                $(this).find('input[type="submit"]').attr('disabled','disabled');
            });

            $('#editC').one('submit',function(){
                $(this).find('input[type="submit"]').attr('disabled','disabled');
            });

            //flat CheckBox
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });


            $('body').on('click','.bg-olive',function () {
                $('.ShowModalLoadingArea').css({'display':'block','position':'sticky',});
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/Notifications/ContactUsShow',
                    data: {_token: token, id: id},
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.state == 1) {
                            $('#ShowMessage').empty();
                            $('#ShowMessage').append(data.message.Message);
                            $('#ShowFullName').val(data.message.FullName);
                            $('#ShowEmail').val(data.message.Email);
                            $('#ShowPhone').val(data.message.Phone);
                            $('#ShowSubject').val(data.message.Subject);
                            $('.ShowModalLoadingArea').css('display','none');
                        } else {
                            swal(data.message, {
                                icon: 'warning',
                                dangerMode: true
                            });
                        }
                    }
                });

            });

            $('.bg-maroon').click(function () {
                var id=$(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                var ele = $(this).parent().parent().parent().parent().parent();
                swal({
                    title: "",
                    text: "آیا از حذف این رکورد مطمئن هستید؟",
                    icon: "warning",
                    buttons: ["خیر", "بله"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '/Notifications/ContactUsRemove',
                            data: {_token: token, id: id},
                            type: 'POST',
                            dataType: 'JSON',
                            success: function (data) {
                                if (data.state == 1) {
                                    ele.fadeOut().remove();
                                    swal("",data.message,"success");
                                } else {
                                    swal(data.message,{
                                        icon:'warning',
                                        dangerMode:true
                                    });
                                }
                            }
                        });
                    }
                });
            });

        })
    </script>
@endsection
