@php
    use \App\MyClasses\CmsFunctions;
    use \App\MyClasses\CmsThemes;
    $BaseSetting=CmsFunctions::BaseSetting()
@endphp
@extends('Themes.ServatAM.ServatAMMainLayout')
@section('Content')
    <div class="warper historyAll">
        <div class="row">
            <img width="200" height="200" src="/{{$News->Image}}">
            <br>
            <h2 class="font-y-bold text-center h2Summary">{{$News['Title']}}</h2>
            <br>
            <p class=" content-text historyDetail">
                <br>
                {!! $News['Content']!!}
            </p>
        </div>
    </div>
@stop