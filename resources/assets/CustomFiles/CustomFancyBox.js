$('[data-fancybox="images"]').fancybox({
    buttons: [
        'slideShow',
        'fullScreen',
        'thumbs',
        'download',
        'zoom',
        'close'
    ],
});