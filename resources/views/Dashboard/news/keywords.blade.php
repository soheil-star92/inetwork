@extends('Layouts.dashboardMainLayout')
@section('BreadCrumbLevel')
    <li class="active">کلمات کلیدی</li>
@Stop
@section('BreadCrumbURL','/news/keywords')
@section('BreadCrumbTitle','کلمات کلیدی')
@section('MainContent')
    <div class="row">
        <section class="col-lg-12 col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <i class="fa fa-info-circle"></i>
                    <h3 class="box-title">
                        {{ \App\Facade\DbSettings::GetDbSetting()->Title }}
                    </h3>
                    <!-- tools box -->
                    <div class="pull-left box-tools">
                        <button type="button" class="btn bg-info btn-sm" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /. tools -->
                </div>
                <div class="box-body">
                    <div class="content">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">کلمات کلیدی سایت</h3>
                            </div>
                            <div class="box-header with-border">
                                <a class="btn btn-app" data-toggle="modal" data-target="#modal-keywords-add">
                                    <i class="fa fa-plus"></i>جدید
                                </a>
                                @include('Dashboard.toolbars.keywordsToolbar')
                            </div>
                            <div class="modal modal-info fade" id="modal-keywords-edit">
                                <div class="modal-dialog">
                                    <form action="/news/keywordsEdit" id="keywordEdit" method="post"
                                          enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="keywordId" id="keywordId">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">ویرایش کلمه کلیدی</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    <label for="ShowTitle">عنوان</label>
                                                    <input type="text" name="ShowTitle" id="ShowTitle"
                                                           class="form-control">
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-outline pull-left"
                                                        data-dismiss="modal">خروج
                                                </button>
                                                <input type="submit" class="btn btn-outline" value="ویرایش">
                                            </div>
                                        </div>
                                    </form>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>

                            <div class="modal modal-info fade" id="modal-keywords-add">
                                <div class="modal-dialog">
                                    <form action="/news/addKeyword" id="keywordAdd" method="post"
                                          enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="keywordId" id="keywordId">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">کلمه کلیدی جدید</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    <label for="ShowTitle">عنوان</label>
                                                    <input type="text" name="txtKeyWord" class="form-control"
                                                           placeholder="عنوان کلمه کلیدی"
                                                           autofocus required>
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-outline pull-left"
                                                        data-dismiss="modal">خروج
                                                </button>
                                                <input type="submit" class="btn btn-outline" value="ثبت">
                                            </div>
                                        </div>
                                    </form>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>

                        <!-- /.box-header -->
                            <div class="box-body">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table id="keyWordDataTable"
                                           class="table table-bordered table-striped table-responsive table-hover">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>عنوان</th>
                                            <th>تعداد استفاده در خبرها</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($keywords as $key=>$kw)
                                            <tr>
                                                <td>{{ ++$key }}</td>
                                                <td>
                                                    <a href="/Press/tags/{{$kw->Title}}" target="_blank">{{ str_replace('#',' ',str_replace('_',' ',$kw->Title)) }}</a>
                                                </td>
                                                <td>{{$kw->countKeyword}}</td>
                                                <td>
                                                    <div class="input-group">
                                                        <button type="button"
                                                                class="btn bg-light-blue-active dropdown-toggle "
                                                                data-toggle="dropdown"
                                                                style="font-size: 9px !important ">
                                                            <span class="fa fa-caret-down"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a class="btn bg-maroon btn-xs" data-id="{{ $kw->id }}"
                                                                   title="حذف رکورد"><span class="fa fa-trash"></span>حذف</a>
                                                            </li>
                                                            <li><a class="btn bg-green btn-xs"
                                                                   data-id="{{ $kw->id  }}"
                                                                   data-toggle="modal"
                                                                   data-target="#modal-keywords-edit"
                                                                   title="ویرایش رکورد"><span
                                                                            class="fa fa-edit"></span>ویرایش</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {{$result->links()}}
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('JsFiles')
    <script src="{{ URL::asset('assets/login/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            @if(Session::has('add_key_words_success'))
                swal("","رکورد مورد نظر با موفقیت اضافه گردید", "success");
            @endif
            @if(Session::has('keyword-edit-success'))
                swal("","رکورد مورد نظر با موفقیت ویرایش گردید", "success");
            @endif
        });
    </script>
    <script>
        $(function () {
            $('#keywordAdd').one('submit', function () {
                $(this).find('input[type="submit"]').attr('disabled', 'disabled');
            });
            $('#keywordEdit').one('submit', function () {
                $(this).find('input[type="submit"]').attr('disabled', 'disabled');
            });

            $('.bg-maroon').click(function () {
                var keyId = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                var ele = $(this).parent().parent().parent().parent().parent();
                swal({
                    title: "",
                    text: "آیا از حذف این رکورد مطمئن هستید؟",
                    icon: "warning",
                    buttons: ["خیر", "بله"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '/news/removeKeyword',
                            data: {_token: token, keyId: keyId},
                            type: 'POST',
                            dataType: 'JSON',
                            success: function (data) {
                                if (data.state == 1) {
                                    swal("", data.message, "success");
                                    ele.fadeOut().remove();
                                } else {
                                    swal(data.message, {
                                        icon: 'warning',
                                        dangerMode: true
                                    });
                                }
                            }
                        });
                    }
                });
            });

            $('.bg-green').click(function () {
                $('.ShowModalLoadingArea').css({'display':'block','position':'sticky',});
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/news/keywordShow',
                    data: {_token: token, id: id},
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.state == 1) {
                            $('#keywordId').val(id);
                            var title=data.message.Title;
                            var t1=title.replace('_',' ');
                            $('#ShowTitle').val(t1.replace('#',''));
                            $('.ShowModalLoadingArea').css('display','none');
                        } else {
                            swal(data.message, {
                                icon: 'warning',
                                dangerMode: true
                            });
                        }
                    }
                });
            });

        })
    </script>
@endsection
