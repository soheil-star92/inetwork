<?php

namespace App\DbModels\Dashboard\Support;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table='notifications';
}
