@extends('Layouts.dashboardMainLayout')
@section('CssFiles')
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('assets/login/bower_components/iCheck/flat/blue.css') }}">
@endsection
@section('BreadCrumbLevel')
    <li class="active">متن پیام</li>
@Stop
@section('BreadCrumbURL','/message/showReceiveMessageLists')
@section('BreadCrumbTitle','پیام ها')
@section('MainContent')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ \App\Facade\DbSettings::GetDbSetting()->Title }} - زیر سیستم پیام ها
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            @include('Dashboard.sidebar.message')

            <div class="col-md-9">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">پیام</h3>

                        <div class="box-tools pull-right">
                            <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Next"><i
                                        class="fa fa-chevron-right"></i></a>
                            <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Previous"><i
                                        class="fa fa-chevron-left"></i></a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="mailbox-read-info">
                            <h3>موضوع: {{ $message->Subject }}</h3>
                            <h5>از:  {{ $message->first_name.' '.$message->last_name }}
                                <span class="mailbox-read-time pull-left">
                                    {{ \App\MyClasses\CmsFunctions::GetGregorianDateAndConvertToJalaliDate($message->created_at)}}
                                </span></h5>
                        </div>
                        <!-- /.mailbox-read-info -->
                        <form action="/message/sendToTrash" method="post" id="sendToTrash">
                        <div class="mailbox-controls with-border text-right">
                            <div class="btn-group">
                                {{ Session::flash('msgId',$message->id) }}
                                    {{ csrf_field() }}
                                    <a href="#" onclick="submitFormForTrash();" class="btn btn-default btn-sm">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                <a href="/message/compose" class="btn btn-default btn-sm" data-toggle="tooltip"
                                        data-container="body" title="Reply">
                                    <i class="fa fa-reply"></i></a>
                                <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip"
                                        data-container="body" title="Forward">
                                    <i class="fa fa-share"></i></button>
                            </div>
                            <!-- /.btn-group -->
                            <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" title="Print">
                                <i class="fa fa-print"></i></button>
                        </div>
                        </form>
                        <!-- /.mailbox-controls -->
                        <div class="mailbox-read-message">
                            {!!  $message->Message !!}
                        </div>
                        <!-- /.mailbox-read-message -->
                    </div>
                    <!-- /.box-body -->
                    @unless(is_null($message->Attachment))
                        <div class="box-footer">
                            <ul class="mailbox-attachments clearfix">
                                <li>
                                    <span class="mailbox-attachment-icon"><i class="fa fa-file-o"></i></span>
                                    <div class="mailbox-attachment-info">
                                        <a href="{{ asset($message->Attachment) }}" target="_blank" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>
                                            {{ str_replace('otherFiles/messageAttachments/','',$message->Attachment) }}
                                        </a>
                                        <span class="mailbox-attachment-size">
                          MB{{ number_format(filesize($message->Attachment) / 1048576,2) }}
                                            <a href="{{ asset($message->Attachment) }}" target="_blank" class="btn btn-default btn-xs pull-left"><i
                                                        class="fa fa-cloud-download"></i></a>
                        </span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                @endunless
                <!-- /.box-footer -->
                    <div class="box-footer">
                        <div class="pull-right">
                            <a href="/message/compose" class="btn btn-default"><i class="fa fa-reply"></i> پاسخ دادن</a>
                            <button type="button" class="btn btn-default"><i class="fa fa-share"></i> فوروارد</button>
                        </div>
                        <a class="btn btn-default" onclick="submitFormForTrash();" ><i class="fa fa-trash-o"></i> حذف</a>
                        <button type="button" class="btn btn-default"><i class="fa fa-print"></i> پرینت</button>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /. box -->
            </div>

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('JsFiles')
    <script src="{{ URL::asset('assets/login/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/login/bower_components/iCheck/icheck.min.js') }}"></script>
    <script>
        $(function () {

            //Enable iCheck plugin for checkboxes
            //iCheck for checkbox and radio inputs
            $('.mailbox-messages input[type="checkbox"]').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });

            //Enable check and uncheck all functionality
            $(".checkbox-toggle").click(function () {
                var clicks = $(this).data('clicks');
                if (clicks) {
                    //Uncheck all checkboxes
                    $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
                    $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
                } else {
                    //Check all checkboxes
                    $(".mailbox-messages input[type='checkbox']").iCheck("check");
                    $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
                }
                $(this).data("clicks", !clicks);
            });

        })

        function submitFormForTrash(){
            swal({
                title: "",
                text: "آیا از حذف پیام/های مورد نظر اطمینان دارید؟",
                icon: "warning",
                buttons: ["خیر", "بله"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    document.getElementById('sendToTrash').submit();
                }
            });
        }
    </script>
@endsection
