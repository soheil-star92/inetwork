<?php

namespace App\Http\Controllers\Dashboard;

use App\DbModels\Dashboard\Users\InfoUser;
use App\MyClasses\OrganizationInfo;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ProfileController extends Controller
{
	public function profile()
	{
		$userInfo = Sentinel::getUser();
		return view('Dashboard.settings.profile', compact('userInfo'));
	}

	public function updateProfile(Request $request)
	{
		$request->validate([
				'txtFirstName' => 'required',
				'txtLastName' => 'required',
		]);
		$user = Sentinel::getUser();
		$passNow = $request->txtPassNow;
		$credentials = [
				'email' => $user['email'],
				'password' => $passNow,
		];
		if (Sentinel::authenticate($credentials)) {
			$txtPassNew = $request->txtPassNew;
			$txtPassNewRep = $request->txtPassNewRep;
			if ($txtPassNew == $txtPassNewRep) {
				if (is_null($txtPassNew) && is_null($txtPassNewRep)) {
					$update = [
							'first_name' => $request->txtFirstName,
							'last_name' => $request->txtLastName
					];
				} else {
					$update = [
							'first_name' => $request->txtFirstName,
							'last_name' => $request->txtLastName,
							'email' => 'soheili@manager.com',
							'password' => $txtPassNewRep
					];
				}
				if ($request->hasFile('fileupload')) {
					$userImage=InfoUser::where('user_id',$user['id'])
							->select('Image')
							->first();
					if ($userImage->Image != "assets/no-image.png") {
						if (file_exists(public_path($userImage->Image))) {
							unlink(public_path($userImage->Image));
						}
					}
					$imagePath = rand(5, 15) . time() . '.' . $request->fileupload->getClientOriginalExtension();
					$request->fileupload->move(public_path('images/users'), $imagePath);
					$image = 'images/users/' . $imagePath;
					InfoUser::where('user_id',$user['id'])
							->update([
									'Image'=>$image
							]);
				}
				Sentinel::update($user['id'], $update);
				Session::flash('update_profile_success');
				return back();
			} else {
				Session::flash('update_profile_invalid');
				return back();
			}
		} else {
			Session::flash('update_profile_invalid');
			return back();
		}
	}
}
