@extends('Layouts.dashboardMainLayout')
@section('BreadCrumbURL','/users/usersList')
@section('BreadCrumbTitle','لیست کاربران')
@section('MainContent')
    <div class="row">
        <section class="col-lg-12 col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <i class="fa fa-info-circle"></i>
                    <h3 class="box-title">
                        {{ \App\Facade\DbSettings::GetDbSetting()->Title }}
                    </h3>
                    <!-- tools box -->
                    <div class="pull-left box-tools">
                        <button type="button" class="btn bg-info btn-sm" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /. tools -->
                </div>
                <!-- modal -->
                <div class="modal modal-info fade" id="modal-add-user">
                    <div class="modal-dialog">
                        <form action="/Test" id="addC" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">تعریف کاربر</h4>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        <label for="FN">نام کاربر :</label>
                                        <input type="text" name="FN" id="FN" class="form-control">
                                    </p>
                                    <p>
                                        <label for="LN">فامیلی کاربر :</label>
                                        <input type="text" name="LN" id="LN" class="form-control">
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">خروج
                                    </button>
                                    <input type="submit" class="btn btn-outline" value="ثبت">
                                </div>
                            </div>
                        </form>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <div class="modal modal-info fade" id="modal-update-user">
                    <div class="modal-dialog">
                        <form action="/Test/51" id="ed" method="post" enctype="multipart/form-data">
                            {{method_field('put')}}
                            {{csrf_field()}}
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">تعریف کاربر</h4>
                                </div>
                                <div class="modal-body">

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">خروج
                                    </button>
                                    <input type="submit" class="btn btn-outline" value="ثبت">
                                </div>
                            </div>
                        </form>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <div class="box-body">
                    <div class="content">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">کاربری های سیستم</h3>
                            </div>
                            <div class="box-header with-border">
                                <a class="btn btn-app" data-toggle="modal" data-target="#modal-add-user">
                                    <i class="fa fa-plus"></i>جدید
                                </a>
                                <a class="btn btn-app" data-toggle="modal" data-target="#modal-update-user">
                                    <i class="fa fa-plus"></i>ویرایش
                                </a>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table id="keyWordDataTable"
                                           class="table table-bordered table-striped table-responsive table-hover">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام</th>
                                            <th>فامیلی</th>
                                            <th>سن</th>
                                            <th>آدرس</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($tests as $key=>$u)
                                            @php
                                                $getUser=\App\Facade\OrganizationInfo::GetUserRoleAccess();
                                            @endphp
                                            @if( $u->id != 1 || $getUser->id == 1)
                                                <tr @if($u->Blocked == 1) style="background-color: rgba(200,0,0,0.3)" @endif>
                                                    <td>{{ $key }}</td>
                                                    <td>{{ $u['FirstName'] }}</td>
                                                    <td>{{ $u['LastName'] }}</td>
                                                    <td>{{ $u['Age'] }}</td>
                                                    <td>{{ $u['Address'] }}</td>
                                                    <td>
                                                        <div class="input-group">
                                                            <button type="button"
                                                                    class="btn bg-light-blue-active dropdown-toggle "
                                                                    data-toggle="dropdown"
                                                                    style="font-size: 9px !important ">
                                                                <span class="fa fa-caret-down"></span></button>
                                                            <ul class="dropdown-menu">
                                                                @if($u->id != 1)
                                                                <li><a class="btn bg-maroon btn-xs"
                                                                       data-id="{{ $u->id }}" title="حذف رکورد"><span
                                                                                class="fa fa-trash"></span>حذف</a></li>
                                                                @endif
                                                                <li><a class="btn bg-green btn-xs"
                                                                       href="/users/showUser/{{ $u->id }}"
                                                                       title="ویرایش رکورد"><span
                                                                                class="fa fa-edit"></span>ویرایش</a>
                                                                </li>
                                                                <li><a class="btn bg-yellow-gradient btn-xs"
                                                                       data-id="{{ $u->id }}"
                                                                       title="تغییر وضعیت رکورد"><span
                                                                                class="fa fa-ban"></span>فعال/غیرفعال</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('JsFiles')
    <script src="{{ URL::asset('assets/login/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            @if(Session::has('add_user_success'))
            swal("", "رکورد جدید با موفقیت اضافه گردید", "success");
            @endif
            @if(Session::has('error_at_insert_user'))
            swal("{{Session::get('error_at_insert_user')}}", {
                icon: "warning",
                dangerMode: true,
            });
            @endif
        });
    </script>
    <script>
        $(function () {
            $('.bg-maroon').click(function () {
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                var ele = $(this).parent().parent().parent().parent().parent();
                swal({
                    title: "",
                    text: "آیا از حذف این رکورد مطمئن هستید؟",
                    icon: "warning",
                    buttons: ["خیر", "بله"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '/Test/'+id,
                            type: 'post',
                            data:{_token:token,_method:'delete'},
                            success: function (data) {
                                if (data.state == 1) {
                                    ele.fadeOut().remove();
                                    swal("", data.message, "success");
                                } else {
                                    swal(data.message, {
                                        icon: 'warning',
                                        dangerMode: true
                                    });
                                }
                            }
                        });
                    }
                });
            });

            $('.bg-yellow-gradient').click(function () {
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                swal({
                    title: "",
                    text: "آیا از تغییر وضعیت این رکورد مطمئن هستید؟",
                    icon: "warning",
                    buttons: ["خیر", "بله"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '/users/ChangeUserStateBlocking',
                            data: {_token: token, id: id},
                            type: 'POST',
                            dataType: 'JSON',
                            success: function (data) {
                                if (data.state == 1) {
                                    swal("", data.message, "success");
                                    location.reload();
                                } else {
                                    swal(data.message, {
                                        icon: 'warning',
                                        dangerMode: true
                                    });
                                }
                            }
                        });
                    }
                });
            });


            $('#addC').one('submit', function () {
                $(this).find('input[type="submit"]').attr('disabled', 'disabled');
            });
        })
    </script>
@endsection
