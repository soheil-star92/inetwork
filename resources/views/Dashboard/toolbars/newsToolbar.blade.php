<div class="modal modal-info fade" id="modal-news-filter">
    <div class="modal-dialog">
        <form action="/news/SearchInNews" id="search" method="post">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">فیلتر</h4>
                </div>
                <div class="modal-body">
                    <p>
                        <label for="searchKey">مبنای جست و جو</label>
                        <select name="sctId" id="sctId" class="form-control">
                            <option value="1">کدخبر</option>
                            <option value="2">روتیتر خبر</option>
                            <option value="3">زیر خبر</option>
                            <option value="4" selected>تیتر خبر</option>
                            <option value="5" >متن خبر</option>
                        </select>
                    </p>
                    <p>
                        <label for="searchKey">کلید واژه</label>
                        <input type="text" name="searchKey" id="searchKey"
                               class="form-control">
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left"
                            data-dismiss="modal">خروج
                    </button>
                    <input type="submit" class="btn btn-outline" value="جست و جو">
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal modal-info fade" id="news_changes_show_modal">
    <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">گزارش گردش خبر [ <span id="ShowNewsTitle"></span> ]</h4>
                </div>
                <div class="modal-body">
                    <p>
                        <label for="searchKey">کاربرانی که بر روی این خبر تغییری ایجاد کرده اند</label>
                        <table class="table table-cell table-bordered table-responsive" style="color: #0a0a0a;text-align: center">
                            <thead>
                                <tr>
                                    <td>ردیف</td>
                                    <td>پروفایل</td>
                                    <td>نام کاربر</td>
                                    <td>نوع تغییر</td>
                                    <td>زمان انجام</td>
                                </tr>
                            </thead>
                            <tbody id="ShowLogNewsChange">

                            </tbody>
                        </table>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left"
                            data-dismiss="modal">خروج
                    </button>
                </div>
            </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


@if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.news.add.news']))
    <a class="btn btn-app" href="/news/addNewsForm">
        <i class="fa fa-plus"></i>جدید
    </a>
@endif
@if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.news.lists']))
    <a class="btn btn-app" href="/news/newsLists">
        <i class="fa fa-list"></i>لیست اخبار
    </a>
@endif
@if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.news.slider.news']))
    <a class="btn btn-app" href="/news/sliderNews">
        <i class="fa fa-sliders"></i>اخبار اسلایدر
    </a>
@endif
@if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.news.my.news']))
    <a class="btn btn-app" href="/news/myNews">
        <i class="fa fa-user"></i>اخبار ارسالی من
    </a>
@endif
@if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.news.allow.note.news.lists']))
    <a class="btn btn-app" href="/news/noteNewsLists">
        <i class="fa fa-pencil"></i>لیست یادداشت ها
    </a>
@endif
<a class="btn btn-app" data-toggle="modal" data-target="#modal-news-filter">
    <i class="fa fa-filter"></i>فیلتر
</a>