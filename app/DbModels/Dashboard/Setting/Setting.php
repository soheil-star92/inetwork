<?php

namespace App\DbModels\Dashboard\Setting;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public $timestamps=false;
}
