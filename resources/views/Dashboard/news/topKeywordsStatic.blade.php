@extends('Layouts.dashboardMainLayout')
@section('BreadCrumbURL','/news/topKeywordsStatic')
@section('BreadCrumbTitle','برترین کلمات کلیدی')
@section('MainContent')
    <div class="row">
        <section class="col-lg-12 col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <i class="fa fa-info-circle"></i>
                    <h3 class="box-title">
                        {{ \App\Facade\DbSettings::GetDbSetting()->Title }}
                    </h3>
                    <!-- tools box -->
                    <div class="pull-left box-tools">
                        <button type="button" class="btn bg-info btn-sm" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /. tools -->
                </div>
                <div class="box-body">
                    <div class="content">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">بیشترین کلمات کلیدی مورد استفاده ماه گذشته در خبر ها</h3>
                            </div>
                            <div class="box-header with-border">
                                @include('Dashboard.toolbars.keywordsToolbar')
                            </div>
                        <!-- /.box-header -->
                            <div class="box-body">
                                <!-- /.box-header -->
                                <input type="hidden" id="BarCount" value="{{ count($keywords) }}">
                                @foreach($keywords AS $key=>$p)
                                    <span style="display: none" id="Title-{{ ++$key }}">{{ str_replace('#',' ',str_replace('_',' ',$p->Title)) }}</span>
                                    <span  style="display: none" id="Count-{{ $key }}">{{ $p->KeywordsCount }}</span>
                                @endforeach
                                <canvas id="myChart"></canvas>

                                <div class="box-body">
                                    <table id="keyWordDataTable"
                                           class="table table-bordered table-striped table-responsive table-hover">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>عنوان</th>
                                            <th>تعداد استفاده در خبرها</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($keywords as $key=>$kw)
                                            <tr>
                                                <td>{{ ++$key }}</td>
                                                <td>
                                                    <a href="/Press/tags/{{$kw->Title}}" target="_blank">{{ str_replace('#',' ',str_replace('_',' ',$kw->Title)) }}</a>
                                                </td>
                                                <td>{{$kw->KeywordsCount}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('JsFiles')
    <script src="{{ URL::asset('assets/login/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- ChartJS -->
    <script src="{{ asset('assets/login/bower_components/other/Chart.js') }}"></script>

    <script>
        $(document).ready(function(){
            @if(Session::has('add_key_words_success'))
                swal("","رکورد مورد نظر با موفقیت اضافه گردید", "success");
            @endif
            @if(Session::has('keyword-edit-success'))
                swal("","رکورد مورد نظر با موفقیت ویرایش گردید", "success");
            @endif
        });
    </script>
    <script>
        $(function () {

            var iCounter=$('#BarCount').val();
            var Labels=new Array("");
            var chartData=new Array("");
            for (var i = 1; i <= iCounter; i++){
                Labels[i]= $("#Title-" + i).text();
                chartData[i] = $("#Count-" + i).text();
            }
            var ctx = document.getElementById("myChart").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: Labels,
                    datasets: [{
                        label: '# بیشترین کلمات کلیدی مورد استفاده ماه گذشته در خبر ها',
                        data: chartData ,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.5)',
                            'rgba(54, 162, 235, 0.5)',
                            'rgba(255, 206, 86, 0.5)',
                            'rgba(75, 192, 192, 0.5)',
                            'rgba(153, 102, 255, 0.5)',
                            'rgba(255, 159, 64, 0.5)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });

            $('#keywordAdd').one('submit', function () {
                $(this).find('button[type="submit"]').attr('disabled', 'disabled');
            });
            $('#keywordEdit').one('submit', function () {
                $(this).find('input[type="submit"]').attr('disabled', 'disabled');
            });

            $('.bg-maroon').click(function () {
                var keyId = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                var ele = $(this).parent().parent().parent().parent().parent();
                swal({
                    title: "",
                    text: "آیا از حذف این رکورد مطمئن هستید؟",
                    icon: "warning",
                    buttons: ["خیر", "بله"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '/news/removeKeyword',
                            data: {_token: token, keyId: keyId},
                            type: 'POST',
                            dataType: 'JSON',
                            success: function (data) {
                                if (data.state == 1) {
                                    swal("", data.message, "success");
                                    ele.fadeOut().remove();
                                } else {
                                    swal(data.message, {
                                        icon: 'warning',
                                        dangerMode: true
                                    });
                                }
                            }
                        });
                    }
                });
            });

            $('.bg-green').click(function () {
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/news/keywordShow',
                    data: {_token: token, id: id},
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.state == 1) {
                            $('#keywordId').val(id);
                            var title=data.message.Title;
                            var t1=title.replace('_',' ');
                            $('#ShowTitle').val(t1.replace('#',''));
                        } else {
                            swal(data.message, {
                                icon: 'warning',
                                dangerMode: true
                            });
                        }
                    }
                });
            });

        })
    </script>
@endsection
