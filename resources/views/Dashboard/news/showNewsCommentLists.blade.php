@extends('Layouts.dashboardMainLayout')
@section('BreadCrumbLevel')
    <li class="active">نمایش نظرات</li>
@Stop
@section('BreadCrumbURL','/news/newsLists')
@section('BreadCrumbTitle','لیست اخبار')
@section('MainContent')
    <div class="row">
        <section class="col-lg-12 col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <i class="fa fa-info-circle"></i>
                    <h3 class="box-title">
                        {{ \App\Facade\DbSettings::GetDbSetting()->Title }}
                    </h3>
                    <!-- tools box -->
                    <div class="pull-left box-tools">
                        <button type="button" class="btn bg-info btn-sm" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /. tools -->
                </div>
                <div class="modal modal-info fade" id="modal-show-news-comment">
                    <div class="modal-dialog">
                        <form action="/news/changeNewsCommentState" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="commentId" id="commentId">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">نمایش نظرات بازدیدکنندگان خبر: {{ $news['Title'] }}</h4>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        <label for="txtRoleName">متن نظر</label>
                                        <textarea name="Message" id="message" class="form-control" cols="30"
                                                  rows="10" disabled></textarea>
                                    </p>
                                    <p>
                                        <label for="txtRoleName">تغییر وضعیت خبر</label>
                                        <select name="commentStateId" class="form-control">
                                            <option value="0">در حال بررسی</option>
                                            <option value="1">تایید</option>
                                            <option value="2">حذف نظر</option>
                                        </select>
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">خروج
                                    </button>
                                    <input type="submit" class="btn btn-outline" value="ثبت">
                                </div>
                            </div>
                        </form>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <div class="box-body">
                    <div class="content">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">نمایش نظرات خبر: {{ $news['Title'] }}</h3>
                            </div>
                            <div class="box-header with-border">
                                @include('Dashboard.toolbars.newsToolbar')
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table id="keyWordDataTable"
                                           class="table table-bordered table-striped table-responsive table-hover">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام</th>
                                            <th>ایمیل</th>
                                            <th>IP</th>
                                            <th>تاریخ ارسال</th>
                                            <th>وضعیت</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($comments as $key=>$n)
                                            <tr @if($n->State == 1) style="background-color: rgba(0,200,0,0.3)" @endif>
                                                <td>{{ ++$key }}</td>
                                                <td>{{ $n['Name'] }}</td>
                                                <td>{{ $n['Email'] }}</td>
                                                <td>{{ $n['IP'] }}</td>
                                                <td>{{ \App\MyClasses\CmsFunctions::GetGregorianDateAndConvertToJalaliDate($n['created_at']) }}</td>
                                                <td>
                                                    {{ ($n->State == 0 ? 'در حال بررسی' : 'تایید شده') }}
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <button type="button"
                                                                class="btn bg-light-blue-active dropdown-toggle "
                                                                data-toggle="dropdown"
                                                                style="font-size: 9px !important ">
                                                            <span class="fa fa-caret-down"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a class="btn bg-aqua btn-xs" data-id="{{ $n->id }}"
                                                                   data-toggle="modal"
                                                                   data-target="#modal-show-news-comment"
                                                                   title="نمایش"><span class="fa fa-search"></span>نمایش</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('JsFiles')
    <script src="{{ URL::asset('assets/login/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            @if(Session::has('update_news_comment_success'))
            swal("","وضعیت رکورد مورد نظر با موفقیت تغییر یافت", "success");
            @endif
        });
    </script>
    <script>
        $(function () {

            $('.bg-aqua').click(function () {
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/news/showNewsCommentAjax',
                    data: {_token: token, id: id},
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.state == 1) {
                            $('#message').text(data.message.Message);
                            $('#commentId').val(id);
                        } else {
                            alert(data.message);
                        }
                    }
                });
            });

        })
    </script>
@endsection
