<?php

namespace App\Http\Controllers\Dashboard;

use App\DbModels\Dashboard\ExclusivePages\ExclusivePageLists;
use App\MyClasses\CmsFunctions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ExclusivePagesController extends Controller
{
	public function ExclusivePagesLists()
	{
		$ExclusivePages = ExclusivePageLists::orderBy('id', 'DESC')->paginate(100);
		return view('Dashboard.ExclusivePages.ExclusivePagesLists', compact('ExclusivePages'));
	}

	public function ExclusivePageAdd(Request $request)
	{
		try {
			DB::transaction(function () use ($request) {
				$request->validate([
						'Title' => 'required',
						'Content' => 'required'
				]);
				$EP=new ExclusivePageLists();
				$EP->Title=$request->Title;
				$EP->Content=$request->Content;
				$EP->save();
			});
			Session::flash('exclusive_page_add_success');
			return back();
		} catch (\Exception $e) {
			Session::flash('error_at_exclusive_page_add', $e->getMessage());
			return back();
		}
	}

	public function ExclusivePageUpdate(Request $request)
	{
		try {
			DB::transaction(function () use ($request) {
				$request->validate([
						'ShowTitle' => 'required',
						'ShowContent' => 'required',
						'EPID' => 'required'
				]);
				ExclusivePageLists::where('id',CmsFunctions::CustomDecoding($request->EPID))
					->update([
							'Title'=>$request->ShowTitle,
							'Content'=>$request->ShowContent
					]);
			});
			Session::flash('exclusive_page_update_success');
			return back();
		} catch (\Exception $e) {
			Session::flash('error_at_exclusive_page_update', $e->getMessage());
			return back();
		}
	}

	public function ExclusivePagesRemove(Request $request)
	{
		try{
			ExclusivePageLists::where('id',$request->id)
				->delete();
			unset($request);
			$message=[
					'state'=>1,
					'message'=>'رکورد مورد نظر با موفقیت حذف گردید',
			];
			return response()->json($message);
		}catch (\Exception $e){
			$message=[
					'state'=>0,
					'message'=>$e->getMessage()
			];
			return response()->json($message);
		}
	}

	public function ExclusivePageShow(Request $request)
	{
		try{
			$EP=ExclusivePageLists::where('id',$request->id)
				->select('Title','Content')
				->first();
			$message=[
					'state'=>1,
					'message'=>$EP,
					'EPID'=>CmsFunctions::CustomCoding($request->id)
			];
			return response()->json($message);
		}catch (\Exception $e){
			$message=[
					'state'=>0,
					'message'=>$e->getMessage()
			];
			return response()->json($message);
		}
	}

	public function ExclusivePageChangeState(Request $request)
	{
		try{
			$EPstate=ExclusivePageLists::where('id',$request->id)
				->select('State')
				->first();
			if($EPstate->State == 0){
				ExclusivePageLists::where('id',$request->id)
						->update([
								'State'=>1
						]);
			}else{
				ExclusivePageLists::where('id',$request->id)
						->update([
								'State'=>0
						]);
			}
			unset($request,$EPstate);
			$message=[
					'state'=>1,
					'message'=>'وضعیت رکورد مورد نظر با موفقیت ویرایش گردید'
			];
			return response()->json($message);
		}catch (\Exception $e){
			$message=[
					'state'=>0,
					'message'=>$e->getMessage()
			];
			return response()->json($message);
		}
	}
}
