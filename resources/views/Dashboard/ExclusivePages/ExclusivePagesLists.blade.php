@extends('Layouts.dashboardMainLayout')
@section('BreadCrumbURL','/ExclusivePages/ExclusivePagesLists')
@section('BreadCrumbTitle','لیست صفحات اختصاصی')
@section('MainContent')
    <div class="row">
        <section class="col-lg-12 col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <i class="fa fa-info-circle"></i>
                    <h3 class="box-title">
                        {{ \App\Facade\DbSettings::GetDbSetting()->Title }}
                    </h3>
                    <!-- tools box -->
                    <div class="pull-left box-tools">
                        <button type="button" class="btn bg-info btn-sm" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /. tools -->
                </div>

                <div class="box-body">
                    <div class="content">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">صفحات اختصاصی</h3>
                            </div>
                            <a class="btn btn-app" data-toggle="modal" data-target="#modal-show-form-add">
                                <i class="fa fa-plus"></i>جدید
                            </a>
                            @include('Dashboard.toolbars.ExclusivePageToolbar')
                            <div class="box-header with-border">
                                <div class="modal modal-info fade" id="modal-show-form-add">
                                    <form action="/ExclusivePages/ExclusivePageAdd" method="post" id="FormAdd">
                                        {{csrf_field()}}
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">افزودن صفحه اختصاصی</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>
                                                        <label for="Title">عنوان</label>
                                                        <input type="text" name="Title" id="Title" class="form-control"
                                                               placeholder="عنوان صفحه">
                                                    </p>
                                                    <p>
                                                        <label for="Content">محتوا</label>
                                                        <textarea name="Content" id="mytextarea" class="form-control"
                                                                  cols="5" rows="5"></textarea>
                                                    </p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-outline pull-left"
                                                            data-dismiss="modal">خروج
                                                    </button>
                                                    <input type="submit" class="btn btn-outline" value="ثبت">
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                    </form>
                                    <!-- /.modal-dialog -->
                                </div>

                                <div class="modal modal-info fade" id="modal-show-form">
                                    <form action="/ExclusivePages/ExclusivePageUpdate" method="post" id="ّFormUpdate">
                                        {{csrf_field()}}
                                        <input type="hidden" name="EPID" id="EPID">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">نمایش صفحه اختصاصی</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>
                                                        <label for="ShowTitle">عنوان</label>
                                                        <input type="text" name="ShowTitle" id="ShowTitle"
                                                               class="form-control"
                                                               placeholder="عنوان صفحه">
                                                    </p>
                                                    <p>
                                                        <label for="ShowContent">محتوا</label>
                                                        <textarea name="ShowContent" id="ShowMyTextArea"
                                                                  class="form-control"
                                                                  cols="5" rows="5"></textarea>
                                                    </p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-outline pull-left"
                                                            data-dismiss="modal">خروج
                                                    </button>
                                                    <input type="submit" class="btn btn-outline" value="بروزرسانی">
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                    </form>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="keyWordDataTable"
                                       class="table table-bordered table-striped table-responsive table-hover">
                                    <thead>
                                    <tr>
                                        <th>ردیف</th>
                                        <th>عنوان</th>
                                        <th>بازدید</th>
                                        <th>وضعیت</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($ExclusivePages as $key=>$n)
                                        <tr @if($n->State == 0) style="background-color: rgba(200,0,0,0.3)" @endif>
                                            <td>{{ ++$key }}</td>
                                            <td>
                                                @if($n['State'] == 0)
                                                {{ $n['Title'] }}
                                                    @else
                                                    <a target="_blank" @if(\App\Facade\OrganizationInfo::GetAccessFromProfessionalSettings('show_url_post_as_text')) href="/Pages/{{str_replace(' ','_',$n['Title'])}}" @else href="/Pages/{{$n['id']}}" @endif>{{$n['Title']}}</a>
                                                @endif
                                            </td>
                                            <td>{{ $n['View'] }}</td>
                                            <td>{{ ($n['State'] == 0) ? 'غیرفعال' : 'فعال' }}</td>
                                            <td>
                                                <div class="input-group">
                                                    <button type="button"
                                                            class="btn bg-light-blue-active dropdown-toggle "
                                                            data-toggle="dropdown"
                                                            style="font-size: 9px !important ">
                                                        <span class="fa fa-caret-down"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a class="btn bg-maroon btn-xs"
                                                               data-id="{{ $n->id }}"
                                                               title="حذف رکورد"><span
                                                                        class="fa fa-trash"></span>حذف</a>
                                                        </li>
                                                        <li><a class="btn bg-yellow-gradient btn-xs"
                                                               data-id="{{ $n->id }}"
                                                               title="تغییر وضعیت"><span
                                                                        class="fa fa-ban"></span>تغییر وضعیت</a>
                                                        </li>
                                                        <li><a class="btn bg-green btn-xs"
                                                               data-id="{{ $n->id }}" data-toggle="modal"
                                                               data-target="#modal-show-form"
                                                               title="نمایش"><span
                                                                        class="fa fa-search"></span>نمایش</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                            {{ $ExclusivePages->links() }}
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection

@section('JsFiles')
    <!-- TinyMCE Editor -->
    <script src="{{ URL::asset('assets/login/bower_components/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ URL::asset('assets/login/bower_components/tinymce/myStyle/CustomizeFunctions.js') }}"></script>
    <script>
        $(document).ready(function () {
            @if(Session::has('exclusive_page_add_success'))
            swal("", "رکورد مورد نظر با موفقیت ثبت گردید", "success");
            @endif
            @if(Session::has('exclusive_page_update_success'))
            swal("", "رکورد مورد نظر با موفقیت ویرایش گردید", "success");
            @endif
            @if(Session::has('error_at_exclusive_page_add'))
            swal("{{Session::get('error_at_exclusive_page_add')}}", {
                icon: "warning",
                dangerMode: true,
            });
            @endif
            @if(Session::has('error_at_exclusive_page_update'))
            swal("{{Session::get('error_at_exclusive_page_update')}}", {
                icon: "warning",
                dangerMode: true,
            });
            @endif
        });
    </script>
    <script>
        $(function () {
            $('#FormAdd').on('submit', function () {
                $(this).find('input[type="submit"]').attr('disabled', 'disabled')
            });
            $('#ّFormUpdate').on('submit', function () {
                $(this).find('input[type="submit"]').attr('disabled', 'disabled')
            });

            $('.bg-maroon').click(function () {
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                var ele = $(this).parent().parent().parent().parent().parent();
                swal({
                    title: "",
                    text: "آیا از حذف این رکورد مطمئن هستید؟",
                    icon: "warning",
                    buttons: ["خیر", "بله"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '/ExclusivePages/ExclusivePagesRemove',
                            data: {_token: token, id: id},
                            type: 'POST',
                            dataType: 'JSON',
                            success: function (data) {
                                if (data.state == 1) {
                                    ele.fadeOut().remove();
                                    swal("", data.message, "success");
                                } else {
                                    swal(data.message, {
                                        icon: 'warning',
                                        dangerMode: true
                                    });
                                }
                            }
                        });
                    }
                });
            });
            $('.bg-yellow-gradient').click(function () {
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                swal({
                    title: "",
                    text: "آیا از تغییر وضعیت این رکورد مطمئن هستید؟",
                    icon: "warning",
                    buttons: ["خیر", "بله"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '/ExclusivePages/ExclusivePageChangeState',
                            data: {_token: token, id: id},
                            type: 'POST',
                            dataType: 'JSON',
                            success: function (data) {
                                if (data.state == 1) {
                                    swal("", data.message, "success");
                                    location.reload();
                                } else {
                                    swal(data.message, {
                                        icon: 'warning',
                                        dangerMode: true
                                    });
                                }
                            }
                        });
                    }
                });
            });

            $('.bg-green').click(function () {
                $('.ShowModalLoadingArea').css({'display': 'block', 'position': 'sticky',});
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/ExclusivePages/ExclusivePageShow',
                    data: {_token: token, id: id},
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.state == 1) {
                            $('#EPID').val(data.EPID);
                            $('#ShowTitle').val(data.message.Title);
                            tinymce.get('ShowMyTextArea').setContent(data.message.Content);
                            $('.ShowModalLoadingArea').css('display', 'none');
                        } else {
                            swal(data.message, {
                                icon: 'warning',
                                dangerMode: true
                            });
                        }
                    }
                });
            });


            $('.bg-black-gradient').click(function () {
                var IP = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/showIpLocation',
                    data: {_token: token, IP: IP},
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.state == 1) {
                            $('#country').val(data.message.country);
                            $('#countryCode').val(data.message.countryCode);
                            $('#region').val(data.message.region);
                            $('#regionName').val(data.message.regionName);
                            $('#city').val(data.message.city);
                            $('#query').val(data.message.query);
                            $('#isp').val(data.message.isp);
                            $('#timezone').val(data.message.timezone);
                            $('#lon').val(data.message.lon);
                            $('#lat').val(data.message.lat);
                        } else {
                            swal(data.message, {
                                icon: 'warning',
                                dangerMode: true
                            });
                        }
                    }
                });
            });

        })
    </script>
@endsection
