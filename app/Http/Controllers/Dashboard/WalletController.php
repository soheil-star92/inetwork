<?php

namespace App\Http\Controllers\Dashboard;

use App\DbModels\Dashboard\News\News;
use App\DbModels\Dashboard\Wallet\Wallet;
use App\DbModels\Dashboard\Wallet\WalletDeposit;
use App\DbModels\Dashboard\Wallet\WalletTransfer;
use App\DbModels\Dashboard\Wallet\WalletWithdraw;
use App\MyClasses\CmsFunctions;
use App\User;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Soheili\ImageWorker\ImageWorker;

class WalletController extends Controller
{
    public function UserAssets()
    {
        $UID = CmsFunctions::GetUID();
        $rows = WalletDeposit::where('user_id', $UID)->orderBy('id', 'DESC')->get();
        return view('Dashboard.Wallet.UserAssets', compact('rows','UID'));
    }

    public function UserWalletList($id)
    {
        $UID = CmsFunctions::DecodedText($id);
        $rows = Wallet::where('user_id',$UID)->orderBy('id','DESC')->paginate(100);
        return view('Dashboard.Wallet.UserTransactions', compact('rows','UID'));
    }

    public function UserTransfer()
    {
        $UID = CmsFunctions::GetUID();
        $rows = WalletTransfer::where('wallet_transfers.from_user_id', $UID)
            ->join('users', 'users.id', 'wallet_transfers.to_user_id')
            ->select('wallet_transfers.*', 'users.email', 'users.first_name', 'users.last_name')
            ->orderBy('wallet_transfers.id', 'DESC')->get();
        return view('Dashboard.Wallet.UserTransfer', compact('rows','UID'));
    }

    public function UserTransactions()
    {
        $UID = CmsFunctions::GetUID();
        $rows = Wallet::where('user_id',$UID)->orderBy('id','DESC')->paginate(100);
        return view('Dashboard.Wallet.UserTransactions', compact('rows','UID'));
    }

    public function UserWithdraw()
    {
        $UID = CmsFunctions::GetUID();
        $rows = WalletWithdraw::where('user_id', $UID)
            ->orderBy('id', 'DESC')->get();
        return view('Dashboard.Wallet.UserWithdraw', compact('rows','UID'));
    }

    public function WithdrawList()
    {
        $UID=CmsFunctions::GetUID();
        $rows = WalletWithdraw::join('users', 'users.id', 'wallet_withdraws.user_id')
            ->select('wallet_withdraws.*', 'users.email', 'users.first_name', 'users.last_name')
            ->orderBy('wallet_withdraws.id', 'DESC')->paginate(100);
        return view('Dashboard.Wallet.WithdrawList', compact('rows','UID'));
    }

    public function DepositList()
    {
        $UID=CmsFunctions::GetUID();
        $rows = WalletDeposit::join('users', 'users.id', 'wallet_deposits.user_id')
            ->select('wallet_deposits.*', 'users.email', 'users.first_name', 'users.last_name')
            ->orderBy('wallet_deposits.id', 'DESC')->paginate(100);
        return view('Dashboard.Wallet.DepositList', compact('rows','UID'));
    }

    public function TransferList()
    {
        $UID=CmsFunctions::GetUID();
        $rows = WalletTransfer::join('users', 'users.id', 'wallet_transfers.to_user_id')
            ->select('wallet_transfers.*', 'users.email', 'users.first_name', 'users.last_name')
            ->orderBy('wallet_transfers.id', 'DESC')->paginate(100);
        return view('Dashboard.Wallet.TransferList', compact('rows','UID'));
    }

    public function addUserTransfer(Request $request)
    {
        try {
            $request->validate([
                'iptPrice' => 'required',
                'iptToEmail' => 'required',
            ]);
            $toEmail = $request->iptToEmail;
            $Price = $request->iptPrice;
            $CurrentUserInfo = Sentinel::getUser();
            $UID = CmsFunctions::GetUID();
            $UserAsset = CmsFunctions::GetUserWalletAssets($UID);
            $chekUser=User::where('email',$toEmail)->select('id')->first();
            if(empty($chekUser)){
                Session::flash('error_at_add_user_transfer','error at invalid user(please,enter correct email)');
                return back();
            }
            if (($UserAsset >= 5) && ($Price >= 5) && ($Price<=$UserAsset)) {
                $toUID=$chekUser->id;
                WalletDeposit::insert([
                    'user_id'=>$toUID,
                    'Price'=>$Price,
                    'TransferLink'=>'deposit from transfer assets('.$CurrentUserInfo['email'].')',
                    'Comments'=>'deposit from transfer assets('.$CurrentUserInfo['email'].')',
                    'Valid'=>1,
                ]);
                $GetLastWalletTransfer=WalletTransfer::orderBy('id','DESC')->select('id')->first();
                $ReferenceID=empty($GetLastWalletTransfer) ? 0 : ++$GetLastWalletTransfer->id;
                WalletTransfer::insert([
                    'id'=>$ReferenceID,
                    'from_user_id'=>$UID,
                    'to_user_id'=>$toUID,
                    'Price'=>$Price,
                    'Comments'=>'deposit from transfer assets('.$CurrentUserInfo['email'].')',
                ]);
                Wallet::insert([
                    'user_id'=>$toUID,
                    'Price'=>$Price,
                    'State'=>2,
                    'ReferenceID'=>$ReferenceID
                ]);
                Wallet::insert([
                    'user_id'=>$UID,
                    'Price'=> -($Price),
                    'State'=>3,
                    'ReferenceID'=>$ReferenceID
                ]);
                Session::flash('add_user_transfer_successfully', config('msg.success'));
                return back();
            }else{
                Session::flash('error_at_add_user_transfer','User assets is insufficient.');
                return back();
            }
        } catch (\Exception $e) {
            Session::flash('error_at_add_user_transfer', $e->getMessage());
            return back();
        }
    }

    public function addUserWithdraw(Request $request)
    {
        try {
            $request->validate([
                'iptPrice' => 'required',
            ]);
            $Price = $request->iptPrice;
            $UID = CmsFunctions::GetUID();
            $UserAsset = CmsFunctions::GetUserWalletAssets($UID);
            $userLogged=\App\Facade\OrganizationInfo::GetUserInfoWithoutParameter();
            $LastWithDraw=WalletWithdraw::orderBy('id','DESC')->select('id')->first();
            $RID=empty($LastWithDraw) ? 1 : ++$LastWithDraw->id;
            if (($UserAsset >= 5) && ($Price >= 5) && ($Price<=$UserAsset)) {
                WalletWithdraw::insert([
                    'id'=>$RID,
                    'user_id'=>$UID,
                    'Price'=>$Price,
                    'TrxFee'=>1,
                    'WalletAddress'=>$userLogged->Wallet,
                    'Valid'=>2,
                ]);
                Wallet::insert([
                    'user_id'=>$UID,
                    'Price'=> -($Price),
                    'State'=>10,
                    'ReferenceID'=>$RID
                ]);
                Session::flash('add_user_withdraw_successfully', config('msg.success'));
                return back();
            }else{
                Session::flash('error_at_add_user_withdraw','User assets is insufficient.');
                return back();
            }
        } catch (\Exception $e) {
            Session::flash('error_at_add_user_withdraw', $e->getMessage());
            return back();
        }
    }

    public function addUserDeposit(Request $request)
    {
        try {
            $request->validate([
                'iptPrice' => 'required',
                'iptTrxHashLink' => 'required',
                'iptAttachment' => 'image|mimes:jpeg,png,jpg|max:5000',
            ]);
            $Path = null;
            if ($request->hasFile('iptAttachment')) {
                $IW = new ImageWorker($request, 'iptAttachment', 'assets/news/');
                $IW->Move();
                $Path = $IW->FullPath;
                unset($IW);
            }
            WalletDeposit::insert([
                'user_id' => CmsFunctions::GetUID(),
                'Price' => $request->iptPrice,
                'TransferLink' => $request->iptTrxHashLink,
                'AttachmentFile' => $Path
            ]);
            Session::flash('add_user_deposit_successfully', config('msg.success'));
            return back();
        } catch (\Exception $e) {
            Session::flash('error_at_add_user_deposit', $e->getMessage());
            return back();
        }
    }

    public function UpdateDepositRow(Request $request)
    {
        try {
            $request->validate([
                'iptShowPrice' => 'required',
                'iptShowTrxHashLink' => 'required',
                'iptKey' => 'required',
            ]);
            $RID = CmsFunctions::DecodedText($request->iptKey);
            $row = WalletDeposit::where('id', $RID)->first();
            $Valid = $request->iptChangeStatus;
            WalletDeposit::where('id', $RID)->update([
                'Valid' => $Valid,
            ]);
            $ReferenceID = $row->id;
            $existInWallet = Wallet::where([
                ['State', '=', 1],
                ['ReferenceID', '=', $ReferenceID]
            ])->first();
            if (($Valid == 1) && (empty($existInWallet))) {
                Wallet::insert([
                    'State' => 1,
                    'ReferenceID' => $ReferenceID,
                    'Price' => $request->iptShowPrice,
                    'user_id' => $row->user_id,
                ]);
            }
            Session::flash('update_deposit_successfully', config('msg.update'));
            return back();
        } catch (\Exception $e) {
            Session::flash('error_at_update_deposit', $e->getMessage());
            return back();
        }
    }

    public function UpdateUserWithdraw(Request $request)
    {
        try {
            $request->validate([
                'iptPrice' => 'required',
                'iptKey' => 'required',
            ]);
            $RID = CmsFunctions::DecodedText($request->iptKey);
            $row = WalletWithdraw::where('id', $RID)->first();
            $Valid = $request->iptChangeStatus;
            WalletWithdraw::where('id', $RID)->update([
                'Valid' => $Valid,
            ]);
            $ReferenceID = $row->id;
            $existInWallet = Wallet::where([
                ['State', '=', 4],
                ['ReferenceID', '=', $ReferenceID]
            ])->first();
            if($Valid == 0){
                Wallet::where([
                    ['ReferenceID','=',$RID],
                    ['State','=',10],
                    ['user_id','=',$row->user_id]
                ])->delete();
            }
            if (($Valid == 1) && (empty($existInWallet))) {
                Wallet::where([
                    ['ReferenceID','=',$RID],
                    ['State','=',10],
                    ['user_id','=',$row->user_id]
                ])->delete();

                Wallet::insert([
                    'State' => 4,
                    'ReferenceID' => $ReferenceID,
                    'Price' => -($request->iptPrice),
                    'user_id' => $row->user_id,
                ]);
            }
            Session::flash('update_withdraw_successfully', config('msg.update'));
            return back();
        } catch (\Exception $e) {
            Session::flash('error_at_update_withdraw', $e->getMessage());
            return back();
        }
    }

    public  function getUserDeposit(Request $request)
    {
        try {
            $row = WalletDeposit::where('wallet_deposits.id', CmsFunctions::DecodedText($request->id))
                ->join('users', 'users.id', 'wallet_deposits.user_id')
                ->select('wallet_deposits.*', 'users.email', 'users.first_name', 'users.last_name')
                ->first();
            $message = array(
                'state' => 1,
                'message' => $row,
                'iptFullname' => $row->first_name . ' ' . $row->last_name,
                'iptEmail' => $row->email,
                'iptKey' => CmsFunctions::EncodedText($row->id),
                'imgTag' => "<a  href='/" . $row->AttachmentFile . "' target='_blank'  >-Click here to show attachments</a>"
            );
            return response()->json($message);
        } catch (\Exception $e) {
            $message = array(
                'state' => 0,
                'message' => $e->getMessage()
            );
            return response()->json($message);
        }
    }

    public  function getUserWithdraw(Request $request)
    {
        try {
            $row = WalletWithdraw::where('wallet_withdraws.id', CmsFunctions::DecodedText($request->id))
                ->join('users', 'users.id', 'wallet_withdraws.user_id')
                ->select('wallet_withdraws.*', 'users.email', 'users.first_name', 'users.last_name')
                ->first();
            $message = array(
                'state' => 1,
                'message' => $row,
                'iptFullname' => $row->first_name . ' ' . $row->last_name,
                'iptEmail' => $row->email,
                'iptKey' => CmsFunctions::EncodedText($row->id),
            );
            return response()->json($message);
        } catch (\Exception $e) {
            $message = array(
                'state' => 0,
                'message' => $e->getMessage()
            );
            return response()->json($message);
        }
    }

    public  function getUserByEmail(Request $request)
    {
        try {
            $row = User::where('email',$request->id)->select('first_name','last_name')->first();
            $State=0;
            if(empty($row)){
                $State=0;
            }else{
                $State=1;
            }
            $message = array(
                'state' => 1,
                'TypeID'=>$State,
                'message' => empty($row) ? 'wrong email,please try again' : 'User info: '.$row->first_name.' '.$row->last_name,
            );
            return response()->json($message);
        } catch (\Exception $e) {
            $message = array(
                'state' => 0,
                'message' => $e->getMessage()
            );
            return response()->json($message);
        }
    }

    public function RemoveWalletByAdmin(Request $request)
    {
        try {
            $RID=CmsFunctions::DecodedText($request->id);
            $GetRow= Wallet::where('id',$RID)->select('State','ReferenceID')->first();
            if($GetRow->State){
                WalletDeposit::where('id',$GetRow->ReferenceID)->delete();
            }
            Wallet::where('id',$RID)->delete();
            $message = array(
                'state' => 1,
                'message' => config('msg.success')
            );
            return response()->json($message);
        } catch (\Exception $e) {
            $message = array(
                'state' => 0,
                'message' => $e->getMessage()
            );
            return response()->json($message);
        }
    }

    public function UserProfits()
    {
        $UID=CmsFunctions::GetUID();
        $rows=Wallet::where('user_id',$UID)->whereIn('State',[7,8])->orderBy('id','DESC')->get();
        return view('Dashboard.Wallet.UserProfits', compact('rows','UID'));
    }

}
