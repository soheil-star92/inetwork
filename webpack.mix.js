const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js([
    'resources/assets/bower_components/jquery/dist/jquery.min.js',
    'resources/assets/bower_components/jquery-ui/jquery-ui.min.js',
    'resources/assets/bower_components/bootstrap/dist/js/bootstrap.min.js',
    'resources/assets/dist/js/adminlte.min.js',
    'resources/assets/bower_components/sweet-alert/sweetalert.min.js',
    'resources/assets/bower_components/gotop/gotop.js',
    'resources/assets/bower_components/PACE/pace.min.js',
    'resources/assets/bower_components/datatables.net/js/jquery.dataTables.min.js',
    'resources/assets/bower_components/datatables.net/custom/CustomDt.js',

], 'public/js/app.js').styles([
    'resources/assets/dist/css/bootstrap-theme.css',
    'resources/assets/dist/css/rtl.css',
    'resources/assets/bower_components/font-awesome/css/font-awesome.min.css',
    'resources/assets/bower_components/Ionicons/css/ionicons.min.css',
    'resources/assets/dist/css/AdminLTE.css',
    'resources/assets/dist/css/skins/_all-skins.min.css',
    'resources/assets/bower_components/gotop/gotop.css',
    'resources/assets/bower_components/PACE/themes/white/pace-theme-minimal.css',
    'resources/assets/CustomFiles/custom.css'
],'public/css/app.css');

mix.copy('resources/assets/dist/fonts/FontAwesome.otf','public/fonts/FontAwesome.otf');
mix.copy('resources/assets/dist/fonts/fontawesome-webfont.eot','public/fonts/fontawesome-webfont.eot');
mix.copy('resources/assets/dist/fonts/fontawesome-webfont.svg','public/fonts/fontawesome-webfont.svg');
mix.copy('resources/assets/dist/fonts/fontawesome-webfont.ttf','public/fonts/fontawesome-webfont.ttf');
mix.copy('resources/assets/dist/fonts/fontawesome-webfont.woff','public/fonts/fontawesome-webfont.woff');
mix.copy('resources/assets/dist/fonts/fontawesome-webfont.woff2','public/fonts/fontawesome-webfont.woff2');
mix.copy('resources/assets/dist/fonts/Vazir-Bold-FD-WOL.eot','public/fonts/Vazir-Bold-FD-WOL.eot');
mix.copy('resources/assets/dist/fonts/Vazir-Bold-FD-WOL.ttf','public/fonts/Vazir-Bold-FD-WOL.ttf');
mix.copy('resources/assets/dist/fonts/Vazir-Bold-FD-WOL.woff','public/fonts/Vazir-Bold-FD-WOL.woff');
mix.copy('resources/assets/dist/fonts/Vazir-FD-WOL.eot','public/fonts/Vazir-FD-WOL.eot');
mix.copy('resources/assets/dist/fonts/Vazir-FD-WOL.ttf','public/fonts/Vazir-FD-WOL.ttf');
mix.copy('resources/assets/dist/fonts/Vazir-FD-WOL.woff','public/fonts/Vazir-FD-WOL.woff');
mix.copy('resources/assets/bower_components/bootstrap/fonts/glyphicons-halflings-regular.eot','public/fonts/glyphicons-halflings-regular.eot');
mix.copy('resources/assets/bower_components/bootstrap/fonts/glyphicons-halflings-regular.svg','public/fonts/glyphicons-halflings-regular.svg');
mix.copy('resources/assets/bower_components/bootstrap/fonts/glyphicons-halflings-regular.ttf','public/fonts/glyphicons-halflings-regular.ttf');
mix.copy('resources/assets/bower_components/bootstrap/fonts/glyphicons-halflings-regular.woff','public/fonts/glyphicons-halflings-regular.woff');
mix.copy('resources/assets/bower_components/bootstrap/fonts/glyphicons-halflings-regular.woff2','public/fonts/glyphicons-halflings-regular.woff2');
mix.copy('resources/assets/bower_components/Ionicons/fonts/ionicons.eot','public/fonts/ionicons.eot');
mix.copy('resources/assets/bower_components/Ionicons/fonts/ionicons.svg','public/fonts/ionicons.svg');
mix.copy('resources/assets/bower_components/Ionicons/fonts/ionicons.ttf','public/fonts/ionicons.ttf');
mix.copy('resources/assets/bower_components/Ionicons/fonts/ionicons.woff','public/fonts/ionicons.woff');
