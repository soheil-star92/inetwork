<a class="btn btn-app" href="/settings/setting">
    <i class="fa fa-cog"></i>تنظیمات پایه سایت
</a>
<a class="btn btn-app" href="/settings/professionalSettings">
    <i class="fa fa-cogs"></i>تنظیمات اختصاصی
</a>
<a class="btn btn-app" href="/settings/socialMedia">
    <i class="fa fa-facebook"></i>شبکه های اجتماعی
</a>
<a class="btn btn-app" href="/settings/links">
    <i class="fa fa-link"></i>پیوندهای سایت
</a>
<a class="btn btn-app" href="/settings/newsCategory">
    <i class="fa fa-object-group"></i>سرویس های خبری
</a>
<a class="btn btn-app" href="/settings/CenterSliders">
    <i class="fa fa-laptop"></i>اسلایدرهای مرکزی
</a>