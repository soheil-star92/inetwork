<?php

namespace App\Http\Controllers;

use App\DbModels\Dashboard\Users\InfoUser;
use App\DbModels\Dashboard\Users\PermissionDescription;
use App\DbModels\Dashboard\Users\Role;
use App\DbModels\Dashboard\Users\RoleUser;
use App\MyClasses\CmsFunctions;
use App\User;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SentinelController extends Controller
{
	public function createUser()
	{
		Sentinel::register(array(
				'email' => 'admin@webgah24.ir',
				'password' => '422$%^(Bist'
		));
	}

	public function activationUser($id)
	{
		Activation::create($id);
	}

	public function checkAuthorized()
	{
		if (Sentinel::check()) {
			return 1;
		} else {
			return 2;
		}
	}

	public function logout()
	{
		Sentinel::logout();
		return redirect('/');
	}

	public function getUser()
	{
		$user = Sentinel::getUser();
		dd($user['first_name']);
	}

	public function addPermissionToRole($actionName, $actionDescription, $actionType)
	{
		$allRoleId = Role::orderBy('id', 'ASC')
				->select('id')
				->get();
		foreach ($allRoleId as $r) {
			$role = \Sentinel::findRoleById($r->id);
			$role->addPermission($actionName, false);
			$role->save();
		}
		$permDesc = new PermissionDescription();
		$permDesc->permission_name = $actionName;
		$permDesc->title = $actionDescription;
		$permDesc->role_type_id = $actionType;
		$permDesc->save();

//        $role->permissions = [
//            'sub.system.my.order' => true,
//            'sub.system.my.list' => true,
//            'sub.system.users' => true,
//            'sub.system.profile' => true,
//        ];

	}

	public function attachUserToRole()
	{
		$user = Sentinel::findById(1);
		$role = Sentinel::findRoleByName('مدیریت');
		$role->users()->attach($user);
	}

	public function detachUserToRole()
	{
		$user = Sentinel::findById(1);
		$role = Sentinel::findRoleByName('مدیریت');
		$role->users()->detach($user);
	}

	public function RemovePermission($actionName)
	{
		$allRoleId = Role::orderBy('id', 'ASC')
				->select('id')
				->get();
		foreach ($allRoleId as $r) {
			$role = \Sentinel::findRoleById($r->id);
			$role->removePermission($actionName);
			$role->save();
		}
		PermissionDescription::where('permission_name',$actionName)->delete();
	}
}
