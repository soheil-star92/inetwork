<!DOCTYPE html>
<html lang="en">
<head>
    <!-- META -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content=""/>
    <meta name="author" content=""/>
    <meta name="robots" content=""/>
    <meta name="description" content=""/>

    <!-- FAVICONS ICON -->
    <link rel="icon" href="/Themes/HomePage/images/mlogo.png" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="/Themes/HomePage/images/mlogo.png"/>

    <!-- PAGE TITLE HERE -->
    <title>invest chain Network | Cryptocurrency trading | crypto mining</title>

    <!-- MOBILE SPECIFIC -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- [if lt IE 9]>
    <script src="/Themes/HomePage/js/html5shiv.min.js"></script>
    <script src="/Themes/HomePage/js/respond.min.js"></script>
    <![endif] -->

    <!-- BOOTSTRAP STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="/Themes/HomePage/css/bootstrap.min.css">
    <!-- FONTAWESOME STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="/Themes/HomePage/css/fontawesome/css/font-awesome.min.css"/>
    <!-- FLATICON STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="/Themes/HomePage/css/flaticon.min.css">
    <!-- ANIMATE STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="/Themes/HomePage/css/animate.min.css">
    <!-- OWL CAROUSEL STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="/Themes/HomePage/css/owl.carousel.min.css">
    <!-- BOOTSTRAP SELECT BOX STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="/Themes/HomePage/css/bootstrap-select.min.css">
    <!-- MAGNIFIC POPUP STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="/Themes/HomePage/css/magnific-popup.min.css">
    <!-- LOADER STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="/Themes/HomePage/css/loader.min.css">
    <!-- MAIN STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="/Themes/HomePage/css/style.css">
    <!-- THEME COLOR CHANGE STYLE SHEET -->
    <link rel="stylesheet" class="skin" type="text/css" href="/Themes/HomePage/css/skin/skin-1.css">
    <!-- CUSTOM  STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="/Themes/HomePage/css/custom.css">
    <!-- SIDE SWITCHER STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="/Themes/HomePage/css/switcher.css">


    <!-- REVOLUTION SLIDER CSS -->
    <link rel="stylesheet" type="text/css" href="/Themes/HomePage/plugins/revolution/revolution/css/settings.css">
    <!-- REVOLUTION NAVIGATION STYLE -->
    <link rel="stylesheet" type="text/css" href="/Themes/HomePage/plugins/revolution/revolution/css/navigation.css">

    <!-- GOOGLE FONTS -->
    <link href="/Themes/HomePage/https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet">
    <link href="/Themes/HomePage/https://fonts.googleapis.com/css?family=Crete+Round:400,400i&amp;subset=latin-ext"
          rel="stylesheet">
    @yield('CssFiles')

    <style>
        #footerFix {
            position:fixed;
            right:80%;
            bottom:0;
            margin:0;
        }
        #footerFix img{
            border-radius: 120%;
        }
    </style>
</head>

<body id="bg">
<div class="page-wraper">
    <!-- HEADER START -->
    <header class="site-header header-style-6">

        <div class="top-bar bg-primary">
            <div class="container">
                <div class="row">
                    <div class="clearfix">
                        <div class="wt-topbar-left">
                            <ul class="list-unstyled e-p-bx pull-left">
                                <li><i class="fa fa-send"></i><a href="https://t.me/ICN_Company" target="_blank">Telegram Support</a></li>
                            </ul>
                        </div>

                        <div class="wt-topbar-right">
{{--                            <div class=" language-select pull-right">--}}
{{--                                <div class="dropdown">--}}
{{--                                    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Language--}}
{{--                                        <span class="caret"></span></button>--}}
{{--                                    <ul class="dropdown-menu dropdown-menu-right">--}}
{{--                                        <li><a href="#"><img src="/Themes/HomePage/images/united-states.png" alt="">English</a>--}}
{{--                                        </li>--}}
{{--                                        <li><a href="#"><img src="/Themes/HomePage/images/france.png" alt="">French</a>--}}
{{--                                        </li>--}}
{{--                                        <li><a href="#"><img src="/Themes/HomePage/images/germany.png" alt="">German</a>--}}
{{--                                        </li>--}}
{{--                                        <li><a href="#"><img src="/Themes/HomePage/images/Turkey.png" alt="">Turkish</a>--}}
{{--                                        </li>--}}
{{--                                        <li><a href="#"><img src="/Themes/HomePage/images/iran.png" alt="">Persian</a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </div>--}}

                            <ul class="list-unstyled e-p-bx pull-right">
                                <li><a href="/login"><i class="fa fa-user"></i>Login</a></li>
                                <li><a href="#" data-toggle="modal" data-target="#Register-form"><i
                                                class="fa fa-sign-in"></i>Register</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Search Link -->

        <!-- Search Form -->
        <div class="main-bar header-middle bg-white">
            <div class="container">
                <div class="logo-header">
                    <a href="/">
                        <img src="/Themes/HomePage/images/mainLogo.jpg" width="216" height="20" alt=""/>
                    </a>
                </div>
                <div class="header-info">
                    <ul>
                        <li>
                            <div>
                                <div class="icon-sm">
                                    <span class="icon-cell  text-primary"><i class="iconmoon-travel"></i></span>
                                </div>
                                <div class="icon-content">
                                    <strong>Location </strong>
                                    <span>13-14 Hanover Street London W1S 1YH</span>
                                </div>
                            </div>
                        </li>
                        <li>
{{--                            <div>--}}
{{--                                <div class="icon-sm">--}}
{{--                                    <span class="icon-cell  text-primary"><i class="iconmoon-smartphone-1"></i></span>--}}
{{--                                </div>--}}
{{--                                <div class="icon-content">--}}
{{--                                    <strong>Phone Number</strong>--}}
{{--                                    <span>(130) 047-6850</span>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </li>
                        <li class="btn-col-last">
                            {{--                            <a class="site-button text-uppercase  font-weight-700">Requet a Quote</a>--}}
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Search Form -->
        <div class="sticky-header main-bar-wraper">
            <div class="main-bar header-botton nav-bg-secondry">
                <div class="container">
                    <!-- NAV Toggle Button -->
                    <button data-target=".header-nav" data-toggle="collapse" type="button"
                            class="navbar-toggle collapsed">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- ETRA Nav -->
                    <div class="extra-nav">
                        <div class="extra-cell">
                            <a href="#search" class="site-search-btn"><i class="fa fa-search"></i></a>
                        </div>

                    </div>
                    <!-- SITE Search -->
                    <div id="search">
                        <span class="close"></span>
                        <form role="search" id="searchform" action="#" method="get" class="radius-xl">
                            <div class="input-group">
                                <input value="" name="q" type="search" placeholder="Type to search"/>
                                <span class="input-group-btn"><button type="button" class="search-btn"><i
                                                class="fa fa-search"></i></button></span>
                            </div>
                        </form>
                    </div>

                    <!-- MAIN Vav -->
                    <div class="header-nav navbar-collapse collapse ">
                        <ul class=" nav navbar-nav">
                            <li class="active">
                                <a href="/">Home</a>
                            </li>
                            <li><a href="/login">Login</a></li>
                            <li><a href="/Gifts">Gifts</a></li>
                            <li><a href="/ProfitPackages">Profit</a></li>
                            <li><a href="#" data-toggle="modal" data-target="#Register-form">Register</a></li>
                            <li><a href="/About">About US</a></li>
                            <li><a href="/Contact">Contact US</a></li>

{{--                            <li>--}}
{{--                                <a href="javascript:;">Contact<i class="fa fa-chevron-down"></i></a>--}}
{{--                                <ul class="sub-menu">--}}
{{--                                    <li>--}}
{{--                                        <a href="/Contact">Contact us</a>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <a href="/About">About us</a>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="javascript:;">Users<i class="fa fa-chevron-down"></i></a>--}}
{{--                                <ul class="sub-menu">--}}
{{--                                    <li>--}}
{{--                                        <a href="/login">Login</a>--}}
{{--                                    </li>--}}
{{--                                    <li><a href="#" data-toggle="modal" data-target="#Register-form">Register</a></li>--}}
{{--                                </ul>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="javascript:;">Gifts<i class="fa fa-chevron-down"></i></a>--}}
{{--                                <ul class="sub-menu">--}}
{{--                                    <li><a href="/Gifts">Gifts</a></li>--}}
{{--                                    <li><a href="/Leaders">For Leaders</a></li>--}}
{{--                                </ul>--}}
{{--                            </li>--}}
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </header>
    <!-- HEADER END -->


    <!-- CONTENT START -->
    <div class="page-content">
        @yield('Content')
    </div>
    <!-- CONTENT END -->

    <!-- FOOTER START -->
    <footer class="site-footer footer-dark bg-no-repeat bg-full-height bg-center "
            style="background-image:url(/Themes/HomePage/images/background/footer-bg.jpg);">
        <!-- FOOTER BLOCKES START -->

        <!-- FOOTER COPYRIGHT -->
        <div class="footer-bottom  overlay-wraper">
            <div class="overlay-main"></div>
            <div class="constrot-strip"></div>
            <div class="container p-t30">
                <div class="row">
                    <div class="wt-footer-bot-left">
                        <span class="copyrights-text">© {{date('Y')}} Invest Chain Network. All Rights Reserved.</span>
                    </div>
                    <div class="wt-footer-bot-right">
                        <ul class="copyrights-nav pull-right">
                            <li><a href="/About">Terms & Condition</a></li>
                            <li><a href="/Contact">Privacy Policy</a></li>
                            <li><a href="/Contact">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- FOOTER END -->

    <div id="footerFix">
        <a href="/Leaders">
            <img src= "/assets/aring.gif" height="250" width="250" />
        </a>
    </div>
    <!-- BUTTON TOP START -->
    <button class="scroltop"><span class=" iconmoon-house relative" id="btn-vibrate"></span>Top</button>

    <!-- MODAL  LOGIN -->

@unless(\Request::getRequestUri() == '/Contact')
    <!-- MODAL  REGISTER -->
        <div id="Register-form" class="modal fade " role="dialog">
            <div class="modal-dialog modal-sm">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-white">Register here</h4>
                    </div>
                    <div class="modal-body p-a30">
                        <form id="reg-form" method="post" action="/PublicRegistration">
                            {{csrf_field()}}
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input class="form-control" placeholder="Enter First name" name="iptFirstName"
                                           type="text" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input class="form-control" placeholder="Enter Last name" name="iptLastName"
                                           type="text"
                                           required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <input class="form-control" placeholder="Enter email" name="iptEmail" type="email"
                                           required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                                    <select name="iptCountry" class="form-control">
                                        @foreach(\App\DbModels\Dashboard\Setting\Country::all() as $row)
                                            <option value="{{$row->id}}">{{$row->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                                    <input class="form-control" placeholder="Enter User City" name="iptCity"
                                           type="text" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                                    <input class="form-control" placeholder="Enter National Unique Code" name="iptNationalCode"
                                           type="text" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-tree"></i></span>
                                    <input class="form-control" placeholder="Enter Referral Code" name="iptReferralCode"
                                           type="text" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-mobile-phone"></i></span>
                                    <input class="form-control" placeholder="Enter Mobile" name="iptPhone" type="text"
                                           required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                    <input class="form-control" placeholder="Enter Password" name="iptPassword"
                                           type="password" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                    <input class="form-control" placeholder="Enter repeat password"
                                           name="iptRepeatPassword"
                                           type="password" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    {!! captcha_img('flat') !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user-secret"></i></span>
                                    <input class="form-control" placeholder="Enter Captcha " name="RegisterCaptcha"
                                           type="text" required>
                                </div>
                            </div>
                            <button type="submit" class="site-button-secondry text-uppercase btn-block m-b10">Submit
                            </button>
                            <span class="font-12">Already Have an Account? <a href="/login"
                                                                              class="text-primary">Login</a></span>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    @endunless
</div>

@if(Session::has('error_at_user_public_registered_captcha'))
    <script type="text/javascript">
        alert('Error: error at Captcha code');
    </script>
@endif
@if(Session::has('error_at_password_mismatch'))
    <script type="text/javascript">
        alert('Error: password mismatch');
    </script>
@endif
@if(Session::has('error_at_user_public_registered'))
    <script type="text/javascript">
        alert('Error: error at registration');
    </script>
@endif
@if(Session::has('error_at_email_already_exist'))
    <script type="text/javascript">
        alert('Error: error at email, email already existed.');
    </script>
@endif
@if(Session::has('error_at_referral_code'))
    <script type="text/javascript">
        alert('Error: error at referral code');
    </script>
@endif

<!-- LOADING AREA START ===== -->
<div class="loading-area">
    <div class="loading-box"></div>
    <div class="loading-pic">
        <div class="cssload-container">
            <div class="cssload-dot bg-primary"><i class="fa fa-bitcoin"></i></div>
            <div class="step" id="cssload-s1"></div>
            <div class="step" id="cssload-s2"></div>
            <div class="step" id="cssload-s3"></div>
        </div>
    </div>
</div>
<!-- LOADING AREA  END ====== -->

<!-- JAVASCRIPT  FILES ========================================= -->
<script src="/Themes/HomePage/js/jquery-1.12.4.min.js"></script><!-- JQUERY.MIN JS -->
<script src="/Themes/HomePage/js/bootstrap.min.js"></script><!-- BOOTSTRAP.MIN JS -->

<script src="/Themes/HomePage/js/bootstrap-select.min.js"></script><!-- FORM JS -->
<script src="/Themes/HomePage/js/jquery.bootstrap-touchspin.min.js"></script><!-- FORM JS -->

<script src="/Themes/HomePage/js/magnific-popup.min.js"></script><!-- MAGNIFIC-POPUP JS -->

<script src="/Themes/HomePage/js/waypoints.min.js"></script><!-- WAYPOINTS JS -->
<script src="/Themes/HomePage/js/counterup.min.js"></script><!-- COUNTERUP JS -->
<script src="/Themes/HomePage/js/waypoints-sticky.min.js"></script><!-- COUNTERUP JS -->

<script src="/Themes/HomePage/js/isotope.pkgd.min.js"></script><!-- MASONRY  -->

<script src="/Themes/HomePage/js/owl.carousel.min.js"></script><!-- OWL  SLIDER  -->

<script src="/Themes/HomePage/js/stellar.min.js"></script><!-- PARALLAX BG IMAGE   -->
<script src="/Themes/HomePage/js/scrolla.min.js"></script><!-- ON SCROLL CONTENT ANIMTE   -->

<script src="/Themes/HomePage/js/custom.js"></script><!-- CUSTOM FUCTIONS  -->
<script src="/Themes/HomePage/js/shortcode.js"></script><!-- SHORTCODE FUCTIONS  -->
<script src="/Themes/HomePage/js/switcher.js"></script><!-- SWITCHER FUCTIONS  -->
<script src="/Themes/HomePage/js/jquery.bgscroll.js"></script><!-- BACKGROUND SCROLL -->
<script src="/Themes/HomePage/js/tickerNews.min.js"></script><!-- TICKERNEWS-->
<!-- TICKERNEWS FUNCTiON -->
<script type="text/javascript">
    jQuery(function () {
        var timer = !1;
        _Ticker = jQuery("#T1").newsTicker();
        _Ticker.on("mouseenter", function () {
            var __self = this;
            timer = setTimeout(function () {
                __self.pauseTicker();
            }, 200);
        });
        _Ticker.on("mouseleave", function () {
            clearTimeout(timer);
            if (!timer) return !1;
            this.startTicker();
        });
    });
</script>
<!-- REVOLUTION JS FILES -->

<script src="/Themes/HomePage/plugins/revolution/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="/Themes/HomePage/plugins/revolution/revolution/js/jquery.themepunch.revolution.min.js"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="/Themes/HomePage/plugins/revolution/revolution/js/extensions/revolution-plugin.js"></script>

<!-- REVOLUTION SLIDER FUNCTION  ===== -->
<script src="/Themes/HomePage/js/rev-script-1.js"></script>
<script src="/Themes/Admin/app-assets/js/scripts/extensions/ext-component-clipboard.js"></script>
<script src="/Themes/Admin/app-assets/js/scripts/extensions/clpadd.js"></script>


<script type="text/javascript">
    $(document).ready(function () {
        @if(Session::has('contact_us_submit_success'))
        alert('your request submitted successfully');
        @endif
        @if(Session::has('contact_us_submit_error'))
        alert('error at wrong data')
        @endif
        @if(Session::has('contact_us_submit_captcha_error'))
        alert('error at captcha')
        @endif
        @if(Session::has('email_verification_successfully'))
        alert('{{Session::get('email_verification_successfully')}}')
        @endif
        @if(Session::has('error_at_email_verification'))
        alert('{{Session::get('error_at_email_verification')}}')
        @endif
    });
    $(function () {
        $('#reg-form').one('submit', function () {
            $(this).find('button[type="submit"]').attr('disabled', 'disabled');
        });

        $('#frmContact').one('submit', function () {
            $(this).find('button[type="submit"]').attr('disabled', 'disabled');
        });
    });
</script>
</body>

</html>
