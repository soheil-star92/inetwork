<?php

namespace App\DbModels\Dashboard\Setting;

use Illuminate\Database\Eloquent\Model;

class SettingsProfessional extends Model
{
    protected $table='settings_professional';
    public $timestamps=false;
}
