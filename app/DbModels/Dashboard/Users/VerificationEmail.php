<?php

namespace App\DbModels\Dashboard\Users;

use Illuminate\Database\Eloquent\Model;

class VerificationEmail extends Model
{
    protected $table='verification_emails';
}
