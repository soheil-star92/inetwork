<?php

namespace App\Http\Controllers\Dashboard;

use App\DbModels\Dashboard\Lang;
use App\DbModels\Dashboard\News\Keyword;
use App\DbModels\Dashboard\News\KeywordsUseInNewsView;
use App\DbModels\Dashboard\News\LogNewsChange;
use App\DbModels\Dashboard\News\News;
use App\DbModels\Dashboard\News\NewsCategory;
use App\DbModels\Dashboard\News\NewsComment;
use App\DbModels\Dashboard\News\NewsKeyword;
use App\DbModels\Dashboard\News\NewsReporters;
use App\DbModels\Dashboard\News\NewsReporterType;
use App\DbModels\Dashboard\News\NewsState;
use App\DbModels\Dashboard\News\NewsType;
use App\DbModels\Dashboard\News\Note;
use App\DbModels\Dashboard\News\Province;
use App\DbModels\Dashboard\Setting\City;
use App\DbModels\Dashboard\Setting\ContactCategory;
use App\DbModels\Dashboard\Setting\ContactList;
use App\DbModels\Dashboard\Setting\Resource;
use App\Facade\OrganizationInfo;
use Carbon\Carbon;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Hekmatinasser\Verta\Verta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use Mockery\Matcher\Not;
use SoapClient;
use function Sodium\compare;
use Soheili\ImageWorker\ImageWorker;

class NewsController extends Controller
{
	public function addNewsForm()
	{
		$keywords = Keyword::all();
		$newsCategories = NewsCategory::where('parent_id', null)->orderBy('id')->get();
		$newsCategoryChildes = NewsCategory::where('parent_id', $newsCategories[0]['id'])->orderBy('id')->get();
		return view('Dashboard.news.addNewsForm', compact('keywords', 'newsCategories', 'newsCategoryChildes'));
	}

	public function GetNewsCategoryChildesByAjax(Request $request)
	{
		try {
			$request->validate([
					'id' => 'required'
			]);
			$Childes = NewsCategory::where('parent_id', $request->id)
					->orderBy('id')
					->get();
			$message = array(
					'state' => '1',
					'message' => $Childes
			);
			return response()->json($message);
		} catch (\Exception $e) {
			$message = array(
					'state' => '0',
					'message' => $e->getMessage()
			);
			return response()->json($message);
		}
	}

	public function keywords()
	{
		$keywords = DB::select('CALL sp_keywords_use_in_news');
		$result=new Paginator($keywords,2,1,[]);
		return view('Dashboard.news.keywords', compact('keywords', 'keywordUseInNewsCount','result'));
	}

	public function addKeyword(Request $request)
	{
		$request->validate([
				'txtKeyWord' => 'required'
		]);
		$title = str_replace(' ', '_', $request->txtKeyWord) . '#';
		$keyWord = new Keyword();
		$keyWord->Title = $title;
		$keyWord->save();
		Session::flash('add_key_words_success');
		return back();
	}

	public function removeKeyword(Request $request)
	{
		try {
			Keyword::where('id', $request->keyId)->delete();
			$message = array(
					'state' => '1',
					'message' => 'رکورد موردنظر با موفقیت حذف گردید'
			);
			return response()->json($message);
		} catch (\Exception $e) {
			$message = array(
					'state' => '0',
					'message' => $e->getMessage()
			);
			return response()->json($message);
		}
	}

	public function addNews(Request $request)
	{
		try {
			DB::transaction(function () use ($request) {
				$request->validate([
						'Title' => 'required',
						'Content' => 'required',
						'imageFile' => 'image|mimes:jpeg,png,jpg|max:5000',
						'keywords' => 'required'
				]);
				$user = Sentinel::getUser();
				$news = new News();
				$news->Image = 'assets/no-image.png';
				if ($request->hasFile('imageFile')) {
					$IW = new ImageWorker($request, 'imageFile', 'assets/news/');
					$IW->Move();
					$news->Image = $IW->FullPath;
					$IW->GenerateSmallSize();
					unset($IW);
				}
				$newsId = News::orderBy('id', 'DESC')
						->Select('id')
						->first();
				$newsId = is_null($newsId) ? 1 : $newsId['id'];
				$newsId++;

				$news->id = $newsId;
				$news->Title = $request->Title;
				$news->Top_Title = $request->TopTitle;
				$news->Bottom_Title = $request->BottomTitle;
				$news->Content = $request->Content;
				$news->user_id = $user['id'];
				$news->news_category_id = $request->NewsCategoryId;
				$news->news_category_child_id = ($request->NewsCategoryChildID == 0) ? null : $request->NewsCategoryChildID;
				$news->news_state_id = 1;
				$news->Slider = ($request->SliderState == 'on') ? 1 : 0;
				$news->save();
				$keywords = $request->keywords;
				foreach ($keywords as $kw) {
					if (!is_numeric($kw)) {
						$chekKeyword = Keyword::where('Title', $kw . '#')
								->select('id')
								->first();
						$lastKeywordId = Keyword::orderBy('id', 'DESC')->select('id')->limit(1)->first();
						if (is_null($chekKeyword)) {
							$k = new Keyword();
							$k->id = ++$lastKeywordId->id;
							$k->Title = $kw . '#';
							$k->save();
							$key = new NewsKeyword();
							$key->news_id = $newsId;
							$key->keyword_id = $lastKeywordId->id;
							$key->save();
						} else {
							$key = new NewsKeyword();
							$key->news_id = $newsId;
							$key->keyword_id = $chekKeyword->id;
							$key->save();
						}
					} else {
						$key = new NewsKeyword();
						$key->news_id = $newsId;
						$key->keyword_id = $kw;
						$key->save();
					}
				}
			});
			Session::flash('add_news_success');
			return back();
		} catch (\Exception $e) {
			Session::flash('error_at_news_add', $e->getMessage());
			return back()->withInput(Input::all());
		}
	}

	public function newsLists()
	{
		$news = News::join('news_categories', 'news_categories.id', '=', 'news.news_category_id')
				->join('news_states', 'news_states.id', '=', 'news.news_state_id')
				->join('users', 'users.id', '=', 'news.user_id')
				->select(
						'news.*',
						'news_states.Title AS newsStateTitle',
						'news_categories.PersianTitle AS newsCategoryTitle',
						'users.first_name', 'users.last_name'
				)
				->orderBy('id', 'DESC')
				->paginate(100);
		return view('Dashboard.news.newsLists', compact('news'));
	}

	public function SearchInNews(Request $request)
	{
		try {
			$request->validate([
					'searchKey' => 'required'
			]);
			$col = '';
			$equal = '';
			$textSearch = '';
			switch ($request->sctId) {
				case 1:
					$col = 'news.id';
					$equal = '=';
					$textSearch = $request->searchKey;
					break;
				case 2:
					$col = 'news.Top_Title';
					$equal = 'like';
					$textSearch = '%' . $request->searchKey . '%';
					break;
				case 3:
					$col = 'news.Bottom_Title';
					$equal = 'like';
					$textSearch = '%' . $request->searchKey . '%';
					break;
				case 4:
					$col = 'news.Title';
					$equal = 'like';
					$textSearch = '%' . $request->searchKey . '%';
					break;
				case 5:
					$col = 'news.Content';
					$equal = 'like';
					$textSearch = '%' . $request->searchKey . '%';
					break;
			}
			$news = News::where($col, $equal, $textSearch)
					->join('news_categories', 'news_categories.id', '=', 'news.news_category_id')
					->join('news_states', 'news_states.id', '=', 'news.news_state_id')
					->join('users', 'users.id', '=', 'news.user_id')
					->select(
							'news.*',
							'news_states.Title AS newsStateTitle',
							'news_categories.PersianTitle AS newsCategoryTitle',
							'users.first_name', 'users.last_name'
					)
					->orderBy('id', 'DESC')
					->paginate(100);
			return view('Dashboard.news.newsLists', compact('news'));
		} catch (\Exception $e) {
			return redirect('/news/newsLists');
		}
	}

	public function removeNews(Request $request)
	{
		try {
			$news = News::where('id', $request->id)
					->select('Image')
					->first();
			$Lang=Lang::where('id','>',1)->select('Title')->get();
			foreach ($Lang AS $row){
				$LangPath=resource_path().'/lang/'.$row['Title'].'/News/'.$request->id.'.php';
				if(file_exists($LangPath)){
					unlink($LangPath);
				}
			}
			if ($news->Image != 'assets/no-image.png') {
				if (file_exists(public_path(str_replace('assets/news/', 'assets/news/small/', $news->Image)))) {
					unlink(public_path(str_replace('assets/news/', 'assets/news/small/', $news->Image)));
				}
				if (file_exists(public_path($news->Image))) {
					unlink(public_path($news->Image));
				}
			}
			News::where('id', $request->id)->delete();
			NewsKeyword::where('news_id', $request->id)->delete();
			$message = array(
					'state' => 1,
					'message' => 'رکورد مورد نظر با موفقیت حذف گردید'
			);
			return response()->json($message);
		} catch (\Exception $e) {
			$message = array(
					'state' => 0,
					'message' => $e->getMessage()
			);
			return response()->json($message);
		}
	}

	public function updateNewsForm($id)
	{
		$news = News::where('id', $id)
				->first();
		if (!OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.news.access.to.edit.other.user.news'])) {
			$user = Sentinel::getUser();
			if ($user['id'] != $news->user_id) {
				Session::flash('wrong-in-permission');
				return back();
			}
		}
		if (($news->news_state_id == 4) && (!OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.news.access.to.edit.enabled.news']))) {
			Session::flash('wrong-in-edit-enabled-news');
			return back();
		}
		$newsCategories = NewsCategory::where('parent_id', null)->orderBy('id')->get();
		$newsCategoryChildes = NewsCategory::where('parent_id', $news['news_category_id'])->orderBy('id')->get();
		$newsStates = NewsState::all();
		$selectedKeywords = NewsKeyword::where('news_keywords.news_id', $id)
				->join('keywords', 'keywords.id', '=', 'news_keywords.keyword_id')
				->select('keywords.*')
				->get();
		$selectedIdKeywords = array();
		app()->setLocale('en');
		$NewsEn=__('News/'.$id);
		if(!is_array($NewsEn)){
			$NewsEn=[
					'Top_Title'=>'',
					'Bottom_Title'=>'',
					'Title'=>'',
					'Content'=>''
			];
		}
		foreach ($selectedKeywords as $key => $s) {
			$selectedIdKeywords[$key] = $s->id;
		}
		$keywords = Keyword::whereNotIn('id', $selectedIdKeywords)->get();
		$Languages=Lang::where('id','>',1)->get();
		return view('Dashboard.news.updateNewsForm', compact('news', 'keywords', 'selectedKeywords', 'newsCategories', 'newsCategoryChildes', 'newsStates','Languages','NewsEn'));
	}


	public function updateNews(Request $request)
	{
		try {
			$request->validate([
					'Title' => 'required',
					'Content' => 'required',
					'NewsCategoryId' => 'required',
					'fileupload' => 'image|mimes:jpeg,png,jpg|max:5000',
			]);
			DB::transaction(function () use ($request) {
				$newsId = $request->NewsId;
				$newsEdited = News::where('id', $newsId)
						->select('Image')
						->first();
				if ($request->hasFile('fileupload')) {
					if ($newsEdited->Image != "assets/no-image.png" && file_exists(public_path($request->NewsImage))) {
						unlink(public_path($request->NewsImage));
						if (file_exists(public_path(str_replace('assets/news/', 'assets/news/small', $request->NewsImage)))) {
							unlink(public_path(str_replace('assets/news/', 'assets/news/small', $request->NewsImage)));
						}
					}
					$IW = new ImageWorker($request, 'fileupload', 'assets/news/');
					$IW->Move();
					News::where('id', $newsId)->update(['Image' => $IW->FullPath]);
					$IW->GenerateSmallSize();
					unset($IW);
				}
				News::where('id', $newsId)
						->update([
								'Title' => $request->Title,
								'Top_Title' => $request->TopTitle,
								'Bottom_Title' => $request->BottomTitle,
								'Content' => $request->Content,
								'news_category_id' => $request->NewsCategoryId,
								'news_category_child_id' => ($request->NewsCategoryChildID == 0) ? null : $request->NewsCategoryChildID,
								'Slider' => ($request->SliderState == 'on') ? 1 : 0
						]);
				if (($request->newsImgRemove == 'on') && ($newsEdited->Image != 'assets/no-image.png')) {
					if (file_exists(public_path($newsEdited->Image))) {
						unlink(public_path($newsEdited->Image));
						News::where('id', $newsId)
								->update([
										'Image' => 'assets/no-image.png',
								]);
					}
					if (file_exists(public_path(str_replace('assets/news/', 'assets/news/small/', $newsEdited->Image)))) {
						unlink(public_path(str_replace('assets/news/', 'assets/news/small/', $newsEdited->Image)));
					}
				}
				if (OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.news.allow.change.news.state'])) {
					News::where('id', $newsId)
							->update([
									'news_state_id' => $request->NewsStateId
							]);
				}
				NewsKeyword::where('News_id', $newsId)
						->delete();
				$keywords = $request->Keywords;
				foreach ($keywords as $kw) {
					if (!is_numeric($kw)) {
						$chekKeyword = Keyword::where('Title', $kw . '#')
								->select('id')
								->first();
						$lastKeywordId = Keyword::orderBy('id', 'DESC')->select('id')->limit(1)->first();
						if (is_null($chekKeyword)) {
							$k = new Keyword();
							$k->id = ++$lastKeywordId->id;
							$k->Title = $kw . '#';
							$k->save();
							$key = new NewsKeyword();
							$key->news_id = $newsId;
							$key->keyword_id = $lastKeywordId->id;
							$key->save();
						} else {
							$key = new NewsKeyword();
							$key->news_id = $newsId;
							$key->keyword_id = $chekKeyword->id;
							$key->save();
						}
					} else {
						$key = new NewsKeyword();
						$key->news_id = $newsId;
						$key->keyword_id = $kw;
						$key->save();
					}
				}
			});
			Session::flash('update_news_success');
			return redirect('/news/newsLists');
		} catch (\Exception $e) {
			Session::flash('error_at_news_update', $e->getMessage());
			return back()->withInput(Input::all());
		}
	}

	public
	function changeSliderState(Request $request)
	{
		try {
			$news = News::where('id', $request->id)
					->select('Slider')
					->first();
			if ($news->Slider == 0) {
				News::where('id', $request->id)
						->update([
								'Slider' => 1
						]);
				$msg = 'خبر مورد نظر تبدیل به اسلایدر شد';
			} else {
				News::where('id', $request->id)
						->update([
								'Slider' => 0
						]);
				$msg = 'خبر مورد نظر از حالت اسلایدر خارج شد';
			}
			$message = array(
					'state' => 1,
					'message' => $msg
			);
			return response()->json($message);
		} catch (\Exception $e) {
			$message = array(
					'state' => 0,
					'message' => $e->getMessage()
			);
			return response()->json($message);
		}
	}

	public
	function sliderNews()
	{
		$news = News::where('Slider', 1)
				->join('news_categories', 'news_categories.id', '=', 'news.news_category_id')
				->join('news_states', 'news_states.id', '=', 'news.news_state_id')
				->join('users', 'users.id', '=', 'news.user_id')
				->join('provinces', 'provinces.id', '=', 'news.province_id')
				->select(
						'news.*',
						'news_states.Title AS newsStateTitle',
						'news_categories.PersianTitle AS newsCategoryTitle',
						'users.first_name', 'users.last_name',
						'provinces.Title AS provinceTitle'
				)
				->orderBy('id', 'DESC')
				->get();
		return view('Dashboard.news.sliderNewsLists', compact('news'));
	}

	public
	function myNews()
	{
		$user = Sentinel::getUser();
		$news = News::where('user_id', $user['id'])
				->join('news_categories', 'news_categories.id', '=', 'news.news_category_id')
				->join('news_states', 'news_states.id', '=', 'news.news_state_id')
				->join('users', 'users.id', '=', 'news.user_id')
				->select(
						'news.*',
						'news_states.Title AS newsStateTitle',
						'news_categories.PersianTitle AS newsCategoryTitle',
						'users.first_name', 'users.last_name'
				)
				->orderBy('id', 'DESC')
				->paginate(100);
		return view('Dashboard.news.myNewsLists', compact('news'));
	}

	public
	function showNewsComments($id)
	{
		$comments = NewsComment::where('news_id', $id)
				->orderBy('id', 'DESC')
				->get();
		$news = News::where('id', $id)
				->select('Title')
				->first();
		return view('Dashboard.news.showNewsCommentLists', compact('comments', 'news'));
	}

	public
	function showNewsCommentAjax(Request $request)
	{
		try {
			$comment = NewsComment::where('id', $request->id)
					->select('Message')
					->first();
			$message = array(
					'state' => 1,
					'message' => $comment
			);
			return response()->json($message);
		} catch (\Exception $e) {
			$message = array(
					'state' => 0,
					'message' => $e->getMessage()
			);
			return response()->json($message);
		}
	}

	public
	function changeNewsCommentState(Request $request)
	{
		$request->validate([
				'commentId' => 'required'
		]);
		if ($request->commentStateId == 2) {
			NewsComment::where('id', $request->commentId)
					->delete();
		} else {
			$cmt = NewsComment::where('id', $request->commentId)
					->select('State')
					->first();
			if ($cmt->State == 0) {
				NewsComment::where('id', $request->commentId)
						->update([
								'State' => 1
						]);
			} else {
				NewsComment::where('id', $request->commentId)
						->update([
								'State' => 0
						]);
			}
		}
		Session::flash('update_news_comment_success');
		return back();
	}

	public
	function noteLists()
	{
		$notes = Note::orderBy('notes.id', 'DESC')
				->join('users', 'users.id', '=', 'notes.user_id')
				->join('news_states', 'news_states.id', '=', 'notes.news_state_id')
				->join('news_reporters', 'news_reporters.id', '=', 'notes.reporter_id')
				->select(
						'notes.*',
						'users.first_name', 'users.last_name',
						'news_states.Title AS StateTitle',
						'news_reporters.first_name AS ReporterFName',
						'news_reporters.last_name AS ReporterLName'
				)
				->paginate(50);
		$states = NewsState::orderBy('id', 'ASC')
				->get();
		$reporters = NewsReporters::orderBy('id')->get();
		return view('Dashboard.news.noteLists', compact('notes', 'states', 'reporters'));
	}

	public
	function addNote(Request $request)
	{
		$request->validate([
				'Title' => 'required',
				'Content' => 'required',
				'Image' => 'image|mimes:jpeg,png,jpg|max:5000',
		]);
		$user = Sentinel::getUser();
		$note = new Note();
		$note->Title = $request->Title;
		$note->Image = '/assets/no-image.png';
		if ($request->hasFile('Image')) {
			$filePath = rand(5, 15) . time() . '.' . $request->Image->getClientOriginalExtension();
			$request->Image->move(public_path('/otherFiles/Notes/'), $filePath);
			$note->Image = '/otherFiles/Notes/' . $filePath;
			$changeSizeImg = Image::make('otherFiles/Notes/' . $filePath);
			$changeSizeImg->resize(400, 400);
			$changeSizeImg->save('otherFiles/Notes/' . $filePath);
		}
		$note->reporter_id = $request->ReporterId;
		$note->Content = $request->Content;
		$note->user_id = $user['id'];
		$note->save();
		Session::flash('add_note_success');
		return back();
	}

	public
	function removeNote(Request $request)
	{
		try {
			$note = Note::where('id', $request->id)
					->select('Image')
					->first();
			if ($note->Image != '/assets/no-image.png') {
				if (file_exists(public_path($note->Image))) {
					unlink(public_path($note->Image));
				}
			}
			Note::where('id', $request->id)
					->delete();
			$message = array(
					'state' => 1,
					'message' => 'رکورد مورد نظر با موفقیت حذف گردید'
			);
			return response()->json($message);
		} catch (\Exception $e) {
			$message = array(
					'state' => 0,
					'message' => $e->getMessage()
			);
			return response()->json($message);
		}
	}

	public
	function showNews(Request $request)
	{
		try {
			$note = Note::where('id', $request->id)
					->select('id', 'Title', 'Content', 'reporter_id', 'Image')
					->first();
			$message = array(
					'state' => 1,
					'message' => $note
			);
			return response()->json($message);
		} catch (\Exception $e) {
			$message = array(
					'state' => 0,
					'message' => $e->getMessage()
			);
			return response()->json($message);
		}
	}

	public
	function editNote(Request $request)
	{
		$request->validate([
				'showTitle' => 'required',
				'showContent' => 'required',
				'noteId' => 'required',
				'ShowImage' => 'image|mimes:jpeg,png,jpg|max:5000'
		]);
		if ($request->hasFile('ShowImage')) {
			$note = Note::where('id', $request->noteId)
					->select('Image')
					->first();
			if ($note->Image != '/assets/no-image.png') {
				if (file_exists(public_path($note->Image))) {
					unlink(public_path($note->Image));
				}
			}
			$filePath = rand(5, 15) . time() . '.' . $request->ShowImage->getClientOriginalExtension();
			$request->ShowImage->move(public_path('/otherFiles/Notes/'), $filePath);
			Note::where('id', $request->noteId)
					->update([
							'Image' => '/otherFiles/Notes/' . $filePath
					]);
			$changeSizeImg = Image::make('otherFiles/Notes/' . $filePath);
			$changeSizeImg->resize(400, 400);
			$changeSizeImg->save('otherFiles/Notes/' . $filePath);
		}
		Note::where('id', $request->noteId)
				->update([
						'Title' => $request->showTitle,
						'Content' => $request->showContent,
						'news_state_id' => $request->noteState,
						'reporter_id' => $request->ShowReporterId,
				]);
		unset($request, $note, $filePath);
		Session::flash('edit_note_success');
		return back();
	}

	public
	function getCityByProvince(Request $request)
	{
		try {
			$cities = City::where('province_id', $request->id)
					->orderBy('id')
					->get();
			$message = array(
					'state' => 1,
					'message' => $cities
			);
			return response()->json($message);
		} catch (\Exception $e) {
			$message = array(
					'state' => 0,
					'message' => $e->getMessage()
			);
			return response()->json($message);
		}
	}

	function keywordShow(Request $request)
	{
		try {
			$keys = Keyword::where('id', $request->id)
					->first();
			unset($request);
			$message = array(
					'state' => 1,
					'message' => $keys
			);
			return response()->json($message);
		} catch (\Exception $e) {
			$message = array(
					'state' => 0,
					'message' => $e->getMessage()
			);
			return response()->json($message);
		}
	}

	public function noteNewsLists()
	{
		$news = News::where('news.note', 1)
				->join('news_categories', 'news_categories.id', '=', 'news.news_category_id')
				->join('news_states', 'news_states.id', '=', 'news.news_state_id')
				->join('users', 'users.id', '=', 'news.user_id')
				->join('provinces', 'provinces.id', '=', 'news.province_id')
				->select(
						'news.*',
						'news_states.Title AS newsStateTitle',
						'news_categories.PersianTitle AS newsCategoryTitle',
						'users.first_name', 'users.last_name',
						'provinces.Title AS provinceTitle'
				)
				->orderBy('id', 'DESC')
				->paginate(100);
		return view('Dashboard.news.noteNewsLists', compact('news'));
	}

	public function keywordsEdit(Request $request)
	{
		$request->validate([
				'keywordId' => 'required',
				'ShowTitle' => 'required'
		]);
		$keyword = '#' . str_replace(' ', '_', $request->ShowTitle);
		Keyword::where('id', $request->keywordId)
				->update([
						'Title' => $keyword
				]);
		unset($request, $keyword);
		Session::flash('keyword-edit-success');
		return back();
	}

	public function logNewsChangesShow(Request $request)
	{
		try {
			$LogNewsChanges = DB::select('CALL sp_log_news_changes_show(' . $request->id . ')');
			$result = '';
			$newsTitle = '';
			foreach ($LogNewsChanges AS $key => $l) {
				$newsTitle = $l->NewsTitle;
				$result .= "<tr>";
				$result .= "<td>" . ++$key . "</td>";
				$result .= "<td><img height='50' width='50' src='/" . $l->Image . "'>";
				$result .= "</img></td>";
				$result .= "<td>" . $l->UserFullName . "</td>";
				$result .= "<td>" . $l->LogNewsChangeTitle . "</td>";
				$da = explode(' ', $l->created_at);
				list($y, $m, $d) = explode('-', $da[0]);
				$jD = Verta::getJalali($y, $m, $d);
				$jdate = $jD[0] . '/' . $jD[1] . '/' . $jD[2] . ' ' . $da[1];
				$result .= "<td>" . $jdate . "</td>";
				$result .= '</tr>';
			}
			unset($request, $d, $y, $m, $da, $jD, $jdate);
			$message = array(
					'state' => 1,
					'message' => $result,
					'newsTitle' => $newsTitle
			);
			return response()->json($message);
		} catch (\Exception $e) {
			$message = array(
					'state' => 0,
					'message' => $e->getMessage()
			);
			return response()->json($message);
		}
	}

	public function SearchInKeywords(Request $request)
	{
		try {
			$key = str_replace(' ', '_', $request->searchKey);
			$keywords = KeywordsUseInNewsView::where('Title', 'like', '%' . $key . '%')
					->paginate(100);
			return view('Dashboard.news.keywords', compact('keywords', 'keywordUseInNewsCount'));
		} catch (\Exception $e) {
			return \redirect('/news/keywords');
		}
	}

	public function topKeywordsStatic()
	{
		$keywords = DB::select('CALL sp_top_news_keyword_last_month()');
		return view('Dashboard.news.topKeywordsStatic', compact('keywords'));
	}

	public function NewsAddInOtherLanguage(Request $request)
	{
		try {
			$request->validate([
					'NID' => 'required',
			]);
			$lang = $request->SelectLanguage;
			$Path = resource_path() . '/lang/' . $lang . '/News/' . $request->NID . '.php';
			if (!file_exists($Path)) {
				$CF = fopen($Path, 'w');
				$Content = "<?php
			return [
				'Top_Title'=>'" . $request->LanguageTopTitle . "',
				'Bottom_Title'=>'" . $request->LanguageBottomTitle . "',
				'Title'=>'" . $request->LanguageTitle . "',
				'Content'=>'" . str_replace('\'','"',$request->LanguageContent) . "',
			];";
				fwrite($CF, $Content);
				fclose($CF);
			}
			Session::flash('news_add_in_other_language_success');
			return back();
		}catch (\Exception $e){
			Session::flash('error_at_add_other_language',$e->getMessage());
			return back();
		}
	}

	public function NewsUpdateInOtherLanguage(Request $request)
	{
		$request->validate([
				'NID'=>'required'
		]);
		$lang=$request->SelectLanguage;
		$Path=resource_path() . '/lang/' . $lang . '/News/' . $request->NID . '.php';
		if(file_exists($Path)) {
			unlink($Path);
			$CF = fopen($Path, 'w');
			$Content = "<?php
			return [
				'Top_Title'=>'" . $request->ShowLanguageTopTitle . "',
				'Bottom_Title'=>'" . $request->ShowLanguageBottomTitle . "',
				'Title'=>'" . $request->ShowLanguageTitle . "',
				'Content'=>'" . str_replace('\'','"',$request->ShowLanguageContent) . "',
			];";
			fwrite($CF,$Content);
			fclose($CF);
		}
		Session::flash('news_update_in_other_language_success');
		return back();
	}
}
