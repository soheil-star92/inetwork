<?php

namespace App\Http\Controllers\Dashboard;

use App\DbModels\Dashboard\Support\Notification;
use App\DbModels\Dashboard\Support\SupportDetail;
use App\DbModels\Dashboard\Support\SupportHeader;
use App\DbModels\Dashboard\Users\InfoUser;
use App\DbModels\Dashboard\Users\User;
use App\MyClasses\CmsFunctions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Soheili\ImageWorker\ImageWorker;

class SupportController extends Controller
{
    public function RequestList()
    {
        $UID = CmsFunctions::GetUID();
        $Rows = SupportHeader::where('user_id', $UID)->orderBy('id', 'DESC')->paginate(100);
        return view('Dashboard.Support.UserRequestList', compact('Rows'));
    }

    public function TotalRequestList()
    {
        $UID = CmsFunctions::GetUID();
        $Rows = SupportHeader::join('users','users.id','=','support_headers.user_id')
            ->select('users.email','users.first_name','users.last_name','support_headers.*')
            ->orderBy('id', 'DESC')->paginate(100);
        return view('Dashboard.Support.UserRequestList', compact('Rows'));
    }

    public function ShowDetails($id)
    {
        $id=CmsFunctions::DecodedText($id);
        $UID = CmsFunctions::GetUID();
        $Rows = SupportDetail::where('support_details.support_header_id', $id)
            ->join('support_headers','support_headers.id','=','support_details.support_header_id')
            ->join('info_users','info_users.user_id','=','support_details.user_id')
            ->select('support_headers.Title','support_headers.user_id as SHuid','support_details.*','info_users.Image')
            ->orderBy('support_details.id', 'DESC')->get();
        $itself = ($Rows[0]['SHuid'] != $UID) ? 0 : 1;
        if($itself){
            SupportDetail::where([
                ['support_header_id','=',$id],
                ['itself','=',$itself]
            ])->update([
                'Seen'=>1
            ]);
        }else{
            SupportDetail::where([
                ['support_header_id','=',$id],
                ['itself','=',$itself]
            ])->update([
                'Seen'=>1
            ]);
        }
        return view('Dashboard.Support.ShowDetails', compact('Rows','id','UID','itself'));
    }

    public function addNewRequest(Request $request)
    {
        try {
            $request->validate([
                'iptTitle' => 'required',
                'iptContext' => 'required',
                'iptAttachment' => 'image|mimes:jpeg,png,jpg|max:5000',
            ]);
            $Path = null;
            $UID=CmsFunctions::GetUID();
            if ($request->hasFile('iptAttachment')) {
                $IW = new ImageWorker($request, 'iptAttachment', 'assets/attachment/');
                $IW->Move();
                $Path = $IW->FullPath;
                unset($IW);
            }
            $getSupportHeader=SupportHeader::orderBy('id','DESC')->select('id')->first();
            $RID=empty($getSupportHeader) ? 1 : ++$getSupportHeader->id;
            SupportHeader::insert([
                'id'=>$RID,
                'user_id'=>$UID,
                'Title'=>$request->iptTitle,
                'Language'=>$request->iptLanguage,
            ]);
            SupportDetail::insert([
                'support_header_id'=>$RID,
                'itself'=>1,
                'user_id'=>$UID,
                'Comments'=>$request->iptContext,
                'attachments'=>$Path
            ]);
            Session::flash('add_support_request_successfully', config('msg.success'));
            return back();
        } catch (\Exception $e) {
            Session::flash('error_at_add_support_request', $e->getMessage());
            return back();
        }
    }

    public function SubmitResponse(Request $request)
    {
        try {
            $request->validate([
                'id' => 'required',
                'iptItself' => 'required',
                'iptContext' => 'required'
            ]);
            $RID = CmsFunctions::DecodedText($request->id);
            $itself=CmsFunctions::DecodedText($request->iptItself);
            $Comments=$request->iptContext;
            if(is_null($Comments) || $Comments=='' || strlen($Comments) == 0)
            {
                $message = array(
                    'state' => 0,
                    'message' => 'error at string null'
                );
                return response()->json($message);
            }
            if($itself) {
                SupportHeader::where('id', $RID)->update([
                    'Status' => 0
                ]);
            }else{
                SupportHeader::where('id', $RID)->update([
                    'Status' => 1
                ]);
            }
            SupportDetail::insert([
                'support_header_id'=>$RID,
                'itself'=>$itself,
                'user_id'=>CmsFunctions::GetUID(),
                'Comments'=>$Comments,
                'attachments'=>null
            ]);
            $message = array(
                'state' => 1,
                'message' => config('msg.success')
            );
            return response()->json($message);
        } catch (\Exception $e) {
            $message = array(
                'state' => 0,
                'message' => $e->getMessage()
            );
            return response()->json($message);
        }
    }

    public function RemoveSupportRow(Request $request)
    {
        try {
            $request->validate([
                'id' => 'required',
            ]);
            $RID = $request->id;
            SupportHeader::where('id',$RID)->delete();
            $message = array(
                'state' => 1,
                'message' => config('msg.success')
            );
            return response()->json($message);
        } catch (\Exception $e) {
            $message = array(
                'state' => 0,
                'message' => $e->getMessage()
            );
            return response()->json($message);
        }
    }

    public function GetNotificationInfo(Request $request)
    {
        try {
            $request->validate([
                'id' => 'required',
            ]);
            $RID = CmsFunctions::DecodedText($request->id);
            $row=Notification::where('id',$RID)->first();
            Notification::where('id',$RID)->update(['Seen'=>1]);
            $message = array(
                'state' => 1,
                'message' => $row
            );
            return response()->json($message);
        } catch (\Exception $e) {
            $message = array(
                'state' => 0,
                'message' => $e->getMessage()
            );
            return response()->json($message);
        }
    }

    public function RemoveUserNotification(Request $request)
    {
        try {
            $request->validate([
                'id' => 'required',
            ]);
            Notification::where('id',$request->id)->delete();
            $message = array(
                'state' => 1,
                'message' => ''
            );
            return response()->json($message);
        } catch (\Exception $e) {
            $message = array(
                'state' => 0,
                'message' => $e->getMessage()
            );
            return response()->json($message);
        }
    }


    public function UserNotification()
    {
        $UID = CmsFunctions::GetUID();
        $Rows = Notification::where('notifications.to_user_id',$UID)
            ->join('users','users.id','=','notifications.to_user_id')
            ->select('users.email','users.first_name','users.last_name','notifications.*')
                ->orderBy('notifications.id','DESC')->paginate(100);
        return view('Dashboard.Support.UserNotification', compact('Rows'));
    }
    public function TotalNotification()
    {
        $Rows = Notification::join('users','users.id','=','notifications.to_user_id')
            ->select('users.email','users.first_name','users.last_name','notifications.*')
            ->orderBy('notifications.id','DESC')->paginate(100);
        return view('Dashboard.Support.UserNotification', compact('Rows'));
    }

    public function addNotification(Request $request)
    {
        try {
            $request->validate([
                'iptTitle' => 'required',
                'iptContext' => 'required',
                'iptUserState' => 'required'
            ]);
            $Path = null;
            $UID=CmsFunctions::GetUID();
            $UserState=$request->iptUserState;
            if($UserState == 1){
                $Users=InfoUser::where([
                    ['user_id','<>',$UID],
                    ['Blocked','=',0],
                ])->select('user_id AS UID')->get();
                foreach ($Users as $uRow){
                    Notification::insert([
                        'from_user_id'=>$UID,
                        'to_user_id'=>$uRow->UID,
                        'Title'=>$request->iptTitle,
                        'Context'=>$request->iptContext,
                    ]);
                }
            }else{
                $Users=implode(',',$request->input('iptMultiUser'));
                $ProgressUsers=explode(',',$Users);
                foreach ($ProgressUsers as $uRow){
                    Notification::insert([
                        'from_user_id'=>$UID,
                        'to_user_id'=>$uRow,
                        'Title'=>$request->iptTitle,
                        'Context'=>$request->iptContext,
                    ]);
                }
            }
            Session::flash('add_support_notification_successfully', config('msg.success'));
            return back();
        } catch (\Exception $e) {
            Session::flash('error_at_add_support_notification', $e->getMessage());
            return back();
        }
    }
}
