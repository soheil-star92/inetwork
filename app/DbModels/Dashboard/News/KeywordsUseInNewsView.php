<?php

namespace App\DbModels\Dashboard\News;

use Illuminate\Database\Eloquent\Model;

class KeywordsUseInNewsView extends Model
{
    protected $table='keywords_use_in_news_count_view';
}
