<?php

namespace App\DbModels\Dashboard\News;

use Illuminate\Database\Eloquent\Model;

class NewsKeyword extends Model
{
    public $timestamps=false;
}
