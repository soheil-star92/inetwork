@php
    use App\DbModels\Dashboard\Fund\UserLevel;
    $settings=\App\Facade\DbSettings::GetDbSetting();
    $userLogged=\App\Facade\OrganizationInfo::GetUserInfoWithoutParameter();
    $RegisteredEmail=$userLogged->RegisteredEmail;
@endphp

@section('CssFiles')

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/extensions/jstree.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/bordered-layout.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/css/plugins/extensions/ext-component-tree.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/assets/css/style.css">
    <!-- END: Custom CSS-->


@stop
@extends('Layouts.dashboardMainLayout')

@section('MainContent')
    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">User Level</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/index">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#">Branches</a>
                                    </li>
                                    <li class="breadcrumb-item active">UserLevel
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Tree section -->
                <section class="basic-custom-icons-tree">
                    <div class="row">
                        <!-- Basic Tree -->
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Branches</h4>
                                </div>
                                <div class="card-body">
                                    <div id="jstree-basic">
                                        <ul>
                                            @for($i=1;$i<=21;$i++)
                                                <li data-jstree='{"icon" : "far fa-folder"}'>
                                                    Level {{$i}}
                                                    @php
                                                        $UserLevels=UserLevel::where([
                                                            ['user_levels.parent_user_id','=',$UID],
                                                            ['user_levels.LevelID','=',$i]
                                                        ])->join('users','users.id','=','user_levels.child_user_id')
                                                            ->select('users.id','users.email','users.first_name','users.last_name')
                                                            ->orderBy('user_levels.id')
                                                            ->get();
                                                    @endphp
                                                    @unless(empty($UserLevels))
                                                        <ul>
                                                            @foreach($UserLevels as $subRow)
                                                                <li data-jstree='{"icon" : "far fa-folder"}'>
                                                                    @php
                                                                        $UserPackages=\App\DbModels\Dashboard\Package\PackageUser::where('user_id',$subRow->id)
                                                                    ->join('packages','packages.id','=','package_users.package_id')
                                                                    ->select('package_users.*','packages.Name')
                                                                    ->orderBy('package_users.id')
                                                                    ->get();
                                                                    @endphp
                                                                    {{$subRow->first_name.' '.$subRow->last_name.' ( '.$subRow->email.' ) | Package Count: '.count($UserPackages)}}
                                                                    @unless(empty($UserPackages))
                                                                        <ul>
                                                                            @foreach($UserPackages as $upRow)
                                                                                <li data-jstree='{"icon" : "far fa-file-image"}'>{{$upRow->Name.' ( '.$upRow->created_at.' ) '}}</li>
                                                                            @endforeach
                                                                        </ul>
                                                                    @endunless
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    @endunless
                                                </li>
                                            @endfor
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Basic Tree -->

                    </div>
                </section>


            </div>
        </div>
    </div>
    <!-- END: Content-->

@stop

@section('JsFiles')

    <!-- BEGIN: Vendor JS-->
    <script src="/Themes/Admin/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/jstree.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/Themes/Admin/app-assets/js/core/app-menu.js"></script>
    <script src="/Themes/Admin/app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="/Themes/Admin/app-assets/js/scripts/extensions/ext-component-tree.js"></script>
    <!-- END: Page JS-->

    <script>
        $(window).on('load', function () {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>
@stop
