$('#keyWordDataTable').DataTable({
    "language": {
        "lengthMenu": "نمایش _MENU_ رکورد در صفحه",
        "zeroRecords": "متاسفم، چیزی پیدا نکردم",
        "info": "نمایش updateNewsFormصفحه _PAGE_ از _PAGES_",
        "infoEmpty": "هیچ رکوردی در دسترس نیست",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "search": "جست و جو: "
    },
    "paging": false,
    "info": false,
    stateSave: false
    // scrollY:        300,
    // scrollCollapse: true,
    // fixedColumns:   true
});
