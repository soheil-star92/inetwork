@php
    $settings=\App\Facade\DbSettings::GetDbSetting();
    $userLogged=\App\Facade\OrganizationInfo::GetUserInfoWithoutParameter();
    $RegisteredEmail=$userLogged->RegisteredEmail;
@endphp

@section('CssFiles')
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/forms/select/select2.min.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/animate/animate.min.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/extensions/sweetalert2.min.css">
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/vendors/css/tables/datatable/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/vendors/css/tables/datatable/responsive.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/vendors/css/tables/datatable/buttons.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/vendors/css/tables/datatable/rowGroup.bootstrap5.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/bordered-layout.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/plugins/forms/form-validation.css">
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/css/plugins/extensions/ext-component-sweet-alerts.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/pages/modal-create-app.css">
    <!-- END: Page CSS-->
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/css/plugins/extensions/ext-component-sweet-alerts.css">

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/assets/css/style.css">
    <!-- END: Custom CSS-->


@stop
@extends('Layouts.dashboardMainLayout')

@section('MainContent')

    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="app-user-view-account">
                    <div class="row">
                        <!-- User Content -->
                        <div class="col-xl-12 col-lg-12 col-md-12 order-0 order-md-1">
                            <div class="d-flex justify-content-around my-2 pt-75">
                                <div class="d-flex align-items-start me-2">
                                            <span class="badge bg-light-primary p-75 rounded">
                                                <i data-feather="check" class="font-medium-2"></i>
                                            </span>
                                    <div class="ms-75">
                                        @php $GetUI=\App\MyClasses\CmsFunctions::GetUserInfo($UID); @endphp
                                        <small>Account: {{$GetUI->first_name.' '.$GetUI->last_name}}</small>
                                        <h4 class="mb-0">${{\App\MyClasses\CmsFunctions::GetTotalIncomeProfits($GetUI->id)}}
                                            </h4>
                                        <small>Total Income Profits</small>
                                    </div>
                                </div>
                            </div>
                            <!-- User Pills -->
                            <ul class="nav nav-pills mb-2">
                                <li class="nav-item">
                                    <a class="nav-link active" href="/AdminPanel/Package/UserPackages">
                                        <i data-feather="user" class="font-medium-3 me-50"></i>
                                        <span class="fw-bold">User Packages</span></a>
                                </li>
                            </ul>
                            <!--/ User Pills -->

                            <!-- Project table -->
                            <div class="card">
                                <h4 class="card-header">User Package List</h4>
                                <div class="table-responsive">
                                    <ul>
                                    <li>the termination any packages isn't possible before 2 months</li>
                                    <li>be removed all profits deposited to wallet , if termination after 2 to 6 months.</li>
                                    <li>the termination after 6 months,to be deducted 60 percent of all profit deposited to user wallet.</li>
                                    <li> pre requirement for extra package: must be purchase one of the package A,B,C or D </li>
                                    </ul>
                                </div>
                                <div class="table-responsive">
                                    <table id="keyWordDataTable" class="table datatable-project">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Daily PercentProfit</th>
                                            <th>User Price($)</th>
                                            <th class="text-nowrap">Created</th>
                                            <th class="text-nowrap">Expire</th>
                                            <th>Total Income Profits</th>
                                            <th>Status</th>
                                            <th>Operation</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($rows as $key=>$row)
                                            <tr>
                                                <td>{{++$key}}</td>
                                                <td>{{$row->Name}}</td>
                                                <td>{{$row->DailyProfitPercent}}</td>
                                                <td>{{$row->Price}}$</td>
                                                <td>{{$row->created_at}}</td>
                                                <td>{{$row->expire_date}}</td>
                                                <td>
                                                    ${{\App\MyClasses\CmsFunctions::TotalUserPackageProfit($row->id,$row->user_id)}} <a class="btn-icon btn btn-primary btn-round  waves-effect waves-float waves-light" type="button" title="Detail" href="/AdminPanel/Package/TotalUserProfit/{{\App\MyClasses\CmsFunctions::EncodedText($row->user_id.'-'.$row->id)}}" target="_blank" ><i data-feather='twitch'></i></a>
                                                </td>
                                                <td>{!! \App\MyClasses\CmsFunctions::UserPackageStatus($row->Disable) !!}</td>
                                                <td>
                                                    @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.package.admin']))
                                                        <button type="button"  data-id="{{\App\MyClasses\CmsFunctions::EncodedText($row->id)}}" class="btn btn-gradient-danger btnRemoveByAdmin" id="btnRemoveByAdmin" >Remove by Admin</button>
                                                    @endif
                                                    @if($row->Disable == 0)
                                                        @php $monthCheck=\Carbon\Carbon::parse($row->created_at)->addMonth(2); @endphp
                                                        @if(\Carbon\Carbon::now()>=$monthCheck)
                                                            <div class="dropdown">
                                                                <button type="button"
                                                                        class="btn btn-sm dropdown-toggle hide-arrow py-0 waves-effect waves-float waves-light"
                                                                        data-bs-toggle="dropdown" aria-expanded="false">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14"
                                                                         height="14" viewBox="0 0 24 24" fill="none"
                                                                         stroke="currentColor" stroke-width="2"
                                                                         stroke-linecap="round" stroke-linejoin="round"
                                                                         class="feather feather-more-vertical">
                                                                        <circle cx="12" cy="12" r="1"></circle>
                                                                        <circle cx="12" cy="5" r="1"></circle>
                                                                        <circle cx="12" cy="19" r="1"></circle>
                                                                    </svg>
                                                                </button>

                                                                <div class="dropdown-menu dropdown-menu-end" style="">
                                                                    <a id="aShowRow" class="dropdown-item aShowRow"
                                                                       data-id="{{\App\MyClasses\CmsFunctions::EncodedText($row->id)}}"
                                                                    >
                                                                        <i data-feather='slash'></i>
                                                                        <span>Termination</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        @else
                                                            <button type="button" class="btn btn-gradient-danger" disabled>Terminate</button>
                                                        @endif
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /Project table -->

                        </div>
                        <!--/ User Content -->
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->
@stop

@section('JsFiles')
    <!-- BEGIN: Vendor JS-->
    <script src="/Themes/Admin/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="/Themes/Admin/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/forms/cleave/cleave.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/forms/cleave/addons/cleave-phone.us.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/moment.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/dataTables.bootstrap5.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/responsive.bootstrap5.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/jszip.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/polyfill.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/Themes/Admin/app-assets/js/core/app-menu.js"></script>
    <script src="/Themes/Admin/app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-add-new-cc.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/page-pricing.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-add-new-address.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-create-app.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-two-factor-auth.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-edit-user.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-share-project.js"></script>
    <!-- END: Page JS-->


    <!-- BEGIN: Page Vendor JS-->
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/polyfill.min.js"></script>
    <!-- END: Page Vendor JS-->


    <script>
        $(document).ready(function () {
            var confirmPackage = $('.aShowRow');
            if (confirmPackage.length) {
                confirmPackage.on('click', function () {
                    var id = $(this).data('id');
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Yes, terminate it!',
                        customClass: {
                            confirmButton: 'btn btn-primary',
                            cancelButton: 'btn btn-outline-danger ms-1'
                        },
                        buttonsStyling: false
                    }).then(function (result) {
                        if (result.value) {
                            var token = $('meta[name="csrf-token"]').attr('content');
                            $.ajax({
                                url: '/AdminPanel/Package/UserPackageTermination',
                                data: {_token: token, id: id},
                                type: 'POST',
                                dataType: 'JSON',
                                success: function (data) {
                                    if (data.state == 1) {
                                        toastr['success'](data.message, 'Success', {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            progressBar: true
                                        });
                                    } else {
                                        toastr['error'](data.message, 'Error', {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            progressBar: true
                                        });
                                    }
                                }
                            });
                        }
                    });
                });
            }

        var btnRemoveByAdmin = $('.btnRemoveByAdmin');
            if (btnRemoveByAdmin.length) {
                btnRemoveByAdmin.on('click', function () {
                    var id = $(this).data('id');
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Yes, Remove it!',
                        customClass: {
                            confirmButton: 'btn btn-primary',
                            cancelButton: 'btn btn-outline-danger ms-1'
                        },
                        buttonsStyling: false
                    }).then(function (result) {
                        if (result.value) {
                            var token = $('meta[name="csrf-token"]').attr('content');
                            $.ajax({
                                url: '/AdminPanel/Package/RemovePackageByAdmin',
                                data: {_token: token, id: id},
                                type: 'POST',
                                dataType: 'JSON',
                                success: function (data) {
                                    if (data.state == 1) {
                                        toastr['success'](data.message, 'Success', {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            progressBar: true
                                        });
                                        location.reload();
                                    } else {
                                        toastr['error'](data.message, 'Error', {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            progressBar: true
                                        });
                                    }
                                }
                            });
                        }
                    });
                });
            }

            $('#keyWordDataTable').DataTable({
                "language": {
                    "lengthMenu": "نمایش _MENU_ رکورد در صفحه",
                    "zeroRecords": "Info Empty",
                    "info": "نمایش updateNewsFormصفحه _PAGE_ از _PAGES_",
                    "infoEmpty": "Info Empty",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "search": "Search: "
                },
                "paging": false,
                "info": false,
                stateSave: false
                // scrollY:        300,
                // scrollCollapse: true,
                // fixedColumns:   true
            });

        });

        $(window).on('load', function () {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>
@stop
