@extends('Layouts.dashboardMainLayout')
@section('BreadCrumbLevel')
    <li class="active">نمایش کاربر</li>
@Stop
@section('BreadCrumbURL','/users/usersList')
@section('BreadCrumbTitle','لیست کاربران')
@section('CssFiles')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/login/bower_components/fancyBox/jquery.fancybox.min.css') }}">
    <style type="text/css">
        #image-preview {
            width: 200px;
            height: 200px;
            position: relative;
            overflow: hidden;
            background-color: #ffffff;
            color: #ecf0f1;
        }
        #image-preview input {
            line-height: 200px;
            font-size: 200px;
            position: absolute;
            opacity: 0;
            z-index: 10;
        }
        #image-preview label {
            position: absolute;
            z-index: 5;
            opacity: 0.8;
            cursor: pointer;
            background-color: #bdc3c7;
            width: 200px;
            height: 50px;
            font-size: 20px;
            line-height: 50px;
            text-transform: uppercase;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: auto;
            text-align: center;
        }
    </style>
@endsection

@section('MainContent')
    <div class="row">
        <section class="col-lg-12 col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <i class="fa fa-info-circle"></i>
                    <h3 class="box-title">
                        {{ \App\Facade\DbSettings::GetDbSetting()->Title }}
                    </h3>
                    <!-- tools box -->
                    <div class="pull-left box-tools">
                        <button type="button" class="btn bg-info btn-sm" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /. tools -->
                </div>
                <div class="box-body">
                    <div class="content">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">ویرایش اطلاعات کاربری</h3>
                            </div>
                        <!-- /.box-header -->
                            <!-- form start -->
                            @include('Dashboard.toolbars.userToolbar')
                            <form method="post" action="/users/updateUser" id="editC" class="form-horizontal"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="box-body">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">عکس پرسنلی</label>
                                        <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">
                                                <a data-fancybox="images" data-caption="توسعه پرداز ریتا - "
                                                   href="@if($users['Image'] != "assets/no-image.png") {{ asset($users['Image']) }} @else {{ asset('assets/no-image.png') }} @endif">
                                                    <img height="60" width="60" src="@if($users['Image'] != "assets/no-image.png") {{ asset($users['Image']) }} @else {{ asset('assets/no-image.png') }}" @endif"
                                                         alt="توسعه پرداز ریتا">
                                                </a>
                                            <br>
                                                <div id="image-preview">
                                                    <label for="image-upload" id="image-label">عکس جدید</label>
                                                    <input type="file" name="fileupload" id="image-upload" />
                                                </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">کاربری</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="email" class="form-control"
                                                   value="{{ $users['email'] }}"
                                                   disabled>
                                            <input type="hidden" name="userId"
                                                   value="{{ $users['id'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">نام</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="first_name" class="form-control"
                                                   value="{{ $users['first_name'] }}"
                                                   required>
                                            <input type="hidden" name="userId"
                                                   value="{{ $users['id'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">نام خانوادگی</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="last_name" class="form-control"
                                                   value="{{ $users['last_name'] }}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">سطح دسترسی</label>
                                        <select name="RoleId" id="RoleId" class="form-control col-lg-10">
                                            @foreach($roles as $r)
                                                <option value="{{ $r->id }}"
                                                        @if($users['RoleId'] == $r->id) selected @endif>{{ $r->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">تغییر کلمه عبور</label>
                                        <div class="col-sm-10">
                                            <input type="password" name="password" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-info pull-right">به روز رسانی</button>
                                </div>
                                <!-- /.box-footer -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('JsFiles')
    <!-- TinyMCE Editor -->
    <script src="{{ URL::asset('assets/login/bower_components/fancyBox/jquery.fancybox.min.js') }}"></script>
    <script src="{{ URL::asset('assets/login/bower_components/imagePreview/jquery.uploadPreview.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            @if(Session::has('update_user_success'))
            swal("", "رکورد موردنظر با موفقیت ویرایش گردید", "success");
            @endif
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('[data-fancybox="images"]').fancybox({
                buttons: [
                    'slideShow',
                    'fullScreen',
                    'thumbs',
                    'download',
                    'zoom',
                    'close'
                ],
            });

            $.uploadPreview({
                input_field: "#image-upload",   // Default: .image-upload
                preview_box: "#image-preview",  // Default: .image-preview
                label_field: "#image-label",    // Default: .image-label
                label_default: "آپلود عکس جدید",   // Default: Choose File
                label_selected: "تغییر عکس",  // Default: Change File
                no_label: false                 // Default: false
            });

            $('#editC').one('submit',function(){
                $(this).find('input[type="submit"]').attr('disabled','disabled');
            });
        })
    </script>
@endsection