@extends('Layouts.dashboardMainLayout')

@section('CssFiles')
@endsection
@section('BreadCrumbLevel')
    <li class="active">شبکه های اجتماعی</li>
@Stop
@section('BreadCrumbURL','/settings/setting')
@section('BreadCrumbTitle','تنظیمات پایه ای سیستم')
@section('MainContent')
    <div class="row">
        <section class="col-lg-12 col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <i class="fa fa-info-circle"></i>
                    <h3 class="box-title">
                        {{ \App\Facade\DbSettings::GetDbSetting()->Title }}
                    </h3>
                    <!-- tools box -->
                    <div class="pull-left box-tools">
                        <button type="button" class="btn bg-info btn-sm" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /. tools -->
                </div>
                <div class="box-body">
                    <div class="content">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">تنظیمات شبکه های اجتماعی </h3>
                            </div>
                            <div class="box-header with-border">
                                @includeIf('Dashboard.toolbars.settingToolbar')
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <form method="post" id="editC" action="/settings/updateSocialMedia" class="form-horizontal"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="box-body">
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">Instagram</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="Instagram" class="form-control text-left"
                                                   value="{{ $social['Instagram'] }}"
                                                   required>
                                        </div>
                                        <label class="col-sm-1 control-label">Telegram</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="Telegram" class="form-control text-left"
                                                   value="{{ $social['Telegram'] }}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">Facebook</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="Facebook" class="form-control text-left"
                                                   value="{{ $social['Facebook'] }}"
                                                   required>
                                        </div>
                                        <label class="col-sm-1 control-label">Twitter</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="Twitter" class="form-control text-left "
                                                   value="{{ $social['Twitter'] }}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">Soroush</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="Soroush" class="form-control text-left"
                                                   value="{{ $social['Soroush'] }}"
                                                   required>
                                        </div>
                                        <label class="col-sm-1 control-label">iGap</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="iGap" class="form-control text-left"
                                                   value="{{ $social['iGap'] }}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">بله</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="Bale" class="form-control text-left"
                                                   value="{{ $social['Bale'] }}"
                                                   required>
                                        </div>
                                        <label class="col-sm-1 control-label">یوتیوب</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="Youtube" class="form-control text-left"
                                                   value="{{ $social['Youtube'] }}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">آپارات</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="Aparat" class="form-control text-left"
                                                   value="{{ $social['Aparat'] }}"
                                                   required>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-info pull-right">بروزرسانی</button>
                                </div>
                                <!-- /.box-footer -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('JsFiles')
    <script>
        $(document).ready(function () {
            @if(Session::has('update_social_media_success'))
            swal("", "تنظیمات سایت بروزرسانی گردید", "success");
            @endif
        });
    </script>
    <Script>
        $(document).ready(function () {
            $('#editC').one('submit', function () {
                $(this).find('button[type="submit"]').attr('disabled', 'disabled');
            });
        });
    </Script>
@stop