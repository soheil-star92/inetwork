@php
    use \App\MyClasses\CmsFunctions;
    use \App\MyClasses\CmsThemes;
@endphp
@extends('Themes.iNetwork.MainLayout')
@section('Content')
    <!-- INNER PAGE BANNER -->
    <div class="wt-bnr-inr overlay-wraper" style="background-image:url(/Themes/HomePage/images/banner/about-banner.jpg);">
        <div class="overlay-main bg-black opacity-07"></div>
        <div class="container">
            <div class="wt-bnr-inr-entry">
                <h1 class="text-white">Gifts for team building </h1>
            </div>
        </div>
    </div>
    <!-- INNER PAGE BANNER END -->

    <!-- BREADCRUMB ROW -->
    <div class="bg-gray-light p-tb20">
        <div class="container">
            <ul class="wt-breadcrumb breadcrumb-style-2">
                <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
                <li>Gifts</li>
            </ul>
        </div>
    </div>
    <!-- BREADCRUMB  ROW END -->

    <!-- ABOUT COMPANY SECTION START -->
    <div class="section-full p-tb100">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="section-head text-left">
                        <span class="wt-title-subline text-gray-dark font-16 m-b15">How to get gifts from invest Chain network?</span>
                        <h2 class="text-uppercase">A New Kind of Investing </h2>
                        <div class="wt-separator-outer"><div class="wt-separator bg-primary"></div></div>
                        <p><strong>$300 For purchase SmartPhone</strong></p>
                        <p>Eligibility condition: Having 5 people in the first level who have each bought at least B package</p>

                        <p><strong>$1000 For purchase Laptop</strong></p>
                        <p>Eligibility condition: Having 5 people in the first level who have each bought at least C package</p>

                        <p><strong>$6000 For purchase Car</strong></p>
                        <p>Eligibility condition: Having 5 people in the first level who have each bought at least D package</p>

                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="wt-media">
                        <img src="/assets/72976503.png" alt="" class="img-responsive"/>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="section-head text-left">
                      </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ABOUT COMPANY SECTION END -->



@stop
