<?php

namespace App\Http\Controllers\Dashboard;


use App\DbModels\Dashboard\News\NewsCategory;
use App\DbModels\Dashboard\News\Province;
use App\DbModels\Dashboard\Setting\City;
use App\DbModels\Dashboard\Setting\Link;
use App\DbModels\Dashboard\Setting\Setting;
use App\DbModels\Dashboard\Setting\SettingSlider;
use App\DbModels\Dashboard\Setting\SettingsProfessional;
use App\DbModels\Dashboard\Setting\SettingsProfessionalType;
use App\DbModels\Dashboard\Setting\SocialMedia;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Soheili\ImageWorker\ImageWorker;
use Symfony\Component\Finder\Iterator\SortableIterator;

class SettingController extends Controller
{
	/****** TODO Settings Start Controller*******/

	public function setting()
	{
		$settings = Setting::first();
		return view('Dashboard.settings.setting', compact('settings'));
	}

	public function updateSetting(Request $request)
	{
		$request->validate([
				'txtTitle' => 'required',
				'txtDescription' => 'required',
				'txtAboutUs' => 'required',
				'txtKeywords' => 'required',
				'txtEmail' => 'required',
				'txtPhone' => 'required',
				'txtAddress' => 'required',
				'MainLogo' => 'image|mimes:jpg,jpeg,png|max:3000',
				'FavIcon' => 'image|mimes:jpg,jpeg,png|max:3000',
		]);
		DB::transaction(function () use ($request) {
			$Setting = Setting::where('id', 1)
					->select('MainLogo', 'FavIcon')
					->first();

			//MainLogo
			if ($request->hasFile('MainLogo')) {
				if (file_exists(public_path($Setting->MainLogo)) && !is_null($Setting->MainLogo)) {
					unlink(public_path($Setting->MainLogo));
				}
				$IW = new ImageWorker($request, 'MainLogo', '/Files/Settings/');
				$IW->Move();
				Setting::where('id', 1)
						->update([
								'MainLogo' => $IW->FullPath
						]);
				unset($IW);
			}

			//FavIcon
			if ($request->hasFile('FavIcon')) {
				if (file_exists(public_path($Setting->FavIcon)) && !is_null($Setting->FavIcon)) {
					unlink(public_path($Setting->FavIcon));
				}
				$IW = new ImageWorker($request, 'FavIcon', '/Files/Settings/');
				$IW->Move();
				Setting::where('id', 1)
						->update([
								'FavIcon' => $IW->FullPath
						]);
				unset($IW);
			}
			Setting::where('id', 1)
					->update([
							'Title' => $request->txtTitle,
							'AboutUs' => $request->txtAboutUs,
							'Description' => $request->txtDescription,
							'Keywords' => $request->txtKeywords,
							'Email' => $request->txtEmail,
							'Phone' => $request->txtPhone,
							'Address' => $request->txtAddress,
					]);
		});
		Session::flash('update_settings_successful');
		return back();
	}

	public function GetNewsCategoryChildes($id)
	{
		return NewsCategory::where('parent_id', $id)
				->orderBy('id')
				->get();
	}

	public function newsCategory()
	{
		$newsCategories = NewsCategory::where('parent_id', null)
				->orderBy('id')
				->get();
		return view('Dashboard.settings.newsCategory', compact('newsCategories'));
	}

	public function removeNewsCategory(Request $request)
	{
		try {
			$ChildCheck = NewsCategory::where('parent_id', $request->id)
					->select('id', 'Logo')
					->first();
			if (is_null($ChildCheck)) {
				$CheckRecord = NewsCategory::where('id', $request->id)->select('Logo')->first();
				if ((file_exists(public_path($CheckRecord->Logo))) && !is_null($CheckRecord->Logo)) {
					unlink(public_path($CheckRecord->Logo));
				}
				NewsCategory::where('id', $request->id)
						->delete();
				$message = array(
						'state' => 1,
						'message' => 'رکورد مورد نظر با موفقیت حذف گردید'
				);
			} else {
				$message = array(
						'state' => 0,
						'message' => 'خطا: رکورد دارای زیر مجموعه می باشد'
				);
			}

			return response()->json($message);
		} catch (\Exception $e) {
			$message = array(
					'state' => 0,
					'message' => $e->getMessage()
			);
			return response()->json($message);
		}
	}

	public function addNewsCategory(Request $request)
	{
		$request->validate([
				'PersianTitle' => 'required',
				'fileupload' => 'image|mimes:png,jpeg,jpg|max:3000',
		]);
		$ParentID = null;
		$newsCategory = new NewsCategory();
		if ($request->ParentID != 0) {
			$ParentID = $request->ParentID;
		}
		if ($request->hasFile('fileupload')) {
			$IW = new ImageWorker($request, 'fileupload', 'otherFiles/NewsCategories/');
			$IW->Move();
			$newsCategory->Logo = $IW->FullPath;
			unset($IW);
		}
		$newsCategory->PersianTitle = $request->PersianTitle;
		$newsCategory->EnglishTitle = $request->EnglishTitle;
		$newsCategory->parent_id = $ParentID;
		$newsCategory->save();
		Session::flash('add-news-category-success');
		return back();
	}

	public function showNewsCategory(Request $request)
	{
		try {
			$result = '';
			$id = $request->id;
			$newsCategory = NewsCategory::where('id', $id)
					->select('PersianTitle','EnglishTitle', 'Logo')
					->first();
			$MainLogo = "<img src='/" . $newsCategory['Logo'] . "' height='150' width='150' >";
			$Childes = $this->GetNewsCategoryChildes($id);
			foreach ($Childes AS $c) {
				$result .= '<li>';
				$result .= '<a data-id="' . $c->id . '" class="btn bg-purple btn-xs" data-toggle="modal" data-target="#modal-show-news-category-child" title="ویرایش"><span class="fa fa-search"></span> ویرایش </a> ';
				$result .= '<a class="btn bg-red btn-xs" onclick="ChildesRemove(' . $c->id . ');" title="حذف رکورد"><span class="fa fa-trash"></span> حذف </a> ';
				$result .= '<span id="ShowChildPersianTitle' . $c->id . '" data-content="' . $c->PersianTitle . '">' . $c->PersianTitle . '</span>';
				$result .= '<hr>';
				$result .= '</li>';
			}
			$message = array(
					'state' => 1,
					'message' => $newsCategory,
					'result' => $result,
					'MainLogo' => $MainLogo
			);
			return response()->json($message);
		} catch (\Exception $e) {
			$message = array(
					'state' => 0,
					'message' => $e->getMessage()
			);
			return response()->json($message);
		}
	}

	public function NewsCategoryChildUpdate(Request $request)
	{
		try {
			$request->validate([
					'PersianTitle' => 'required',
					'id' => 'required'
			]);
			DB::transaction(function () use ($request) {
				NewsCategory::where('id', $request->id)
						->update([
								'PersianTitle' => $request->PersianTitle
						]);
			});
			unset($request);
			$message = array(
					'state' => 1,
					'message' => 'رکورد مورد نظر با موفقیت ویرایش گردید'
			);
			return response()->json($message);
		} catch (\Exception $e) {
			$message = array(
					'state' => 0,
					'message' => $e->getMessage()
			);
			return response()->json($message);
		}
	}

	public function editNewsCategory(Request $request)
	{
		$request->validate([
				'ShowPersianTitle' => 'required',
				'showNewsCategoryId' => 'required',
				'NewImage' => 'image|mimes:png,jpeg,jpg|max:3000'
		]);
		if ($request->hasFile('NewImage')) {
			$NewsCategory = NewsCategory::where('id', $request->showNewsCategoryId)->select('Logo')->first();
			if ((!is_null($NewsCategory['Logo'])) && (file_exists(public_path($NewsCategory['Logo'])))) {
				unlink(public_path($NewsCategory['Logo']));
			}
			$IW = new ImageWorker($request, 'NewImage', 'otherFiles/NewsCategories/');
			$IW->Move();
			NewsCategory::where('id', $request->showNewsCategoryId)
					->update([
							'Logo' => $IW->FullPath
					]);
			unset($IW);
		}
		NewsCategory::where('id', $request->showNewsCategoryId)
				->update([
						'PersianTitle' => $request->ShowPersianTitle,
						'EnglishTitle' => $request->ShowEnglishTitle
				]);
		Session::flash('edit-news-category-success');
		return back();
	}

	public function editNewsCategoryChild(Request $request)
	{
		$request->validate([
				'ShowPersianTitleChild' => 'required',
				'showNewsCategoryIdChild' => 'required',
				'NewImageChild' => 'image|mimes:png,jpeg,jpg|max:3000'
		]);
		if ($request->hasFile('NewImageChild')) {
			$NewsCategory = NewsCategory::where('id', $request->showNewsCategoryIdChild)->select('Logo')->first();
			if ((!is_null($NewsCategory['Logo'])) && (file_exists(public_path($NewsCategory['Logo'])))) {
				unlink(public_path($NewsCategory['Logo']));
			}
			$IW = new ImageWorker($request, 'NewImageChild', 'otherFiles/NewsCategories/');
			$IW->Move();
			NewsCategory::where('id', $request->showNewsCategoryIdChild)
					->update([
							'Logo' => $IW->FullPath
					]);
			unset($IW);
		}
		NewsCategory::where('id', $request->showNewsCategoryIdChild)
				->update([
						'PersianTitle' => $request->ShowPersianTitleChild,
						'EnglishTitle' => $request->ShowEnglishTitleChild
				]);
		Session::flash('edit-news-category-success');
		return back();
	}

	public function socialMedia()
	{
		$social = SocialMedia::first();
		return view('Dashboard.settings.socialMedia', compact('social'));
	}

	public function updateSocialMedia(Request $request)
	{
		$request->validate([
				'Instagram' => 'required',
				'Soroush' => 'required',
				'iGap' => 'required',
				'Telegram' => 'required',
				'Twitter' => 'required',
				'Bale' => 'required'
		]);
		SocialMedia::where('id', 1)
				->update([
						'Instagram' => $request->Instagram,
						'Soroush' => $request->Soroush,
						'iGap' => $request->iGap,
						'Telegram' => $request->Telegram,
						'Facebook' => $request->Facebook,
						'Twitter' => $request->Twitter,
						'Bale' => $request->Bale,
						'Youtube' => $request->Youtube,
						'Aparat' => $request->Aparat,
				]);
		Session::flash('update_social_media_success');
		return back();
	}

	public function links()
	{
		$links = Link::orderBy('id', 'DESC')
				->get();
		return view('Dashboard.settings.linkLists', compact('links'));
	}

	public function addLink(Request $request)
	{
		$request->validate([
				'Title' => 'required',
				'Link' => 'required'
		]);
		$link = new Link();
		$link->Title = $request->Title;
		$link->Link = $request->Link;
		$link->save();
		Session::flash('add_link_success');
		return back();
	}

	public function changeStateLink(Request $request)
	{
		try {
			$link = Link::where('id', $request->id)
					->select('Disabled')
					->first();
			if ($link->Disabled == 0) {
				Link::where('id', $request->id)
						->update([
								'Disabled' => 1
						]);
			} else {
				Link::where('id', $request->id)
						->update([
								'Disabled' => 0
						]);
			}
			$message = array(
					'state' => 1,
					'message' => 'تغییر وضعیت رکورد با موفقیت انجام گرفت'
			);
			return response()->json($message);
		} catch (\Exception $e) {
			$message = array(
					'state' => 0,
					'message' => $e->getMessage()
			);
			return response()->json($message);
		}
	}

	public function removeLink(Request $request)
	{
		try {
			Link::where('id', $request->id)
					->delete();
			$message = array(
					'state' => 1,
					'message' => 'رکورد موردنظر با موفقیت حذف گردید'
			);
			return response()->json($message);
		} catch (\Exception $e) {
			$message = array(
					'state' => 0,
					'message' => $e->getMessage()
			);
			return response()->json($message);
		}
	}

	public function provinces()
	{
		$provinces = Province::all();
		return view('Dashboard.settings.provincesSetting', compact('provinces'));
	}

	public function addProvince(Request $request)
	{
		$request->validate([
				'Title' => 'required',
		]);
		$p = new Province();
		$p->Title = $request->Title;
		$p->save();
		Session::flash('add-province-success');
		return back();
	}

	public function removeProvince(Request $request)
	{
		try {
			Province::where('id', $request->id)
					->delete();
			$message = array(
					'state' => 1,
					'message' => 'رکورد موردنظر با موفقیت حذف گردید'
			);
			return response()->json($message);
		} catch (\Exception $e) {
			$message = array(
					'state' => 0,
					'message' => $e->getMessage()
			);
			return response()->json($message);
		}
	}

	public function showProvince(Request $request)
	{
		try {
			$province = Province::where('id', $request->id)
					->first();
			$message = array(
					'state' => 1,
					'message' => $province
			);
			return response()->json($message);
		} catch (\Exception $e) {
			$message = array(
					'state' => 0,
					'message' => $e->getMessage()
			);
			return response()->json($message);
		}
	}

	public function editProvince(Request $request)
	{
		$request->validate([
				'showProvinceId' => 'required',
				'showTitle' => 'required',
		]);
		Province::where('id', $request->showProvinceId)
				->update([
						'Title' => $request->showTitle,
						'Disabled' => ($request->showDisabled == 'on') ? 1 : 0,
				]);
		Session::flash('edit-province-success');
		return back();
	}

	public function addCity(Request $request)
	{
		$request->validate([
				'city' => 'required'
		]);
		$city = new City();
		$city->Title = $request->city;
		$city->province_id = $request->provinceId;
		$city->save();
		unset($city, $request);
		Session::flash('add-city-success');
		return back();
	}

	public function ProvinceCitiesShow(Request $request)
	{
		try {
			$cities = City::where('province_id', $request->id)
					->get();
			unset($request);
			$message = array(
					'state' => 1,
					'message' => $cities
			);
			return response()->json($message);
		} catch (\Exception $e) {
			$message = array(
					'state' => 0,
					'message' => $e->getMessage()
			);
			return response()->json($message);
		}
	}

	public function cityRemove(Request $request)
	{
		try {
			City::where('id', $request->id)
					->delete();
			unset($request);
			$message = array(
					'state' => 1,
					'message' => 'رکورد مورد نظر با موفقیت حذف گردید'
			);
			return response()->json($message);
		} catch (\Exception $e) {
			$message = array(
					'state' => 0,
					'message' => $e->getMessage()
			);
			return response()->json($message);
		}
	}

	public function professionalSettings()
	{
		$professionalSettings = SettingsProfessional::all();
		$professionalSettingType = SettingsProfessionalType::all();
		return view('Dashboard.settings.professionalSettings', compact('professionalSettings', 'professionalSettingType'));
	}

	public function UpdateProfessionalSetting(Request $request)
	{
		DB::transaction(function () use ($request) {
			$professionalSettings = SettingsProfessional::all();
			foreach ($professionalSettings AS $p) {
				$chk = $p->Title;
				$checkChk = $request->$chk;
				if ($checkChk) {
					SettingsProfessional::where('id', $p->id)
							->update([
									'State' => 1
							]);
				} else {
					SettingsProfessional::where('id', $p->id)
							->update([
									'State' => 0
							]);
				}
			}
		});
		Session::flash('update_professional_setting_success');
		return back();
	}


	public function CenterSliders()
	{
		$sliders = SettingSlider::all();
		return view('Dashboard.settings.CenterSliders', compact('sliders'));
	}

	public function CenterSliderAdd(Request $request)
	{
		$request->validate([
				'Title' => 'required',
				'fileupload' => 'required|image|mimes:png,jpg,jpeg|max:6000'
		]);
		DB::transaction(function () use ($request) {
			$lastId = SettingSlider::orderBy('Order', 'DESC')
					->select('Order')
					->limit(1)
					->first();
			$SID = is_null($lastId) ? 1 : ++$lastId->Order;
			$slider = new SettingSlider();
			if ($request->hasFile('fileupload')) {
				$IW = new ImageWorker($request, 'fileupload', 'otherFiles/Settings/Sliders/');
				$IW->Move();
				$IW->GenerateSmallSize();
				$IW->ChaneImageSizeToOtherSize();
				$slider->Image = $IW->FullPath;
				unset($IW);
			}
			$slider->Title = $request->Title;
			$slider->Order = $SID;
			$slider->save();
			unset($slider, $imagePath, $SID, $lastId);
		});
		unset($request);
		Session::flash('center_slider_add_success');
		return back();
	}

	public function CenterSliderRemove(Request $request)
	{
		try {
			DB::transaction(function () use ($request) {
				$slider = SettingSlider::where('id', $request->id)
						->select('Image')
						->first();
				$smallPath = str_replace('/Sliders/', '/Sliders/small/', $slider->Image);
				if (file_exists(public_path($slider->Image))) {
					unlink(public_path($slider->Image));
				}
				if (file_exists(public_path($smallPath))) {
					unlink(public_path($smallPath));
				}
				SettingSlider::where('id', $request->id)->delete();
				unset($smallPath, $slider);
			});
			unset($request);
			$message = array(
					'state' => 1,
					'message' => 'رکورد مورد نظر با موفقیت حذف گردید'
			);
			return response()->json($message);
		} catch (\Exception $e) {
			$message = array(
					'state' => 0,
					'message' => $e->getMessage()
			);
			return response()->json($message);
		}
	}

	public function CenterSliderChangeState(Request $request)
	{
		try {
			DB::transaction(function () use ($request) {
				$slider = SettingSlider::where('id', $request->id)
						->select('State')
						->first();
				if ($slider->State == 1) {
					SettingSlider::where('id', $request->id)
							->update([
									'State' => 0
							]);
				} else {
					SettingSlider::where('id', $request->id)
							->update([
									'State' => 1
							]);
				}
			});
			unset($request);
			$message = array(
					'state' => 1,
					'message' => 'وضعیت رکورد موردنظر با موفقیت تغییر یافت'
			);
			return response()->json($message);
		} catch (\Exception $e) {
			$message = array(
					'state' => 0,
					'message' => $e->getMessage()
			);
			return response()->json($message);
		}
	}

	public function CenterSliderShow(Request $request)
	{
		try {
			$slider = SettingSlider::where('id', $request->id)
					->select('Title', 'Order')
					->first();
			unset($request);
			$message = array(
					'state' => 1,
					'message' => $slider
			);
			return response()->json($message);
		} catch (\Exception $e) {
			$message = array(
					'state' => 0,
					'message' => $e->getMessage()
			);
			return response()->json($message);
		}
	}

	public function CenterSliderEdit(Request $request)
	{
		$request->validate([
				'SID' => 'required',
				'ShowOrder' => 'required|numeric',
				'fileupload' => 'image|mimes:png,jpg,jpeg|max:6000'
		]);
		DB::transaction(function () use ($request) {
			$orderLast = SettingSlider::where('Order', $request->ShowOrder)
					->select('Order', 'id')
					->first();
			$thisOrder = SettingSlider::where('id', $request->SID)
					->select('Order')
					->first();
			if (!is_null($orderLast)) {
				SettingSlider::where('id', $orderLast->id)
						->update([
								'Order' => $thisOrder->Order
						]);
			}
			SettingSlider::where('id', $request->SID)
					->update([
							'Title' => $request->ShowTitle,
							'Order' => $request->ShowOrder
					]);
			if ($request->hasFile('fileupload')) {
				$mediaG = SettingSlider::where('id', $request->SID)
						->select('Image')
						->first();
				$smallPath = str_replace('/Sliders/', '/Sliders/small/', $mediaG->Image);
				if (file_exists(public_path($mediaG->Image))) {
					unlink(public_path($mediaG->Image));
				}
				if (file_exists(public_path($smallPath))) {
					unlink(public_path($smallPath));
				}
				$IW = new ImageWorker($request, 'fileupload', 'otherFiles/Settings/Sliders/');
				$IW->Move();
				$IW->GenerateSmallSize();
				$IW->ChaneImageSizeToOtherSize();
				SettingSlider::where('id', $request->SID)
						->update([
								'Image' => $IW->FullPath
						]);
				unset($IW, $smallPath, $mediaG);
			}
		});
		unset($request);
		Session::flash('center_slider_edit_success');
		return back();
	}

	public function NewsCategoryChangeState(Request $request)
	{
		try {
			$NewsCategory = NewsCategory::where('id', $request->id)
					->select('State')
					->first();
			if ($NewsCategory->State == 1) {
				NewsCategory::where('id', $request->id)->update(['State' => 0]);
			} else {
				NewsCategory::where('id', $request->id)->update(['State' => 1]);
			}
			unset($request);
			$message = array(
					'state' => 1,
					'message' => 'تغییر وضعیت با موفقیت صورت پذیرفت'
			);
			return response()->json($message);
		} catch (\Exception $e) {
			$message = array(
					'state' => 0,
					'message' => $e->getMessage()
			);
			return response()->json($message);
		}
	}
	/****** TODO Settings End Controller*******/

}
