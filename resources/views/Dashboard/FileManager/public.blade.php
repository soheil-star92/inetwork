<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2">
    <title>فایل های عمومی</title>

    <!-- Section CSS -->
    <!-- jQuery UI (REQUIRED) -->
    <link rel="stylesheet" href="/FileManager/jquery/jquery-ui-1.12.0.css" type="text/css">

    <!-- elfinder css -->
    <link rel="stylesheet" href="/FileManager/css/commands.css"    type="text/css">
    <link rel="stylesheet" href="/FileManager/css/common.css"      type="text/css">
    <link rel="stylesheet" href="/FileManager/css/contextmenu.css" type="text/css">
    <link rel="stylesheet" href="/FileManager/css/cwd.css"         type="text/css">
    <link rel="stylesheet" href="/FileManager/css/dialog.css"      type="text/css">
    <link rel="stylesheet" href="/FileManager/css/fonts.css"       type="text/css">
    <link rel="stylesheet" href="/FileManager/css/navbar.css"      type="text/css">
    <link rel="stylesheet" href="/FileManager/css/places.css"      type="text/css">
    <link rel="stylesheet" href="/FileManager/css/quicklook.css"   type="text/css">
    <link rel="stylesheet" href="/FileManager/css/statusbar.css"   type="text/css">
    <link rel="stylesheet" href="/FileManager/css/theme.css"       type="text/css">
    <link rel="stylesheet" href="/FileManager/css/toast.css"       type="text/css">
    <link rel="stylesheet" href="/FileManager/css/toolbar.css"     type="text/css">

    <!-- Section JavaScript -->
    <!-- jQuery and jQuery UI (REQUIRED) -->
    <script src="/FileManager/jquery/jquery-1.12.4.js" type="text/javascript" charset="utf-8"></script>
    <script src="/FileManager/jquery/jquery-ui-1.12.0.js" type="text/javascript" charset="utf-8"></script>

    <!-- elfinder core -->
    <script src="/FileManager/js/elFinder.js"></script>
    <script src="/FileManager/js/elFinder.version.js"></script>
    <script src="/FileManager/js/jquery.elfinder.js"></script>
    <script src="/FileManager/js/elFinder.mimetypes.js"></script>
    <script src="/FileManager/js/elFinder.options.js"></script>
    <script src="/FileManager/js/elFinder.options.netmount.js"></script>
    <script src="/FileManager/js/elFinder.history.js"></script>
    <script src="/FileManager/js/elFinder.command.js"></script>
    <script src="/FileManager/js/elFinder.resources.js"></script>

    <!-- elfinder dialog -->
    <script src="/FileManager/js/jquery.dialogelfinder.js"></script>

    <!-- elfinder default lang -->
    <script src="/FileManager/js/i18n/elfinder.en.js"></script>

    <!-- elfinder ui -->
    <script src="/FileManager/js/ui/button.js"></script>
    <script src="/FileManager/js/ui/contextmenu.js"></script>
    <script src="/FileManager/js/ui/cwd.js"></script>
    <script src="/FileManager/js/ui/dialog.js"></script>
    <script src="/FileManager/js/ui/fullscreenbutton.js"></script>
    <script src="/FileManager/js/ui/navbar.js"></script>
    <script src="/FileManager/js/ui/navdock.js"></script>
    <script src="/FileManager/js/ui/overlay.js"></script>
    <script src="/FileManager/js/ui/panel.js"></script>
    <script src="/FileManager/js/ui/path.js"></script>
    <script src="/FileManager/js/ui/places.js"></script>
    <script src="/FileManager/js/ui/searchbutton.js"></script>
    <script src="/FileManager/js/ui/sortbutton.js"></script>
    <script src="/FileManager/js/ui/stat.js"></script>
    <script src="/FileManager/js/ui/toast.js"></script>
    <script src="/FileManager/js/ui/toolbar.js"></script>
    <script src="/FileManager/js/ui/tree.js"></script>
    <script src="/FileManager/js/ui/uploadButton.js"></script>
    <script src="/FileManager/js/ui/viewbutton.js"></script>
    <script src="/FileManager/js/ui/workzone.js"></script>

    <!-- elfinder commands -->
    <script src="/FileManager/js/commands/archive.js"></script>
    <script src="/FileManager/js/commands/back.js"></script>
    <script src="/FileManager/js/commands/chmod.js"></script>
    <script src="/FileManager/js/commands/colwidth.js"></script>
    <script src="/FileManager/js/commands/copy.js"></script>
    <script src="/FileManager/js/commands/cut.js"></script>
    <script src="/FileManager/js/commands/download.js"></script>
    <script src="/FileManager/js/commands/duplicate.js"></script>
    <script src="/FileManager/js/commands/edit.js"></script>
    <script src="/FileManager/js/commands/empty.js"></script>
    <script src="/FileManager/js/commands/extract.js"></script>
    <script src="/FileManager/js/commands/forward.js"></script>
    <script src="/FileManager/js/commands/fullscreen.js"></script>
    <script src="/FileManager/js/commands/getfile.js"></script>
    <script src="/FileManager/js/commands/help.js"></script>
    <script src="/FileManager/js/commands/hidden.js"></script>
    <script src="/FileManager/js/commands/hide.js"></script>
    <script src="/FileManager/js/commands/home.js"></script>
    <script src="/FileManager/js/commands/info.js"></script>
    <script src="/FileManager/js/commands/mkdir.js"></script>
    <script src="/FileManager/js/commands/mkfile.js"></script>
    <script src="/FileManager/js/commands/netmount.js"></script>
    <script src="/FileManager/js/commands/open.js"></script>
    <script src="/FileManager/js/commands/opendir.js"></script>
    <script src="/FileManager/js/commands/opennew.js"></script>
    <script src="/FileManager/js/commands/paste.js"></script>
    <script src="/FileManager/js/commands/places.js"></script>
    <script src="/FileManager/js/commands/preference.js"></script>
    <script src="/FileManager/js/commands/quicklook.js"></script>
    <script src="/FileManager/js/commands/quicklook.plugins.js"></script>
    <script src="/FileManager/js/commands/reload.js"></script>
    <script src="/FileManager/js/commands/rename.js"></script>
    <script src="/FileManager/js/commands/resize.js"></script>
    <script src="/FileManager/js/commands/restore.js"></script>
    <script src="/FileManager/js/commands/rm.js"></script>
    <script src="/FileManager/js/commands/search.js"></script>
    <script src="/FileManager/js/commands/selectall.js"></script>
    <script src="/FileManager/js/commands/selectinvert.js"></script>
    <script src="/FileManager/js/commands/selectnone.js"></script>
    <script src="/FileManager/js/commands/sort.js"></script>
    <script src="/FileManager/js/commands/undo.js"></script>
    <script src="/FileManager/js/commands/up.js"></script>
    <script src="/FileManager/js/commands/upload.js"></script>
    <script src="/FileManager/js/commands/view.js"></script>

    <!-- elfinder 1.x connector API support (OPTIONAL) -->
    <script src="/FileManager/js/proxy/elFinderSupportVer1.js"></script>

    <!-- Extra contents editors (OPTIONAL) -->
    <script src="/FileManager/js/extras/editors.default.js"></script>

    <!-- GoogleDocs Quicklook plugin for GoogleDrive Volume (OPTIONAL) -->
    <script src="/FileManager/js/extras/quicklook.googledocs.js"></script>

    <!-- elfinder initialization  -->
    <script>
        $(function() {
            $('#elfinder').elfinder(
                // 1st Arg - options
                {
                    // Disable CSS auto loading
                    cssAutoLoad : false,

                    // Base URL to css/*, js/*
                    baseUrl : './',

                    // Connector URL
                    url : '/FileManager/php/connector.minimal.php',

                    // Callback when a file is double-clicked
                    getFileCallback : function(file) {
                        // ...
                    },
                },

                // 2nd Arg - before boot up function
                function(fm, extraObj) {
                    // `init` event callback function
                    fm.bind('init', function() {
                        // Optional for Japanese decoder "extras/encoding-japanese.min"
                        delete fm.options.rawStringDecoder;
                        if (fm.lang === 'ja') {
                            fm.loadScript(
                                [ fm.baseUrl + 'js/extras/encoding-japanese.min.js' ],
                                function() {
                                    if (window.Encoding && Encoding.convert) {
                                        fm.options.rawStringDecoder = function(s) {
                                            return Encoding.convert(s,{to:'UNICODE',type:'string'});
                                        };
                                    }
                                },
                                { loadType: 'tag' }
                            );
                        }
                    });

                    // Optional for set document.title dynamically.
                    var title = document.title;
                    fm.bind('open', function() {
                        var path = '',
                            cwd  = fm.cwd();
                        if (cwd) {
                            path = fm.path(cwd.hash) || null;
                        }
                        document.title = path? path + ':' + title : title;
                    }).bind('destroy', function() {
                        document.title = title;
                    });
                }
            );
        });
    </script>
</head>
<body>
<div id="elfinder"></div>
</body>
</html>
