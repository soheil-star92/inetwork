<?php

namespace App\DbModels\Dashboard\Package;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table='packages';
}
