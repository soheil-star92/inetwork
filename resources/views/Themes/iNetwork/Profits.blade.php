@php
    use \App\MyClasses\CmsFunctions;
    use \App\MyClasses\CmsThemes;
@endphp
@extends('Themes.iNetwork.MainLayout')
@section('Content')
    <!-- INNER PAGE BANNER -->
    <div class="wt-bnr-inr overlay-wraper" style="background-image:url(/Themes/HomePage/images/banner/about-banner.jpg);">
        <div class="overlay-main bg-black opacity-07"></div>
        <div class="container">
            <div class="wt-bnr-inr-entry">
                <h1 class="text-white">Profits for Packages </h1>
            </div>
        </div>
    </div>
    <!-- INNER PAGE BANNER END -->

    <!-- BREADCRUMB ROW -->
    <div class="bg-gray-light p-tb20">
        <div class="container">
            <ul class="wt-breadcrumb breadcrumb-style-2">
                <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
                <li>Packages</li>
            </ul>
        </div>
    </div>
    <!-- BREADCRUMB  ROW END -->

    <!-- ABOUT COMPANY SECTION START -->
    <div class="section-full p-tb100">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="section-head text-left">
                        <span class="wt-title-subline text-gray-dark font-16 m-b15">How to get more profit from invest Chain network?</span>
                        <h2 class="text-uppercase">A New Kind of Investing </h2>
                        <div class="wt-separator-outer"><div class="wt-separator bg-primary"></div></div>
                        <p><strong>
                                Received commission for purchasing and renewing the subset.
                                <br><br><br><br>
                                -First Level: 5% <br><br>
                                -Second Level: 2% <br><br>
                                -Third Level: 1% <br><br>
                                -Fourth Level: 0.75% <br><br>
                                -5th to 8th Level: 0.50% <br><br>
                                -9th to 12th Level: 0.25% <br><br>
                                -13th to 21th Level: 0.125% <br><br>
                                <br>
                                * <b>The above percentages are deposited in higher level wallets at the same time as activating the package.</b>
                                * <b>You can get commissions from all packages of a subset.</b>
                            </strong></p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="wt-media">
                        <img src="/assets/dollar-earn.jpg" alt="" class="img-responsive"/>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="section-head text-left">
                      </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ABOUT COMPANY SECTION END -->



@stop
