<?php

namespace App\DbModels\Dashboard\Wallet;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $table='wallets';
}
