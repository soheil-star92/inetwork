@php
    $settings=\App\Facade\DbSettings::GetDbSetting();
    $userLogged=\App\Facade\OrganizationInfo::GetUserInfoWithoutParameter();
    $RegisteredEmail=$userLogged->RegisteredEmail;
@endphp

@section('CssFiles')
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/forms/select/select2.min.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/animate/animate.min.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/extensions/sweetalert2.min.css">
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/vendors/css/tables/datatable/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/vendors/css/tables/datatable/responsive.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/vendors/css/tables/datatable/buttons.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/vendors/css/tables/datatable/rowGroup.bootstrap5.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/bordered-layout.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/plugins/forms/form-validation.css">
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/css/plugins/extensions/ext-component-sweet-alerts.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/pages/modal-create-app.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/assets/css/style.css">
    <!-- END: Custom CSS-->


@stop
@extends('Layouts.dashboardMainLayout')

@section('MainContent')


    <div class="modal fade" id="showRowModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-edit-user">
            <div class="modal-content">
                <div class="modal-header bg-transparent">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body pb-5 px-sm-5 pt-50">
                    <div class="text-center mb-2">
                        <h1 class="mb-1">Show Record</h1>
                    </div>
                    <form id="editUserForm" class="row gy-1 pt-75" action="/AdminPanel/Wallet/UpdateDepositRow"
                          method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="iptKey" id="iptKey">
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptShowFullname">Fullname</label>
                            <input type="text" id="iptShowFullname" name="iptShowFullname"
                                   class="form-control" placeholder="0" />
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptShowEmail">Email</label>
                            <input type="text" id="iptShowEmail" name="iptShowEmail"
                                   class="form-control" placeholder="0" required/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="modalAddPrice">Price($)</label>
                            <input type="number" id="iptShowPrice" name="iptShowPrice"
                                   class="form-control" placeholder="0" required/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="modalEditUserLastName">Trx Hash Link</label>
                            <input type="text" id="iptShowTrxHashLink" name="iptShowTrxHashLink"
                                   class="form-control"
                                   required/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="modalEditUserLastName">Change Status</label>
                            <select id="iptChangeStatus" name="iptChangeStatus"  class="form-select form-select-lg">
                                <option value="0">not valid</option>
                                <option value="1">valid</option>
                            </select>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="modalEditUserEmail">Attachment(transaction Screenshot
                                ):</label>
                            <div id="showImgTag"></div>
                        </div>
                        <div class="col-12 text-center mt-2 pt-50">
                            <button type="submit" class="btn btn-primary me-1">Submit</button>
                            <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal"
                                    aria-label="Close">
                                Discard
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--/ Add Row Modal -->


    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="app-user-view-account">
                    <div class="row">
                        <!-- User Content -->
                        <div class="col-xl-12 col-lg-12 col-md-12 order-0 order-md-1">
                            <!-- User Pills -->
                            <ul class="nav nav-pills mb-2">
                                <li class="nav-item">
                                    <a class="nav-link " href="/AdminPanel/Wallet/DepositList">
                                        <i data-feather="user" class="font-medium-3 me-50"></i>
                                        <span class="fw-bold">Deposit list</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/AdminPanel/Wallet/WithdrawList">
                                        <i data-feather="lock" class="font-medium-3 me-50"></i>
                                        <span class="fw-bold">Withdraw list</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" href="/AdminPanel/Wallet/TransferList">
                                        <i data-feather="bookmark" class="font-medium-3 me-50"></i>
                                        <span class="fw-bold">Transfer list</span>
                                    </a>
                                </li>
                            </ul>
                            <!--/ User Pills -->

                            <!-- Project table -->
                            <div class="card">
                                <h4 class="card-header">User's Transfer List</h4>
                                <div class="table-responsive">
                                    <table id="keyWordDataTable" class="table datatable-project">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>fromFullname</th>
                                            <th>fromEmail</th>
                                            <th>toFullname</th>
                                            <th>toEmail</th>
                                            <th>Price($)</th>
                                            <th class="text-nowrap">Created</th>
                                            <th>Operation</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($rows as $key=>$row)
                                            @php $fromUserInfo=\App\MyClasses\CmsFunctions::GetUserInfo($row->from_user_id); @endphp
                                            <tr>
                                                <td>{{++$key}}</td>
                                                <td>{{$fromUserInfo->first_name.' '.$fromUserInfo->last_name}}</td>
                                                <td>{{$fromUserInfo->email}}</td>
                                                <td>{{$row->first_name.' '.$row->last_name}}</td>
                                                <td>{{$row->email}}</td>
                                                <td>{{$row->Price}}$</td>
                                                <td>{{$row->created_at}}</td>
                                                <td>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /Project table -->

                        </div>
                        <!--/ User Content -->
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->
@stop

@section('JsFiles')
    <!-- BEGIN: Vendor JS-->
    <script src="/Themes/Admin/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="/Themes/Admin/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/forms/cleave/cleave.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/forms/cleave/addons/cleave-phone.us.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/moment.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/dataTables.bootstrap5.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/responsive.bootstrap5.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/jszip.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/polyfill.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/Themes/Admin/app-assets/js/core/app-menu.js"></script>
    <script src="/Themes/Admin/app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-add-new-cc.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/page-pricing.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-add-new-address.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-create-app.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-two-factor-auth.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-edit-user.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-share-project.js"></script>
    <!-- END: Page JS-->


    <script>
        $(document).ready(function () {
            $('body').on('click', '#aShowRow', function () {
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/AdminPanel/Wallet/getUserDeposit',
                    data: {_token: token, id: id},
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.state == 1) {
                            $('#iptShowPrice').val(data.message.Price);
                            $('#iptShowFullname').val(data.iptFullname);
                            $('#iptShowEmail').val(data.iptEmail);
                            $('#iptKey').val(data.iptKey);
                            $('#iptShowTrxHashLink').val(data.message.TransferLink);
                            $('#showImgTag').empty();
                            $('#showImgTag').append(data.imgTag);
                            $('#iptChangeStatus option[value=' + data.message.Valid + ']').attr('selected', 'selected');
                        } else {
                            toastr['error'](data.message, 'Error', {
                                closeButton: true,
                                tapToDismiss: false,
                                progressBar: true  });
                        }
                    }
                });

            });
            $('#showRowModal').one('submit', function () {
                $(this).find('button[type="submit"]').attr('disabled', 'disabled');
            });
            @if(Session::has('error_at_update_deposit'))
                toastr['error']('{{Session::get('error_at_update_deposit')}}', 'Error', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true
            });
            @endif
                    @if(Session::has('update_deposit_successfully'))
                toastr['success']('{{Session::get('update_deposit_successfully')}}', 'Success', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true
            });
            @endif

            $('#keyWordDataTable').DataTable({
                "language": {
                    "lengthMenu": "نمایش _MENU_ رکورد در صفحه",
                    "zeroRecords": "Info Empty",
                    "info": "نمایش updateNewsFormصفحه _PAGE_ از _PAGES_",
                    "infoEmpty": "Info Empty",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "search": "Search: "
                },
                "paging": false,
                "info": false,
                stateSave: false
                // scrollY:        300,
                // scrollCollapse: true,
                // fixedColumns:   true
            });

        });

        $(window).on('load', function () {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>
@stop
