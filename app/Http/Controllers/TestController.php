<?php

namespace App\Http\Controllers;

use App\DbModels\Dashboard\Users\User;
use App\TestModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
	public function index()
	{
		$tests=TestModel::all();
		return view('Dashboard.Test.index',compact('tests'));
	}

	public function show($id)
	{
		return TestModel::find($id);
	}

	public function insert(Request $request)
	{
		$t=new TestModel();
		$t->FirstName=$request->FN;
		$t->LastName=$request->LN;
		$t->Age=26;
		$t->Address = 'RtTTTT';
		$t->save();
		return 1;
	}

	public function update($id)
	{
		TestModel::where('id',$id)
			->update([
					'FirstName'=>'Mohammad',
					'LastName'=>'Soheili'
			]);
		return 1;
	}

	public function delete($id)
	{
		try {
			TestModel::where('id',$id)->delete();
			$message=[
					'state'=>1,
					'message'=>'record has been deleted'
			];
			return response()->json($message);
		}catch (\Exception $e){
			$message=[
					'state'=>0,
					'message'=>$e->getMessage()
			];
			return response()->json($message);
		}
	}

    public function PV()
    {
        $ApiKey = "v7bffh281w92yih";
        $SecKey = "5WVV123MC1QP2CRARJ61VGEWKFVVH2YS";

        ///Params
        $Amount = "10";
        $WorthValue = "USD";
        //Your Website Invoice ID
        $InvoiceID = "4520610";



//        $this->PVGetBalance($ApiKey);
        $this->PVCreateVouchers($ApiKey,$SecKey,$Amount,$WorthValue,$InvoiceID);
//        $this->PVSearch('123');
	}
    public function PVGetBalance($ApiKey)
    {
        $curl = curl_init();

        $baseUrl = "https://www.premiumvouchers.com/api/vouchers/GetBalance?";
        $params = "ApiKey=" . $ApiKey ;
        curl_setopt_array($curl, array(
            CURLOPT_URL => $baseUrl . $params,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            )
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }
    }

    public function PVSearch($Voucher)
    {
        $curl = curl_init();

        $baseUrl = "https://www.premiumvouchers.com/Search/VoucherSearch";
        $params = "Code=" . $Voucher ;
        curl_setopt_array($curl, array(
            CURLOPT_URL => $baseUrl . $params,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            )
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }
    }

    public function PVCreateVouchers($ApiKey,$SecKey,$Amount,$WorthValue,$InvoiceID)
    {
        $curl = curl_init();

        //Create Security Hash (SHA512) Based on Params and SecKey
        $SecurityHash = hash_hmac('sha512', $ApiKey . ":" . $SecKey . ":" . $Amount . ":" . $InvoiceID . ":" . $WorthValue, $SecKey);

        $baseUrl = "https://www.premiumvouchers.com/api/vouchers/CreateVouchers?";
        $params = "ApiKey=" . $ApiKey . "&Amount=" . $Amount . "&WorthValue=" . $WorthValue . "&InvoiceID=" . $InvoiceID . "&SecurityHash=" . $SecurityHash;

        curl_setopt_array($curl, array(
            CURLOPT_URL => $baseUrl . $params,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            )
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }
    }


}
