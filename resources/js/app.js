/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
/*
require('./components/Example');
*/

//jQuery 3
require('../assets/bower_components/jquery/dist/jquery.min.js');

//jQuery UI 1.11.4
require('../assets/bower_components/jquery-ui/jquery-ui.min.js');
//Resolve conflict in jQuery UI tooltip with Bootstrap tooltip
$.widget.bridge('uibutton', $.ui.button);

//Bootstrap 3.3.7
require('../assets/bower_components/bootstrap/dist/js/bootstrap.min.js');

//AdminLTE App
require('../assets/dist/js/adminlte.min.js');

//Sweet Alert
require('../assets/bower_components/sweet-alert/sweetalert.min.js');

//Goto top
require('../assets/bower_components/gotop/gotop.js');

//Pace progrees bar(Loading)
//require('../assets/bower_components/PACE/pace.min.js');

//DataTables
require('../assets/bower_components/datatables.net/js/jquery.dataTables.min.js');

//Custom
require('../assets/bower_components/datatables.net/custom/CustomDt.js');
















