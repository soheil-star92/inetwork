@extends('Layouts.dashboardMainLayout')
@section('BreadCrumbURL','/users/usersList')
@section('BreadCrumbTitle','لیست کاربران')
@section('MainContent')
    <div class="row">
        <section class="col-lg-12 col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <i class="fa fa-info-circle"></i>
                    <h3 class="box-title">
                        {{ \App\Facade\DbSettings::GetDbSetting()->Title }}
                    </h3>
                    <!-- tools box -->
                    <div class="pull-left box-tools">
                        <button type="button" class="btn bg-info btn-sm" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /. tools -->
                </div>
                <!-- modal -->
                <div class="modal modal-info fade" id="modal-add-user">
                    <div class="modal-dialog">
                        <form action="/users/addUser" id="addC" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">تعریف کاربر</h4>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        <label for="first_name">نام کاربر :</label>
                                        <input type="text" name="first_name" id="first_name" class="form-control">
                                    </p>
                                    <p>
                                        <label for="last_name">نام خانوادگی کاربر :</label>
                                        <input type="text" name="last_name" id="last_name" class="form-control">
                                    </p>
                                    <p>
                                        <label for="email">پست الکترونیکی کاربر :</label>
                                        <input type="text" name="email" id="email" class="form-control">
                                    </p>
                                    <p>
                                        <label for="password">کلمه عبور کاربر :</label>
                                        <input type="password" name="password" id="password" class="form-control">
                                    </p>
                                    <p>
                                        <label for="roleId">نوع کاربری :</label>
                                        <select name="roleId" id="roleId" class="form-control">
                                            @foreach($roles as $r)
                                                <option value="{{ $r->id }}">{{ $r->name }}</option>
                                            @endforeach
                                        </select>
                                    </p>
                                    <p>
                                        <label for="orgPic">عکس پرسنلی :</label>
                                        <input type="file" name="orgPic" id="orgPic" class="form-control">
                                    </p>
                                    <p>
                                        <label for="id_i">کد ملی :</label>
                                        <input type="text" name="id_i" id="id_i" class="form-control" required>
                                    </p>
                                    <p>
                                        <label for="Phone"> تلفن تماس:</label>
                                        <input type="text" name="Phone" id="Phone" class="form-control">
                                    </p>
                                    <p>
                                        <label for="Address"> آدرس:</label>
                                        <input type="text" name="Address" id="Address" class="form-control">
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">خروج
                                    </button>
                                    <input type="submit" class="btn btn-outline" value="ثبت">
                                </div>
                            </div>
                        </form>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <div class="box-body">
                    <div class="content">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">کاربری های سیستم</h3>
                            </div>
                            <div class="box-header with-border">
                                <a class="btn btn-app" data-toggle="modal" data-target="#modal-add-user">
                                    <i class="fa fa-plus"></i>جدید
                                </a>
                                @include('Dashboard.toolbars.userToolbar')
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table id="keyWordDataTable"
                                           class="table table-bordered table-striped table-responsive table-hover">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام نام خانوادگی</th>
                                            <th>پست الکترونیکی</th>
                                            <th>سطح دسترسی</th>
                                            <th>آخرین ورود</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($users as $key=>$u)
                                            @php
                                                $getUser=\App\Facade\OrganizationInfo::GetUserRoleAccess();
                                            @endphp
                                            @if( $u->id != 1 || $getUser->id == 1)
                                                <tr @if($u->Blocked == 1) style="background-color: rgba(200,0,0,0.3)" @endif>
                                                    <td>{{ $key }}</td>
                                                    <td>{{ $u['first_name'].' '.$u['last_name'] }}</td>
                                                    <td>{{ $u['email'] }}</td>
                                                    <td>{{ $u['RoleTitle'] }}</td>
                                                    @if(!is_null($u['last_login']))
                                                        <td>
                                                            {{ \App\MyClasses\CmsFunctions::GetGregorianDateAndConvertToJalaliDate($u['last_login']) }}
                                                        </td>
                                                    @else
                                                        <td>
                                                            به سیستم وارد نشده است
                                                        </td>
                                                    @endif
                                                    <td>
                                                        <div class="input-group">
                                                            <button type="button"
                                                                    class="btn bg-light-blue-active dropdown-toggle "
                                                                    data-toggle="dropdown"
                                                                    style="font-size: 9px !important ">
                                                                <span class="fa fa-caret-down"></span></button>
                                                            <ul class="dropdown-menu">
                                                                @if($u->id != 1)
                                                                <li><a class="btn bg-maroon btn-xs"
                                                                       data-id="{{ $u->id }}" title="حذف رکورد"><span
                                                                                class="fa fa-trash"></span>حذف</a></li>
                                                                @endif
                                                                <li><a class="btn bg-green btn-xs"
                                                                       href="/users/showUser/{{ $u->id }}"
                                                                       title="ویرایش رکورد"><span
                                                                                class="fa fa-edit"></span>ویرایش</a>
                                                                </li>
                                                                <li><a class="btn bg-yellow-gradient btn-xs"
                                                                       data-id="{{ $u->id }}"
                                                                       title="تغییر وضعیت رکورد"><span
                                                                                class="fa fa-ban"></span>فعال/غیرفعال</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('JsFiles')
    <script src="{{ URL::asset('assets/login/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            @if(Session::has('add_user_success'))
            swal("", "رکورد جدید با موفقیت اضافه گردید", "success");
            @endif
            @if(Session::has('error_at_insert_user'))
            swal("{{Session::get('error_at_insert_user')}}", {
                icon: "warning",
                dangerMode: true,
            });
            @endif
        });
    </script>
    <script>
        $(function () {
            $('.bg-maroon').click(function () {
                var userId = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                var ele = $(this).parent().parent().parent().parent().parent();
                swal({
                    title: "",
                    text: "آیا از حذف این رکورد مطمئن هستید؟",
                    icon: "warning",
                    buttons: ["خیر", "بله"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '/users/removeUser',
                            data: {_token: token, userId: userId},
                            type: 'POST',
                            dataType: 'JSON',
                            success: function (data) {
                                if (data.state == 1) {
                                    ele.fadeOut().remove();
                                    swal("", data.message, "success");
                                } else {
                                    swal(data.message, {
                                        icon: 'warning',
                                        dangerMode: true
                                    });
                                }
                            }
                        });
                    }
                });
            });

            $('.bg-yellow-gradient').click(function () {
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                swal({
                    title: "",
                    text: "آیا از تغییر وضعیت این رکورد مطمئن هستید؟",
                    icon: "warning",
                    buttons: ["خیر", "بله"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '/users/ChangeUserStateBlocking',
                            data: {_token: token, id: id},
                            type: 'POST',
                            dataType: 'JSON',
                            success: function (data) {
                                if (data.state == 1) {
                                    swal("", data.message, "success");
                                    location.reload();
                                } else {
                                    swal(data.message, {
                                        icon: 'warning',
                                        dangerMode: true
                                    });
                                }
                            }
                        });
                    }
                });
            });


            $('#addC').one('submit', function () {
                $(this).find('input[type="submit"]').attr('disabled', 'disabled');
            });
        })
    </script>
@endsection
