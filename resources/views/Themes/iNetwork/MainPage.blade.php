@php
    use \App\MyClasses\CmsFunctions;
    use \App\MyClasses\CmsThemes;
@endphp
@extends('Themes.iNetwork.MainLayout')
@section('CssFiles')
    <style>
        .owl-carousel .owl-item img {width: 30% !important;}
    </style>
    @stop
@section('Content')

    <!-- SLIDER START -->
    <div class="main-slider style-two default-banner">
        <div class="tp-banner-container">
            <div class="tp-banner">
                <!-- START REVOLUTION SLIDER 5.4.1 -->
                <div id="rev_slider_1014_1_wrapper" class="rev_slider_wrapper fullscreen-container"
                     data-alias="typewriter-effect" data-source="gallery">
                    <div id="rev_slider_1014_1" class="rev_slider fullscreenbanner" style="display:none;"
                         data-version="5.4.1">
                        <ul>
                            <!-- SLIDE 1 -->
                            <li data-index="rs-1000" data-transition="slidingoverlayhorizontal"
                                data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"
                                data-easein="default" data-easeout="default" data-masterspeed="default"
                                data-thumb="images/main-slider/slider2/slide1.jpg" data-rotate="0"
                                data-saveperformance="off" data-title="Slide" data-param1="" data-param2=""
                                data-param3="" data-param4="" data-param5="" data-param6="" data-param7=""
                                data-param8="" data-param9="" data-param10="" data-description="">
                                <!-- MAIN IMAGE -->
                                <img src="/Themes/HomePage/images/main-slider/slider2/slide1.jpg" alt=""
                                     data-bgposition="center bottom" data-bgfit="cover" data-bgrepeat="no-repeat"
                                     class="rev-slidebg" data-no-retina/>
                                <!-- LAYERS -->

                                <!-- LAYER NR. 1 [ for overlay ] -->
                                <div class="tp-caption tp-shape tp-shapewrapper "
                                     id="slide-100-layer-1"
                                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                                     data-width="full"
                                     data-height="full"
                                     data-whitespace="nowrap"
                                     data-type="shape"
                                     data-basealign="slide"
                                     data-responsive_offset="off"
                                     data-responsive="off"
                                     data-frames='[
                                    {"from":"opacity:0;","speed":1000,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power4.easeOut"}
                                    ]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 12;background-color:rgba(0, 0, 0, 0.3);border-color:rgba(0, 0, 0, 0);border-width:0px;">
                                </div>
                                <!-- LAYER NR. 2 [ for title ] -->
                                <div class="tp-caption   tp-resizeme"
                                     id="slide-100-layer-2"
                                     data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                                     data-y="['top','top','top','top']" data-voffset="['308','308','308','308']"
                                     data-fontsize="['60','60','60','60']"
                                     data-lineheight="['110','110','110','110']"
                                     data-width="['6','6','6','6']"
                                     data-height="['110,'110','110','110']"
                                     data-whitespace="['normal','normal','normal','normal']"

                                     data-type="text"
                                     data-responsive_offset="on"
                                     data-frames='[
                                    {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 13;
                                    white-space: normal;
                                    ">

                                    <div class="bg-primary">&nbsp;</div>

                                </div>

                                <!-- LAYER NR. 3 [ for title ] -->
                                <div class="tp-caption   tp-resizeme"
                                     id="slide-100-layer-3"
                                     data-x="['left','left','left','left']" data-hoffset="['60','60','60','100']"
                                     data-y="['top','top','top','top']" data-voffset="['300','300','300','300']"
                                     data-fontsize="['55','55','55','45']"
                                     data-lineheight="['60','60','60','65']"
                                     data-width="['700','700','700','700']"
                                     data-height="['none','none','none','none']"
                                     data-whitespace="['normal','normal','normal','normal']"

                                     data-type="text"
                                     data-responsive_offset="on"
                                     data-frames='[
                                    {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 13;
                                    white-space: normal;
                                    font-weight: 700;
                                    color: rgb(75, 57, 65);
                                    border-width:0px;">

                                    <div style="font-family: 'Poppins', sans-serif; text-transform:uppercase;">
                                        <span class="text-white" style="padding-right:10px;">The most</span><span
                                                class="text-primary">Secure</span>
                                    </div>

                                </div>

                                <!-- LAYER NR. 4 [ for title ] -->
                                <div class="tp-caption   tp-resizeme"
                                     id="slide-100-layer-4"
                                     data-x="['left','left','left','left']" data-hoffset="['60','60','60','100']"
                                     data-y="['top','top','top','top']" data-voffset="['360','360','360','360']"
                                     data-fontsize="['53','53','53','45']"
                                     data-lineheight="['70','70','70','75']"
                                     data-width="['700','700','700','700']"
                                     data-height="['none','none','none','none']"
                                     data-whitespace="['normal','normal','normal','normal']"

                                     data-type="text"
                                     data-responsive_offset="on"
                                     data-frames='[
                                    {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":1000,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 13;
                                    white-space: normal;
                                    font-weight: 700;
                                    border-width:0px;">
                                    <div style="font-family: 'Poppins', sans-serif; text-transform:uppercase ;">
                                        <span class="text-primary" style="padding-right:10px;">Crypto</span><span
                                                class="text-white">Currency</span>
                                    </div>

                                </div>

                                <!-- LAYER NR. 5 [ for paragraph] -->
                                <div class="tp-caption  tp-resizeme"
                                     id="slide-100-layer-5"
                                     data-x="['left','left','left','left']" data-hoffset="['30','30','30','100']"
                                     data-y="['top','top','top','top']" data-voffset="['440','440','440','440']"
                                     data-fontsize="['16','16','16','30']"
                                     data-lineheight="['30','30','30','40']"
                                     data-width="['600','600','600','600']"
                                     data-height="['none','none','none','none']"
                                     data-whitespace="['normal','normal','normal','normal']"

                                     data-type="text"
                                     data-responsive_offset="on"
                                     data-frames='[
                                    {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":1500,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 13;
                                    font-weight: 500;
                                    color:#fff;
                                    border-width:0px;">
                                    <span style="font-family: 'Poppins', sans-serif;">Invest Chain Network the best company for secure investing in cryptocurrency field.</span>
                                </div>

                                <!-- LAYER NR. 6 [ for see all service botton ] -->
                                <div class="tp-caption tp-resizeme"
                                     id="slide-100-layer-6"
                                     data-x="['left','left','left','left']" data-hoffset="['30','30','30','100']"

                                     data-y="['top','top','top','top']" data-voffset="['530','530','530','600']"
                                     data-lineheight="['none','none','none','none']"
                                     data-width="['300','300','300','300']"
                                     data-height="['none','none','none','none']"
                                     data-whitespace="['normal','normal','normal','normal']"

                                     data-type="text"
                                     data-responsive_offset="on"
                                     data-frames='[
                                    {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index:13; text-transform:uppercase;">
                                    <a href="/Contact" class="site-button slider-btn-left">Contact
                                        Us</a>
                                </div>

                                <!-- LAYER NR. 7 [ for more detail botton ] -->
                                <div class="tp-caption tp-resizeme"
                                     id="slide-100-layer-7"
                                     data-x="['left','left','left','left']" data-hoffset="['220','220','220','320']"
                                     data-y="['top','top','top','top']" data-voffset="['530','530','530','600']"
                                     data-lineheight="['none','none','none','none']"
                                     data-width="['300','300','300','300']"
                                     data-height="['none','none','none','none']"
                                     data-whitespace="['normal','normal','normal','normal']"

                                     data-type="text"
                                     data-responsive_offset="on"
                                     data-frames='[
                                    {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index:13;
                                    text-transform:uppercase;
                                    font-weight:500;
                                    ">
                                    <a href="/About"
                                       class=" site-button white slider-btn-right">Join Us</a>
                                </div>

                                <!-- LAYER NR. 8 [ for more detail botton ] -->
                                <div class="tp-caption tp-resizeme"
                                     id="slide-100-layer-8"
                                     data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
                                     data-y="['bottom','bottom','bottom','bottom']"
                                     data-voffset="['100','100','100','100']"

                                     data-frames='[
                                    {"from":"y:200px(R);opacity:0;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'

                                     style="z-index: 13;">
                                    <img src="/Themes/HomePage/images/main-slider/slider2/earth.png" alt=""
                                         class="spin-city">
                                </div>

                                <!-- LAYER NR. 9 [ for more detail botton ] -->
                                <div class="tp-caption tp-resizeme"
                                     id="slide-100-layer-9"

                                     data-x="['right','right','right','right']"
                                     data-hoffset="['120','120','120','120']"
                                     data-y="['bottom','bottom','bottom','bottom']"
                                     data-voffset="['180','180','180','180']"

                                     data-height="none"
                                     data-whitespace="nowrap"

                                     data-type="image"
                                     data-responsive_offset="on"

                                     data-frames='[{"from":"x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.75;sY:0.75;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":3000,"ease":"Power3.easeOut"},
                                    {"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'

                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"


                                     style="z-index: 13;">
                                    <img src="/Themes/HomePage/images/main-slider/slider2/bitcoin.png" alt="">
                                </div>


                            </li>

                            <!-- SLIDE 2 -->
                            <li data-index="rs-1001" data-transition="slidingoverlayhorizontal"
                                data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"
                                data-easein="default" data-easeout="default" data-masterspeed="default"
                                data-thumb="images/main-slider/slider2/slide1.jpg" data-rotate="0"
                                data-saveperformance="off" data-title="Slide" data-param1="" data-param2=""
                                data-param3="" data-param4="" data-param5="" data-param6="" data-param7=""
                                data-param8="" data-param9="" data-param10="" data-description="">
                                <!-- MAIN IMAGE -->
                                <img src="/Themes/HomePage/images/main-slider/slider2/slide1.jpg" alt=""
                                     data-bgposition="center bottom" data-bgfit="cover" data-bgrepeat="no-repeat"
                                     class="rev-slidebg" data-no-retina/>
                                <!-- LAYERS -->

                                <!-- LAYER NR. 1 [ for overlay ] -->
                                <div class="tp-caption tp-shape tp-shapewrapper "
                                     id="slide-101-layer-1"
                                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                                     data-width="full"
                                     data-height="full"
                                     data-whitespace="nowrap"
                                     data-type="shape"
                                     data-basealign="slide"
                                     data-responsive_offset="off"
                                     data-responsive="off"
                                     data-frames='[
                                    {"from":"opacity:0;","speed":1000,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power4.easeOut"}
                                    ]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 12;background-color:rgba(0, 0, 0, 0.3);border-color:rgba(0, 0, 0, 0);border-width:0px;">
                                </div>

                                <!-- LAYER NR. 2 [ for title ] -->
                                <div class="tp-caption   tp-resizeme"
                                     id="slide-101-layer-2"
                                     data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                                     data-y="['top','top','top','top']" data-voffset="['308','308','308','308']"
                                     data-fontsize="['60','60','60','60']"
                                     data-lineheight="['110','110','110','110']"
                                     data-width="['6','6','6','6']"
                                     data-height="['110,'110','110','110']"
                                     data-whitespace="['normal','normal','normal','normal']"

                                     data-type="text"
                                     data-responsive_offset="on"
                                     data-frames='[
                                    {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 13;
                                    white-space: normal;
                                    ">

                                    <div class="bg-primary">&nbsp;</div>

                                </div>

                                <!-- LAYER NR. 3 [ for title ] -->
                                <div class="tp-caption   tp-resizeme"
                                     id="slide-101-layer-3"
                                     data-x="['left','left','left','left']" data-hoffset="['60','60','60','100']"
                                     data-y="['top','top','top','top']" data-voffset="['300','300','300','300']"
                                     data-fontsize="['55','55','55','45']"
                                     data-lineheight="['60','60','60','65']"
                                     data-width="['700','700','700','700']"
                                     data-height="['none','none','none','none']"
                                     data-whitespace="['normal','normal','normal','normal']"

                                     data-type="text"
                                     data-responsive_offset="on"
                                     data-frames='[
                                    {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 13;
                                    white-space: normal;
                                    font-weight: 700;
                                    color: rgb(75, 57, 65);
                                    border-width:0px;">

                                    <div style="font-family: 'Poppins', sans-serif; text-transform:uppercase; ">
                                        <span class="text-white" style="padding-right:10px;">Invest</span><span
                                                class="text-primary">in Crypto</span>
                                    </div>

                                </div>

                                <!-- LAYER NR. 4 [ for title ] -->
                                <div class="tp-caption   tp-resizeme"
                                     id="slide-101-layer-4"
                                     data-x="['left','left','left','left']" data-hoffset="['60','60','60','100']"
                                     data-y="['top','top','top','top']" data-voffset="['360','360','360','360']"
                                     data-fontsize="['53','53','53','45']"
                                     data-lineheight="['70','70','70','70']"
                                     data-width="['700','700','700','700']"
                                     data-height="['none','none','none','none']"
                                     data-whitespace="['normal','normal','normal','normal']"

                                     data-type="text"
                                     data-responsive_offset="on"
                                     data-frames='[
                                    {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":1000,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 13;
                                    white-space: normal;
                                    font-weight: 700;
                                    border-width:0px;">
                                    <div style="font-family: 'Poppins', sans-serif; text-transform:uppercase ;">
                                        <span class="text-primary" style="padding-right:10px;">Without </span><span
                                                class="text-white">Buying Coins</span>
                                    </div>

                                </div>

                                <!-- LAYER NR. 5 [ for paragraph] -->
                                <div class="tp-caption  tp-resizeme"
                                     id="slide-101-layer-5"
                                     data-x="['left','left','left','left']" data-hoffset="['30','30','30','100']"
                                     data-y="['top','top','top','top']" data-voffset="['440','440','440','440']"
                                     data-fontsize="['16','16','16','30']"
                                     data-lineheight="['30','30','30','40']"
                                     data-width="['600','600','600','600']"
                                     data-height="['none','none','none','none']"
                                     data-whitespace="['normal','normal','normal','normal']"

                                     data-type="text"
                                     data-responsive_offset="on"
                                     data-frames='[
                                    {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":1500,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 13;
                                    font-weight: 500;
                                    color:#fff;
                                    border-width:0px;">
                                    <span style="font-family: 'Poppins', sans-serif;">The easiest way to get investment exposure to crypto without buying crypto itself is to investing in our company with professional traders in the future of cryptocurrency coins or blockchain technology. </span>
                                </div>

                                <!-- LAYER NR. 6 [ for see all service botton ] -->
                                <div class="tp-caption tp-resizeme"
                                     id="slide-101-layer-6"
                                     data-x="['left','left','left','left']" data-hoffset="['30','30','30','100']"
                                     data-y="['top','top','top','top']" data-voffset="['530','530','530','600']"
                                     data-lineheight="['none','none','none','none']"
                                     data-width="['300','300','300','300']"
                                     data-height="['none','none','none','none']"
                                     data-whitespace="['normal','normal','normal','normal']"

                                     data-type="text"
                                     data-responsive_offset="on"
                                     data-frames='[
                                    {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index:13; text-transform:uppercase;">
                                    <a href="/Contact" class="site-button slider-btn-left">Contact
                                        Us</a>
                                </div>

                                <!-- LAYER NR. 7 [ for more detail botton ] -->
                                <div class="tp-caption tp-resizeme"
                                     id="slide-101-layer-7"
                                     data-x="['left','left','left','left']" data-hoffset="['220','220','220','320']"
                                     data-y="['top','top','top','top']" data-voffset="['530','530','530','600']"
                                     data-lineheight="['none','none','none','none']"
                                     data-width="['300','300','300','300']"
                                     data-height="['none','none','none','none']"
                                     data-whitespace="['normal','normal','normal','normal']"

                                     data-type="text"
                                     data-responsive_offset="on"
                                     data-frames='[
                                    {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index:13;
                                    text-transform:uppercase;
                                    font-weight:500;
                                    ">
                                    <a href="/About"
                                       class=" site-button white slider-btn-right">Join Us</a>
                                </div>

                                <!-- LAYER NR. 8 [ for more detail botton ] -->
                                <div class="tp-caption tp-resizeme"
                                     id="slide-101-layer-8"
                                     data-x="['right','right','right','right']"
                                     data-hoffset="['-100','-100','-100','-100']"
                                     data-y="['bottom','bottom','bottom','bottom']"
                                     data-voffset="['-650','-650','-650','-650']"

                                     data-frames='[{"from":"x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.75;sY:0.75;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":3000,"ease":"Power3.easeOut"},
                                    {"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'

                                     style="z-index: 13;">
                                    <img src="/Themes/HomePage/images/main-slider/slider2/earth2.png" alt=""
                                         class="spin-city">
                                </div>

                                <!-- LAYER NR. 9 [ for more detail botton ] -->
                                <div class="tp-caption tp-resizeme"
                                     id="slide-101-layer-9"
                                     data-x="['right','right','right','right']"
                                     data-hoffset="['-300','-100','-100','-100']"
                                     data-y="['bottom','bottom','bottom','bottom']"
                                     data-voffset="['-200','-200','-200','-200']"

                                     data-frames='[{"from":"x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.75;sY:0.75;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":3000,"ease":"Power3.easeOut"},
                                    {"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'

                                     style="z-index: 13;">
                                    <img src="/Themes/HomePage/images/main-slider/slider2/earth2-shadow.png" alt="">
                                </div>

                                <!-- LAYER NR. 10 [ for more detail botton ] -->
                                <div class="tp-caption tp-resizeme"
                                     id="slide-101-layer-10"

                                     data-x="['right','right','right','right']"
                                     data-hoffset="['200','200','200','200']"
                                     data-y="['top','bottom','bottom','bottom']"
                                     data-voffset="['150','150','150','150']"

                                     data-height="none"
                                     data-whitespace="nowrap"

                                     data-type="image"
                                     data-responsive_offset="on"

                                     data-frames='[{"from":"x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.75;sY:0.75;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":3000,"ease":"Power3.easeOut"},
                                    {"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"


                                     style="z-index: 16;">
                                    <img src="/Themes/HomePage/images/main-slider/slider2/rocket.png" alt=""
                                         class="floating">
                                </div>

                                <!-- LAYER NR. 11 [ for more detail botton ] -->
                                <div class="tp-caption tp-resizeme"
                                     id="slide-101-layer-11"

                                     data-x="['right','right','right','right']"
                                     data-hoffset="['278','278','278','278']"
                                     data-y="['bottom','bottom','bottom','bottom']"
                                     data-voffset="['180','100','100','100']"

                                     data-height="none"
                                     data-whitespace="nowrap"

                                     data-type="image"
                                     data-responsive_offset="on"

                                     data-frames='[{"from":"x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.75;sY:0.75;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":4000,"ease":"Power3.easeOut"},
                                    {"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"


                                     style="z-index: 15;">
                                    <img src="/Themes/HomePage/images/main-slider/slider2/fire.gif" alt=""
                                         class="floating">
                                </div>

                                <!-- LAYER NR. 12 [ for more detail botton ] -->
                                <div class="tp-caption tp-resizeme"
                                     id="slide-101-layer-12"

                                     data-x="['right','right','right','right']"
                                     data-hoffset="['100','100','100','100']"
                                     data-y="['top','bottom','bottom','bottom']" data-voffset="['0','0','0','0']"
                                     data-lineheight="['none','none','none','none']"
                                     data-width="['500','500','500','500']"
                                     data-height="['none','none','none','none']"
                                     data-whitespace="nowrap"
                                     data-type="image"
                                     data-responsive_offset="on"

                                     data-frames='[
                                    {"from":"y:0px(R);opacity:0;","speed":2000,"to":"o:1;","delay":4000,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"


                                     style="z-index: 12;">
                                    <div class="coin-slide bg-full-width bg-repeat-y coin-slide-rotate"
                                         style="background-image:url(/Themes/HomePage/images/main-slider/slider2/coin-sky.png);height:100vh;"></div>
                                </div>

                            </li>

                            <!-- SLIDE  3 -->
                            <li data-index="rs-1002" data-transition="slidingoverlayhorizontal"
                                data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"
                                data-easein="default" data-easeout="default" data-masterspeed="default"
                                data-thumb="images/main-slider/slider2/slide1.jpg" data-rotate="0"
                                data-saveperformance="off" data-title="Slide" data-param1="" data-param2=""
                                data-param3="" data-param4="" data-param5="" data-param6="" data-param7=""
                                data-param8="" data-param9="" data-param10="" data-description="">
                                <!-- MAIN IMAGE -->
                                <img src="/Themes/HomePage/images/main-slider/slider2/slide1.jpg" alt=""
                                     data-bgposition="center bottom" data-bgfit="cover" data-bgrepeat="no-repeat"
                                     class="rev-slidebg" data-no-retina/>
                                <!-- LAYERS -->

                                <!-- LAYER NR. 1 [ for overlay ] -->
                                <div class="tp-caption tp-shape tp-shapewrapper "
                                     id="slide-102-layer-1"
                                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                                     data-width="full"
                                     data-height="full"
                                     data-whitespace="nowrap"
                                     data-type="shape"
                                     data-basealign="slide"
                                     data-responsive_offset="off"
                                     data-responsive="off"
                                     data-frames='[
                                    {"from":"opacity:0;","speed":1000,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power4.easeOut"}
                                    ]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 12;background-color:rgba(0, 0, 0, 0.3);border-color:rgba(0, 0, 0, 0);border-width:0px;">
                                </div>

                                <!-- LAYER NR. 2 [ for title ] -->
                                <div class="tp-caption   tp-resizeme"
                                     id="slide-102-layer-2"
                                     data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                                     data-y="['top','top','top','top']" data-voffset="['308','308','308','308']"
                                     data-fontsize="['60','60','60','60']"
                                     data-lineheight="['110','110','110','110']"
                                     data-width="['6','6','6','6']"
                                     data-height="['110,'110','110','110']"
                                     data-whitespace="['normal','normal','normal','normal']"

                                     data-type="text"
                                     data-responsive_offset="on"
                                     data-frames='[
                                    {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 13;
                                    white-space: normal;
                                    ">

                                    <div class="bg-primary">&nbsp;</div>

                                </div>

                                <!-- LAYER NR. 3 [ for title ] -->
                                <div class="tp-caption   tp-resizeme"
                                     id="slide-102-layer-3"
                                     data-x="['left','left','left','left']" data-hoffset="['60','60','60','100']"
                                     data-y="['top','top','top','top']" data-voffset="['300','300','300','300']"
                                     data-fontsize="['55','55','55','45']"
                                     data-lineheight="['60','60','60','65']"
                                     data-width="['700','700','700','700']"
                                     data-height="['none','none','none','none']"
                                     data-whitespace="['normal','normal','normal','normal']"

                                     data-type="text"
                                     data-responsive_offset="on"
                                     data-frames='[
                                    {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 13;
                                    white-space: normal;
                                    font-weight: 700;
                                    color: rgb(75, 57, 65);
                                    border-width:0px;">

                                    <div style="font-family: 'Poppins', sans-serif; text-transform:uppercase; ">
                                        <span class="text-primary" style="padding-right:10px;">the Massive</span><span
                                                class="text-white">Mining </span>
                                    </div>

                                </div>

                                <!-- LAYER NR. 4 [ for title ] -->
                                <div class="tp-caption   tp-resizeme"
                                     id="slide-102-layer-4"
                                     data-x="['left','left','left','left']" data-hoffset="['60','60','60','100']"
                                     data-y="['top','top','top','top']" data-voffset="['360','360','360','360']"
                                     data-fontsize="['53','53','53','45']"
                                     data-lineheight="['70','70','70','70']"
                                     data-width="['700','700','700','700']"
                                     data-height="['none','none','none','none']"
                                     data-whitespace="['normal','normal','normal','normal']"

                                     data-type="text"
                                     data-responsive_offset="on"
                                     data-frames='[
                                    {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":1000,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 13;
                                    white-space: normal;
                                    font-weight: 700;
                                    border-width:0px;">
                                    <div style="font-family: 'Poppins', sans-serif; text-transform:uppercase ;">
                                        <span class="text-white" style="padding-right:10px;">Easy Way</span><span
                                                class="text-primary">to miners </span>
                                    </div>

                                </div>

                                <!-- LAYER NR. 5 [ for paragraph] -->
                                <div class="tp-caption  tp-resizeme"
                                     id="slide-102-layer-5"
                                     data-x="['left','left','left','left']" data-hoffset="['30','30','30','100']"
                                     data-y="['top','top','top','top']" data-voffset="['440','440','440','440']"
                                     data-fontsize="['16','16','16','30']"
                                     data-lineheight="['30','30','30','40']"
                                     data-width="['600','600','600','600']"
                                     data-height="['none','none','none','none']"
                                     data-whitespace="['normal','normal','normal','normal']"

                                     data-type="text"
                                     data-responsive_offset="on"
                                     data-frames='[
                                    {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":1500,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index: 13;
                                    font-weight: 500;
                                    color:#fff;
                                    border-width:0px;">
                                    <span style="font-family: 'Poppins', sans-serif;">We have recently implemented a new changes in crypto mining fields to make it simpler and more predictable.</span>
                                </div>

                                <!-- LAYER NR. 6 [ for see all service botton ] -->
                                <div class="tp-caption tp-resizeme"
                                     id="slide-102-layer-6"
                                     data-x="['left','left','left','left']" data-hoffset="['30','30','30','100']"
                                     data-y="['top','top','top','top']" data-voffset="['530','530','530','600']"
                                     data-lineheight="['none','none','none','none']"
                                     data-width="['300','300','300','300']"
                                     data-height="['none','none','none','none']"
                                     data-whitespace="['normal','normal','normal','normal']"

                                     data-type="text"
                                     data-responsive_offset="on"
                                     data-frames='[
                                    {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index:13; text-transform:uppercase;">
                                    <a href="/Contact" class="site-button slider-btn-left">Contact
                                        Us</a>
                                </div>

                                <!-- LAYER NR. 7 [ for more detail botton ] -->
                                <div class="tp-caption tp-resizeme"
                                     id="slide-102-layer-7"
                                     data-x="['left','left','left','left']" data-hoffset="['220','220','220','320']"
                                     data-y="['top','top','top','top']" data-voffset="['530','530','530','600']"
                                     data-lineheight="['none','none','none','none']"
                                     data-width="['300','300','300','300']"
                                     data-height="['none','none','none','none']"
                                     data-whitespace="['normal','normal','normal','normal']"

                                     data-type="text"
                                     data-responsive_offset="on"
                                     data-frames='[
                                    {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"

                                     style="z-index:13;
                                    text-transform:uppercase;
                                    font-weight:500;
                                    ">
                                    <a href="/About"
                                       class=" site-button white slider-btn-right">Join Us</a>
                                </div>

                                <!-- LAYER NR. 8 [ for more detail botton ] -->
                                <div class="tp-caption tp-resizeme"
                                     id="slide-102-layer-8"
                                     data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
                                     data-y="['bottom','bottom','bottom','bottom']"
                                     data-voffset="['-20','-20','-20','-20']"

                                     data-frames='[
                                    {"from":"y:200px(R);opacity:0;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'

                                     style="z-index: 13;">
                                    <img src="/Themes/HomePage/images/main-slider/slider2/rock.png" alt="">
                                </div>

                                <!-- LAYER NR. 9 [ for more detail botton ] -->
                                <div class="tp-caption tp-resizeme"
                                     id="slide-102-layer-9"
                                     data-x="['right','right','right','right']"
                                     data-hoffset="['320','320','320','320']"
                                     data-y="['bottom','bottom','bottom','bottom']"
                                     data-voffset="['130','130','130','130']"

                                     data-frames='[
                                    {"from":"y:-500px(R);opacity:0;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'

                                     style="z-index: 13;">
                                    <img src="/Themes/HomePage/images/main-slider/slider2/agent.png" alt="">
                                </div>

                                <!-- LAYER NR. 10 [ for more detail botton ] -->
                                <div class="tp-caption tp-resizeme"
                                     id="slide-102-layer-10"

                                     data-x="['right','right','right','right']" data-hoffset="['20','20','20','20']"
                                     data-y="['bottom','bottom','bottom','bottom']"
                                     data-voffset="['80','80','80','80']"

                                     data-height="none"
                                     data-whitespace="nowrap"

                                     data-type="image"
                                     data-responsive_offset="on"

                                     data-frames='[{"from":"x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.75;sY:0.75;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":3000,"ease":"Power3.easeOut"},
                                    {"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                                     data-textAlign="['left','left','left','left']"
                                     data-paddingtop="[0,0,0,0]"
                                     data-paddingright="[0,0,0,0]"
                                     data-paddingbottom="[0,0,0,0]"
                                     data-paddingleft="[0,0,0,0]"


                                     style="z-index: 13;">
                                    <img src="/Themes/HomePage/images/main-slider/slider2/plant.png" alt="">
                                </div>
                            </li>

                        </ul>

                    </div>
                </div>
                <!-- END REVOLUTION SLIDER -->
            </div>
        </div>
    </div>
    <!-- SLIDER END -->

    <!-- OUR VALUE SECTION START -->
    <div class="section-full bg-primary">
        <div class="container">
            <div class="section-content ">
                <!-- COLL-TO ACTION START -->
                <div class="wt-subscribe-box">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-9 col-sm-9">
                                <div class="call-to-action-left p-tb20 ">
                                    <h4 class="text-uppercase m-b10 font-weight-600">Invest in Cryptocurrency Mining
                                        & Easy Way to Trade Crypto.</h4>
                                    <p>The Invest Chain Network is the global company of impact cryptocurrency
                                        investing, dedicated to increasing its scale and effectiveness around the
                                        world. Impact investments are investments made with the intention to
                                        generate positive, measurable social and environmental impact alongside a
                                        financial return.</p>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="call-to-action-right p-tb30">
                                    <a href="/About" class="site-button-secondry text-uppercase font-weight-600">
                                        Read More
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- OUR VALUE SECTION  END -->

    <!-- ABOUT COMPANY SECTION START -->
    <div class="section-full home-about-section p-t80 bg-no-repeat bg-bottom-right"
         style="background-image:url(/Themes/HomePage/images/background/bg-coin.png)">
        <div class="container-fluid ">
            <div class="row">
                <div class="col-md-6">
                    <div class="wt-box text-right">
                        <img src="/Themes/HomePage/images/background/bg-laptop.png" alt="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="wt-right-part p-b80">
                        <!-- TITLE START -->
                        <div class="section-head text-left">
                            <span class="wt-title-subline font-16 text-gray-dark m-b15">What is invest chain network</span>
                            <h2 class="text-uppercase">Invest Chain Network</h2>
                            <div class="wt-separator-outer">
                                <div class="wt-separator bg-primary"></div>
                            </div>
                        </div>
                        <!-- TITLE END -->
                        <div class="section-content">
                            <div class="wt-box">
                                <video width="420" height="240" controls>
                                    <source src="/assets/Invest.mp4" type="video/mp4">
                                    Your browser does not support the video tag.
                                </video>
                                <p>
                                    <strong>Invest Chain Network with registration 13047680 Company number, has an experienced team in the field of trading and mining cryptocurrencies. which has reached to 100% monthly profit return in the humane and bots way, and to guarantee the principal and profit of investors, are locked 100,000 £  as initial investment  and 10,000,000 £ as real estate  to Companies for England and wales.
                                    </strong>
                                </p>
                                <p>We provide 12 to 16 percent monthly and 144 to 172 percent annually profit for people who do not have the knowledge of trading and mining and entrust their investment to us. Which as the work progresses will pay higher profits.</p>
                                <a href="/About" class="site-button text-uppercase m-r15">Read More</a>
                                <a href="/Contact" class="site-button-secondry text-uppercase">Contact
                                    us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ABOUT COMPANY SECTION  END -->

    <!-- WHY CHOOSE US SECTION START  -->
    <div class="section-full  p-t80 p-b80 bg-gray">
        <div class="container">
            <!-- TITLE START-->
            <div class="section-head text-center">
                <span class="wt-title-subline font-16 text-gray-dark m-b15">Buy and Sell Bitcoin</span>
                <h2 class="text-uppercase">Why Choose Bitcoin</h2>
                <div class="wt-separator-outer">
                    <div class="wt-separator bg-primary"></div>
                </div>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                    the industry's standard dummy text ever since the 1500s.</p>
            </div>
            <!-- TITLE END-->
            <div class="section-content no-col-gap">
                <div class="row">

                    <div class="col-md-4 col-sm-6 animate_line">
                        <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                            <img src="/assets/docs/0001.jpg" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 animate_line">
                        <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                            <img src="/assets/docs/0002.jpg" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 animate_line">
                        <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                            <img src="/assets/docs/0003.jpg" alt=""></a>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <!-- WHY CHOOSE US SECTION END -->

    <!-- HOW IT WORK SECTION START  -->
    <div class="section-full  p-t80 p-b80 bg-gray">
        <div class="container">
            <!-- TITLE START-->
            <div class="section-head text-center">
                <span class="wt-title-subline font-16 text-gray-dark m-b15">Choose a plan for investing</span>
                <h2 class="text-uppercase">invest chain network</h2>
                <div class="wt-separator-outer">
                    <div class="wt-separator bg-primary"></div>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga eos optio ducimus odit, labore hic
                    fugiat iusto veniam necessitatibus quas doloremque sapiente maiores.</p>
            </div>
            <!-- TITLE END-->
            <div class="section-content no-col-gap">
                <div class="row">
                    <!-- COLUMNS 1 -->
                    <div class="col-md-3 col-sm-3 step-number-block">
                        <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                            <div class="icon-lg text-primary m-b20">
                                <a href="#" class="icon-cell"><img
                                            src="/Themes/HomePage/images/icon/pick-4.png" alt=""></a>
                            </div>
                            <div class="icon-content">
                                <div class="step-number">1</div>
                                <h4 class="wt-tilte text-uppercase font-weight-500">Package A / annually</h4>
                                <p>
                                    <strong> 50$ to 499$ USDt</strong>
                                </p>
                                <ul>
                                    <li><strong>Monthly Profit</strong> 12 - 12.50 percent <br> </li>
                                    <li><strong>Daily Profit</strong> 0.4 percent<br></li>
                                    <li>* Daily deposit to user wallet * <br></li>
                                    <li></li>
                                    <li><a href="/AdminPanel/Package/PackageList" class="site-button text-uppercase m-r15">Buy</a></li>
                                </ul>

                            </div>
                        </div>
                    </div>
                    <!-- COLUMNS 2 -->
                    <div class="col-md-3 col-sm-3 step-number-block">
                        <div class="wt-icon-box-wraper  p-a30 center bg-secondry m-a5 ">
                            <div class="icon-lg m-b20">
                                <a href="#" class="icon-cell"><img
                                            src="/Themes/HomePage/images/icon/pick-28.png" alt=""></a>
                            </div>
                            <div class="icon-content text-white">
                                <div class="step-number active">2</div>
                                <h4 class="wt-tilte text-uppercase font-weight-500">Package B / annually</h4>
                                <p>
                                    <strong> 500$ to 1999$ USDt</strong>
                                </p>
                                <ul>
                                    <li><strong>Monthly Profit</strong> 12 - 13.50 percent <br> </li>
                                    <li><strong>Daily Profit</strong> 0.43 percent<br></li>
                                    <li>* Daily deposit to user wallet * <br></li>
                                    <li></li>
                                    <li><a href="/AdminPanel/Package/PackageList" class="site-button text-uppercase m-r15">Buy</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- COLUMNS 3 -->
                    <div class="col-md-3 col-sm-3 step-number-block">
                        <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                            <div class="icon-lg text-primary m-b20">
                                <a href="#" class="icon-cell"><img
                                            src="/Themes/HomePage/images/icon/pick-12.png" alt=""></a>
                            </div>
                            <div class="icon-content">
                                <div class="step-number">3</div>
                                <h4 class="wt-tilte text-uppercase font-weight-500">Package C / annually</h4>
                                <p>
                                    <strong> 2000$ to 9999$ USDt</strong>
                                </p>
                                <ul>
                                    <li><strong>Monthly Profit</strong> 13 - 14.50 percent <br> </li>
                                    <li><strong>Daily Profit</strong> 0.47 percent<br></li>
                                    <li>* Daily deposit to user wallet * <br></li>
                                    <li></li>
                                    <li><a href="/AdminPanel/Package/PackageList" class="site-button text-uppercase m-r15">Buy</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- COLUMNS 4 -->
                    <div class="col-md-3 col-sm-3 step-number-block">
                        <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                            <div class="icon-lg text-primary m-b20">
                                <a href="#" class="icon-cell"><img
                                            src="/Themes/HomePage/images/icon/pick-19.png" alt=""></a>
                            </div>
                            <div class="icon-content">
                                <div class="step-number">4</div>
                                <h4 class="wt-tilte text-uppercase font-weight-500">Package D / annually</h4>
                                <p>
                                    <strong> 10000$ USDt</strong>
                                </p>
                                <ul>
                                    <li><strong>Monthly Profit</strong> 15 - 16 percent <br> </li>
                                    <li><strong>Daily Profit</strong> 0.53 percent<br></li>
                                    <li>* Daily deposit to user wallet * <br></li>
                                    <li></li>
                                    <li><a href="/AdminPanel/Package/PackageList" class="site-button text-uppercase m-r15">Buy</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
{{--                <div class="row">--}}
{{--                    <!-- COLUMNS 2 -->--}}
{{--                    <div class="col-md-3 col-sm-3 step-number-block">--}}
{{--                        <div class="wt-icon-box-wraper  p-a30 center bg-secondry m-a5 ">--}}
{{--                            <div class="icon-lg m-b20">--}}
{{--                                <a href="#" class="icon-cell"><img--}}
{{--                                            src="/Themes/HomePage/images/icon/pick-29.png" alt=""></a>--}}
{{--                            </div>--}}
{{--                            <div class="icon-content text-white">--}}
{{--                                <div class="step-number active">X</div>--}}
{{--                                <h4 class="wt-tilte text-uppercase font-weight-500">Package Extra / for investors</h4>--}}
{{--                                <p>--}}
{{--                                    <strong> 20$-49$ USDt</strong>--}}
{{--                                </p>--}}
{{--                                <ul>--}}
{{--                                    <li><strong>Monthly Profit</strong> 12  percent <br> </li>--}}
{{--                                    <li>* Daily deposit to user wallet * <br></li>--}}
{{--                                    <li>* pre requirement: must be purchase one of the package A,B,C or D *<br></li>--}}
{{--                                    <li></li>--}}
{{--                                    <li><a href="/AdminPanel/Package/PackageList" class="site-button text-uppercase m-r15">Buy</a></li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                </div>--}}
            </div>

        </div>
    </div>
    <!-- HOW IT WORK  SECTION END -->

    <!-- SECTION CONTENT START -->
    <div class="section-full  p-tb80 bg-full-height bg-repeat-x graph-slide-image"
         style="background-image:url(/Themes/HomePage/images/background/bg-1.jpg);">

        <div class="container">
            <div class="row">
                <div class="col-md-12  clearfix">
                    <div class="bit-converter p-a40 p-b15 bg-white">
                        <div class="wt-box">
                            <h2 class="text-uppercase m-t0 text-primary">the important note about termination</h2>
                            <ul class="list-check-circle primary">
                                <li><strong>the termination any packages isn't possible before 2 months</strong></li>
                                <li><strong>be removed all profits deposited to wallet , if termination after 2 to 6 months.</strong></li>
                                <li><strong>the termination after  6 months,to be deducted 60 percent of all profit deposited to user wallet.</strong> </li>
{{--                                <li><strong>* pre requirement for extra package: must be purchase one of the package A,B,C or D *</strong> </li>--}}
                            </ul>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- SECTION CONTENT  END -->


    <!-- SECTION CONTENT START -->
    <div class="section-full no-col-gap bg-repeat">
        <div class="container-fluid">

            <div class="row">

            </div>

        </div>
    </div>
    <!-- SECTION CONTENT  END -->




    <!-- SECTION CONTENT START -->
    <div class="section-full p-t80 p-b50 bg-center bg-full-height bg-no-repeat"
         style="background-image:url(/Themes/HomePage/images/background/bg-testimonial.jpg);">
        <div class="container">
            <!-- TITLE START -->
            <div class="section-head text-center">
                <span class="wt-title-subline font-16 text-gray-dark m-b15">Claimer</span>
                <h2 class="text-uppercase">Invet Chain Network Claimer</h2>
                <div class="wt-separator-outer">
                    <div class="wt-separator bg-primary"></div>
                </div>
            </div>
            <!-- TITLE END -->

            <!-- TESTIMONIAL 4 START ON BACKGROUND -->
            <div class="section-content">
                <div class="owl-carousel home-carousel-1">
                    <div class="item">
                        <div class="testimonial-10">
                            <div class="testimonial-pic-block radius-bx">
                                <div class="testimonial-pic radius">

                                </div>
                            </div>
                            <div class="testimonial-text clearfix">
                                <div class="testimonial-paragraph">
                                    <span class="fa fa-quote-left text-primary"></span>
                                    <p>Invest Chain Network has registered the following Wallet address in Companies for England and Wales as an official account. Takes responsibility for all its deposits, and guarantees the repayment of initial investment and profits.
                                    </p>
                                    <span class="fa fa-quote-left text-primary"></span>
                                </div>
                                <div class="testimonial-detail clearfix">
                                    <strong class="testimonial-name"></strong>
                                    <span class="testimonial-position text-primary p-t10"></span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-10">
                            <div class="testimonial-pic-block radius-bx">
                                <div class="testimonial-pic radius">

                                </div>
                            </div>
                            <div class="testimonial-text clearfix">
                                <div class="testimonial-paragraph">
                                    <img  src="/assets/swwwa.png" width="150"
                                         height="150" alt="">
                                    <p>Public Address to Receive USDT : <input type="text" class="form-control" id="copy-to-clipboard-input" value="TPkYKKMUUWdxiq5LHc8Vc2GGDAhgoJQstK"  /></p><button class="btn btn-outline-primary" id="btn-copy">Copy</button>
                                </div>
                                <div class="testimonial-detail clearfix">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- SECTION CONTENT END -->
    <div class="section-head text-center p-tb50">
        <h2 class="text-uppercase">Map Location</h2>
        <div class="wt-separator-outer"><div class="wt-separator bg-primary"></div>13-14 Hanover Street London W1S 1YH
            <div class="wt-separator bg-primary"></div></div>
        <div class="mapouter">
            <div class="gmap_canvas">

               <P>
                   <iframe width="600" height="350" id="gmap_canvas"
                           src="https://maps.google.com/maps?q=13%2014%20hanover%20street%20london%20england%20w1s%201yh&t=&z=13&ie=UTF8&iwloc=&output=embed"
                           frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>

                        </P>
            </div>
        </div>
    </div>
@stop
