<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item me-auto"><a class="navbar-brand" href="/index"><span class="brand-logo">
                            </span>
                    <h2 class="brand-text">icNetwork.</h2>
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i
                            class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i
                            class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary"
                            data-feather="disc" data-ticon="disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

            <li class=" navigation-header"><span data-i18n="Apps &amp; Pages">Apps &amp; Pages</span><i
                        data-feather="more-horizontal"></i>
            </li>
            @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.user.wallet']))
            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather='dollar-sign'></i><span
                            class="menu-title text-truncate" data-i18n="Wallet"><strong>Wallet</strong></span></a>
                <ul class="menu-content">
                    <li>
                        <a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                    class="menu-item text-truncate" data-i18n="Users"><h4 class="text-primary">Users</h4></span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="/AdminPanel/Wallet/UserAssets" ><span
                                            class="menu-item text-truncate" data-i18n="Deposit"><h4 class="text-success">Deposit</h4></span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="/AdminPanel/Wallet/UserWithdraw" ><span
                                            class="menu-item text-truncate" data-i18n="Withdraw"><h4 class="text-success">Withdraw</h4></span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="/AdminPanel/Wallet/UserTransfer" ><span
                                            class="menu-item text-truncate" data-i18n="Transfer"><h4 class="text-success">Transfer</h4></span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="/AdminPanel/Wallet/UserTransactions" ><span
                                            class="menu-item text-truncate" data-i18n="Transactions"><h4 class="text-success">Transactions</h4></span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="/AdminPanel/Wallet/UserProfits" ><span
                                            class="menu-item text-truncate" data-i18n="Profits"><h4 class="text-success">Profits</h4></span></a>
                            </li>
                        </ul>
                    </li>
                    @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.deposit.admin']))
                    <li>
                        <a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                    class="menu-item text-truncate" data-i18n="Register"><h4 class="text-primary">Administrators</h4></span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="/AdminPanel/Wallet/DepositList"
                                ><span class="menu-item text-truncate" data-i18n="Basic"><h4 class="text-success">Deposit List</h4></span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="/AdminPanel/Wallet/WithdrawList"
                                ><span class="menu-item text-truncate" data-i18n="Cover"><h4 class="text-success">Withdraw List</h4></span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="/AdminPanel/Wallet/TransferList"
                                ><span class="menu-item text-truncate" data-i18n="Cover"><h4 class="text-success">Transfer List</h4></span></a>
                            </li>
                        </ul>
                    </li>
                        @endif
                </ul>
            </li>
            @endif
            @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.user.package']))
            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather='box'></i><span
                            class="menu-title text-truncate" data-i18n="Wallet"><strong>Packages</strong></span></a>
                <ul class="menu-content">
                    <li>
                        <a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                    class="menu-item text-truncate" data-i18n="Users"><h4 class="text-primary">Users</h4></span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="/AdminPanel/Package/PackageList" ><span
                                            class="menu-item text-truncate" data-i18n="Assets"><h4 class="text-success">PackageList</h4></span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="/AdminPanel/Package/UserPackages" ><span
                                            class="menu-item text-truncate" data-i18n="Deposit"><h4 class="text-success">User Packages</h4></span></a>
                            </li>
                        </ul>
                    </li>
                    @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.package.admin']))
                    <li>
                        <a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                    class="menu-item text-truncate" data-i18n="Administrators"><h4 class="text-primary">Administrators</h4></span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="/AdminPanel/Package/TotalUserPackages" ><span
                                            class="menu-item text-truncate" data-i18n="TotalUserPackets"><h4 class="text-success">TotalUserPackets</h4></span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="/AdminPanel/Package/TerminationList" ><span
                                            class="menu-item text-truncate" data-i18n="TerminationList"><h4 class="text-success">TerminationList</h4></span></a>
                            </li>
                        </ul>
                    </li>
                        @endif
                </ul>
            </li>
            @endif
            @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.user.branch']))
            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather='git-pull-request'></i><span
                            class="menu-title text-truncate" data-i18n="Branches"><strong>Branches</strong></span></a>
                <ul class="menu-content">
                    <li>
                        <a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                    class="menu-item text-truncate" data-i18n="Users"><h4 class="text-primary">Users</h4></span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="/AdminPanel/Branches/UserLevel" ><span
                                            class="menu-item text-truncate" data-i18n="UserLevel"><h4 class="text-success">UserLevel</h4></span></a>
                            </li>
                            <li><a class="d-flex align-items-center" href="/AdminPanel/Branches/UpLine" ><span
                                            class="menu-item text-truncate" data-i18n="UpLine"><h4 class="text-success">UpLine</h4></span></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            @endif
            @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.user.support']))
            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather='send'></i><span
                            class="menu-title text-truncate" data-i18n="Wallet"><strong>Support</strong></span></a>
                <ul class="menu-content">
                    <li>
                        <a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                    class="menu-item text-truncate" data-i18n="Users"><h4 class="text-primary">Users</h4></span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="/AdminPanel/Support/RequestList" ><span
                                            class="menu-item text-truncate" data-i18n="RequestList"><h4 class="text-success">RequestList</h4></span></a>
                            </li>
                        </ul>
                    </li>
                    @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.support.admin']))
                        <li>
                            <a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="Administrators"><h4 class="text-primary">Administrators</h4></span></a>
                            <ul class="menu-content">
                                <li><a class="d-flex align-items-center" href="/AdminPanel/Support/TotalRequestList" ><span
                                                class="menu-item text-truncate" data-i18n="TotalRequestList"><h4 class="text-success">TotalRequestList</h4></span></a>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </li>
            @endif
            @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.user.support']))
            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather='bell'></i><span
                            class="menu-title text-truncate" data-i18n="Notifications"><strong>Notification</strong></span></a>
                <ul class="menu-content">
                    <li>
                        <a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                    class="menu-item text-truncate" data-i18n="Users"><h4 class="text-primary">Users</h4></span></a>
                        <ul class="menu-content">
                            <li><a class="d-flex align-items-center" href="/AdminPanel/Support/UserNotification" ><span
                                            class="menu-item text-truncate" data-i18n="UserNotification"><h4 class="text-success">UserNotification</h4></span></a>
                            </li>
                        </ul>
                    </li>
                    @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.support.admin']))
                        <li>
                            <a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span
                                        class="menu-item text-truncate" data-i18n="Administrators"><h4 class="text-primary">Administrators</h4></span></a>
                            <ul class="menu-content">
                                <li><a class="d-flex align-items-center" href="/AdminPanel/Support/TotalNotification" ><span
                                                class="menu-item text-truncate" data-i18n="TotalNotification"><h4 class="text-success">TotalNotification</h4></span></a>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </li>
            @endif
            @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.notifications']))
            <li class=" nav-item"><a class="d-flex align-items-center" href="/AdminPanel/ContactUsList"><i
                            data-feather="message-square"></i><span class="menu-title text-truncate" data-i18n="ContactUs"><strong>ContactUs</strong></span></a>
            </li>
            @endif
            @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.users']))
                            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="user"></i><span
                                                        class="menu-title text-truncate" data-i18n="User"><strong>User</strong></span></a>
                                    <ul class="menu-content">
                                            <li><a class="d-flex align-items-center" href="/AdminPanel/Users/UsersList"><i
                                                                           data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List"><h4 class="text-primary">List</h4></span></a>
                                                </li>
                                        </ul>
                </li>
            @endif

        </ul>
    </div>
</div>
<!-- END: Main Menu-->
