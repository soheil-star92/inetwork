<?php

namespace App\DbModels\Dashboard\Wallet;

use Illuminate\Database\Eloquent\Model;

class WalletWithdraw extends Model
{
    protected $table='wallet_withdraws';
}
