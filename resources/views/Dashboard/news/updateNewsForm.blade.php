@extends('Layouts.dashboardMainLayout')
@section('BreadCrumbLevel')
    <li class="active">ویرایش خبر</li>
@Stop
@section('BreadCrumbURL','/news/newsLists')
@section('BreadCrumbTitle','لیست اخبار')
@section('CssFiles')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ URL::asset('assets/login/bower_components/select2/dist/css/select2.min.css') }}">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="/assets/login/bower_components/iCheck/all.css">
    <link rel="stylesheet" href="/assets/login/CustomFiles/ImagePreview.css">
    <link rel="stylesheet" type="text/css" href="/assets/login/bower_components/fancyBox/jquery.fancybox.min.css">

@endsection

@section('MainContent')
    <div class="row">
        <section class="col-lg-12 col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <i class="fa fa-info-circle"></i>
                    <h3 class="box-title">
                        {{ \App\Facade\DbSettings::GetDbSetting()->Title }}
                    </h3>
                    <!-- tools box -->
                    <div class="pull-left box-tools">
                        <button type="button" class="btn bg-info btn-sm" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /. tools -->
                </div>
                <div class="box-body">
                    <div class="modal modal-info fade" id="modal-news-language">
                        <form action="/news/NewsAddInOtherLanguage" method="post" id="AddF">
                            {{csrf_field()}}
                            <input type="hidden" name="NID" id="NID" value="{{$news['id']}}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">خبر در زبان های دیگر</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>
                                            <label for="SelectLanguage">زبان</label>
                                            <select name="SelectLanguage" id="SelectLanguage" class="form-control">
                                                @foreach($Languages AS $row)
                                                    <option value="{{$row->Title}}">{{$row->Title}}</option>
                                                @endforeach
                                            </select>
                                        </p>
                                        <p>
                                            <label for="LanguageTopTitle">رو تیتر خبر</label>
                                            <input type="text" class="form-control" name="LanguageTopTitle"
                                                   id="LanguageTopTitle">
                                        </p>
                                        <p>
                                            <label for="LanguageBottomTitle">زیر تیتر خبر</label>
                                            <input type="text" class="form-control" name="LanguageBottomTitle"
                                                   id="LanguageBottomTitle">
                                        </p>
                                        <p>
                                            <label for="LanguageTitle"> تیتر خبر</label>
                                            <input type="text" class="form-control" name="LanguageTitle"
                                                   id="LanguageTitle">
                                        </p>
                                        <p>
                                            <label for="LanguageContent">متن خبر</label>
                                            <textarea name="LanguageContent" id="mytextarea" cols="5" rows="5"
                                                      class="form-control"></textarea>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">
                                            خروج
                                        </button>
                                        <input type="submit" class="btn btn-outline" value="ثبت">
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                        </form>
                        <!-- /.modal-dialog -->
                    </div>
                    <div class="modal modal-info fade" id="modal-news-language-update">
                        <form action="/news/NewsUpdateInOtherLanguage" method="post" id="EditF">
                            {{csrf_field()}}
                            <input type="hidden" name="NID" id="NID" value="{{$news['id']}}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">ویرایش خبر در زبان های دیگر</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>
                                            <label for="SelectLanguage">زبان</label>
                                            <select name="SelectLanguage" id="SelectLanguage" class="form-control">
                                                @foreach($Languages AS $row)
                                                    <option value="{{$row->Title}}">{{$row->Title}}</option>
                                                @endforeach
                                            </select>
                                        </p>
                                        <p>
                                            <label for="ShowLanguageTopTitle">رو تیتر خبر</label>
                                            <input type="text" class="form-control" name="ShowLanguageTopTitle"
                                                   id="ShowLanguageTopTitle" value="{{$NewsEn['Top_Title']}}">
                                        </p>
                                        <p>
                                            <label for="ShowLanguageBottomTitle">زیر تیتر خبر</label>
                                            <input type="text" class="form-control" name="ShowLanguageBottomTitle"
                                                   id="ShowLanguageBottomTitle" value="{{$NewsEn['Bottom_Title']}}">
                                        </p>
                                        <p>
                                            <label for="ShowLanguageTitle"> تیتر خبر</label>
                                            <input type="text" class="form-control" name="ShowLanguageTitle"
                                                   id="ShowLanguageTitle" value="{{$NewsEn['Title']}}">
                                        </p>
                                        <p>
                                            <label for="ShowLanguageContent">متن خبر</label>
                                            <textarea name="ShowLanguageContent" id="mytextarea" cols="5" rows="5"
                                                      class="form-control">{{$NewsEn['Content']}}</textarea>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">
                                            خروج
                                        </button>
                                        <input type="submit" class="btn btn-outline" value="ویرایش">
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                        </form>
                        <!-- /.modal-dialog -->
                    </div>
                    <div class="content">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                @include('Dashboard.toolbars.newsToolbar')
                                <a class="btn btn-app" data-toggle="modal" data-target="#modal-news-language">
                                    <i class="fa fa-language"></i>خبر در زبان دیگر
                                </a>
                                <a class="btn btn-app" data-toggle="modal" data-target="#modal-news-language-update">
                                    <i class="fa fa-language"></i>ویرایش خبر در زبان دیگر
                                </a>
                            </div>
                            <div class="box-header with-border">
                                <h3 class="box-title">ویرایش خبر</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            @php
                                $url=URL::previous();
                               if (strpos($url, '/news/myNews') !== false) {
                                    $checkPost=true;
                                }else{
                                    $checkPost=false;
                                }
                            @endphp
                            <form method="post" id="updateNewsForm" @if($checkPost == false) action="/news/updateNews"
                                  @else action="/news/updateMyNews" @endif class="form-horizontal"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="NewsId" value="{{ $news['id'] }}">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">عکس خبر</label>
                                        <div class="col-lg-5 col-sm-4 col-md-4 col-xs-4">
                                            <a data-fancybox="images" data-caption="{{ $news['Title'] }}"
                                               href="{{ asset($news['Image']) }}">
                                                <img height="180" width="180" src="{{ asset($news['Image']) }}" alt="">
                                            </a>
                                        </div>
                                        <label class="col-sm-1 control-label">ویرایش عکس</label>
                                        <div class="col-lg-5 col-sm-4 col-md-4 col-xs-4">
                                            <div id="image-preview">
                                                <label for="image-upload" id="image-label">عکس جدید</label>
                                                <input type="file" name="fileupload" id="image-upload"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">روتیتر خبر</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="TopTitle" class="form-control"
                                                   placeholder="روتیتر خبر"
                                                   value="{{ $news['Top_Title'] }}"
                                            >
                                        </div>
                                        <label class="col-sm-1 control-label">زیرتیتر خبر</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="BottomTitle" class="form-control"
                                                   placeholder="زیرتیتر خبر"
                                                   value="{{ $news['Bottom_Title'] }}"
                                            >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">تیتر خبر</label>
                                        <input type="hidden" id="flag" value="0">
                                        <div class="col-sm-11">
                                            <input type="text" name="Title" id="Title" class="form-control"
                                                   placeholder="تیتر خبر"
                                                   value="{{ $news['Title'] }}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">متن خبر</label>
                                        <div class="col-sm-11">
                                            <textarea name="Content" id="mytextarea"
                                                      style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                                      required>
                                                {{ $news['Content'] }}
                                            </textarea>
                                            <input type="hidden" name="txtNewsId" value="{{ $news['id'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">سرویس</label>
                                        <div class="col-sm-2">
                                            <select class="form-control" id="NewsCategoryId" name="NewsCategoryId">
                                                @foreach($newsCategories as $k)
                                                    <option value="{{ $k['id'] }}"
                                                            @if($k->id == $news['news_category_id']) selected @endif>{{ $k['PersianTitle'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <label class="col-sm-1 control-label">زیرمجموعه</label>
                                        <div class="col-sm-2">
                                            <select name="NewsCategoryChildID" id="NewsCategoryChildID"
                                                    class="form-control">
                                                <option value="0">بدون زیرمجموعه</option>
                                                @foreach($newsCategoryChildes as $nc)
                                                    <option value="{{ $nc->id }}"
                                                            @if($nc->id == $news['news_category_child_id']) selected @endif>{{ $nc->PersianTitle }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.news.allow.change.news.state']))
                                            <label class="col-sm-2 control-label">تغییر وضعیت خبر</label>
                                            <div class="col-sm-3">
                                                <select class="form-control" name="NewsStateId">
                                                    @foreach($newsStates as $k)
                                                        <option value="{{ $k['id'] }}"
                                                                @if($k->id == $news['news_state_id']) selected @endif>{{ $k['Title'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">کلمات کلیدی</label>
                                        <div class="col-sm-11">
                                            <select class="form-control select2" multiple="multiple" id="keyword"
                                                    data-placeholder="کلمه کلیدی" name="Keywords[]">
                                                @foreach($selectedKeywords as $k)
                                                    <option value="{{ $k['id'] }}" selected>{{ $k['Title'] }}</option>
                                                @endforeach
                                                @foreach($keywords as $k)
                                                    <option value="{{ $k->id }}">{{ $k->Title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">ویژگی خبر</label>
                                        <div class="col-sm-11">
                                            اسلایدر:<input type="checkbox" name="SliderState" id="SliderState"
                                                           class="flat-red" @if($news['Slider'] == 1) checked @endif>
                                            عکس خبر حذف شود:<input type="checkbox" name="newsImgRemove"
                                                                   id="newsImgRemove" class="flat-red">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.news.enable.update.news.button']))
                                        <button type="submit" class="btn btn-info pull-right">به روز رسانی</button>
                                    @endif
                                </div>
                                <!-- /.box-footer -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('JsFiles')
    <!-- TinyMCE Editor -->
    <script src="{{ URL::asset('assets/login/bower_components/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ URL::asset('assets/login/bower_components/tinymce/myStyle/CustomizeFunctions.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ URL::asset('assets/login/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ URL::asset('assets/login/bower_components/fancyBox/jquery.fancybox.min.js') }}"></script>
    <script src="{{ URL::asset('assets/login/bower_components/imagePreview/jquery.uploadPreview.min.js') }}"></script>
    <!-- iCheck 1.0.1 -->
    <script src="/assets/login/bower_components/iCheck/icheck.min.js"></script>
    <!-- Custom Functions -->
    <script src="/assets/login/CustomFiles/ImagePreview.js"></script>
    <script src="/assets/login/CustomFiles/CustomFancyBox.js"></script>
    <script src="/assets/login/CustomFiles/CustomSelect2.js"></script>
    <script src="/assets/login/CustomFiles/CustomFlatCheckBox.js"></script>
    <script>
        $(document).ready(function () {
            @if(Session::has('error_at_news_update'))
            swal("{{Session::get('error_at_news_update')}}", {
                icon: "warning",
                dangerMode: true,
            });
            @endif
            @if(Session::has('error_at_add_other_language'))
            swal("{{Session::get('error_at_add_other_language')}}", {
                icon: "warning",
                dangerMode: true,
            });
            @endif
            @if(Session::has('news_add_in_other_language_success'))
            swal("","اطلاعات مورد نظر با موفقیت ایجاد گردید", "success");
            @endif
            @if(Session::has('news_update_in_other_language_success'))
            swal("","اطلاعات مورد نظر با موفقیت ویرایش گردید", "success");
            @endif

        });
    </script>
    <script type="text/javascript">

        $(function () {

            $('#NewsCategoryId').change(function () {
                var id = $(this).val();
                var token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/news/GetNewsCategoryChildesByAjax',
                    data: {_token: token, id: id},
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.state == 1) {
                            $('#NewsCategoryChildID').empty();
                            $('#NewsCategoryChildID').append('<option value="0">بدون     طبقه بندی</option>');
                            jQuery.each(data.message, function (key, value) {
                                $('#NewsCategoryChildID').append('<option value="' + value.id + '">' + value.PersianTitle + '</option>');
                            });
                        } else {
                            alert(data.message);
                        }
                    }
                });
            });


            $('#Title').change(function () {
                var titleValue = $(this).val();
                var sliceTitle = titleValue.split(' ');
                var flag = $('#flag').val();
                jQuery.each(sliceTitle, function (key, value) {
                    if ((value.length > 3) && (flag === '0')) {
                        $('#keyword').append('<option value="' + value + '">' + value + '</option>');
                    }
                });
                $('#flag').val('1');
            });

            $('#updateNewsForm').one('submit', function () {
                $(this).find('button[type="submit"]').attr('disabled', 'disabled');
            });
            $('#AddF').one('submit', function () {
                $(this).find('input[type="submit"]').attr('disabled', 'disabled');
            });
            $('#EditF').one('submit', function () {
                $(this).find('input[type="submit"]').attr('disabled', 'disabled');
            });
        })
    </script>
@endsection