<?php


namespace App\Facade;
use Illuminate\Support\Facades\Facade;

class DbSettings extends Facade
{
    public static function getFacadeAccessor()
    {
        return 'DbSettings';
    }
}