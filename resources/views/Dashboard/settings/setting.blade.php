@extends('Layouts.dashboardMainLayout')

@section('CssFiles')
    <link rel="stylesheet" href="{{ URL::asset('assets/login/bower_components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="/assets/login/CustomFiles/ImagePreview.css">
    <link rel="stylesheet" type="text/css" href="/assets/login/bower_components/fancyBox/jquery.fancybox.min.css">

@endsection

@section('MainContent')
    <div class="row">
        <section class="col-lg-12 col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <i class="fa fa-info-circle"></i>
                    <h3 class="box-title">
                        {{ \App\Facade\DbSettings::GetDbSetting()->Title }}
                    </h3>
                    <!-- tools box -->
                    <div class="pull-left box-tools">
                        <button type="button" class="btn bg-info btn-sm" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /. tools -->
                </div>
                <div class="box-body">
                    <div class="content">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">تنظیمات پایه سایت</h3>
                            </div>
                            <div class="box-header with-border">
                                @includeIf('Dashboard.toolbars.settingToolbar')
                            </div>
                        <!-- /.box-header -->
                            <!-- form start -->
                            <form method="post" action="/settings/updateSetting" id="editC" class="form-horizontal"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="box-body">
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">لوگو سایت</label>
                                        <div class="col-lg-5 col-sm-4 col-md-4 col-xs-4">
                                            <a data-fancybox="images" data-caption="لوگو سایت"
                                               href="{{ asset($settings['MainLogo']) }}">
                                                <img height="150" width="150" src="{{ asset($settings['MainLogo']) }}" alt="">
                                            </a>
                                            <input type="file" name="MainLogo"/>
                                        </div>
                                        <label class="col-sm-1 control-label">FavIcon</label>
                                        <div class="col-lg-5 col-sm-4 col-md-4 col-xs-4">
                                            <a data-fancybox="images" data-caption="FavIcon"
                                               href="{{ asset($settings['FavIcon']) }}">
                                                <img height="150" width="150" src="{{ asset($settings['FavIcon']) }}" alt="">
                                            </a>
                                            <input type="file" name="FavIcon" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">عنوان سایت</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="txtTitle" class="form-control"
                                                   placeholder="عنوان سایت"
                                                   value="{{ $settings['Title'] }}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">درباره سایت</label>
                                        <div class="col-sm-10">
                                            <textarea name="txtAboutUs" id="mytextarea" cols="5" rows="5" class="form-control">{{ $settings['AboutUs'] }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">شرح سایت(طول بین 250 - 270 عدد)</label>
                                        <div class="col-sm-10">
                                            <textarea name="txtDescription" id="txtDescription" class="form-control"
                                                      placeholder="شرح سایت"
                                                      required>{{ $settings['Description'] }}</textarea>
                                            <span id="descriptionLength">

                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">کلمات کلیدی سایت</label>
                                        <div class="col-sm-10">
                                            <textarea name="txtKeywords" class="form-control"
                                                      placeholder="کلمات کلیدی سایت"
                                                      required>{{ $settings['Keywords'] }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ایمیل سایت</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="txtEmail" class="form-control"
                                                   placeholder="ایمیل سایت"
                                                   value="{{ $settings['Email'] }}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">شماره تماس</label>
                                        <div class="col-sm-10">
                                            <textarea name="txtPhone" class="form-control"  id="mytextarea" cols="5" rows="5">{{ $settings['Phone'] }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">آدرس</label>
                                        <div class="col-sm-10">
                                            <textarea name="txtAddress"  id="mytextarea" class="form-control"
                                                      placeholder="آدرس"
                                                      required>{{ $settings['Address'] }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-info pull-right">بروزرسانی</button>
                                </div>
                                <!-- /.box-footer -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('JsFiles')
    <!-- TinyMCE Editor -->
    <script src="{{ URL::asset('assets/login/bower_components/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ URL::asset('assets/login/bower_components/tinymce/myStyle/CustomizeFunctions.js') }}"></script>
    <script src="{{ URL::asset('assets/login/bower_components/fancyBox/jquery.fancybox.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            @if(Session::has('update_settings_successful'))
            swal("", "تنظیمات سایت بروزرسانی گردید", "success");
            @endif
        });
    </script>
<Script>
    $(document).ready(function () {
        $('#editC').one('submit',function(){
            $(this).find('button[type="submit"]').attr('disabled','disabled');
        });
        $('#txtDescription').change(function () {
            $('#descriptionLength').text('');
            $('#descriptionLength').text($('#txtDescription').val().length);
        });
</Script>
@endsection