<?php

namespace App\DbModels\Dashboard\Support;

use Illuminate\Database\Eloquent\Model;

class SupportDetail extends Model
{
    protected $table='support_details';
}
