@extends('Layouts.dashboardMainLayout')
@section('BreadCrumbURL','/profile/profile')
@section('BreadCrumbTitle','پروفایل')
@section('CssFiles')
    <style type="text/css">
        #image-preview {
            width: 200px;
            height: 200px;
            position: relative;
            overflow: hidden;
            background-color: #ffffff;
            color: #ecf0f1;
        }
        #image-preview input {
            line-height: 200px;
            font-size: 200px;
            position: absolute;
            opacity: 0;
            z-index: 10;
        }
        #image-preview label {
            position: absolute;
            z-index: 5;
            opacity: 0.8;
            cursor: pointer;
            background-color: #bdc3c7;
            width: 200px;
            height: 50px;
            font-size: 20px;
            line-height: 50px;
            text-transform: uppercase;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: auto;
            text-align: center;
        }
    </style>
@endsection

@section('MainContent')
    <div class="row">
        <section class="col-lg-12 col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <i class="fa fa-info-circle"></i>
                    <h3 class="box-title">
                        {{ \App\Facade\DbSettings::GetDbSetting()->Title }}
                    </h3>
                    <!-- tools box -->
                    <div class="pull-left box-tools">
                        <button type="button" class="btn bg-info btn-sm" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /. tools -->
                </div>
                <div class="box-body">
                    <div class="content">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">پروفایل کاربری</h3>
                            </div>
                            @if(Session::has('update_profile_success'))
                                <div class="alert alert-info alert-dismissible">
                                    <button type="button" class="close pull-left" data-dismiss="alert"
                                            aria-hidden="true">
                                        &times;
                                    </button>
                                    <h4><i class="icon fa fa-info"></i> توجه</h4>
                                    پروفایل کاربری با موفقیت بروز رسانی گردید
                                </div>
                        @endif
                            @if(Session::has('update_profile_invalid'))
                                <div class="alert alert-error alert-dismissible">
                                    <button type="button" class="close pull-left" data-dismiss="alert"
                                            aria-hidden="true">
                                        &times;
                                    </button>
                                    <h4><i class="icon fa fa-info"></i> توجه</h4>
                                    خطا در ورود اطلاعات
                                </div>
                        @endif
                        <!-- /.box-header -->
                            <!-- form start -->
                            <div class="content-wrapper">
                                <!-- Content Header (Page header) -->
                                <section class="content-header">
                                    <h1>
                                        پروفایل کاربری
                                    </h1>
                                </section>

                                <!-- Main content -->
                                <section class="content">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <!-- Profile Image -->
                                            <div class="box box-primary">
                                                <div class="box-body box-profile">
                                                    <img class="profile-user-img img-responsive img-circle"
                                                         src="{{ URL::asset(\App\Facade\OrganizationInfo::GetUserInfoWithoutParameter()->Image ) }}"
                                                         alt="User profile picture">

                                                    <h3 class="profile-username text-center">
                                                        {{ $userInfo['first_name'].' '.$userInfo['last_name'] }}
                                                    </h3>

                                                    <ul class="list-group list-group-unbordered">
                                                        <li class="list-group-item">
                                                            <b>دنبال شونده</b> <a class="pull-left">1,322</a>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <b>دنبال کننده</b> <a class="pull-left">543</a>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <b>دوستان</b> <a class="pull-left">13,287</a>
                                                        </li>
                                                    </ul>

                                                    <a href="#" class="btn btn-primary btn-block"><b>دنبال کردن</b></a>
                                                </div>
                                                <!-- /.box-body -->
                                            </div>
                                            <!-- /.box -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-9">
                                            <div class="nav-tabs-custom">
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a href="#settings" data-toggle="tab">تنظیمات</a></li>
                                                </ul>
                                                <div class="tab-content">

                                                    <div class="tab-pane active" id="settings">
                                                        <form class="form-horizontal" id="editC" action="/profile/updateProfile" method="post" enctype="multipart/form-data">
                                                            {{ csrf_field() }}
                                                            <br>
                                                            <div id="image-preview">
                                                                <label for="image-upload" id="image-label">عکس جدید</label>
                                                                <input type="file" name="fileupload" id="image-upload" />
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="txtEmail" class="col-sm-2 control-label">پست الکترونیکی</label>

                                                                <div class="col-sm-10">
                                                                    <input type="text" name="txtEmail" value="{{ $userInfo['email'] }}" class="form-control"
                                                                           id="txtEmail"  disabled>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="txtFirstName" class="col-sm-2 control-label">نام</label>

                                                                <div class="col-sm-10">
                                                                    <input type="text" name="txtFirstName" value="{{ $userInfo['first_name'] }}" class="form-control"
                                                                           id="txtFirstName" placeholder="نام" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="txtLastName" class="col-sm-2 control-label">نام خانوادگی</label>

                                                                <div class="col-sm-10">
                                                                    <input type="text" name="txtLastName" value="{{ $userInfo['last_name'] }}" class="form-control"
                                                                           id="txtLastName" placeholder="نام خانوادگی" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="txtPassNow" class="col-sm-2 control-label">کلمه عبور فعلی</label>

                                                                <div class="col-sm-10">
                                                                    <input type="password" name="txtPassNow"  class="form-control"
                                                                           id="txtPassNow">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="txtPassNew" class="col-sm-2 control-label">کلمه عبور جدید</label>

                                                                <div class="col-sm-10">
                                                                    <input type="password" name="txtPassNew"  class="form-control"
                                                                           id="txtPassNew">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="txtPassNewRep" class="col-sm-2 control-label">تکرار کلمه عبور جدید</label>

                                                                <div class="col-sm-10">
                                                                    <input type="password" name="txtPassNewRep"  class="form-control"
                                                                           id="txtPassNewRep">
                                                                </div>
                                                            </div>
                                                            <span id="passError">

                                                            </span>
                                                            <div class="form-group">
                                                                <div class="col-sm-offset-2 col-sm-10">
                                                                    <button type="submit" class="btn btn-danger">به روز رسانی
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <!-- /.tab-pane -->
                                                </div>
                                                <!-- /.tab-content -->
                                            </div>
                                            <!-- /.nav-tabs-custom -->
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('JsFiles')
    <script src="{{ URL::asset('assets/login/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/login/bower_components/imagePreview/jquery.uploadPreview.min.js') }}"></script>
    <script>
        $(function () {
            var txtPassNew=$('#txtPassNew');
            var txtPassNewRep=$('#txtPassNewRep');
            var passError=$('#passError')
            $(txtPassNew).change(function () {
                if(txtPassNew.val() == txtPassNewRep.val()){
                    $(passError).css('color','green');
                    $(passError).text('کلمه عبورها باهم یکسان هستند');
                }else{
                    $(passError).css('color','red');
                    $(passError).text('کلمه عبورها باهم یکسان نیستند');
                }
            });
            $(txtPassNewRep).change(function () {
                if(txtPassNew.val() == txtPassNewRep.val()){
                    $(passError).css('color','green');
                    $(passError).text('کلمه عبورها باهم یکسان هستند');
                }else{
                    $(passError).css('color','red');
                    $(passError).text('کلمه عبورها باهم یکسان نیستند');
                }
            });
            $('.bg-maroon').click(function () {
                var keyId = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                var ele = $(this).parent().parent();
                if (confirm('آیا از حذف این رکورد مطمئن هستید؟')) {
                    $.ajax({
                        url: '/settings/delKeyWord',
                        data: {_token: token, keyId: keyId},
                        type: 'POST',
                        dataType: 'JSON',
                        success: function (data) {
                            if (data.state == 1) {
                                alert(data.message);
                                ele.fadeOut().remove();
                            } else {
                                alert(data.message);
                            }
                        }
                    });
                }
            });

            $.uploadPreview({
                input_field: "#image-upload",   // Default: .image-upload
                preview_box: "#image-preview",  // Default: .image-preview
                label_field: "#image-label",    // Default: .image-label
                label_default: "آپلود عکس جدید",   // Default: Choose File
                label_selected: "تغییر عکس",  // Default: Change File
                no_label: false                 // Default: false
            });

            $('#editC').one('submit',function(){
                $(this).find('button[type="submit"]').attr('disabled','disabled');
            });
        })
    </script>
@endsection
