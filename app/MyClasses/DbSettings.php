<?php

namespace App\MyClasses;


use App\DbModels\Dashboard\Setting\Setting;

class DbSettings
{
    public function GetDbSetting()
    {
        $settings=Setting::first();
        return $settings;
    }
}