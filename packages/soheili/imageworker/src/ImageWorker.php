<?php

namespace Soheili\ImageWorker;


use Intervention\Image\Facades\Image;

class ImageWorker
{
	private $Request;
	private $ElementName;
	private $TargetPath;
	public $CurrentGeneratedName;
	public $FullPath;

	public function GetExtension()
	{
		$el=$this->ElementName;
		return $this->Request->$el->getClientOriginalExtension();
	}

	public function GenerateName(int $key=0)
	{
		return date('Ymd'). time() .$key.rand(10, 50).'.'.$this->GetExtension();
	}

	public function Move()
	{
		$el=$this->ElementName;
		$this->CurrentGeneratedName = $this->GenerateName();
		$this->FullPath = $this->TargetPath . $this->CurrentGeneratedName;
		return $this->Request->$el->move(public_path($this->TargetPath),$this->CurrentGeneratedName);
	}

	public function GenerateSmallSize(int $height=150,int $width=150)
	{
		if(!is_dir($this->TargetPath.'small/')){
			mkdir($this->TargetPath.'small/');
		}
		$changeSize = Image::make($this->FullPath);
		$changeSize->resize($height, $width);
		$changeSize->save($this->TargetPath.'small/'.$this->CurrentGeneratedName);
		unset($changeSize);
	}

	public function __construct($Request,string $ElementName,string $TargetPath='')
	{
		$this->Request=$Request;
		$this->ElementName=$ElementName;
		$this->TargetPath=$TargetPath;
	}

	public function ChaneImageSizeToOtherSize(int $height=400,int $width=400)
	{
		$changeSize = Image::make($this->FullPath);
		$changeSize->resize($height, $width);
		$changeSize->save($this->TargetPath.$this->CurrentGeneratedName);
		unset($changeSize);
	}

}