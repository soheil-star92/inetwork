<?php

namespace App\Http\Controllers\Dashboard;

use App\DbModels\Dashboard\Notifications\NotificationContactUs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GeneralController extends Controller
{
    public function ContactUsList()
    {
        $Rows=NotificationContactUs::orderBy('id','DESC')->paginate(100);
        return view('Dashboard.Notifications.ContactUsRequests',compact('Rows'));
    }
}
