@php
    $settings=\App\Facade\DbSettings::GetDbSetting();
    $userLogged=\App\Facade\OrganizationInfo::GetUserInfoWithoutParameter();
    $RegisteredEmail=$userLogged->RegisteredEmail;
    $UID=$userLogged->UserId;
@endphp

@section('CssFiles')
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/forms/select/select2.min.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/animate/animate.min.css">
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/vendors/css/tables/datatable/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/vendors/css/tables/datatable/responsive.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/vendors/css/tables/datatable/buttons.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/vendors/css/tables/datatable/rowGroup.bootstrap5.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/bordered-layout.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/plugins/forms/form-validation.css">
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/css/plugins/extensions/ext-component-sweet-alerts.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/pages/modal-create-app.css">
    <!-- END: Page CSS-->
    <link rel="stylesheet" type="text/css"
          href="/Themes/Admin/app-assets/css/plugins/extensions/ext-component-sweet-alerts.css">

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/assets/css/style.css">
    <!-- END: Custom CSS-->


@stop
@extends('Layouts.dashboardMainLayout')

@section('MainContent')

    <!-- Add Row Modal -->
    <div class="modal fade" id="showRowNewModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-edit-user">
            <div class="modal-content">
                <div class="modal-header bg-transparent">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="/AdminPanel/Users/AddUser" method="post" id="addNewRowModal">
                    {{csrf_field()}}
                    <input type="hidden" name="iptKey" id="iptKey">
                    <div class="modal-body pb-5 px-sm-5 pt-50">
                        <div class="text-center mb-2">
                            <h1 class="mb-1">New Row information</h1>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptNewFName">First Name</label>
                            <input type="text" id="iptNewFName" name="iptNewFName"
                                   class="form-control"/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptNewLName">Last Name</label>
                            <input type="text" id="iptNewLName" name="iptNewLName"
                                   class="form-control"/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptNewEmail">Email</label>
                            <input type="text" id="iptNewEmail" name="iptNewEmail"
                                   class="form-control"/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptNewRoleName">Role</label>
                            <select name="iptNewRoleName" id="iptNewRoleName" class="form-select form-select-lg">
                                @foreach(\App\DbModels\Dashboard\Users\Role::all() as $row)
                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptNewWallet">Wallet Address</label>
                            <input type="text" id="iptNewWallet" name="iptNewWallet"
                                   class="form-control"/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptNewCity">City</label>
                            <input type="text" id="iptNewCity" name="iptNewCity"
                                   class="form-control"/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptNewPassword">Password</label>
                            <input type="password" id="iptNewPassword" name="iptNewPassword"
                                   class="form-control"/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptNewPhone">Phone</label>
                            <input type="text" id="iptNewPhone" name="iptNewPhone"
                                   class="form-control"/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptNewReferralCode">User Referral Code</label>
                            <input type="text" id="iptNewReferralCode" name="iptNewReferralCode"
                                   class="form-control"/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptNewHeadCode">UpLine referral Code</label>
                            <input type="text" id="iptNewHeadCode" name="iptNewHeadCode"
                                   class="form-control"/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptNewConfirmWallet">Verify Wallet</label>
                            <select id="iptNewConfirmWallet" name="iptNewConfirmWallet"
                                    class="form-select form-select-lg">
                                <option value="0">not verify</option>
                                <option value="1">Confirm</option>
                            </select>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptNewVerifyEmail">Verify Email</label>
                            <select id="iptNewVerifyEmail" name="iptNewVerifyEmail" class="form-select form-select-lg">
                                <option value="0">not verify</option>
                                <option value="1">Confirm</option>
                            </select>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="modalEditUserCountry">Country</label>
                            <select id="iptNewCountryID" name="iptNewCountryID"
                                    class="select2 form-select">
                                <option value="">Select Value</option>
                                @foreach(\App\DbModels\Dashboard\Setting\Country::all() as $row)
                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="col-12 text-center mt-2 pt-50">
                            <button type="submit" class="btn btn-primary me-1">Submit</button>
                            <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal"
                                    aria-label="Close">
                                Discard
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="showRowModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-edit-user">
            <div class="modal-content">
                <div class="modal-header bg-transparent">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="/AdminPanel/Users/UpdateInfo" method="post" id="addRowModal">
                    {{csrf_field()}}
                    <input type="hidden" name="iptKey" id="iptKey">
                    <div class="modal-body pb-5 px-sm-5 pt-50">
                        <div class="text-center mb-2">
                            <h1 class="mb-1">Show Row information</h1>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptFName">First Name</label>
                            <input type="text" id="iptFName" name="iptFName"
                                   class="form-control"/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptLName">Last Name</label>
                            <input type="text" id="iptLName" name="iptLName"
                                   class="form-control"/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptEmail">Email</label>
                            <input type="text" id="iptEmail" name="iptEmail"
                                   class="form-control" disabled/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptRoleName">Role</label>
                            <input type="text" id="iptRoleName" name="iptRoleName"
                                   class="form-control" disabled/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptWallet">Wallet Address</label>
                            <input type="text" id="iptWallet" name="iptWallet"
                                   class="form-control"/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptCity">City</label>
                            <input type="text" id="iptCity" name="iptCity"
                                   class="form-control"/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptPassword">Password</label>
                            <input type="password" id="iptPassword" name="iptPassword"
                                   class="form-control"/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptPhone">Phone</label>
                            <input type="text" id="iptPhone" name="iptPhone"
                                   class="form-control"/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptReferralCode">User Referral Code</label>
                            <input type="text" id="iptReferralCode" name="iptReferralCode"
                                   class="form-control"/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptHeadCode">UpLine referral Code</label>
                            <input type="text" id="iptHeadCode" name="iptHeadCode"
                                   class="form-control"/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptChangeStatus">User Status</label>
                            <select id="iptChangeStatus" name="iptChangeStatus" class="form-select form-select-lg">
                                <option value="0">inflow</option>
                                <option value="1">Block</option>
                            </select>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptConfirmWallet">Verify Wallet</label>
                            <select id="iptConfirmWallet" name="iptConfirmWallet" class="form-select form-select-lg">
                                <option value="0">not verify</option>
                                <option value="1">Confirm</option>
                            </select>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="iptVerifyEmail">Verify Email</label>
                            <select id="iptVerifyEmail" name="iptVerifyEmail" class="form-select form-select-lg">
                                <option value="0">not verify</option>
                                <option value="1">Confirm</option>
                            </select>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="modalEditUserCountry">Country</label>
                            <select id="iptCountryID" name="iptCountryID"
                                    class="select2 form-select">
                                <option value="">Select Value</option>
                                @foreach(\App\DbModels\Dashboard\Setting\Country::all() as $row)
                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="col-12 text-center mt-2 pt-50">
                            <button type="submit" class="btn btn-primary me-1">Submit</button>
                            <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal"
                                    aria-label="Close">
                                Discard
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="showDetailModel" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-edit-user">
            <div class="modal-content">
                <div class="modal-header bg-transparent">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body pb-5 px-sm-5 pt-50">
                    <div class="text-center mb-2">
                        <h1 class="mb-1">Show Row information</h1>
                    </div>
                    <!-- Tabs with Icon starts -->
                    <div class="col-xl-12 col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">User Assets : <p id="iptUserFullName"></p></h4>
                            </div>
                            <div class="card-body">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="homeIcon-tab" data-bs-toggle="tab"
                                           href="#homeIcon" aria-controls="home" role="tab" aria-selected="true"><i
                                                    data-feather="home"></i> Assets</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="profileIcon-tab" data-bs-toggle="tab"
                                           href="#profileIcon" aria-controls="profile" role="tab" aria-selected="false"><i
                                                    data-feather='archive'></i> Package</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="aboutIcon-tab" data-bs-toggle="tab" href="#aboutIcon"
                                           aria-controls="about" role="tab" aria-selected="false"><i
                                                    data-feather="user"></i> Subset</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="walletIcon-tab" data-bs-toggle="tab" href="#walletIcon"
                                           aria-controls="wallet" role="tab" aria-selected="false"><i
                                                    data-feather='dollar-sign'></i></i> Wallet</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="homeIcon" aria-labelledby="homeIcon-tab"
                                         role="tabpanel">
                                        <div class="d-flex flex-row">
                                            <div class="avatar bg-light-success me-2">
                                                <div class="avatar-content">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14"
                                                         viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                         stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                         class="feather feather-dollar-sign avatar-icon">
                                                        <line x1="12" y1="1" x2="12" y2="23"></line>
                                                        <path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="my-auto">
                                                <h4 class="fw-bolder mb-0" id="iptMainAssets">$0</h4>
                                                <p class="card-text font-small-3 mb-0">Assets</p>
                                                <form action="/AdminPanel/Users/UserAssetManualIncrease" method="post"
                                                      id="frmUserAssetManualIncrease">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="iptDetailKey" id="iptDetailKey">
                                                    <input type="number" name="iptUserPrice" id="iptUserPrice"
                                                           class="form-control" value="0">
                                                    <button type="submit"
                                                            class="btn btn-primary me-1 mt-1 waves-effect waves-float waves-light">
                                                        Increase
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="profileIcon" aria-labelledby="profileIcon-tab"
                                         role="tabpanel">
                                        <p>
                                        <div class="card-body" id="tagUserPackage">
                                            <a href="#" id="btnUserPackage" target="_blank"
                                               class="btn btn-primary me-1 mt-1 waves-effect waves-float waves-light">User
                                                Package</a>
                                        </div>
                                        </p>
                                    </div>
                                    <div class="tab-pane" id="aboutIcon" aria-labelledby="aboutIcon-tab"
                                         role="tabpanel">
                                        <p>
                                        <div class="card-body" id="tagUserSubset">
                                            <a href="#" id="btnUserLevels" target="_blank"
                                               class="btn btn-primary me-1 mt-1 waves-effect waves-float waves-light">
                                                User Subset
                                            </a>
                                        </div>
                                        </p>

                                    </div>
                                    <div class="tab-pane" id="walletIcon" aria-labelledby="walletIcon-tab"
                                         role="tabpanel">
                                        <p>
                                        <div class="card-body" id="tagUserWallet">
                                            <a href="#" id="btnUserWallet" target="_blank"
                                               class="btn btn-primary me-1 mt-1 waves-effect waves-float waves-light">
                                                User Wallet
                                            </a>
                                        </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Tabs with Icon ends -->
                    <div class="col-12 text-center mt-2 pt-50">
                        <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal"
                                aria-label="Close">
                            Discard
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="app-user-view-account">
                    <div class="row">
                        <!-- User Content -->
                        <div class="col-xl-12 col-lg-12 col-md-12 order-0 order-md-1">
                            <!-- User Pills -->
                            <!--/ User Pills -->
                            <button data-bs-toggle="modal" data-bs-target="#showRowNewModal" type="button"
                                    class="btn btn-primary btn-sm btn-add-new waves-effect waves-float waves-light"
                                    data-repeater-create="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-plus me-25">
                                    <line x1="12" y1="5" x2="12" y2="19"></line>
                                    <line x1="5" y1="12" x2="19" y2="12"></line>
                                </svg>
                                <span class="align-middle">New</span>
                            </button>

                            <!-- Project table -->
                            <div class="card">
                                <h4 class="card-header">User's List</h4>
                                <div class="table-responsive">
                                    <table id="keyWordDataTable" class="table datatable-project">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Fullname</th>
                                            <th>Email</th>
                                            <th>Role</th>
                                            <th>LastLogin</th>
                                            <th>Status</th>
                                            <th>created</th>
                                            <th>Operation</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($Rows as $key=>$row)
                                            <tr>
                                                <td>{{++$key}}</td>
                                                <td>{{$row->first_name.' '.$row->last_name}}</td>
                                                <td>{{$row->email}}</td>
                                                <td>{{$row->RoleName}}</td>
                                                <td>{{$row->last_login}}</td>
                                                <td>{!! \App\MyClasses\CmsFunctions::CheckUserStatus($row->Blocked) !!}</td>
                                                <td>{{$row->created_at}}</td>
                                                <td>
                                                    <div class="dropdown">
                                                        <button type="button"
                                                                class="btn btn-sm dropdown-toggle hide-arrow py-0 waves-effect waves-float waves-light"
                                                                data-bs-toggle="dropdown" aria-expanded="false">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="14"
                                                                 height="14" viewBox="0 0 24 24" fill="none"
                                                                 stroke="currentColor" stroke-width="2"
                                                                 stroke-linecap="round" stroke-linejoin="round"
                                                                 class="feather feather-more-vertical">
                                                                <circle cx="12" cy="12" r="1"></circle>
                                                                <circle cx="12" cy="5" r="1"></circle>
                                                                <circle cx="12" cy="19" r="1"></circle>
                                                            </svg>
                                                        </button>

                                                        <div class="dropdown-menu dropdown-menu-end" style="">
                                                            <a id="aShowRow" class="dropdown-item "
                                                               data-bs-toggle="modal" data-bs-target="#showRowModal"
                                                               data-id="{{($row->id)}}">
                                                                <i data-feather='monitor'></i>
                                                                <span>Show</span>
                                                            </a>
                                                            <a id="aShowDetailRow" class="dropdown-item "
                                                               data-bs-toggle="modal" data-bs-target="#showDetailModel"
                                                               data-id="{{($row->id)}}">
                                                                <i data-feather='list'></i>
                                                                <span>Assets</span>
                                                            </a>
                                                            <a class="dropdown-item aRemoveRow"
                                                               data-id="{{($row->id)}}">
                                                                <i data-feather='user-x'></i>
                                                                <span>Remove</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /Project table -->

                        </div>
                        <!--/ User Content -->
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->
@stop

@section('JsFiles')
    <!-- BEGIN: Vendor JS-->
    <script src="/Themes/Admin/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="/Themes/Admin/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/forms/cleave/cleave.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/forms/cleave/addons/cleave-phone.us.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/moment.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/dataTables.bootstrap5.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/responsive.bootstrap5.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/jszip.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/polyfill.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/Themes/Admin/app-assets/js/core/app-menu.js"></script>
    <script src="/Themes/Admin/app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-add-new-cc.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/page-pricing.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-add-new-address.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-create-app.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-two-factor-auth.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-edit-user.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-share-project.js"></script>
    <!-- END: Page JS-->



    <script>
        $(document).ready(function () {
            $('body').on('click', '.aRemoveRow', function () {
                var id = $(this).data('id');
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, Remove it!',
                    customClass: {
                        confirmButton: 'btn btn-primary',
                        cancelButton: 'btn btn-outline-danger ms-1'
                    },
                    buttonsStyling: false
                }).then(function (result) {
                    if (result.value) {
                        var token = $('meta[name="csrf-token"]').attr('content');
                        $.ajax({
                            url: '/AdminPanel/Users/removeUser',
                            data: {_token: token, userId: id},
                            type: 'POST',
                            dataType: 'JSON',
                            success: function (data) {
                                if (data.state == 1) {
                                    location.reload();
                                } else {
                                    toastr['error'](data.message, 'Error', {
                                        closeButton: true,
                                        tapToDismiss: false,
                                        progressBar: true
                                    });
                                }
                            }
                        });
                    }
                });
            });

            $('body').on('click', '#aShowRow', function () {
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/AdminPanel/Users/GetUserInfo',
                    data: {_token: token, id: id},
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.state == 1) {
                            // $('#iptFullname').val(data.message.first_name+' '+data.message.last_name);
                            $('#iptFName').val(data.message.first_name);
                            $('#iptLName').val(data.message.last_name);
                            $('#iptKey').val(id);
                            $('#iptEmail').val(data.message.email);
                            $('#iptWallet').val(data.message.Wallet);
                            $('#iptCity').val(data.message.City);
                            $('#iptRoleName').val(data.message.RoleName);
                            $('#iptPhone').val(data.message.Phone);
                            $('#iptReferralCode').val(data.message.ReferralCode);
                            $('#iptHeadCode').val(data.message.HeadCode);
                            $('#iptChangeStatus option[value=' + data.message.Blocked + ']').attr('selected', 'selected');
                            $('#iptVerifyEmail option[value=' + data.message.RegisteredEmail + ']').attr('selected', 'selected');
                            $('#iptConfirmWallet option[value=' + data.message.confirmWallet + ']').attr('selected', 'selected');
                            $('#iptCountryID option[value=' + data.message.country_id + ']').attr('selected', 'selected');
                        } else {
                            swal(data.message, {
                                icon: 'warning',
                                dangerMode: true
                            });
                        }
                    }
                });

            });

            $('body').on('click', '#aShowDetailRow', function () {
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/AdminPanel/Users/GetUserFinancialInfo',
                    data: {_token: token, id: id},
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.state == 1) {
                            $('#iptDetailKey').val(id);
                            $('#iptUserFullName').empty();
                            $('#iptUserFullName').append(data.FullName);
                            $('#iptMainAssets').empty();
                            $('#iptMainAssets').append('$ ' + data.TotalAssets);

                            $('#tagUserPackage').empty();
                            $('#tagUserPackage').append(data.tagUserPackage);

                            $('#tagUserSubset').empty();
                            $('#tagUserSubset').append(data.tagUserSubset);

                            $('#tagUserWallet').empty();
                            $('#tagUserWallet').append(data.tagUserWallet);
                        } else {
                            swal(data.message, {
                                icon: 'warning',
                                dangerMode: true
                            });
                        }
                    }
                });

            });

            $('#addRowModal').one('submit', function () {
                $(this).find('button[type="submit"]').attr('disabled', 'disabled');
            });

            $('#keyWordDataTable').DataTable({
                "language": {
                    "lengthMenu": "نمایش _MENU_ رکورد در صفحه",
                    "zeroRecords": "Info Empty",
                    "info": "نمایش updateNewsFormصفحه _PAGE_ از _PAGES_",
                    "infoEmpty": "Info Empty",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "search": "Search: "
                },
                "paging": false,
                "info": false,
                stateSave: false
                // scrollY:        300,
                // scrollCollapse: true,
                // fixedColumns:   true
            });

        })
        ;

        $(window).on('load', function () {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
        $(document).ready(function () {
            $('#editUserForm').one('submit', function () {
                $(this).find('button[type="submit"]').attr('disabled', 'disabled');
            });
            $('#frmUserAssetManualIncrease').one('submit', function () {
                $(this).find('button[type="submit"]').attr('disabled', 'disabled');
            });
            $('#addNewRowModal').one('submit', function () {
                $(this).find('button[type="submit"]').attr('disabled', 'disabled');
            });
            @if(Session::has('error_at_update_user_info'))
                toastr['error']('{{Session::get('error_at_update_user_info')}}', 'Error', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true
            });
            @endif
                    @if(Session::has('error_at_user_asset_manual_increase'))
                toastr['error']('{{Session::get('error_at_user_asset_manual_increase')}}', 'Error', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true
            });
            @endif
                    @if(Session::has('error_at_insert_user'))
                toastr['error']('{{Session::get('error_at_insert_user')}}', 'Error', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true
            });
            @endif
                    @if(Session::has('update_user_info_successfully'))
                toastr['success']('{{Session::get('update_user_info_successfully')}}', 'Success', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true
            });
            @endif
                    @if(Session::has('user_asset_manual_increase_successful'))
                toastr['success']('{{Session::get('user_asset_manual_increase_successful')}}', 'Success', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true
            });
            @endif
                    @if(Session::has('add_user_success'))
                toastr['success']('{{Session::get('add_user_success')}}', 'Success', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true
            });
            @endif
        });


    </script>
@stop
