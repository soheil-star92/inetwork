<?php

namespace App\DbModels\Dashboard\Package;

use Illuminate\Database\Eloquent\Model;

class PackageUser extends Model
{
    protected $table='package_users';
}
