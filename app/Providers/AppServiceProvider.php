<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        /***************TODO BINDING Start******************/

        App::bind('DbSettings', function () {
            return new \App\MyClasses\DbSettings();
        });

        App::bind('OrganizationInfo', function () {
            return new \App\MyClasses\OrganizationInfo();
        });

        /*****************TODO BINDING End****************/
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
