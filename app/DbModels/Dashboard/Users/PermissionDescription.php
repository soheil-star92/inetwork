<?php

namespace App\DbModels\Dashboard\Users;

use Illuminate\Database\Eloquent\Model;

class PermissionDescription extends Model
{
    public $timestamps=false;
}
