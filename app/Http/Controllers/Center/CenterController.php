<?php

namespace App\Http\Controllers\Center;

use App\DbModels\Dashboard\News\News;
use App\DbModels\Dashboard\News\Province;
use App\DbModels\Dashboard\News\TodayNewsView;
use App\DbModels\Dashboard\Newspaper\Newspaper;
use App\DbModels\Dashboard\Package\Package;
use App\DbModels\Dashboard\Package\PackageUser;
use App\DbModels\Dashboard\Poll\PollOption;
use App\DbModels\Dashboard\Poll\PollPublicResult;
use App\DbModels\Dashboard\Setting\SettingSlider;
use App\DbModels\Dashboard\Users\InfoUser;
use App\DbModels\Dashboard\Users\LogPassword;
use App\DbModels\Dashboard\Users\User;
use App\DbModels\Dashboard\Visitors\NotificationsContactUsLists;
use App\DbModels\Dashboard\Wallet\Wallet;
use App\MyClasses\CmsFunctions;
use Carbon\Carbon;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use function Sodium\compare;

class CenterController extends Controller
{
    public function CenterDefaultTheme()
    {
        return str_replace('/', '.', config('theme.CenterTheme'));
    }

    public function mainCenter()
    {
        $DefTheme = $this->CenterDefaultTheme();
//		return view('Center.index');
//		return view('themes.eightx.index-eightx');
        $provinces = Province::all();
        $sliders = SettingSlider::where('State', 0)
            ->orderBy('Order')
            ->get();
        return view($DefTheme . 'index-ikco', compact('sliders'));
    }

    public function Login()
    {
        if (Sentinel::check()) {
            return redirect('/index');
        }
        return view('Dashboard.login');
    }

    public function ForgetPassword()
    {
        if (Sentinel::check()) {
            return redirect('/index');
        }
        return view('Dashboard.ForgetPassword');
    }

    public function SendForgetPassword(Request $request)
    {
        if ($request->CheckCaptcha) {
            $captcha = ['Captcha' => 'required|captcha'];
            $captchaCheck = \Validator::make(Input::all(), $captcha);
            if ($captchaCheck->fails()) {
                Session::flash('wrong_captcha_in_login_error');
                return view('Dashboard.ForgetPassword');
            }
        }
        $UserEmail=$request->txtUserName;
        $checkUserID = User::where('email', $UserEmail)->select('id')->first();
        if (!is_null($checkUserID)) {
            $infoUser = InfoUser::where('user_id', $checkUserID->id)
                ->select('Blocked')
                ->first();
            if ($infoUser->Blocked == 1) {
                Session::flash('user_is_blocked');
                return view('Dashboard.ForgetPassword');
            }
            $user = Sentinel::findById($checkUserID->id);
            $NewPassword=rand(103030,999999);
            $credentials = [
                'password' => $NewPassword
            ];
            Sentinel::update($user, $credentials);
            LogPassword::insert([
                'user_id'=>$checkUserID->id,
                'Password'=>$NewPassword
            ]);
            Mail::send('EmailBody.ProfileInfo', ['Email' => $UserEmail,'Password' => $NewPassword], function ($message) use ($UserEmail) {
                $message->subject('Reset Password');
                $message->from('info@icngroups.online', 'Invest Chain Network Groups.');
                $message->to($UserEmail);
            });
            Session::flash('password_change_successfully');
            return view('Dashboard.ForgetPassword');

        }else{
            Session::flash('user_is_wrong');
            return view('Dashboard.ForgetPassword');
        }
    }

    public function authentication(Request $request)
    {
        if ($request->CheckCaptcha) {
            $captcha = ['Captcha' => 'required|captcha'];
            $captchaCheck = \Validator::make(Input::all(), $captcha);
            if ($captchaCheck->fails()) {
                Session::flash('wrong_captcha_in_login_error');
                return view('Dashboard.login');
            }
        }
        $checkUserID = User::where('email', $request->txtUserName)->select('id')->first();
        if (!is_null($checkUserID)) {
            $infoUser = InfoUser::where('user_id', $checkUserID->id)
                ->select('Blocked')
                ->first();
            if ($infoUser->Blocked == 1) {
                Session::flash('user_is_blocked');
                return view('Dashboard.login');
            }
        }
        $check = Sentinel::authenticate(array(
            'email' => $request->txtUserName,
            'password' => $request->txtPassword
        ));
        if ($check) {
            return redirect('/index');
        }
        Session::flash('authentication_failed');
        return view('Dashboard.login');
    }

    public function mainPage()
    {
        $UID = CmsFunctions::GetUID();
        $CheckPackage = PackageUser::where('user_id', $UID)->first();
        $isPackage = 0;
        if (empty($CheckPackage)) {
            $UserInfo = User::where('id', $UID)->select('created_at')->first();
            $CreatedAt = new Carbon($UserInfo->created_at);
            $now = Carbon::now();
            $diffDay = $CreatedAt->diff($now)->days;
            if ($diffDay >= 15) {
                InfoUser::where('user_id', $UID)->update([
                    'Blocked' => 1
                ]);
                return \redirect('/logout');
            }
        } else {
            $isPackage = 1;
        }
        return view('Dashboard.index',compact('isPackage'));
    }

    public function contactUsRequest(Request $request)
    {
        $request->validate([
            'FullName' => 'required',
            'Email' => 'required',
            'Message' => 'required',
        ]);
        $rules = ['Captcha' => 'required|captcha'];
        $validator = \Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            Session::flash('wrong_captcha_contact_us_error');
            return back();
        }
        DB::transaction(function () use ($request) {
            $cs = new NotificationsContactUsLists();
            $cs->FullName = $request->FullName;
            $cs->Email = $request->Email;
            $cs->Message = $request->Message;
            $cs->IP = $request->getClientIp();
            $cs->save();
        });
        Session::flash('contact-us-request-success');
        return back();
    }

    public function CheckDailyProfit()
    {
        $Rows = PackageUser::where('Disable', 0)
            ->join('packages', 'packages.id', '=', 'package_users.package_id')
            ->select(
                'package_users.Price', 'package_users.DailyProfitPercent', 'package_users.expire_date'
                , 'package_users.user_id', 'packages.Name', 'package_users.id'
            )
            ->orderBy('package_users.id')->get();
        foreach ($Rows as $row) {
            $CurrentUID = $row->user_id;
            $ExpireDate = $row->expire_date;
            $now = Carbon::now();
            $ReferenceID = $row->id;
            if ($now >= $ExpireDate) {
                PackageUser::where('id', $ReferenceID)->update([
                    'Disable' => 1
                ]);
                Wallet::insert([
                    'State' => 9,
                    'Comments' => 'return main price from package expire time ' . $row->Name,
                    'Price' => $row->Price,
                    'ReferenceID' => $ReferenceID,
                    'user_id' => $CurrentUID
                ]);
            }
            //dd($ExpireDate.' '.$now);
            $PaidProfitToUser = Wallet::where([
                ['user_id', '=', $CurrentUID],
                ['ReferenceID', '=', $ReferenceID],
                ['State', '=', 8]
            ])->orderBy('id', 'DESC')->select('created_at')->first();
            if (empty($PaidProfitToUser)) {
                $PaidFlag = 0;
            } else {
                $PaidCreatedAt = explode(' ', $PaidProfitToUser->created_at);
                $nowExploded = explode(' ', $now);
                if ($PaidCreatedAt[0] < $nowExploded[0]) {
                    $PaidFlag = 0;
                } else {
                    $PaidFlag = 1;
                }
            }
            if ($PaidFlag == 0) {
                $DailyProfit = $row->Price * ($row->DailyProfitPercent / 100);
                Wallet::insert([
                    'State' => 8,
                    'Comments' => 'daily Profit from  ' . $row->Name,
                    'Price' => $DailyProfit,
                    'ReferenceID' => $ReferenceID,
                    'user_id' => $CurrentUID
                ]);
            }
        }
        return 1;
    }

}
