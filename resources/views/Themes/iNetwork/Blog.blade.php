@php
    use \App\MyClasses\CmsFunctions;
    use \App\MyClasses\CmsThemes;
    $BaseSetting=CmsFunctions::BaseSetting()
@endphp
@extends('Themes.ServatAM.ServatAMMainLayout')
@section('Content')
    <div class="row newssec warper">
        <div class="col-md-12 mx-auto text-center">
            <p class="orange-text title-font-main px-5 text-right noty">{{$News[0]['NewsCategoryTitleFa']}}</p>
            <div class="containerCustom">
                <div class="row-hold text-center">
                    @foreach($News AS $row)
                        <a href="/ShowNews/{{$row->id}}" class = "news-card text-center">
                            <img class="news-img" src="/{{$row->Image}}">
                            <h5 class="news-title p-3 text-right">{{$row->Title}}</h5>
                        </a>
                    @endforeach
                </div>
                {{$News->links()}}
            </div>
        </div>
    </div>
@stop