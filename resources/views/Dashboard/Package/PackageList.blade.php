@php
    $settings=\App\Facade\DbSettings::GetDbSetting();
    $userLogged=\App\Facade\OrganizationInfo::GetUserInfoWithoutParameter();
    $RegisteredEmail=$userLogged->RegisteredEmail;

@endphp

@section('CssFiles')
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/forms/wizard/bs-stepper.min.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/extensions/toastr.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/bordered-layout.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/pages/app-ecommerce.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/plugins/forms/pickers/form-pickadate.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/plugins/forms/form-wizard.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/plugins/extensions/ext-component-toastr.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/plugins/forms/form-number-input.css">
    <!-- END: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/plugins/extensions/ext-component-sweet-alerts.css">
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/assets/css/style.css">
    <!-- END: Custom CSS-->


@stop
@extends('Layouts.dashboardMainLayout')

@section('MainContent')
    <!-- BEGIN: Content-->
    <div class="app-content content ecommerce-application">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Package</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active"><a href="#">Packages</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div class="bs-stepper checkout-tab-steps">
                    <!-- Wizard starts -->
                    <div class="bs-stepper-header">
                        <div class="step" data-target="#step-cart" role="tab" id="step-cart-trigger">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">
                                    <i data-feather='credit-card' class="font-medium-3"></i>
                                </span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">Packages</span>
                                    <span class="bs-stepper-subtitle">select your package for invest</span>
                                </span>
                            </button>
                        </div>
                        <div class="line">
                            <i data-feather="chevron-right" class="font-medium-2"></i>
                        </div>
                    </div>
                    <!-- Wizard ends -->
                    <div class="d-flex justify-content-around my-2 pt-75">
                        <div class="d-flex align-items-start me-2">
                                            <span class="badge bg-light-primary p-75 rounded">
                                                <i data-feather="check" class="font-medium-2"></i>
                                            </span>
                            <div class="ms-75">
                                @php $GetUI=\App\MyClasses\CmsFunctions::GetUserInfo($UID); @endphp
                                <small>Account: {{$GetUI->first_name.' '.$GetUI->last_name}}</small>
                                <h4 class="mb-0">{{\App\MyClasses\CmsFunctions::GetUserWalletAssets($GetUI->id)}}
                                    $</h4>
                                <small>Total Assets</small>
                            </div>
                        </div>
                    </div>
                    <div class="bs-stepper-content">

                        <!-- Checkout Place order starts -->
                        <div id="step-cart" class="content" role="tabpanel" aria-labelledby="step-cart-trigger">
                            <div id="place-order" class="list-view product-checkout">
                                <!-- Checkout Place Order Left starts -->
                                <form action="/AdminPanel/Package/adduserPackage" method="post">
                                    {{csrf_field()}}
                                <div class="checkout-items">
                                    @foreach($rows as $row)
                                        <input type="hidden" name="iptKey_{{$row->id}}" id="iptKey_{{$row->id}}" value="{{\App\MyClasses\CmsFunctions::EncodedText($row->id)}}" >
                                    <div class="card ecommerce-card">
                                        <div class="item-img">
                                            <a href="#">
                                                <img src="/Themes/Admin/app-assets/images/elements/decore-right.png" alt="img-placeholder" />
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <div class="item-name">
                                                <h6 class="mb-0"><a href="#" class="text-body">{{$row->Name}}</a></h6>
                                                <span class="item-company">By <a href="#" class="company-name">invest chain network</a></span>
                                                <div class="item-rating">
                                                    <ul class="unstyled-list list-inline">
                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <span class="text-success mb-1"></span>
                                            <div class="item-quantity">
                                                <span class="quantity-title">Price:</span>
                                                <div class="quantity-counter-wrapper">
                                                    <div class="input-group">
                                                        <input type="text" id="iptUserPrice{{$row->id}}" name="iptUserPrice{{$row->id}}" class="quantity-counter" value="{{$row->MaxPrice}}" />
                                                    </div>
                                                </div>
                                            </div>
                                            <span class="delivery-date text-muted"> contract expire : annually after register package</span>
                                            <span class="text-success">Daily Profit by Percent: {{$row->DailyProfitPercent}}</span>
                                        </div>
                                        <div class="item-options text-center">
                                            <div class="item-wrapper">
                                                <div class="item-cost">
                                                    <h4 class="item-price">${{$row->MinPrice.' -  $'.$row->MaxPrice}}</h4>
                                                    <p class="card-text shipping">
                                                        <span class="badge rounded-pill badge-light-success">@if($row->extra) Extra Package @endif</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <button type="button" data-id="{{$row->id}}" class="btn btn-primary btn-cart move-cart confirm-package">
                                                <i data-feather="heart" class="align-middle me-25"></i>
                                                <span class="text-truncate">Buy</span>
                                            </button>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <!-- Checkout Place Order Left ends -->
                                </form>
                            </div>
                            <!-- Checkout Place order Ends -->
                        </div>
                        <!-- Checkout Customer Address Starts -->
                        <!-- </div> -->
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- END: Content-->
@stop

@section('JsFiles')


    <!-- BEGIN: Vendor JS-->
    <script src="/Themes/Admin/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="/Themes/Admin/app-assets/vendors/js/forms/wizard/bs-stepper.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/toastr.min.js"></script>
    <!-- END: Page Vendor JS-->


    <!-- BEGIN: Page Vendor JS-->
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/polyfill.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/Themes/Admin/app-assets/js/core/app-menu.js"></script>
    <script src="/Themes/Admin/app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="/Themes/Admin/app-assets/js/scripts/pages/app-ecommerce-checkout.js"></script>
    <!-- END: Page JS-->

    <script>
        $(function () {
            var confirmPackage = $('.confirm-package');
            if (confirmPackage.length) {
                confirmPackage.on('click', function () {
                    var id = $(this).data('id');
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Yes, Buy it!',
                        customClass: {
                            confirmButton: 'btn btn-primary',
                            cancelButton: 'btn btn-outline-danger ms-1'
                        },
                        buttonsStyling: false
                    }).then(function (result) {
                        if (result.value) {
                            var price = $('#iptUserPrice'+id).val();
                            var token = $('meta[name="csrf-token"]').attr('content');
                            $.ajax({
                                url: '/AdminPanel/Package/addUserPackage',
                                data: {_token: token, id: id,iptPrice:price},
                                type: 'POST',
                                dataType: 'JSON',
                                success: function (data) {
                                    if (data.state == 1) {
                                        toastr['success'](data.message, 'Success', {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            progressBar: true  });
                                    } else {
                                        toastr['error'](data.message, 'Error', {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            progressBar: true  });
                                    }
                                }
                            });
                        }
                    });
                });
            }
        });
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>
@stop
