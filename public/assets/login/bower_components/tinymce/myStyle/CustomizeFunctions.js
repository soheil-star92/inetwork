tinymce.init({
    selector: 'textarea#mytextarea,textarea#ShowMyTextArea',
    plugins: 'advlist autolink link lists preview table code pagebreak image imagetools imageupload  print media wordcount emoticons textcolor ',
    menubar: false,
    language: 'fa',
    height: 200,
    relative_urls: false,
    toolbar: 'undo redo | removeformat print preview code | fontsizeselect bullist numlist forecolor backcolor | alignleft aligncenter alignright alignjustify | bold italic emoticons | pagebreak table link | image media wordcount ',
    content_css:[
        '/assets/login/bower_components/tinymce/myStyle/style.css'
    ],
});
