@php
    use \App\MyClasses\CmsFunctions;
    use \App\MyClasses\CmsThemes;
@endphp
@extends('Themes.iNetwork.MainLayout')
@section('Content')

    <div class="section-full p-t80 p-b50">
    </div>
        <!-- SECTION CONTENT -->
        <div class="section-full p-t80 p-b50">
            <div class="container">

                <div class="wt-box col-md-6">
                    <h4 class="text-uppercase">Contact Detail </h4>
                    <div class="wt-separator-outer m-b30">
                        <div class="wt-separator bg-primary"></div>
                    </div>
                    <div class="wt-separator bg-primary"></div><i class="fa fa-send"></i> Telegram Support: <a href="https://t.me/ICN_Company" target="_blank">https://t.me/ICN_Company</a><div class="wt-separator bg-primary"></div>
                    <br><br>
                    <div class="wt-separator bg-primary"></div><i class="fa fa-map-marker"></i>  13-14 Hanover Street London W1S 1YH<div class="wt-separator bg-primary"></div>
                    <P><iframe width="600" height="350" id="gmap_canvas"
                               src="https://maps.google.com/maps?q=13%2014%20hanover%20street%20london%20england%20w1s%201yh&t=&z=13&ie=UTF8&iwloc=&output=embed"
                               frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></P>

                </div>
                <div class="wt-box col-md-6">
                    <h4 class="text-uppercase">Contact Form </h4>
                    <div class="wt-separator-outer m-b30">
                        <div class="wt-separator bg-primary"></div>
                    </div>
                    <div class="p-a50 p-b60 bg-gray">
                        <form  id="frmContact" method="post" action="/ContactUsSubmit">
                            {{csrf_field()}}
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input name="FullName" type="text" required class="form-control" placeholder="Name" >
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <input name="Email" type="text" class="form-control" required placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon v-align-t"><i class="fa fa-pencil"></i></span>
                                    <textarea name="Message" rows="5" class="form-control Message" required placeholder="Message"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                {!! captcha_img('flat') !!}
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input name="Captcha" type="text" required class="form-control" placeholder="Captcha" >
                                </div>
                            </div>
                            <div class="text-right">
                                <button name="submit" type="submit" value="Submit" class="site-button-secondry  m-r15">Submit</button>
                                <button name="Resat" type="reset" value="Reset"  class="site-button " >Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- SECTION CONTENT END -->
@stop
