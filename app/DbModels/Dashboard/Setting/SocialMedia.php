<?php

namespace App\DbModels\Dashboard\Setting;

use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model
{
    protected $table='social_media';
    public $timestamps=false;
}
