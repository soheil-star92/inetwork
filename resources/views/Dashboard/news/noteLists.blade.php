@extends('Layouts.dashboardMainLayout')

@section('MainContent')
    <div class="row">
        <section class="col-lg-12 col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <i class="fa fa-info-circle"></i>
                    <h3 class="box-title">
                        {{ \App\Facade\DbSettings::GetDbSetting()->Title }}
                    </h3>
                    <!-- tools box -->
                    <div class="pull-left box-tools">
                        <button type="button" class="btn bg-info btn-sm" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /. tools -->
                </div>
                <!-- modal -->
                <div class="modal modal-info fade" id="modal-add-note">
                    <div class="modal-dialog">
                        <form action="/news/addNote" method="post" enctype="multipart/form-data" id="noteAdd">
                            {{ csrf_field() }}
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">یادداشت جدید</h4>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        <label for="Title">عنوان</label>
                                        <input type="text" name="Title" id="Title" class="form-control">
                                    </p>
                                    <p>
                                        <label for="ReporterId">خبرنگار</label>
                                        <select name="ReporterId" id="ReporterId" class="form-control">
                                            @foreach($reporters AS $r)
                                                <option value="{{$r->id}}">{{$r->first_name.' '.$r->last_name}}</option>
                                            @endforeach
                                        </select>
                                    </p>
                                    <p>
                                        <label for="Image">عکس</label>
                                        <input type="file" name="Image" id="Image">
                                    </p>
                                    <p>
                                        <label for="Content">متن</label>
                                        <textarea name="Content" id="Content" class="form-control" cols="30"
                                                  rows="10"></textarea>
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">خروج
                                    </button>
                                    <input type="submit" class="btn btn-outline" value="ثبت">
                                </div>
                            </div>
                        </form>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <div class="modal modal-info fade" id="modal-show-note">
                    <div class="modal-dialog">
                        <form action="/news/editNote" method="post" id="noteEdit" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="noteId" id="noteId" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">ویرایش یادداشت</h4>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        <label for="showTitle">عنوان</label>
                                        <input type="text" name="showTitle" id="showTitle" class="form-control">
                                    </p>
                                    <p>
                                        <label for="ShowReporterId">خبرنگار</label>
                                        <select name="ShowReporterId" id="ShowReporterId" class="form-control">
                                            @foreach($reporters AS $r)
                                                <option value="{{$r->id}}">{{$r->first_name.' '.$r->last_name}}</option>
                                            @endforeach
                                        </select>
                                    </p>
                                    <span id="ImgContent"></span>
                                    <p>
                                        <label for="ShowImage">عکس</label>
                                        <input type="file" name="ShowImage" id="ShowImage">
                                    </p>
                                    <p>
                                        <label for="showContent">متن</label>
                                        <textarea name="showContent" id="showContent" class="form-control" cols="30"
                                                  rows="10"></textarea>
                                    </p>
                                    <p>
                                        <label >تغییر وضعیت</label>
                                        <select name="noteState" class="form-control" id="">
                                            @foreach($states as $s)
                                                <option value="{{ $s->id }}">{{ $s->Title }}</option>
                                            @endforeach
                                        </select>

                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">خروج
                                    </button>
                                    <input type="submit" class="btn btn-outline" value="ویرایش">
                                </div>
                            </div>
                        </form>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <div class="box-body">
                    <div class="content">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">یادداشت های سایت</h3>
                            </div>
                            <div class="box-header with-border">
                                <a class="btn btn-app" data-toggle="modal" data-target="#modal-add-note">
                                    <i class="fa fa-plus"></i>جدید
                                </a>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table id="keyWordDataTable"
                                           class="table table-bordered table-striped table-responsive table-hover">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>عنوان</th>
                                            <th>کاربر ارسال کننده</th>
                                            <th>تاریخ ارسال</th>
                                            <th>بازدید</th>
                                            <th>وضعیت</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($notes as $key=>$u)
                                            <tr @if($u->news_state_id == 4) style="background-color: rgba(0,200,0,0.3)" @elseif($u->news_state_id == 3) style="background-color: rgba(200,0,0,0.3)"  @endif>
                                                <td>{{ ++$key }}</td>
                                                <td>@if($u->news_state_id == 4)<a href="/Press/ShowNote/{{$u->id}}" target="_blank">@endif{{ $u['Title'] }}</a></td>
                                                <td>{{ $u['first_name'].' '.$u['last_name'] }}</td>
                                                <td>
                                                    @if(\App\Facade\OrganizationInfo::GetAccessFromProfessionalSettings('show_date_in_human_function'))
                                                        {{\App\MyClasses\CmsFunctions::GetDateForHumans($n['created_at'])}}
                                                    @else
                                                        {{ \App\MyClasses\CmsFunctions::GetGregorianDateAndConvertToJalaliDate($u['created_at']) }}
                                                    @endif
                                                </td>
                                                <td>{{ $u['View'] }}</td>
                                                <td>{{ $u['StateTitle'] }}</td>
                                                <td>
                                                    <div class="input-group">
                                                        <button type="button"
                                                                class="btn bg-light-blue-active dropdown-toggle "
                                                                data-toggle="dropdown"
                                                                style="font-size: 9px !important ">
                                                            <span class="fa fa-caret-down"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a class="btn bg-maroon btn-xs" data-id="{{ $u->id }}"
                                                                   title="حذف رکورد"><span class="fa fa-trash"></span>حذف</a>
                                                            </li>
                                                            @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess(['sub.system.news.notes.allow.edit']))
                                                            <li><a class="btn bg-green btn-xs"
                                                                   data-toggle="modal"
                                                                   data-target="#modal-show-note"
                                                                   data-id="{{ $u->id }}"
                                                                   title="ویرایش رکورد"><span class="fa fa-edit"></span>ویرایش</a>
                                                            </li>
                                                                @endif
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('JsFiles')
    <script src="{{ URL::asset('assets/login/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            @if(Session::has('add_note_success'))
            swal("", "رکورد جدید با موفقیت اضافه گردید", "success");
            @endif
            @if(Session::has('edit_note_success'))
            swal("", "رکورد موردنظر با موفقیت ویرایش گردید", "success");
            @endif
        });
    </script>
    <!-- CK Editor -->
    <script src="/assets/login/bower_components/ckeditor/ckeditor.js"></script>
    <script>
        $(function () {
            $('#noteAdd').on('submit', function () {
                $(this).find('input[type="submit"]').attr('disabled', 'disabled')
            });
            $('#noteEdit').on('submit', function () {
                $(this).find('input[type="submit"]').attr('disabled', 'disabled')
            });
            $('.bg-maroon').click(function () {
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                var ele = $(this).parent().parent().parent().parent().parent();
                swal({
                    title: "",
                    text: "آیا از حذف این رکورد مطمئن هستید؟",
                    icon: "warning",
                    buttons: ["خیر", "بله"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '/news/removeNote',
                            data: {_token: token, id: id},
                            type: 'POST',
                            dataType: 'JSON',
                            success: function (data) {
                                if (data.state == 1) {
                                    ele.fadeOut().remove();
                                    swal("",data.message,"success");
                                } else {
                                    swal(data.message, {
                                        icon: "warning",
                                        dangerMode: true,
                                    });
                                }
                            }
                        });
                    }
                });
            });

            $('.bg-green').click(function () {
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                var ele = $(this).parent().parent().parent().parent().parent();
                $.ajax({
                    url: '/news/showNews',
                    data: {_token: token, id: id},
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.state == 1) {
                            $('#noteId').val(id);
                            $('#showTitle').val(data.message.Title);
                            $('#showContent').val(data.message.Content);
                            $('#ImgContent').empty();
                            $('#ImgContent').append("<img height='100' width='100' src='" + data.message.Image + "'>");
                            $('#ShowReporterId option[value=' + data.message.reporter_id + ']').attr('selected', 'selected');
                        } else {
                            swal(data.message, {
                                icon: "warning",
                                dangerMode: true,
                            });
                        }
                    }
                });
            });


        })
    </script>
@endsection
