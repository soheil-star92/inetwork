<?php

namespace App\DbModels\Dashboard\Support;

use Illuminate\Database\Eloquent\Model;

class SupportHeader extends Model
{
    protected $table='support_headers';
}
