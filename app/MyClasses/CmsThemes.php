<?php


namespace App\MyClasses;


class CmsThemes
{
    public static function ThemeLists()
    {
        return [
            ['Name' => 'ServatAM', 'Path' => '/Themes/ServatAM/', 'Status' => 0],
            ['Name' => 'iNetwork', 'Path' => '/Themes/iNetwork/', 'Status' => 1]
        ];
    }

    public static function CurrentThemePath()
    {
        $themes = Self::ThemeLists();
        foreach ($themes AS $row) {
            if ($row['Status'] == 1) {
                return $row['Path'];
            }
        }
        throw new \Exception('Nothing themes is enable');
    }
}
