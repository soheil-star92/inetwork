<?php

namespace App\Http\Controllers\Dashboard;

use App\DbModels\Dashboard\Users\InfoUser;

use App\DbModels\Dashboard\Users\PermissionDescription;
use App\DbModels\Dashboard\Users\PermissionType;
use App\DbModels\Dashboard\Users\Role;
use App\DbModels\Dashboard\Users\RoleUser;
use App\DbModels\Dashboard\Users\User;
use App\DbModels\Dashboard\Users\VerificationEmail;
use App\DbModels\Dashboard\Wallet\Wallet;
use App\MyClasses\CmsFunctions;
use App\MyClasses\CmsThemes;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Soheili\ImageWorker\ImageWorker;

class UserController extends Controller
{
    /****** TODO Users Start Controller*******/

    public function roles()
    {
        $roles = Role::all();
        $permissionType = PermissionType::all();
        $permissionDescription = PermissionDescription::all();
        return view('Dashboard.roles.rolesList', compact('roles', 'permissionType', 'permissionDescription'));
    }

    public function removeRole(Request $request)
    {
        try {
            Role::where('id', $request->roleId)
                ->delete();
            $message = array(
                'state' => 1,
                'message' => 'رکورد مورد نظر با موفقیت حذف گردید'
            );
            return response()->json($message);
        } catch (\Exception $e) {
            $message = array(
                'state' => 2,
                'message' => $e->getMessage()
            );
            return response()->json($message);
        }
    }

    public function addRole(Request $request)
    {
        $request->validate([
            'txtRoleName' => 'required',
        ]);
        DB::transaction(function () use ($request) {
            Sentinel::getRoleRepository()->createModel()->create([
                'name' => $request->txtRoleName,
                'slug' => $request->txtRoleName,
            ]);
            Session::flash('add_role_success');
            $role = Role::orderBy('id', 'DESC')
                ->Select('id')
                ->limit(0)
                ->first();
            $perRole = Role::orderBy('id', 'DESC')
                ->Select('id', 'permissions')
                ->offset(1)
                ->limit(1)
                ->first();
            Role::where('id', $role->id)
                ->update(['permissions' => $perRole->permissions]);
        });
        return back();
    }

    public function showPermissions(Request $request)
    {
        try {
            $RoleId = $request->RoleId;
            $i = 0;
            $permissionDescription = array();
            $User = Sentinel::getUser();
            $permissionType = PermissionType::all();
            $role = Role::where('id', $RoleId)->first();
            if (($RoleId == 1) || ($User['id'] == 1)) {
                $permissionDescription = PermissionDescription::all()->toArray();
            } else {
                $role = Role::where('id', 2)->first();
                $CurrentRole = Role::where('id', $RoleId)->select('name')->first();
                $role->name = $CurrentRole->name;
                $PermRole = json_decode($role->permissions);
                foreach ($PermRole as $key => $row) {
                    if ($row) {
                        $PermissionDetail = PermissionDescription::where('permission_name', $key)->first()->toArray();
                        $permissionDescription[$i]['id'] = $PermissionDetail['id'];
                        $permissionDescription[$i]['permission_name'] = $PermissionDetail['permission_name'];
                        $permissionDescription[$i]['title'] = $PermissionDetail['title'];
                        $permissionDescription[$i]['role_type_id'] = $PermissionDetail['role_type_id'];
                        $i++;
                        unset($PermissionDetail, $CurrentRole);
                    }
                }
            }
            $response = '';
            $response .= "<div class=\"col-md-12\"><div class=\"nav-tabs-custom\"> <ul class=\"nav nav-tabs bg-gray-active\">";
            foreach ($permissionType as $key => $pt) {
                $response .= "<li ";
                if ($key == 0) {
                    $response .= "class=\"active\" ";
                }
                $response .= "><a href=\"#tab_" . $key . "\" data-toggle=\"tab\">" . $pt->Title . "</a></li>";
            }
            $response .= " </ul><div class=\"tab-content\">";
            foreach ($permissionType as $key => $pt) {
                $response .= "<div class=\"tab-pane";
                if ($key == 0) {
                    $response .= ' active ';
                }
                $response .= "\" id=\"tab_" . $key . "\" >";
                foreach ($permissionDescription as $key => $pd) {
                    if ($pd['role_type_id'] == $pt->id) {
                        $response .= "<p><input type=\"checkbox\" name=\"sctPermissions[]\" value=\"" . $pd['permission_name'] . "\" class=\"flat-red\" ";
                        if (CmsFunctions::CheckPermissionInRole($RoleId, $pd['permission_name'])) {
                            $response .= " checked ";
                        }
                        $response .= "> " . $pd['title'] . "</p>";
                    }
                }
                $response .= "</div>";
            }
            $response .= "</div><!-- /.tab-content --></div><!-- nav-tabs-custom --></div>";
            $message = [
                'state' => 1,
                'message' => $response,
                'Title' => $role->name,
                'Test' => $permissionDescription
            ];
            unset($response, $RoleId, $role, $permissionType, $permissionDescription, $response);
            return response()->json($message);
        } catch (\Exception $e) {
            $message = [
                'state' => 0,
                'message' => $e->getMessage()
            ];
            return response()->json($message);
        }
    }

    public function fetchRole($id)
    {
        $role = Role::where('id', $id)
            ->first();
        return \response()->json($role);
    }

    public function showRolePermission($id, $ptId)
    {
        $role = Role::where([
            ['id', '=', $id]
        ])
            ->first();
        $per = $role->permissions;
        $roleName = array();
        $roleValue = array();
        $permTitle = array();
        $permType = array();
        $permissionType = PermissionType::all();
//        $response='<select id="sctPermType" class="pt">';
//        foreach ($permissionType as $pt)
//            $response .= '<option value='.$pt->id.'>'.$pt->Title.'</option>';
//        $response .='</select> <br>';
        $response = '';
        $i = 0;
        foreach (json_decode($per) as $key => $p) {
            $roleName[$i] = $key;
            $val = $p == '' ? 0 : 1;
            $roleValue[$i] = $val;
            $perm = PermissionDescription::where([['permission_name', '=', $key]
            ])->first();
            if ($perm->role_type_id == $ptId) {
                $permTitle[$i] = $perm->title;
                $permType[$i] = $perm->role_type_id;
                $state = $val == 0 ? '' : 'checked';
                $response .= "<input type=\"checkbox\" name=\"sctPermissions[]\" value=\"" . $key . "\" " . $state . "> $perm->title <br>";
            }
            $i++;
        }
        return $response;
    }

    public function changePermissions(Request $request)
    {
        try {
            DB::transaction(function () use ($request) {
                $RoleId = $request->txtRoleId;
                $reqForTrue = $request->sctPermissions;
                $permissions = PermissionDescription::all();
                $role = Sentinel::findRoleById($RoleId);
                foreach ($permissions as $key => $r) {
                    $role->updatePermission($r->permission_name, false);
                }
                if (isset($reqForTrue)) {
                    foreach ($reqForTrue as $key => $r) {
                        $role->updatePermission($r, true);
                    }
                }
                $role->save();
                unset($request, $RoleId, $reqForTrue, $permissions, $role);
            });
            Session::flash('change_permission_success');
            return back();
        } catch (\Exception $e) {
            return $e->getMessage() . '-->Line:' . $e->getLine();
        }
    }

    public function usersList()
    {
        $users = User::join('role_users', 'role_users.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'role_users.role_id')
            ->leftJoin('info_users', 'info_users.user_id', '=', 'users.id')
            ->select('users.*', 'roles.name as RoleTitle', 'info_users.Blocked')
            ->get();
        $roles = Role::all();
        return view('Dashboard.roles.usersList', compact('users', 'roles'));
    }

    public function addUser(Request $request)
    {
        try {
            DB::transaction(function () use ($request) {
                $request->validate([
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'email' => 'required',
                    'password' => 'required',
                    'roleId' => 'required',
                    'id_i' => 'required',
                    'orgPic' => 'image|mimes:jpeg,png,jpg|max:5000',
                ]);
                $image = 'assets/no-image.png';
                if ($request->hasFile('orgPic')) {
                    $imagePath = rand(5, 15) . time() . '.' . $request->orgPic->getClientOriginalExtension();
                    $request->orgPic->move(public_path('images/users'), $imagePath);
                    $image = 'images/users/' . $imagePath;
                }
                Sentinel::register(array(
                    'email' => $request->email,
                    'password' => $request->password,
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                ));
                $lastUserId = User::orderBy('id', 'DESC')
                    ->select('id')
                    ->first();
                $userId = $lastUserId->id;
                $user = Sentinel::findById($userId);
                $role = Sentinel::findRoleById($request->roleId);
                $role->users()->attach($user);


                $infoUser = new InfoUser();
                $infoUser->user_id = $userId;
                $infoUser->id_i = $request->id_i;
                $infoUser->Phone = $request->Phone;
                $infoUser->Address = $request->Address;
                $infoUser->Image = $image;
                $infoUser->Save();
            });
            Session::flash('add_user_success');
            return back();
        } catch (\Exception $e) {
            Session::flash('error_at_insert_user', $e->getMessage());
            return back();
        }
    }

    public function AddUserNew(Request $request)
    {
        try {
            DB::transaction(function () use ($request) {
                $request->validate([
                    'iptNewFName' => 'required',
                    'iptNewLName' => 'required',
                    'iptNewEmail' => 'required',
                    'iptNewPassword' => 'required',
                    'iptNewRoleName' => 'required',
                ]);
                $image = 'assets/no-image.png';
                Sentinel::register(array(
                    'email' => $request->iptNewEmail,
                    'password' => $request->iptNewPassword,
                    'first_name' => $request->iptNewFName,
                    'last_name' => $request->iptNewLName,
                ));
                $lastUserId = User::orderBy('id', 'DESC')
                    ->select('id')
                    ->first();
                $userId = $lastUserId->id;
                $user = Sentinel::findById($userId);
                $role = Sentinel::findRoleById($request->iptNewRoleName);
                $role->users()->attach($user);

                $ReferralCode = is_null($request->iptNewReferralCode) ? 0 : $request->iptNewReferralCode;
                $GetLastReferralRow = InfoUser::orderBy('ReferralCode', 'DESC')->select('ReferralCode')->first();
                if ($ReferralCode == 0) {
                    $ReferralCode = $GetLastReferralRow->ReferralCode + rand(10, 50);
                }else{
                    if ($ReferralCode <= $GetLastReferralRow->ReferralCode) {
                        $ReferralCode = $GetLastReferralRow->ReferralCode + rand(10, 50);
                    }else{
                        $ReferralCode = $ReferralCode + rand(10, 50);
                    }
                }
                $infoUser = new InfoUser();
                $infoUser->user_id = $userId;
                $infoUser->id_i = 0;
                $infoUser->Phone = $request->iptNewPhone;
                $infoUser->Address = '';
                $infoUser->ReferralCode = $ReferralCode;
                $infoUser->HeadCode = is_null($request->iptNewHeadCode) ? 0 : $request->iptNewHeadCode;
                $infoUser->RegisteredEmail = $request->iptNewVerifyEmail;
                $infoUser->confirmWallet = $request->iptNewConfirmWallet;
                $infoUser->Wallet = $request->iptNewWallet;
                $infoUser->City = $request->iptNewCity;
                $infoUser->country_id = is_null($request->iptNewCountryID) ? 1 : $request->iptNewCountryID;
                $infoUser->Image = $image;
                $infoUser->Save();
            });
            Session::flash('add_user_success', config('msg.success'));
            return back();
        } catch (\Exception $e) {
            Session::flash('error_at_insert_user', $e->getMessage());
            return back();
        }
    }

    public function removeUser(Request $request)
    {
        try {
            $image = InfoUser::where('user_id', $request->userId)
                ->select('Image')
                ->first();
            if ($image->Image != 'assets/no-image.png') {
                if (file_exists(public_path($image->Image))) {
                    unlink(public_path($image->Image));
                }
            }
            User::where('id', $request->userId)
                ->delete();

            $message = array(
                'state' => 1,
                'message' => 'the row remove was successfully.'
            );
            return response()->json($message);
        } catch (\Exception $e) {
            $message = array(
                'state' => 0,
                'message' => $e->getMessage()
            );
            return response()->json($message);
        }
    }

    public function showUser($id)
    {
        $getUser = Sentinel::getUser();
        if ($id != 1 || $getUser->id == 1) {
            $users = User::join('role_users', 'role_users.user_id', '=', 'users.id')
                ->join('roles', 'roles.id', '=', 'role_users.role_id')
                ->leftJoin('info_users', 'info_users.user_id', '=', 'users.id')
                ->select(
                    'users.*',
                    'roles.name as RoleTitle', 'roles.id as RoleId',
                    'info_users.Image'
                )
                ->find($id);
            $roles = Role::all();
            return view('Dashboard.roles.showUser', compact('users', 'roles'));
        }
        return back();
    }

    public function updateUser(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
        ]);
        DB::transaction(function () use ($request) {
            if ($request->hasFile('fileupload')) {
                $userImage = InfoUser::where('user_id', $request->userId)
                    ->select('Image')
                    ->first();
                if ($userImage->Image != "assets/no-image.png") {
                    if (file_exists(public_path($userImage->Image))) {
                        unlink(public_path($userImage->Image));
                    }
                }
                $imagePath = rand(5, 15) . time() . '.' . $request->fileupload->getClientOriginalExtension();
                $request->fileupload->move(public_path('images/users'), $imagePath);
                $image = 'images/users/' . $imagePath;
                InfoUser::where('user_id', $request->userId)
                    ->update([
                        'Image' => $image
                    ]);
            }
            User::where('id', $request->userId)
                ->update([
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                ]);
            if (!is_null($request->password)) {
                $user = Sentinel::findById($request->userId);
                $credentials = [
                    'password' => $request->password,
                ];
                Sentinel::update($user, $credentials);
            }
            RoleUser::where('user_id', $request->userId)
                ->update([
                    'role_id' => $request->RoleId,
                ]);

        });
        Session::flash('update_user_success');
        return back();
    }

    public function ChangeUserStateBlocking(Request $request)
    {
        try {
            $user = InfoUser::where('user_id', $request->id)
                ->select('Blocked')
                ->first();
            if ($user->Blocked == 1) {
                InfoUser::where('user_id', $request->id)
                    ->update([
                        'Blocked' => 0
                    ]);
            } else {
                InfoUser::where('user_id', $request->id)
                    ->update([
                        'Blocked' => 1
                    ]);
            }
            $message = array(
                'state' => 1,
                'message' => 'وضعیت رکورد موردنظر با موفقیت تغییر یافت.'
            );
            return response()->json($message);
        } catch (\Exception $e) {
            $message = array(
                'state' => 0,
                'message' => $e->getMessage()
            );
            return response()->json($message);
        }
    }

    public function UpdateUserInfo(Request $request)
    {
        try {
            $request->validate([
                'iptKey' => 'required',
                'iptImage' => 'image|mimes:jpeg,png,jpg|max:5000',
            ]);
            $UID = CmsFunctions::DecodedText($request->iptKey);
            $row = InfoUser::where('user_id', $UID)->select('RegisteredEmail', 'confirmWallet')->first();
            $VerifyEmail = $row->RegisteredEmail;
            $ConfirmWallet = $row->confirmWallet;
            if ($request->hasFile('iptImage')) {
                $IW = new ImageWorker($request, 'iptImage', 'images/users/');
                $IW->Move();
                InfoUser::where('user_id', $UID)->update([
                    'Image' => $IW->FullPath
                ]);
            }
            if ($VerifyEmail != 1) {
                User::where('id', $UID)->update([
                    'first_name' => $request->iptFirstName,
                    'last_name' => $request->iptLastName,
                ]);
                InfoUser::where('user_id', $UID)->update([
                    'Phone' => $request->iptPhone,
                    'country_id' => $request->iptCountryID,
                    'Wallet' => $request->iptWallet,
                    'City' => $request->iptCity,
                    'id_i' => $request->iptNationalCode,
                ]);
            }
            InfoUser::where('user_id', $UID)->update([
                'Phone' => $request->iptPhone,
                'country_id' => $request->iptCountryID,
                'City' => $request->iptCity,
            ]);
            if ($ConfirmWallet != 1) {
                InfoUser::where('user_id', $UID)->update([
                    'Wallet' => $request->iptWallet
                ]);
            }
            if ($request->iptConfirmWallet) {
                InfoUser::where('user_id', $UID)->update([
                    'Wallet' => $request->iptWallet,
                    'confirmWallet' => 1
                ]);
            }
            $txtPassNew = $request->iptPassword;
            $txtPassNewRep = $request->iptRepPassword;
            if (!is_null($txtPassNew)) {
                if ($txtPassNew == $txtPassNewRep) {
                    $update = [
                        'password' => $txtPassNew
                    ];
                    Sentinel::update($UID, $update);
                } else {
                    Session::flash('error_at_update_user_info', 'error at input password');
                    return back();
                }
            }
            Session::flash('update_user_info_successfully', config('msg.success'));
            return back();
        } catch (\Exception $e) {
            Session::flash('error_at_update_user_info', $e->getMessage());
            return back();
        }
    }

    /****** TODO Users End Controller*******/


    public function UsersList2()
    {
        $Rows = User::join('info_users', 'info_users.user_id', '=', 'users.id')
            ->join('role_users', 'role_users.user_id', 'users.id')
            ->join('roles', 'roles.id', 'role_users.role_id')
            ->select('info_users.*', 'users.*', 'roles.name as  RoleName')
            ->orderBy('users.id')->paginate(100);
        return view('Dashboard.Users.UsersList', compact('Rows'));
    }

    public function GetUserInfo(Request $request)
    {
        try {
            $row = User::where('users.id', $request->id)->join('info_users', 'info_users.user_id', '=', 'users.id')
                ->join('role_users', 'role_users.user_id', 'users.id')
                ->join('roles', 'roles.id', 'role_users.role_id')
                ->select('info_users.*', 'users.*', 'roles.name as  RoleName')
                ->orderBy('users.id')->first();
            $message = array(
                'state' => 1,
                'message' => $row
            );
            return response()->json($message);
        } catch (\Exception $e) {
            $message = array(
                'state' => 0,
                'message' => $e->getMessage()
            );
            return response()->json($message);
        }
    }

    public function GetUserFinancialInfo(Request $request)
    {
        try {
            $RID = $request->id;
            $ERID = CmsFunctions::EncodedText($RID);
            $row = User::where('users.id', $RID)->join('info_users', 'info_users.user_id', '=', 'users.id')
                ->join('role_users', 'role_users.user_id', 'users.id')
                ->join('roles', 'roles.id', 'role_users.role_id')
                ->select('info_users.*', 'users.*', 'roles.name as  RoleName')
                ->orderBy('users.id')->first();
            $TotalAssets = CmsFunctions::GetUserWalletAssets($RID);
            $message = array(
                'state' => 1,
                'FullName' => $row->first_name . ' ' . $row->last_name,
                'TotalAssets' => $TotalAssets,
                'tagUserPackage' => '<a href="/AdminPanel/Users/UserPackageList/' . $ERID . '" id="btnUserPackage" target="_blank" class="btn btn-primary me-1 mt-1 waves-effect waves-float waves-light">User Package</a>',
                'tagUserSubset' => '<a href="/AdminPanel/Users/UserSubsetList/' . $ERID . '" id="btnUserPackage" target="_blank" class="btn btn-primary me-1 mt-1 waves-effect waves-float waves-light">User Subset</a>',
                'tagUserWallet' => '<a href="/AdminPanel/Users/UserWalletList/' . $ERID . '" id="btnUserPackage" target="_blank" class="btn btn-primary me-1 mt-1 waves-effect waves-float waves-light">User Transaction</a>'
            );
            return response()->json($message);
        } catch (\Exception $e) {
            $message = array(
                'state' => 0,
                'message' => $e->getMessage()
            );
            return response()->json($message);
        }
    }

    public function SendUserEmailVerification(Request $request)
    {
        try {
            $user = Sentinel::getUser();
            $UID = $user['id'];
            $to = $user['email'];
            $row = VerificationEmail::where('user_id', $UID)->orderBy('id', 'DESC')->first();
            $now = date('Y-m-d h:i:s');
            $hash = hash('sha256', $UID . ' ' . $now);
            if (empty($row)) {
                VerificationEmail::insert([
                    'user_id' => $UID,
                    'HashCode' => $hash,
                    'created_at' => Carbon::now()
                ]);
                Mail::send('EmailBody.EmailVerification', ['HashCode' => $hash], function ($message) use ($to) {
                    $message->subject('Verification Email in Invest Chain Network Groups.co');
                    $message->from('info@icngroups.online', 'Invest Chain Network Groups.');
                    $message->to($to);
                });
                $message = array(
                    'state' => 1,
                    'message' => 'verification request sent to user email.please, check your email inbox or spam box.'
                );
                return response()->json($message);
            } else {
                $CarbonNow = Carbon::now();
                $CarbonCreated = Carbon::parse($row->created_at);
                $diffs = $CarbonNow->diff($CarbonCreated);
                if (($diffs->i <= 10) && ($diffs->h == 0)) {
                    $message = array(
                        'state' => 0,
                        'message' => 'verification request sent to user email.'
                    );
                    return response()->json($message);
                } else {
                    VerificationEmail::insert([
                        'user_id' => $UID,
                        'HashCode' => $hash,
                        'created_at' => Carbon::now()
                    ]);
                    Mail::send('EmailBody.EmailVerification', ['HashCode' => $hash], function ($message) use ($to) {
                        $message->subject('Verification Email in Invest Chain Network Groups.co');
                        $message->from('info@icngroups.online', 'Invest Chain Network Groups.');
                        $message->to($to);
                    });
                    $message = array(
                        'state' => 1,
                        'message' => 'verification request sent to user email.please, check your email inbox or spam box.'
                    );
                    return response()->json($message);
                }
            }

        } catch (\Exception $e) {
            $message = array(
                'state' => 0,
                'message' => $e->getMessage()
            );
            return response()->json($message);
        }
    }

    public function UpdateInfo(Request $request)
    {
        try {
            $request->validate([
                'iptKey' => 'required'
            ]);
            $UID = $request->iptKey;
            User::where('id', $UID)->update([
                'first_name' => $request->iptFName,
                'last_name' => $request->iptLName,
            ]);
            InfoUser::where('user_id', $UID)->update([
                'Phone' => $request->iptPhone,
                'country_id' => $request->iptCountryID,
                'Wallet' => $request->iptWallet,
                'City' => $request->iptCity,
                'Blocked' => $request->iptChangeStatus,
                'confirmWallet' => $request->iptConfirmWallet,
            ]);
            $verifyEmail = $request->iptVerifyEmail;
            if ($verifyEmail) {
                InfoUser::where('user_id', $UID)->update([
                    'RegisteredEmail' => 1
                ]);
                RoleUser::where('user_id', $UID)->update([
                    'role_id' => 4
                ]);
            }
            $pwd = $request->iptPassword;
            if (!is_null($pwd)) {
                $user = Sentinel::findById($UID);
                $credentials = [
                    'password' => $pwd,
                ];
                Sentinel::update($user, $credentials);
            }
            Session::flash('update_user_info_successfully', config('msg.success'));
            return back();
        } catch (\Exception $e) {
            Session::flash('error_at_update_user_info', $e->getMessage());
            return back();
        }
    }

    public function ConfirmationEmail($hash)
    {
        try {
            $find = VerificationEmail::where([
                ['HashCode', '=', $hash],
                ['Submit', '=', 0]
            ])->first();
            if (!empty($find)) {
                $UID = $find->user_id;
                $CarbonNow = Carbon::now();
                $CarbonCreated = Carbon::parse($find->created_at);
                $diffs = $CarbonNow->diff($CarbonCreated);
                $diffInMinute = $diffs->i;
                if ($diffInMinute <= 10) {
                    VerificationEmail::where('id', $find->id)->update([
                        'Submit' => 1
                    ]);
                    InfoUser::where('user_id', $UID)->update([
                        'RegisteredEmail' => 1
                    ]);
                    RoleUser::where('user_id', $UID)->update([
                        'role_id' => 4
                    ]);

                    Session::flash('email_verification_successfully', 'Email verification successfully');
                    return view(CmsThemes::CurrentThemePath() . 'MainPage');
                } else {
                    Session::flash('error_at_email_verification', 'Email verification token expired.3');
                    return view(CmsThemes::CurrentThemePath() . 'MainPage');
                }
            } else {
                Session::flash('error_at_email_verification', 'Email verification token expired.2');
                return view(CmsThemes::CurrentThemePath() . 'MainPage');
            }
        } catch (\Exception $e) {
            Session::flash('error_at_email_verification', 'Email verification token expired.1');
            return view(CmsThemes::CurrentThemePath() . 'MainPage');
        }
    }

    public function UserAssetManualIncrease(Request $request)
    {
        try {
            $request->validate([
                'iptDetailKey' => 'required',
                'iptUserPrice' => 'required'
            ]);
            Wallet::insert([
                'user_id' => $request->iptDetailKey,
                'Price' => $request->iptUserPrice,
                'State' => 11
            ]);
            Session::flash('user_asset_manual_increase_successful', config('msg.success'));
            return back();

        } catch (\Exception $e) {
            Session::flash('error_at_user_asset_manual_increase', $e->getMessage());
            return back();
        }
    }

}
