<?php

namespace App\Http\Controllers\Dashboard;

use App\DbModels\Dashboard\News\NewsComment;
use App\MyClasses\CmsFunctions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;


class CommentsController extends Controller
{
    public function showNewsCommentsLists()
    {
        $newsComments = NewsComment::join('news', 'news.id', '=', 'news_comments.news_id')
            ->select(
                'news_comments.*',
                'news.Title', 'news.id AS news_id'
            )
            ->orderBy('news_comments.id', 'DESC')
            ->paginate(100);
        return view('Dashboard.comments.showNewsCommentsLists', compact('newsComments'));
    }


    public function removeNewsComment(Request $request)
    {
        try {
            NewsComment::where('id', $request->id)
                ->delete();
            $message = array(
                'state' => 1,
                'message' => 'رکورد مورد نظر با موفقیت حذف گردید'
            );
            return response()->json($message);
        } catch (\Exception $e) {
            $message = array(
                'state' => 0,
                'message' => $e->getMessage()
            );
            return response()->json($message);
        }
    }


    public function changeStateNewsComment(Request $request)
    {
        try {
            $n = NewsComment::where('id', $request->id)
                ->select('State')
                ->first();
            if ($n->State == 0) {
                NewsComment::where('id', $request->id)
                    ->update([
                        'State' => 1
                    ]);
            } else {
                NewsComment::where('id', $request->id)
                    ->update([
                        'State' => 0
                    ]);
            }
            $message = array(
                'state' => 1,
                'message' => 'وضعیت رکورد تغییر پیدا کرد'
            );
            return response()->json($message);
        } catch (\Exception $e) {
            $message = array(
                'state' => 0,
                'message' => $e->getMessage()
            );
            return response()->json($message);
        }
    }


    public function showNewsComment(Request $request)
    {
        try {
            $NCID = $request->id;
            $newsComment = NewsComment::where('id', $NCID)
                ->select('Message', 'IP','Website','Name','Email','created_at')
                ->first();
            NewsComment::where('id', $NCID)
                ->update([
                    'Seen' => 0
                ]);
            $message = array(
                'state' => 1,
                'message' => $newsComment,
                'NCID'=>CmsFunctions::CustomCoding($NCID),
                'ShowDate'=>CmsFunctions::GetGregorianDateAndConvertToJalaliDate($newsComment['created_at'])
            );
            return response()->json($message);
        } catch (\Exception $e) {
            $message = array(
                'state' => 0,
                'message' => $e->getMessage()
            );
            return response()->json($message);
        }
    }


    public function showInfoForSendEmail(Request $request)
    {
        try {
            switch ($request->Type) {
                case 1:
                    $info = NewsComment::where('id', $request->id)
                        ->select('id', 'Email', 'Message')
                        ->first();
                    break;

            }
            $message = array(
                'state' => 1,
                'message' => $info
            );
            return response()->json($message);
        } catch (\Exception $e) {
            $message = array(
                'state' => 0,
                'message' => $e->getMessage()
            );
            return response()->json($message);
        }
    }

    public function SendEmail(Request $request)
    {
        try {
            DB::transaction(function () use ($request) {
                $responseText = $request->ResponseEmail;
                switch ($request->Type) {
                    case 1:
                        $comment = NewsComment::where('id', $request->CommentId)
                            ->select('Email', 'Message')
                            ->first();
                        $to = $comment->Email;
                        $newsCommentResponse = new NewsCommentResponse();
                        $newsCommentResponse->Response = $responseText;
                        $newsCommentResponse->news_comment_id = $request->CommentId;
                        $newsCommentResponse->save();
                        break;
                }
                Mail::send('EmailBody.newsCommentResponse', ['ResponseContent' => $responseText, 'Message' => $comment->Message], function ($message) use ($to) {
                    $message->subject('پاسخ به نظر شما ازطرف #');
                    $message->from('info@#.com', '#');
                    $message->to($to);
                });
            });
            Session::flash('send-email-for-comment-news-success');
            return back();
        } catch (\Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    public function NewsCommentUpdate(Request $request)
    {
        $request->validate([
            'ShowName'=>'required',
            'ShowMessage'=>'required',
            'NCID'=>'required'
        ]);
        DB::transaction(function () use ($request){
            NewsComment::where('id',CmsFunctions::CustomDecoding($request->NCID))
                ->update([
                    'Name'=>$request->ShowName,
                    'Message'=>$request->ShowMessage,
                    'Email'=>$request->ShowEmail,
                    'Website'=>$request->ShowWebsite,
                    'created_at'=>CmsFunctions::GetJalaliDateAndConvertToGregorianDate(str_replace('/','-',$request->ShowDate))
                ]);
        });
        unset($request);
        Session::flash('news_comment_update_success');
        return back();
    }
}
