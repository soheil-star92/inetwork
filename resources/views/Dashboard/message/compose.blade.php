@extends('Layouts.dashboardMainLayout')
@section('CssFiles')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ URL::asset('assets/login/bower_components/select2/dist/css/select2.min.css') }}">
@endsection
@section('BreadCrumbLevel')
    <li class="active">ارسال پیام</li>
@Stop
@section('BreadCrumbURL','/message/showReceiveMessageLists')
@section('BreadCrumbTitle','پیام ها')
@section('MainContent')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ \App\Facade\DbSettings::GetDbSetting()->Title }} - زیر سیستم پیام ها
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
        @include('Dashboard.sidebar.message')
        <!-- /.col -->
            <div class="col-md-9">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">ارسال پیام جدید</h3>
                    </div>
                    <!-- /.box-header -->
                    <form action="/message/sendMessage" method="post" enctype="multipart/form-data"
                          id="fromSendMessage">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group">
                                <select name="sctUserId[]" id="userId" class="form-control select2" multiple="multiple" required>
                                    @foreach($users as $u)
                                        <option value="{{ $u->id }}">{{ $u->first_name.' '.$u->last_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <input class="form-control" name="Subject" placeholder="موضوع" required>
                            </div>
                            <div class="form-group">
                    <textarea id="mytextarea" class="form-control" name="Message" style="height: 300px">
                        <p>متن پیام ...</p>
                    </textarea>
                            </div>
                            <div class="form-group">
                                <div class="btn btn-default btn-file">
                                    <i class="fa fa-paperclip"></i> ضمیمه
                                    <input type="file" name="Attachment">
                                </div>
                                <p class="help-block">حداکثر سایز 10MB</p>
                                <p class="help-block">فایل های مجاز : pdf,zip,rar,doc,docx,png,jpg,jpeg</p>
                            </div>
                        </div>
                    </form>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="pull-right">
                            <button type="button" class="btn btn-default"><i class="fa fa-pencil"></i> پیش نویس</button>
                            <a href="#" onclick="document.getElementById('fromSendMessage').submit();"
                               class="btn btn-primary"><i class="fa fa-envelope-o"></i> ارسال
                            </a>
                        </div>
                        <a href="/message/showReceiveMessageLists" class="btn btn-default"><i class="fa fa-times"></i>
                            انصراف</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('JsFiles')
    <script src="{{ URL::asset('assets/login/bower_components/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ URL::asset('assets/login/bower_components/tinymce/myStyle/CustomizeFunctions.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ URL::asset('assets/login/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            @if(Session::has('send-message-success'))
            swal("", "پیام شما با موفقیت ارسال گردید", "success");
            @endif
        });
    </script>
    <script>
        $(function () {

            //Initialize Select2 Elements
            $('.select2').select2()
//            CKEDITOR.replace('mytextarea')

        })
    </script>
@endsection