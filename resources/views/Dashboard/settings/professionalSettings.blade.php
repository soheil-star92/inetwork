@extends('Layouts.dashboardMainLayout')
@section('BreadCrumbURL','/settings/setting')
@section('BreadCrumbTitle','تنظیمات اختصاصی سیستم')
@section('CssFiles')
    <link rel="stylesheet" href="/assets/login/bower_components/iCheck/all.css">
@endsection

@section('MainContent')
    <div class="row">
        <section class="col-lg-12 col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <i class="fa fa-info-circle"></i>
                    <h3 class="box-title">
                        {{ \App\Facade\DbSettings::GetDbSetting()->Title }}
                    </h3>
                    <!-- tools box -->
                    <div class="pull-left box-tools">
                        <button type="button" class="btn bg-info btn-sm" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /. tools -->
                </div>
                <div class="box-body">
                    <div class="content">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">تنظیمات اختصاصی سیستم</h3>
                            </div>
                            <div class="box-header with-border">
                                @includeIf('Dashboard.toolbars.settingToolbar')
                            </div>
                        <!-- /.box-header -->
                            <!-- form start -->
                            <form method="post" action="/settings/UpdateProfessionalSetting">
                                {{csrf_field()}}
                                <div class="box-body">
                                    <div class="col-md-12">
                                        <div class="box box-solid">
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <div class="box-group" id="accordion">
                                                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                                                    <div class="col-md-12">
                                                        <!-- Custom Tabs -->
                                                        <div class="nav-tabs-custom">
                                                            <ul class="nav nav-tabs bg-gray-active">
                                                                @foreach($professionalSettingType AS $key=>$t)
                                                                <li @if($key==0) class="active" @endif><a href="#tab_{{$key}}" data-toggle="tab">{{$t->Title}}</a></li>
                                                                @endforeach
                                                            </ul>
                                                            <div class="tab-content">
                                                                @foreach($professionalSettingType AS $key=>$t)
                                                                <div class="tab-pane  @if($key==0) active @endif" id="tab_{{$key}}">
                                                                    @foreach($professionalSettings AS $p)
                                                                        @if($p->setting_professional_type_id == $t->id)
                                                                            <p><input type="checkbox"
                                                                                      name="{{$p->Title}}"
                                                                                      class="flat-red"
                                                                                      @if($p->State) checked @endif> {{$p->Description}}
                                                                            </p>
                                                                        @endif
                                                                    @endforeach
                                                                </div>
                                                                @endforeach
                                                            </div>
                                                            <!-- /.tab-content -->
                                                        </div>
                                                        <!-- nav-tabs-custom -->
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                        <!-- /.box -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-info pull-right">بروزرسانی</button>
                                </div>
                            </form>
                            <!-- /.box-footer -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('JsFiles')
    <script>
        $(document).ready(function () {
            @if(Session::has('update_professional_setting_success'))
            swal("", "تنظیمات سایت بروزرسانی گردید", "success");
            @endif
        });
    </script>
    <script src="/assets/login/bower_components/iCheck/icheck.min.js"></script>
    <Script>
        $(document).ready(function () {
            $('#editC').one('submit', function () {
                $(this).find('button[type="submit"]').attr('disabled', 'disabled');
            });

            //flat CheckBox
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });
    </Script>
@endsection