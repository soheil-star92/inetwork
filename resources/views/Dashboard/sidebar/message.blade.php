<div class="col-md-3">
    <a href="/message/compose" class="btn btn-primary btn-block margin-bottom">ارسال پیام جدید</a>
    <div class="box box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">پوشه ها</h3>
            <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                            class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
                <li @if(Request::path() == urldecode('message/showReceiveMessageLists')) class="active" @endif>
                    @php
                        $UnreadMessage=\App\MyClasses\CmsFunctions::GetUnreadMessage();
                    @endphp
                    <a href="/message/showReceiveMessageLists"><i class="fa fa-inbox"></i>دریافتی
                        @if(count($UnreadMessage) >0)
                            <span class="label label-primary pull-left">{{ count($UnreadMessage) }}</span>
                        @endif
                    </a>
                </li>
                <li @if(Request::path() == urldecode('message/showSendMessageLists')) class="active" @endif><a
                            href="/message/showSendMessageLists"><i class="fa fa-envelope-o"></i>ارسال شده</a></li>
                <li><a href="#"><i class="fa fa-file-text-o"></i> پیش نویس</a></li>
                @if(\App\Facade\OrganizationInfo::GetUserRoleAccess()->hasAccess('sub.system.message.show.trash.icon'))
                    <li @if(Request::path() == urldecode('message/trashMessageLists')) class="active" @endif><a
                                href="/message/trashMessageLists"><i class="fa fa-trash-o"></i> سطل زباله</a></li>
                @endif
            </ul>
        </div>
        <!-- /.box-body -->
    </div>
</div>