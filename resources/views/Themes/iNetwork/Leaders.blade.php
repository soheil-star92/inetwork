@php
    use \App\MyClasses\CmsFunctions;
    use \App\MyClasses\CmsThemes;
@endphp
@extends('Themes.iNetwork.MainLayout')
@section('Content')
    <!-- INNER PAGE BANNER -->
    <div class="wt-bnr-inr overlay-wraper" style="background-image:url(/Themes/HomePage/images/banner/about-banner.jpg);">
        <div class="overlay-main bg-black opacity-07"></div>
        <div class="container">
            <div class="wt-bnr-inr-entry">
                <h1 class="text-white">Profits for Investors </h1>
            </div>
        </div>
    </div>
    <!-- INNER PAGE BANNER END -->

    <!-- BREADCRUMB ROW -->
    <div class="bg-gray-light p-tb20">
        <div class="container">
            <ul class="wt-breadcrumb breadcrumb-style-2">
                <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
                <li>Leaders</li>
            </ul>
        </div>
    </div>
    <!-- BREADCRUMB  ROW END -->

    <!-- ABOUT COMPANY SECTION START -->
    <div class="section-full p-tb100">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="section-head text-left">
                        <span class="wt-title-subline text-gray-dark font-16 m-b15">How to get more profit from invest Chain network?</span>
                        <h2 class="text-uppercase">A New Kind of Investing </h2>
                        <div class="wt-separator-outer"><div class="wt-separator bg-primary"></div></div>
                        <p><strong>
                                Sign up with the marketing code "123173" and get  $50 investment with support request (as loan).
                                <br>

                                We will invest $50 in you and deduct from your profits.  <br>
                                In the first month of your work, if you have a collection with at least 10 person(in 3 Lines), we will add 7% of the total investment of the collection to your investment (at least 2 person in each Line).  <br>
                                <br>
                                If you have a collection  with at least 15 person(in 4 Lines), we will add 10% of the total collection to your investment (at least 2 person per Line).  <br>

                            </strong></p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="wt-media">
                        <img src="/assets/234234c.jpg" alt="" class="img-responsive"/>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="section-head text-left">
                      </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ABOUT COMPANY SECTION END -->



@stop
