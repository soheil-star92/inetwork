@extends('Layouts.dashboardMainLayout')
@section('BreadCrumbLevel')
    <li class="active">سرویس های خبری</li>
@Stop
@section('BreadCrumbURL','/settings/setting')
@section('BreadCrumbTitle','تنظیمات پایه ای سیستم')
@section('CssFiles')
    <link rel="stylesheet" type="text/css"
          href="{{ URL::asset('assets/login/bower_components//fancyBox/jquery.fancybox.min.css') }}">
    <style type="text/css">
        #image-preview {
            width: 200px;
            height: 200px;
            position: relative;
            overflow: hidden;
            background-color: #ffffff;
            color: #ecf0f1;
        }
        #image-preview-edit {
            width: 200px;
            height: 200px;
            position: relative;
            overflow: hidden;
            background-color: #ffffff;
            color: #ecf0f1;
        }

        #image-preview input {
            line-height: 200px;
            font-size: 200px;
            position: absolute;
            opacity: 0;
            z-index: 10;
        }

        #image-preview-edit input {
            line-height: 200px;
            font-size: 200px;
            position: absolute;
            opacity: 0;
            z-index: 10;
        }

        #image-preview label {
            position: absolute;
            z-index: 5;
            opacity: 0.8;
            cursor: pointer;
            background-color: #bdc3c7;
            width: 200px;
            height: 50px;
            font-size: 20px;
            line-height: 50px;
            text-transform: uppercase;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: auto;
            text-align: center;
        }

        #image-preview-edit label {
            position: absolute;
            z-index: 5;
            opacity: 0.8;
            cursor: pointer;
            background-color: #bdc3c7;
            width: 200px;
            height: 50px;
            font-size: 20px;
            line-height: 50px;
            text-transform: uppercase;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: auto;
            text-align: center;
        }
    </style>
@endsection

@section('MainContent')
    <div class="row">
        <section class="col-lg-12 col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <i class="fa fa-info-circle"></i>
                    <h3 class="box-title">
                        {{ \App\Facade\DbSettings::GetDbSetting()->Title }}
                    </h3>
                    <!-- tools box -->
                    <div class="pull-left box-tools">
                        <button type="button" class="btn bg-info btn-sm" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /. tools -->
                </div>

                <!-- modal -->
                <div class="modal modal-info fade" id="modal-add-news-category">
                    <div class="modal-dialog">
                        <form action="/settings/addNewsCategory" id="addC" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">تعریف سرویس خبری جدید</h4>
                                </div>
                                <div class="modal-body">
                                    <p>
                                    <div id="image-preview">
                                        <label for="image-upload" id="image-label">لوگو</label>
                                        <input type="file" name="fileupload" id="image-upload"/>
                                    </div>
                                    </p>
                                    <p>
                                        <label for="PersianTitle">عنوان [ فارسی ]</label>
                                        <input type="text" name="PersianTitle" id="PersianTitle" class="form-control"
                                               required>
                                    </p>
                                    <p>
                                        <label for="EnglishTitle">عنوان [English]</label>
                                        <input type="text" name="EnglishTitle" id="EnglishTitle" class="form-control">
                                    </p>
                                    <p>
                                        <label for="ParentID">سرگروه</label>
                                        <select name="ParentID" id="ParentID" class="form-control">
                                            <option value="0">بدون سرگروه</option>
                                            @foreach($newsCategories As $c)
                                                <option value="{{$c->id}}">{{$c->PersianTitle}}</option>
                                            @endforeach
                                        </select>
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">خروج
                                    </button>
                                    <input type="submit" class="btn btn-outline" value="ثبت">
                                </div>
                            </div>
                        </form>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <div class="modal modal-info fade" id="modal-show-news-category">
                    <div class="modal-dialog">
                        <form action="/settings/editNewsCategory" id="editC" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">نمایش سرویس خبری </h4>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        <span id="ShowLogo"></span>
                                    </p>
                                    <p>
                                        <label for="NewImage">ویرایش لوگو</label>
                                        <input type="file" name="NewImage" id="NewImage"
                                               class="form-control">
                                    </p>
                                    <p>
                                        <input type="hidden" name="showNewsCategoryId" id="showNewsCategoryId">
                                        <label for="ShowPersianTitle">عنوان [ فارسی ]</label>
                                        <input type="text" name="ShowPersianTitle" id="ShowPersianTitle"
                                               class="form-control"
                                               required>
                                    </p>
                                    <p>
                                        <label for="ShowEnglishTitle">عنوان [ English ]</label>
                                        <input type="text" name="ShowEnglishTitle" id="ShowEnglishTitle"
                                               class="form-control">
                                    </p>
                                    <p>
                                        <label for="ShowChildes">زیر مجموعه </label>
                                    <ul id="ShowChildes"></ul>
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">خروج
                                    </button>
                                    <input type="submit" class="btn btn-outline" value="ویرایش">
                                </div>
                            </div>
                        </form>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <div class="modal modal-primary fade" id="modal-show-news-category-child">
                    <div class="modal-dialog">
                        <form action="/settings/editNewsCategoryChild" id="editChild" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">نمایش سرویس خبری </h4>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        <span id="ShowLogoChild"></span>
                                    </p>
                                    <p>
                                        <label for="NewImageChild">ویرایش لوگو</label>
                                        <input type="file" name="NewImageChild" id="NewImageChild"
                                               class="form-control">
                                    </p>
                                    <p>
                                        <input type="hidden" name="showNewsCategoryIdChild" id="showNewsCategoryIdChild">
                                        <label for="ShowPersianTitleChild">عنوان [ فارسی ]</label>
                                        <input type="text" name="ShowPersianTitleChild" id="ShowPersianTitleChild"
                                               class="form-control"
                                               required>
                                    </p>
                                    <p>
                                        <label for="ShowEnglishTitleChild">عنوان [ English ]</label>
                                        <input type="text" name="ShowEnglishTitleChild" id="ShowEnglishTitleChild"
                                               class="form-control">
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">خروج
                                    </button>
                                    <input type="submit" class="btn btn-outline" value="ویرایش">
                                </div>
                            </div>
                        </form>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

                <!-- /.modal -->
                <div class="box-body">
                    <div class="content">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">طبقه بندی سرویس های خبری</h3>
                            </div>
                            <div class="box-header with-border">
                                <a class="btn btn-app" data-toggle="modal" data-target="#modal-add-news-category">
                                    <i class="fa fa-plus"></i>جدید
                                </a>
                                @includeIf('Dashboard.toolbars.settingToolbar')
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table id="keyWordDataTable"
                                           class="table table-bordered table-striped table-responsive table-hover">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>عنوان</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($newsCategories as $key=>$r)
                                            <tr @if($r->State == 1) style="background-color: rgba(200,0,0,0.3)" @endif>
                                                <td>{{ ++$key }}</td>
                                                <td>{{ $r->PersianTitle }}</td>
                                                <td>
                                                    <div class="input-group">
                                                        <button type="button"
                                                                class="btn bg-light-blue-active dropdown-toggle "
                                                                data-toggle="dropdown"
                                                                style="permissionType font-size: 9px !important ">
                                                            <span class="fa fa-caret-down"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a class="btn bg-maroon btn-xs" data-id="{{ $r->id }}"
                                                                   title="حذف رکورد"><span class="fa fa-trash"></span>حذف</a>
                                                            </li>
                                                            <li><a class="btn bg-yellow-gradient btn-xs"
                                                                   data-id="{{ $r->id }}"
                                                                   title="تغییر وضعیت خبر"><span
                                                                            class="fa fa-ban"></span>تغییر وضعیت</a>
                                                            </li>
                                                            <li><a class="btn bg-olive btn-xs" title="ویرایش"
                                                                   data-toggle="modal" data-target="#modal-show-news-category"
                                                                   data-id="{{ $r->id }}"><span
                                                                            class="fa fa-check-square-o"></span>ویرایش</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('JsFiles')
    <script src="{{ URL::asset('assets/login/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/login/bower_components/fancyBox/jquery.fancybox.min.js') }}"></script>
    <script src="{{ URL::asset('assets/login/bower_components/imagePreview/jquery.uploadPreview.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            @if(Session::has('add-news-category-success'))
            swal("", "رکورد جدید با موفقیت اضافه گردید", "success");
            @endif
            @if(Session::has('edit-news-category-success'))
            swal("", "رکورد موردنظر با موفقیت ویرایش گردید", "success");
            @endif
        });
    </script>
    <script>
        function ChildesRemove(id) {
            var token = $('meta[name="csrf-token"]').attr('content');
            swal({
                title: "",
                text: "آیا از حذف این رکورد مطمئن هستید؟",
                icon: "warning",
                buttons: ["خیر", "بله"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: '/settings/removeNewsCategory',
                        data: {_token: token, id: id},
                        type: 'POST',
                        dataType: 'JSON',
                        success: function (data) {
                            if (data.state == 1) {
                                swal("", data.message, "success");
                                location.reload();
                            } else {
                                swal(data.message, {
                                    icon: 'warning',
                                    dangerMode: true
                                });
                            }
                        }
                    });
                }
            });
        }

        $(function () {
            $('#addC').one('submit', function () {
                $(this).find('input[type="submit"]').attr('disabled', 'disabled');
            });

            $('#editC').one('submit', function () {
                $(this).find('input[type="submit"]').attr('disabled', 'disabled');
            });
            $('#editChild').one('submit', function () {
                $(this).find('input[type="submit"]').attr('disabled', 'disabled');
            });
            $('body').on('click', '.bg-olive', function () {
                $('.ShowModalLoadingArea').css({'display':'block','position':'sticky',});
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/settings/showNewsCategory',
                    data: {_token: token, id: id},
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.state == 1) {
                            $('#ShowPersianTitle').val(data.message.PersianTitle);
                            $('#ShowEnglishTitle').val(data.message.EnglishTitle);
                            $('#ShowLogo').empty();
                            $('#ShowLogo').append(data.MainLogo);
                            $('#ShowChildes').empty();
                            $('#ShowChildes').append(data.result);
                            $('#showNewsCategoryId').val(id);
                            $('.ShowModalLoadingArea').css('display','none');
                        } else {
                            swal(data.message, {
                                icon: 'warning',
                                dangerMode: true
                            });
                        }
                    }
                });
            });
            $('body').on('click', '.bg-purple', function () {
                $('.ShowModalLoadingArea').css({'display':'block','position':'sticky',});
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/settings/showNewsCategory',
                    data: {_token: token, id: id},
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.state == 1) {
                            $('#ShowPersianTitleChild').val(data.message.PersianTitle);
                            $('#ShowEnglishTitleChild').val(data.message.EnglishTitle);
                            $('#ShowLogoChild').empty();
                            $('#ShowLogoChild').append(data.MainLogo);
                            $('#showNewsCategoryIdChild').val(id);
                            $('.ShowModalLoadingArea').css('display','none');
                        } else {
                            swal(data.message, {
                                icon: 'warning',
                                dangerMode: true
                            });
                        }
                    }
                });
            });
            $('.bg-maroon').click(function () {
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                var ele = $(this).parent().parent().parent().parent().parent();
                swal({
                    title: "",
                    text: "آیا از حذف این رکورد مطمئن هستید؟",
                    icon: "warning",
                    buttons: ["خیر", "بله"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '/settings/removeNewsCategory',
                            data: {_token: token, id: id},
                            type: 'POST',
                            dataType: 'JSON',
                            success: function (data) {
                                if (data.state == 1) {
                                    ele.fadeOut().remove();
                                    swal("", data.message, "success");
                                } else {
                                    swal(data.message, {
                                        icon: 'warning',
                                        dangerMode: true
                                    });
                                }
                            }
                        });
                    }
                });
            });

        });
        $('.bg-yellow-gradient').click(function () {
            var id = $(this).data('id');
            var token = $('meta[name="csrf-token"]').attr('content');
            swal({
                title: "",
                text: "آیا از تغییر وضعیت این رکورد مطمئن هستید؟",
                icon: "warning",
                buttons: ["خیر", "بله"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: '/settings/NewsCategoryChangeState',
                        data: {_token: token, id: id},
                        type: 'POST',
                        dataType: 'JSON',
                        success: function (data) {
                            if (data.state == 1) {
                                swal("", data.message, "success");
                                location.reload();
                            } else {
                                swal(data.message, {
                                    icon: 'warning',
                                    dangerMode: true
                                });
                            }
                        }
                    });
                }
            });
        });
        $.uploadPreview({
            input_field: "#image-upload,#image-upload-edit",   // Default: .image-upload
            preview_box: "#image-preview,#image-preview-edit",  // Default: .image-preview
            label_field: "#image-label,#image-label-edit",    // Default: .image-label
            label_default: "آپلود عکس جدید",   // Default: Choose File
            label_selected: "تغییر عکس",  // Default: Change File
            no_label: false                 // Default: false
        });

        $('[data-fancybox="images"]').fancybox({
            buttons: [
                'slideShow',
                'fullScreen',
                'thumbs',
                'download',
                'zoom',
                'close'
            ],
        });
    </script>
@endsection
