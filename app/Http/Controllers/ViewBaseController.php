<?php

namespace App\Http\Controllers;

use App\DbModels\Dashboard\ExclusivePages\ExclusivePageLists;
use App\DbModels\Dashboard\News\Keyword;
use App\DbModels\Dashboard\News\News;
use App\DbModels\Dashboard\News\NewsComment;
use App\DbModels\Dashboard\News\NewsKeyword;
use App\DbModels\Dashboard\Notifications\NotificationContactUs;
use App\DbModels\Dashboard\Users\InfoUser;
use App\DbModels\Dashboard\Users\User;
use App\Facade\OrganizationInfo;
use App\MyClasses\CmsFunctions;
use App\MyClasses\CmsThemes;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class ViewBaseController extends Controller
{
    public function MainPage()
    {
        CmsFunctions::SetLocalLanguage();
        return view(CmsThemes::CurrentThemePath() . 'MainPage');
    }

    public function UserRegister()
    {
        CmsFunctions::SetLocalLanguage();
        return view(CmsThemes::CurrentThemePath() . 'UserRegister');
    }

    public function AboutUs()
    {
        CmsFunctions::SetLocalLanguage();
        return view(CmsThemes::CurrentThemePath() . 'AboutUs');
    }

    public function Gifts()
    {
        CmsFunctions::SetLocalLanguage();
        return view(CmsThemes::CurrentThemePath() . 'Gifts');
    }

    public function ProfitPackages()
    {
        CmsFunctions::SetLocalLanguage();
        return view(CmsThemes::CurrentThemePath() . 'Profits');
    }

    public function Leaders()
    {
        CmsFunctions::SetLocalLanguage();
        return view(CmsThemes::CurrentThemePath() . 'Leaders');
    }

    public function ContactUs()
    {
        CmsFunctions::SetLocalLanguage();
        return view(CmsThemes::CurrentThemePath() . 'ContactUs');
    }

    public function ContactUsSubmit(Request $request)
    {
        try {
            $request->validate([
                'FullName' => 'required',
                'Email' => 'required',
                'Message' => 'required',
                'Captcha' => 'required',
            ]);
            $rules = ['Captcha' => 'required|captcha'];
            $validator = \Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                Session::flash('contact_us_submit_captcha_error');
                return back();
            }
            $ncu = new NotificationContactUs();
            $ncu->FullName = $request->FullName;
            $ncu->Email = $request->Email;
            $ncu->Phone = $request->Phone;
            $ncu->Subject = $request->Subject;
            $ncu->Message = $request->Message;
            $ncu->IP = request()->getClientIp();
            $ncu->save();
            Session::flash('contact_us_submit_success');
            return back();
        } catch (\Exception $e) {
            Session::flash('contact_us_submit_error', $e->getMessage());
            return back();
        }
    }

    public function Blog()
    {
        CmsFunctions::SetLocalLanguage();
        $News = News::where('news_state_id', 4)
            ->select('id', 'Title', 'Content', 'Image', 'View')
            ->orderBy('id', 'DESC')
            ->simplePaginate(30);
        return view(CmsThemes::CurrentThemePath() . 'Blog', compact('News'));
    }

    public function NewsCategory($id)
    {
        CmsFunctions::SetLocalLanguage();
        $News = News::where([
            ['news.news_state_id', '=', 4],
            ['news.news_category_id', '=', $id]
        ])
            ->orWhere([
                ['news.news_category_child_id', '=', $id]
            ])
            ->join('news_categories', 'news_categories.id', '=', 'news.news_category_id')
            ->select('news.id', 'news.Title', 'news.created_at', 'news.Content', 'news.Image', 'news.View', 'news.news_category_id', 'news_categories.PersianTitle AS NewsCategoryTitleFa', 'news_categories.EnglishTitle AS NewsCategoryTitleEn')
            ->orderBy('news.id', 'DESC')
            ->simplePaginate(30);
        return view(CmsThemes::CurrentThemePath() . 'Blog', compact('News'));
    }

    public function ShowNews($id)
    {
        try {
            CmsFunctions::SetLocalLanguage();
            if (OrganizationInfo::GetAccessFromProfessionalSettings('show_url_post_as_text')) {
                $News = News::where([
                    ['news.news_state_id', '=', 4],
                    ['news.Title', 'like', str_replace('_', ' ', $id)]
                ])
                    ->join('news_categories', 'news_categories.id', '=', 'news.news_category_id')
                    ->select('news.id', 'news.Title', 'news.Content', 'news.Image', 'news.View', 'news.created_at', 'news.news_category_id', 'news_categories.PersianTitle AS NewsCategoryTitleFa', 'news_categories.EnglishTitle AS NewsCategoryTitleEn')
                    ->first();
            } else {
                $News = News::where([
                    ['news.news_state_id', '=', 4],
                    ['news.id', '=', $id]
                ])
                    ->join('news_categories', 'news_categories.id', '=', 'news.news_category_id')
                    ->select('news.id', 'news.Title', 'news.Content', 'news.Image', 'news.View', 'news.created_at', 'news.news_category_id', 'news_categories.PersianTitle AS NewsCategoryTitle')
                    ->first();
            }
            if (is_null($News)) {
                return redirect('/');
            }
            $View = $News['View'];
            News::where('id', $News['id'])
                ->update([
                    'View' => ++$View
                ]);
            $NewsKeywords = NewsKeyword::where('news_id', $News['id'])
                ->join('keywords', 'keywords.id', '=', 'news_keywords.keyword_id')
                ->select('keywords.id', 'keywords.Title')
                ->get();
            $Comments = NewsComment::where([
                ['State', '=', 1],
                ['news_id', '=', $News['id']]
            ])
                ->select('Name', 'id', 'created_at', 'Message')
                ->orderBy('id', 'DESC')
                ->get();
            return view(CmsThemes::CurrentThemePath() . 'ShowNews', compact('News', 'View', 'NewsKeywords', 'Comments'));
        } catch (\Exception $e) {
            return back();
        }
    }

    public function ShowPages($id)
    {
        try {
            CmsFunctions::SetLocalLanguage();
            if (OrganizationInfo::GetAccessFromProfessionalSettings('show_url_post_as_text')) {
                $Page = ExclusivePageLists::where([
                    ['state', '=', 1],
                    ['Title', 'like', str_replace('_', ' ', $id)]
                ])
                    ->select('id', 'Title', 'Content', 'created_at', 'View')
                    ->first();
            } else {
                $Page = ExclusivePageLists::where([
                    ['state', '=', 1],
                    ['id', '=', $id]
                ])
                    ->select('id', 'Title', 'Content', 'created_at', 'View')
                    ->first();
            }
            if (is_null($Page)) {
                return redirect('/');
            }
            $View = $Page['View'];
            ExclusivePageLists::where('id', $Page['id'])
                ->update([
                    'View' => ++$View
                ]);
            return view(CmsThemes::CurrentThemePath() . 'ShowPage', compact('Page', 'View'));
        } catch (\Exception $e) {
            return back();
        }
    }

    public function tag($Title)
    {
        try {
            CmsFunctions::SetLocalLanguage();
            $keyword = Keyword::where('Title', $Title . '#')->select('id')->first();
            $News = DB::select('CALL sp_show_news_by_keyword(' . $keyword['id'] . ')');
            return view(CmsThemes::CurrentThemePath() . 'Blog', compact('News', 'Title'));
        } catch (\Exception $e) {
            return back();
        }
    }

    public function NewsCommentSubmit(Request $request)
    {
        $captcha = ['Captcha' => 'required|captcha'];
        $captchaCheck = \Validator::make(Input::all(), $captcha);
        if ($captchaCheck->fails()) {
            Session::flash('wrong_captcha_in_news_comment');
            return back()->withInput(Input::all());
        }
        $request->validate([
            'Name' => 'required',
            'Email' => 'required',
            'Message' => 'required',
        ]);
        $nc = new NewsComment();
        $nc->news_id = CmsFunctions::CustomDecoding($request->NewsID);
        $nc->Name = $request->Name;
        $nc->Email = $request->Email;
        $nc->Message = $request->Message;
        $nc->Website = $request->Website;
        $nc->IP = request()->getClientIp();
        $nc->save();
        Session::flash('news_comment_submit_success');
        return bacK();
    }

    public function UserRegisterSubmit(Request $request)
    {
        try {
            if ($request->RegisterCaptcha) {
                $captcha = ['RegisterCaptcha' => 'required|captcha'];
                $captchaCheck = \Validator::make(Input::all(), $captcha);
                if ($captchaCheck->fails()) {
                    Session::flash('error_at_user_public_registered_captcha');
                    return redirect('/UserRegister');
                }
            }
            $request->validate([
                'FirstName' => 'required',
                'FamilyName' => 'required',
                'Email' => 'required',
                'Password' => 'required',
                'NationalCode' => 'required',
            ]);

            $image = 'assets/no-image.png';
            Sentinel::register(array(
                'email' => $request->Email,
                'password' => $request->Password,
                'first_name' => $request->FirstName,
                'last_name' => $request->FamilyName,
            ));

            $lastUserId = User::orderBy('id', 'DESC')
                ->select('id')
                ->first();
            $InfoUserRow = InfoUser::orderBy('BusinessCode', 'DESC')
                ->select('BusinessCode')
                ->first();
            $BusinessCode = $InfoUserRow->BusinessCode;

            $userId = $lastUserId->id;
            $user = Sentinel::findById($userId);
            $role = Sentinel::findRoleById(3);
            $role->users()->attach($user);

            $infoUser = new InfoUser();
            $infoUser->user_id = $userId;
            $infoUser->id_i = $request->NationalCode;
            $infoUser->Phone = '';
            $infoUser->Address = '';
            $infoUser->Image = $image;
            $infoUser->BusinessCode = ++$BusinessCode;
            $infoUser->Save();
            Session::flash('user_public_registered_success');
            return back();
        } catch (\Exception $e) {
            Session::flash('error_at_user_public_registered', $e->getMessage());
            return back();
        }
    }

    public function PublicRegistration(Request $request)
    {
        try {
            if ($request->RegisterCaptcha) {
                $captcha = ['RegisterCaptcha' => 'required|captcha'];
                $captchaCheck = \Validator::make(Input::all(), $captcha);
                if ($captchaCheck->fails()) {
                    Session::flash('error_at_user_public_registered_captcha');
                    return redirect('/');
                }
            }
            $request->validate([
                'iptFirstName' => 'required',
                'iptLastName' => 'required',
                'iptEmail' => 'required',
                'iptPassword' => 'required',
                'iptRepeatPassword' => 'required',
                'iptReferralCode' => 'required',
                'iptPhone' => 'required',
                'iptNationalCode' => 'required',
            ]);

            if ($request->iptRepeatPassword != $request->iptPassword) {
                Session::flash('error_at_password_mismatch');
                return redirect('/');
            }

            $userExist = User::where('email',$request->iptEmail)->select('id')->first();
            if (!empty($userExist)) {
                Session::flash('error_at_email_already_exist');
                return redirect('/');
            }
            $MainReferralCode=$request->iptReferralCode;
            $getUserByReferralCode = InfoUser::where('ReferralCode',$MainReferralCode)->orderBy('ReferralCode', 'DESC')
                ->select('ReferralCode', 'user_id')
                ->first();
            if (empty($getUserByReferralCode)) {
                Session::flash('error_at_referral_code');
                return redirect('/');
            }
            $image = 'assets/no-image.png';
            $UserEmail=$request->iptEmail;
            $Password=$request->iptPassword;
            Sentinel::register(array(
                'email' => $UserEmail,
                'password' => $Password,
                'first_name' => $request->iptFirstName,
                'last_name' => $request->iptLastName,
            ));

            $lastUserId = User::orderBy('id', 'DESC')
                ->select('id')
                ->first();
            $InfoUserRow = InfoUser::orderBy('ReferralCode', 'DESC')
                ->select('ReferralCode')
                ->first();

            $ReferralCode = $InfoUserRow->ReferralCode;

            $userId = $lastUserId->id;
            $user = Sentinel::findById($userId);
            $role = Sentinel::findRoleById(4);
            $role->users()->attach($user);
            $userRef=($ReferralCode + (rand(9, 39)));
            $infoUser = new InfoUser();
            $infoUser->user_id = $userId;
            $infoUser->id_i = $request->iptNationalCode;
            $infoUser->Phone = $request->iptPhone;
            $infoUser->Image = $image;
            $infoUser->City = $request->iptCity;
            $infoUser->ReferralCode = $userRef;
            $infoUser->HeadCode = $request->iptReferralCode;
            $infoUser->country_id = $request->iptCountry;
            $infoUser->Save();
            if(!is_null($MainReferralCode)){
                $ParentUserID=$getUserByReferralCode->user_id;
                $CurrentUserID=$userId;
                CmsFunctions::UserAndLevelsCheck($CurrentUserID,$ParentUserID,$userRef,$MainReferralCode,0);
            }

            Mail::send('EmailBody.ProfileInfo', ['Email' => $UserEmail,'Password' => $Password], function ($message) use ($UserEmail) {
                $message->subject('Welcome to Invest Chain Network Groups.co');
                $message->from('info@icngroups.online', 'Invest Chain Network Groups.');
                $message->to($UserEmail);
            });
            Session::flash('user_public_registered_success');
            return redirect('/login');
        } catch (\Exception $e) {
            //dd($e->getMessage());
            Session::flash('error_at_user_public_registered', $e->getMessage());
            return back();
        }
    }


}
