@php
    $settings=\App\Facade\DbSettings::GetDbSetting();
    $userLogged=\App\Facade\OrganizationInfo::GetUserInfoWithoutParameter();
    $RegisteredEmail=$userLogged->RegisteredEmail;
    $UID=$userLogged->UserId;
@endphp

@section('CssFiles')
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/forms/wizard/bs-stepper.min.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/vendors/css/forms/select/select2.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/bordered-layout.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/plugins/forms/form-wizard.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/plugins/forms/form-validation.css">
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/pages/modal-create-app.css">
    <!-- END: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/app-assets/css/plugins/extensions/ext-component-sweet-alerts.css">
    <!-- BEGIN: Custom CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/Themes/Admin/assets/css/style.css">
    <!-- END: Custom CSS-->

@stop
@extends('Layouts.dashboardMainLayout')

@section('MainContent')
    @unless($RegisteredEmail)
        <!-- two factor auth modal -->
        <div class="modal fade" id="twoFactorAuthModal" tabindex="-1" aria-labelledby="twoFactorAuthTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg two-factor-auth">
                <div class="modal-content">
                    <div class="modal-header bg-transparent">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body pb-5 px-sm-5 mx-50">
                        <h1 class="text-center mb-1" id="twoFactorAuthTitle">Send Verification Link</h1>

                        <div class="custom-options-checkable">
                            <input class="custom-option-item-check" type="radio" name="twoFactorAuthRadio"
                                   id="twoFactorAuthApps" value="apps-auth" checked/>
                            <label for="twoFactorAuthApps"
                                   class="custom-option-item d-flex align-items-center flex-column flex-sm-row px-3 py-2 mb-2">
                                <span><i data-feather="inbox" class="font-large-2 me-sm-2 mb-2 mb-sm-0"></i></span>
                                <span>
                                            <span class="custom-option-item-title h3"> Send verification link</span>
                                            <span class="d-block mt-75">
                                                 email: <strong>{{$userLogged->email}}</strong>
                                            </span>
                                        </span>
                            </label>

                        </div>

                        <button id="btnSendVerification" class="btn btn-primary float-end mt-3">
                            <span class="me-50">Send</span>
                            <i data-feather="chevron-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- / two factor auth modal -->
    @endunless
    <!-- Edit User Modal -->

    <div class="modal fade" id="editUser" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-edit-user">
            <div class="modal-content">
                <div class="modal-header bg-transparent">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                    <div class="modal-body pb-5 px-sm-5 pt-50">
                        <div class="text-center mb-2">
                            <h1 class="mb-1">Edit User Information</h1>
                        </div>
                        <form id="editUserForm" class="row gy-1 pt-75" action="/AdminPanel/UpdateUserInfo" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="iptKey" value="{{\App\MyClasses\CmsFunctions::EncodedText($UID)}}">
                            <div class="col-12 col-md-6">
                                <label class="form-label" for="">Profile Image</label>
                                <img src="/{{$userLogged->Image}}" height="150" width="150" alt="">
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="form-label" for="iptImage">Upload New Image (*.png , *.jpg)</label>
                                <input type="file" id="iptImage" name="iptImage" class="form-control" >
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="form-label" for="modalEditUserFirstName">First Name</label>
                                <input type="text" id="modalEditUserFirstName" name="iptFirstName"
                                       class="form-control" placeholder="John" value="{{$userLogged->first_name}}" @if($RegisteredEmail) disabled @endif
                                       data-msg="Please enter your first name"/>
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="form-label" for="modalEditUserLastName">Last Name</label>
                                <input type="text" id="modalEditUserLastName" name="iptLastName"
                                       class="form-control" placeholder="Doe" value="{{$userLogged->last_name}}"
                                       data-msg="Please enter your last name" @if($RegisteredEmail) disabled @endif />
                            </div>

                            <div class="col-12 col-md-6">
                                <label class="form-label" for="modalEditUserEmail">Email:</label>
                                <input type="text" id="modalEditUserEmail" name="modalEditUserEmail"
                                       class="form-control" value="{{$userLogged->email}}"
                                       placeholder="example@domain.com" disabled/>
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="form-label" for="modalEditUserPhone">Contact</label>
                                <input type="text" id="modalEditUserPhone" name="iptPhone"
                                       class="form-control phone-number-mask" placeholder="+1 (609) 933-44-22"
                                       value="{{$userLogged->Phone}}"/>
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="form-label" for="modalEditUserCountry">Country</label>
                                <select id="modalEditUserCountry" name="iptCountryID"
                                        class="select2 form-select">
                                    <option value="">Select Value</option>
                                    @foreach(\App\DbModels\Dashboard\Setting\Country::all() as $row)
                                        <option value="{{$row->id}}"
                                                @if($row->id == $userLogged->country_id) selected @endif>{{$row->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="form-label" for="iptCity">City</label>
                                <input type="text" id="iptCity" name="iptCity"
                                       class="form-control" placeholder="City"
                                       value="{{$userLogged->City}}"/>
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="form-label" for="iptWallet">Wallet Address</label>
                                <input type="text" id="iptWallet" name="iptWallet"
                                       class="form-control" placeholder="wallet" @if($userLogged->confirmWallet) disabled @endif
                                       value="{{$userLogged->Wallet}}"/>
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="form-label" for="iptNationalCode">NationallCode</label>
                                <input type="text" id="iptNationalCode" name="iptNationalCode"
                                       class="form-control" placeholder="National Unique Code" @if($RegisteredEmail) disabled @endif
                                       value="{{$userLogged->id_i}}"/>
                            </div>
                            <div class="col-12">
                                <div class="d-flex align-items-center mt-1">
                                    <div class="form-check form-switch form-check-primary">
                                        <input type="checkbox" class="form-check-input" id="customSwitch10" name="iptConfirmWallet"
                                               @if($userLogged->confirmWallet) checked="checked" disabled @endif />
                                        <label class="form-check-label" for="customSwitch10">
                                            <span class="switch-icon-left"><i data-feather="check"></i></span>
                                            <span class="switch-icon-right"><i data-feather="x"></i></span>
                                        </label>
                                    </div>
                                    <label class="form-check-label fw-bolder" for="customSwitch10">The user is responsible for confirming the wallet, Are you sure?</label>
                                </div>
                            </div>

                            <div class="col-12 col-md-6">
                                <label class="form-label" for="iptPassword">Change Password</label>
                                <input type="password" id="iptPassword" name="iptPassword"
                                       class="form-control" placeholder=""
                                      />
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="form-label" for="iptRepPassword">Repeat Password</label>
                                <input type="password" id="iptRepPassword" name="iptRepPassword"
                                       class="form-control"
                                       />
                            </div>

                            <div class="col-12">
                                <div class="d-flex align-items-center mt-1">
                                    <div class="form-check form-switch form-check-primary">
                                        <input type="checkbox" class="form-check-input" id="customSwitch10"
                                               @if($RegisteredEmail) checked @endif disabled/>
                                        <label class="form-check-label" for="customSwitch10">
                                            <span class="switch-icon-left"><i data-feather="check"></i></span>
                                            <span class="switch-icon-right"><i data-feather="x"></i></span>
                                        </label>
                                    </div>
                                    <label class="form-check-label fw-bolder" for="customSwitch10">Verified
                                        Email</label>
                                </div>
                            </div>
                            <div class="col-12 text-center mt-2 pt-50">
                                <button type="submit" class="btn btn-primary me-1">Submit</button>
                                <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal"
                                        aria-label="Close">
                                    Discard
                                </button>
                            </div>
                        </form>
                    </div>
            </div>
        </div>
    </div>
    <!--/ Edit User Modal -->

    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Analytics Start -->
                <section id="dashboard-analytics">
                    <div class="row match-height">
                        <div class="col-xl-12 col-md-12 col-12">
                            <div class="card card-statistics">
                                <div class="card-header">
                                    <h4 class="card-title">Statistics</h4>
                                    <div class="d-flex align-items-center">
                                        <p class="card-text font-small-2 me-25 mb-0">User Detail information</p>
                                    </div>
                                </div>
                                <div class="card-body statistics-body">
                                    <div class="row">
                                        <div class="col-xl-4 col-sm-6 col-12 mb-2 mb-xl-0">
                                            <div class="d-flex flex-row">
                                                <div class="avatar bg-light-info me-2">
                                                    <div class="avatar-content">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user avatar-icon"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                                    </div>
                                                </div>
                                                <div class="my-auto">
                                                    <h4 class="fw-bolder mb-0">{{\App\MyClasses\CmsFunctions::GetTotalUserSubset($UID)}}</h4>
                                                    <p class="card-text font-small-3 mb-0">Total User SubSet</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-4 col-sm-6 col-12 mb-2 mb-sm-0">
                                            <div class="d-flex flex-row">
                                                <div class="avatar bg-light-danger me-2">
                                                    <div class="avatar-content">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-box avatar-icon"><path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path><polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline><line x1="12" y1="22.08" x2="12" y2="12"></line></svg>
                                                    </div>
                                                </div>
                                                <div class="my-auto">
                                                    <h4 class="fw-bolder mb-0">{{\App\MyClasses\CmsFunctions::GetTotalActiveUserPackage($UID)}}</h4>
                                                    <p class="card-text font-small-3 mb-0">Total Active Package</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-4 col-sm-6 col-12">
                                            <div class="d-flex flex-row">
                                                <div class="avatar bg-light-success me-2">
                                                    <div class="avatar-content">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign avatar-icon"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>
                                                    </div>
                                                </div>
                                                <div class="my-auto">
                                                    <h4 class="fw-bolder mb-0">${{\App\MyClasses\CmsFunctions::GetUserWalletAssets($UID)}}</h4>
                                                    <p class="card-text font-small-3 mb-0">Assets</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @unless($RegisteredEmail)
                        <!-- Greetings Card starts -->
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <!-- two factor auth -->
                                <div class="card">
                                    <div class="card-body text-center">
                                        <i data-feather="check-square" class="font-large-2 mb-1"></i>
                                        <h5 class="card-title">Verify Your Email Address</h5>
                                        <p class="card-text">
                                            Please click the button below to send verification link to your email
                                            address.
                                        </p>

                                        <!-- modal trigger button -->
                                        <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                                data-bs-target="#twoFactorAuthModal">
                                            Send
                                        </button>
                                    </div>
                                </div>
                                <!-- / two factor auth  -->
                            </div>
                            <!-- Greetings Card ends -->
                    @endunless
                    <!-- Orders Chart Card starts -->
                        @unless($isPackage)
                        <div class="col-lg-6 col-sm-6 col-12">
                            <div class="card">
                                <div class="card-header flex-column align-items-start pb-0">
                                    <div class="avatar bg-light-warning p-50 m-0">
                                        <div class="avatar-content">
                                            <i data-feather="package" class="font-medium-5"></i>
                                        </div>
                                    </div>
                                    <p class="card-text"><strong>Package Activation Time </strong></p>
                                    <h2 class="fw-bolder mt-1">15 Days after registration</h2>
                                    <p class="card-text">after 15 days registration , If isn't activated a package ,
                                        your account deactivated will be automatically.</p>
                                </div>
                                <div id="order-chart"></div>
                            </div>
                        </div>
                        @endunless
                        @if($RegisteredEmail && $isPackage)
                        <div class="col-lg-6 col-sm-6 col-12">
                            <div class="card">
                                <div class="card-header flex-column align-items-start pb-0">
                                    <div class="avatar bg-light-warning p-50 m-0">
                                        <div class="avatar-content">
                                            <i data-feather="users" class="font-medium-5"></i>
                                        </div>
                                    </div>
                                    <p class="card-text">User Referral Code</p>
                                    <h2 class="fw-bolder mt-1">Referral Code : {{$userLogged->ReferralCode}}</h2>
                                </div>
                                <div id="order-chart"></div>
                            </div>
                        </div>
                        @endif
                        <div class="col-lg-6 col-sm-6 col-12">
                            <!-- edit user  -->

                            <div class="card">
                                <div class="card-body text-center">
                                    <i data-feather="user" class="font-large-2 mb-1"></i>
                                    <h5 class="card-title">Edit User Info</h5>
                                    <p class="card-text">click the button below to modify the your account
                                        information.</p>
                                    <!-- modal trigger button -->
                                    <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                            data-bs-target="#editUser">Show
                                    </button>
                                </div>
                            </div>
                            <!-- / edit user  -->
                        </div>
                        <!-- Orders Chart Card ends -->
                    </div>

                </section>
                <!-- Dashboard Analytics end -->

            </div>
        </div>
    </div>

@stop

@section('JsFiles')
    <!-- BEGIN: Vendor JS-->
    <script src="/Themes/Admin/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="/Themes/Admin/app-assets/vendors/js/forms/wizard/bs-stepper.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/forms/cleave/cleave.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/forms/cleave/addons/cleave-phone.us.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/Themes/Admin/app-assets/js/core/app-menu.js"></script>
    <script src="/Themes/Admin/app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-add-new-cc.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/page-pricing.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-add-new-address.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-create-app.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-two-factor-auth.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-edit-user.js"></script>
    <script src="/Themes/Admin/app-assets/js/scripts/pages/modal-share-project.js"></script>
    <!-- END: Page JS-->
    <!-- BEGIN: Page Vendor JS-->
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <script src="/Themes/Admin/app-assets/vendors/js/extensions/polyfill.min.js"></script>
    <!-- END: Page Vendor JS-->

    <script>

        $(window).on('load', function () {
            var countNotify=$('#iptCountNotify').val();
            if(countNotify>0) {
                Swal.fire({
                    title: 'information!',
                    text: 'You have notification from administrators!',
                    icon: 'info',
                    customClass: {
                        confirmButton: 'btn btn-primary'
                    },
                    buttonsStyling: false
                });
            }
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        });

        $(document).ready(function () {
            var btnSendEmailVerification = $('#btnSendVerification');
            if (btnSendEmailVerification.length) {
                btnSendEmailVerification.on('click', function () {
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Yes, send it!',
                        customClass: {
                            confirmButton: 'btn btn-primary',
                            cancelButton: 'btn btn-outline-danger ms-1'
                        },
                        buttonsStyling: false
                    }).then(function (result) {
                        if (result.value) {
                            var token = $('meta[name="csrf-token"]').attr('content');
                            $.ajax({
                                url: '/AdminPanel/SendUserEmailVerification',
                                data: {_token: token},
                                type: 'POST',
                                dataType: 'JSON',
                                success: function (data) {
                                    if (data.state == 1) {
                                        toastr['success'](data.message, 'Success', {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            progressBar: true  });
                                    } else {
                                        toastr['error'](data.message, 'Error', {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            progressBar: true  });
                                    }
                                }
                            });
                        }
                    });
                });
            }

            $('#editUserForm').one('submit', function () {
                $(this).find('button[type="submit"]').attr('disabled', 'disabled');
            });
            @if(Session::has('error_at_update_user_info'))
                toastr['error']('{{Session::get('error_at_update_user_info')}}', 'Error', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true
            });
            @endif
            @if(Session::has('update_user_info_successfully'))
                toastr['success']('{{Session::get('update_user_info_successfully')}}', 'Success', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true
            });
            @endif
        });

    </script>
@stop
