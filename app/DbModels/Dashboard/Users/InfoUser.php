<?php

namespace App\DbModels\Dashboard\Users;

use Illuminate\Database\Eloquent\Model;

class InfoUser extends Model
{
    public $timestamps=false;
}
