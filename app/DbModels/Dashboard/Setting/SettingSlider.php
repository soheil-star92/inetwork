<?php

namespace App\DbModels\Dashboard\Setting;

use Illuminate\Database\Eloquent\Model;

class SettingSlider extends Model
{
		protected $table='settings_sliders';
    public $timestamps=false;
}
