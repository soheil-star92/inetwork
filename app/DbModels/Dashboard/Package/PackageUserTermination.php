<?php

namespace App\DbModels\Dashboard\Package;

use Illuminate\Database\Eloquent\Model;

class PackageUserTermination extends Model
{
    protected $table='user_package_termination';
}
