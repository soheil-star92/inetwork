@php
    use \App\MyClasses\CmsFunctions;
    use \App\MyClasses\CmsThemes;
    $BaseSetting=CmsFunctions::BaseSetting()
@endphp
@extends('Themes.ServatAM.ServatAMMainLayout')
@section('Content')
    <div class="row confirmationLicense warper">
        <div class="col-sm-12 col-md-5 px-0">
            <table class="h-100 w-100">
                <tbody>
                <tr class="">
                    <td class="align-middle font-weight-bold">
                        <p class="orange-text title-font-main orange-title">ثبت نام</p>
                        <h2 class="font-weight-bold comLicenseSummary">همین حالا می توانید سرمایه گذاری را شروع کنید</h2>
                        <p class="font-weight-light comLicenseDetail">برای استفاده از تمامی امکانات ما کافی است در سامانه ثروت آفرینی مدرن ثبت نام کرده و سپس از مزایای سیستم استفاده نمایید
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
        <div class="col-sm-12 col-md-1"></div>
        <div class="col-sm-12 col-md-6 mx-auto text-center d-lg-inline-block px-0">
            <table class="h-100 w-100">
                <tbody>
                <tr>
                    <td class="align-middle">
                        <div id="license_body" class="font-weight-light comLicenseDetail" style="display:None;text-align: right;direction: rtl;">
                        </div>
                        <form action="/UserRegisterSubmit" method="post" class="licenseConfirmForm" id="license_form">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-sm-6 col-md-6 px-3">
                                    <div class="form-group">
                                        <label class=" inputPlaceholder" for="FirstName">نام</label>
                                        <input type="text"
                                               class="form-control form-control-lg login-form-input "
                                               id="FirstName" name="FirstName"
                                               aria-describedby="user">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 px-3">
                                    <div class="form-group">
                                        <label class=" inputPlaceholder" for="FamilyName">نام خانوادگی</label>
                                        <input type="text"
                                               class="form-control form-control-lg login-form-input "
                                               id="FamilyName" name="FamilyName">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-6 px-3">
                                    <div class="form-group">
                                        <label class=" inputPlaceholder" for="NationalCode">کدملی</label>
                                        <input type="text"
                                               class="form-control form-control-lg login-form-input "
                                               id="NationalCode" name="NationalCode"
                                               aria-describedby="user">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 px-3">
                                    <div class="form-group">
                                        <label class=" inputPlaceholder" for="Email">پست الکترونیکی</label>
                                        <input type="email"
                                               class="form-control form-control-lg login-form-input "
                                               id="Email" name="Email">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-6 px-3">
                                    <div class="form-group">
                                        <label class=" inputPlaceholder" for="Password">کلمه عبور</label>
                                        <input type="password"
                                               class="form-control form-control-lg login-form-input "
                                               id="Password" name="Password">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-6 px-3">
                                    <div class="form-group">
                                        <label class=" inputPlaceholder" for="RegisterCaptcha">کد امنیتی</label>
                                        <input type="text"
                                               class="form-control form-control-lg login-form-input "
                                               id="RegisterCaptcha" name="RegisterCaptcha"
                                               aria-describedby="user">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 px-3">
                                    <div class="form-group">
                                        {!! captcha_img('flat') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12 px-3">
                                    <div class="form-group" style="text-align: right;">
                                        <button type="submit"
                                                class="btn funds-more-Button w-100 license-form-button">
                                            ثبت نام
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            @if(Session::has('user_public_registered_success'))
            swal("", "کاربری شما با موفقیت ایجاد گردید", "success");
            @endif
            @if(Session::has('error_at_user_public_registered_captcha'))
            swal("خطا در ورود کد امنیتی", {
                icon: "warning",
                dangerMode: true,
            });
            @endif
            @if(Session::has('error_at_user_public_registered'))
            swal("{{Session::get('error_at_user_public_registered')}}", {
                icon: "warning",
                dangerMode: true,
            });
            @endif
        });
    </script>
@stop