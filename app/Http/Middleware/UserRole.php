<?php

namespace App\Http\Middleware;

use App\DbModels\Dashboard\Users\RoleUser;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Closure;

class UserRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $RoleTitle)
    {
        $getUser = Sentinel::getUser();
        $roleId = $getUser['id'];
        $roleUser = RoleUser::where('user_id', $roleId)
            ->select('role_id')
            ->first();
        $roleId = $roleUser->role_id;

        $user = Sentinel::findRoleById($roleId);
        if ($user->hasAccess([$RoleTitle])) {
            return $next($request);
        } else {
            return back();
        }
    }
}
