<?php

namespace App\DbModels\Dashboard\Notifications;

use Illuminate\Database\Eloquent\Model;

class NotificationContactUs extends Model
{
    protected $table = 'notifications_contact_us';
}
