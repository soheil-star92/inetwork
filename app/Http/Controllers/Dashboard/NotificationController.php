<?php

namespace App\Http\Controllers\Dashboard;

use App\DbModels\Dashboard\Notifications\NotificationContactUs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class NotificationController extends Controller
{
    public function ContactUsRequests()
    {
        $ContactUsRequests = NotificationContactUs::orderBy('id', 'DESC')
            ->paginate(100);
        return view('Dashboard.Notifications.ContactUsRequests', compact('ContactUsRequests'));
    }

    public function ContactUsShow(Request $request)
    {
        try {
            DB::transaction(function () use ($request) {
                NotificationContactUs::where('id', $request->id)
                    ->update([
                        'State' => 0
                    ]);
            });
            $ncu = NotificationContactUs::where('id', $request->id)
                ->select('FullName', 'Email', 'Phone', 'Subject', 'Message')
                ->first();
            $message = array(
                'state' => 1,
                'message' => $ncu
            );
            unset($request);
            return response()->json($message);
        } catch (\Exception $e) {
            $message = [
                'state' => 0,
                'message' => $e->getMessage()
            ];
            return response()->json($message);
        }
    }

    public function ContactUsRemove(Request $request)
    {
        try {
            DB::transaction(function () use ($request) {
                NotificationContactUs::where('id', $request->id)
                    ->delete();
            });
            $message = array(
                'state' => 1,
                'message' => 'رکورد مورد نظر با موفقیت حذف گردید'
            );
            return response()->json($message);
        } catch (\Exception $e) {
            $message = [
                'state' => 0,
                'message' => $e->getMessage()
            ];
            return response()->json($message);
        }
    }
}
