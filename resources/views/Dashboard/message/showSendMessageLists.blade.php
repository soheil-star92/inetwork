@extends('Layouts.dashboardMainLayout')
@section('CssFiles')
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('assets/login/bower_components/iCheck/flat/blue.css') }}">
@endsection
@section('BreadCrumbLevel')
    <li class="active">پیام های ارسالی</li>
@Stop
@section('BreadCrumbURL','/message/showReceiveMessageLists')
@section('BreadCrumbTitle','پیام ها')
@section('MainContent')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ \App\Facade\DbSettings::GetDbSetting()->Title }} - زیر سیستم پیام ها
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
        @include('Dashboard.sidebar.message')
        <!-- /.col -->
            <form action="/message/sendToTrashByFrom" method="post" id="formSendToTrash">
                {{ csrf_field() }}
                <div class="col-md-9">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">پیام های ارسالی</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <div class="mailbox-controls">
                                <!-- Check all button -->
                                <div class="pull-right">
                                    <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i
                                                class="fa fa-square-o"></i>
                                    </button>
                                    <div class="btn-group">
                                        <a href="#" onclick="submitForm();" class="btn btn-default btn-sm"><i
                                                    class="fa fa-trash-o"></i>
                                        </a>
                                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i>
                                        </button>
                                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.btn-group -->
                                <a href="/message/showSendMessageLists" class="btn btn-default btn-sm"><i
                                            class="fa fa-refresh"></i></a>
                                <!-- /.pull-right -->
                            </div>
                            <div class="table-responsive mailbox-messages">
                                <table id="keyWordDataTable"
                                       class="table table-bordered table-striped table-responsive table-hover">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>کاربر دریافت کننده</th>
                                        <th>عنوان</th>
                                        <th></th>
                                        <th>تاریخ ارسال</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($messages AS $m)
                                        <tr>
                                            <td><input type="checkbox" name="msgId[]" value="{{ $m->id }}"></td>
                                            <td class="mailbox-name">
                                                <a href="/message/readMessage/{{ $m->id }}">
                                                    {{ $m->first_name.' '.$m->last_name }}
                                                </a>
                                            </td>
                                            <td class="mailbox-subject"><b>{{ $m->Subject }}</b>
                                            </td>
                                            <td class="mailbox-attachment">
                                                @unless(is_null($m->Attachment))
                                                    <i class="fa fa-paperclip"></i>
                                                @endunless
                                            </td>
                                            <td class="mailbox-date">
                                                @if(\App\Facade\OrganizationInfo::GetAccessFromProfessionalSettings('show_date_in_human_function'))
                                                    {{\App\MyClasses\CmsFunctions::GetDateForHumans($m['created_at'])}}
                                                @else
                                                    {{ \App\MyClasses\CmsFunctions::GetGregorianDateAndConvertToJalaliDate($m->created_at) }}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <!-- /.table -->
                            </div>
                            <!-- /.mail-box-messages -->
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer no-padding">
                            <div class="mailbox-controls">
                                <!-- Check all button -->
                                <div class="pull-right">
                                    <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i
                                                class="fa fa-square-o"></i>
                                    </button>
                                    <div class="btn-group">
                                        <a href="#" onclick="submitForm();" class="btn btn-default btn-sm"><i
                                                    class="fa fa-trash-o"></i>
                                        </a>
                                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i>
                                        </button>
                                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.btn-group -->
                                <a href="/message/showSendMessageLists" class="btn btn-default btn-sm"><i
                                            class="fa fa-refresh"></i></a>
                                <!-- /.pull-right -->
                            </div>
                        </div>
                    </div>
                    <!-- /. box -->
                </div>
            </form>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('JsFiles')
    <script src="{{ URL::asset('assets/login/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/login/bower_components/iCheck/icheck.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            @if(Session::has('send-message-to-trash-by-from-success'))
            swal("","پیام های انتخابی به سطل زباله انتقال یافت", "success");
            @endif
        });
    </script>
    <script>
        $(function () {
            //Enable iCheck plugin for checkboxes
            //iCheck for checkbox and radio inputs
            $('.mailbox-messages input[type="checkbox"]').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });

            //Enable check and uncheck all functionality
            $(".checkbox-toggle").click(function () {
                var clicks = $(this).data('clicks');
                if (clicks) {
                    //Uncheck all checkboxes
                    $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
                    $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
                } else {
                    //Check all checkboxes
                    $(".mailbox-messages input[type='checkbox']").iCheck("check");
                    $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
                }
                $(this).data("clicks", !clicks);
            });

        })

        function submitForm(){
            swal({
                title: "",
                text: "آیا از حذف پیام/های مورد نظر اطمینان دارید؟",
                icon: "warning",
                buttons: ["خیر", "بله"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    document.getElementById('formSendToTrash').submit();
                }
            });
        }
    </script>
@endsection
