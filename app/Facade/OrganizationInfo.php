<?php

namespace App\Facade;


use Illuminate\Support\Facades\Facade;

class OrganizationInfo extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'OrganizationInfo';
    }
}