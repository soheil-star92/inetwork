@extends('Layouts.dashboardMainLayout')
@section('BreadCrumbLevel')
    <li class="active"><a href="/news/addNewsForm">ارسال خبر جدید</a></li>
@Stop
@section('BreadCrumbURL','/news/newsLists')
@section('BreadCrumbTitle','لیست اخبار')
@section('CssFiles')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ URL::asset('assets/login/bower_components/select2/dist/css/select2.min.css') }}">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="/assets/login/bower_components/iCheck/all.css">
@endsection

@section('MainContent')
    <div class="row">
        <section class="col-lg-12 col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <i class="fa fa-info-circle"></i>
                    <h3 class="box-title">
                        {{ \App\Facade\DbSettings::GetDbSetting()->Title }}
                    </h3>
                    <!-- tools box -->
                    <div class="pull-left box-tools">
                        <button type="button" class="btn bg-info btn-sm" data-widget="collapse"><i
                                class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /. tools -->
                </div>
                <div class="box-body">
                    <div class="content">
                        @include('Dashboard.toolbars.newsToolbar')
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">خبر جدید</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <form method="post" action="/news/addNews" id="addNewsForm" class="form-horizontal"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="box-body">
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">روتیتر خبر</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="TopTitle" class="form-control"
                                                   placeholder="روتیتر خبر" value="{{ old('TopTitle') }}" autofocus>
                                        </div>
                                        <label class="col-sm-1 control-label">زیرتیتر خبر</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="BottomTitle" class="form-control"
                                                   placeholder="زیرتیتر خبر" value="{{ old('BottomTitle') }}"
                                            >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">تیتر خبر</label>
                                        <div class="col-sm-11">
                                            <input type="text" name="Title" id="Title" class="form-control"
                                                   placeholder="تیتر خبر" value="{{ old('Title') }}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">متن خبر</label>
                                        <div class="col-sm-11">
                                            <textarea name="Content" id="mytextarea"
                                                      style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                                      required>
                                                {{ old('Content') }}
                                            </textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">سرویس</label>
                                        <div class="col-sm-2">
                                            <select name="NewsCategoryId" id="NewsCategoryId" class="form-control">
                                                @foreach($newsCategories as $nc)
                                                    <option value="{{ $nc->id }}">{{ $nc->PersianTitle }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <label class="col-sm-1 control-label">زیرمجموعه</label>
                                        <div class="col-sm-2">
                                            <select name="NewsCategoryChildID" id="NewsCategoryChildID"
                                                    class="form-control">
                                                <option value="0">بدون زیرمجموعه</option>
                                                @foreach($newsCategoryChildes as $nc)
                                                    <option value="{{ $nc->id }}">{{ $nc->PersianTitle }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <label class="col-sm-1 control-label">عکس خبر</label>

                                        <div class="col-sm-3">
                                            <input type="file" name="imageFile" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">کلمات کلیدی</label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2" multiple="multiple"
                                                    data-placeholder="کلمه کلیدی" name="keywords[]" id="keyword">
                                                @foreach($keywords as $kw)
                                                    <option value="{{ $kw->id }}">{{ $kw->Title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">ویژگی خبر</label>
                                        <div class="col-sm-10">
                                            اسلایدر:<input type="checkbox" name="SliderState" id="SliderState"
                                                           class="flat-red">

                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-info pull-right">ثبت</button>
                                </div>
                                <!-- /.box-footer -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <input type="hidden" id="flag" value="0">
    </div>
@endsection

@section('JsFiles')
    <!-- TinyMCE Editor -->
    <script src="{{ URL::asset('assets/login/bower_components/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ URL::asset('assets/login/bower_components/tinymce/myStyle/CustomizeFunctions.js') }}"></script>
    <!-- CK Editor -->
    {{--<script src="/assets/login/bower_components/ckeditor/ckeditor.js"></script>--}}
    <!-- Select2 -->
    <script src="{{ URL::asset('assets/login/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- iCheck 1.0.1 -->
    <script src="/assets/login/bower_components/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function () {
            @if(Session::has('add_news_success'))
            swal("", "خبر ارسالی شما در صف اخبار در دست بررسی قرار گرفت و پس از تایید مدیریت برروی سایت قرار می گیرد", "success");
            @endif
            @if(Session::has('error_at_news_add'))
            swal("{{Session::get('error_at_news_add')}}", {
                icon: "warning",
                dangerMode: true,
            });
            @endif
        });
    </script>
    <script type="text/javascript">
        $(function () {

            $('#NewsCategoryId').change(function () {
                var id = $(this).val();
                var token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/news/GetNewsCategoryChildesByAjax',
                    data: {_token: token, id: id},
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.state == 1) {
                            $('#NewsCategoryChildID').empty();
                            $('#NewsCategoryChildID').append('<option value="0">بدون     طبقه بندی</option>');
                            jQuery.each(data.message, function (key, value) {
                                $('#NewsCategoryChildID').append('<option value="' + value.id + '">' + value.PersianTitle + '</option>');
                            });
                        } else {
                            alert(data.message);
                        }
                    }
                });
            });


            //Initialize Select2 Elements
            $('.select2').select2();
            // CKEDITOR.replace('mytextarea');
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            })
        });
        $('#Title').change(function () {
            var titleValue = $(this).val();
            var sliceTitle = titleValue.split(' ');
            var flag = $('#flag').val();
            jQuery.each(sliceTitle, function (key, value) {
                if ((value.length > 3) && (flag === '0')) {
                    $('#keyword').append('<option value="' + value + '">' + value + '</option>');
                }
            });
            $('#flag').val('1');
        });
        $('#addNewsForm').one('submit', function () {
            $(this).find('button[type="submit"]').attr('disabled', 'disabled');
        });
    </script>
@endsection
