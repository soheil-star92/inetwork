@extends('Layouts.dashboardMainLayout')
@section('BreadCrumbURL','/users/roles')
@section('BreadCrumbTitle','لیست سطوح دسترسی')
@section('CssFiles')
    <link rel="stylesheet" href="/assets/login/bower_components/iCheck/all.css">
@endsection
@section('MainContent')
    <div class="row">
        <section class="col-lg-12 col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <i class="fa fa-info-circle"></i>
                    <h3 class="box-title">
                        {{ \App\Facade\DbSettings::GetDbSetting()->Title }}
                    </h3>
                    <!-- tools box -->
                    <div class="pull-left box-tools">
                        <button type="button" class="btn bg-info btn-sm" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /. tools -->
                </div>

                <!-- modal -->
                <div class="modal modal-info fade" id="modal-add-role">
                    <div class="modal-dialog">
                        <form action="/users/addRole" id="addC" method="post">
                            {{ csrf_field() }}
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">تعریف سطح دسترسی جدید</h4>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        <label for="txtRoleName">عنوان سطح دسترسی</label>
                                        <input type="text" name="txtRoleName" id="txtRoleName" class="form-control"
                                               required>
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">خروج
                                    </button>
                                    <input type="submit" class="btn btn-outline" value="ثبت">
                                </div>
                            </div>
                        </form>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <div class="modal modal-info fade" id="modal-show-permissions">
                    <div class="modal-dialog">
                        <form action="/users/changePermissions" id="editC" method="post">
                            {{ csrf_field() }}
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">دسترسی های( <span id="roleTitle"></span> )</h4>
                                </div>
                                <div class="modal-body">
                                    <p><input type="hidden" name="txtRoleId" value="" id="txtRoleId"></p>
                                    <p id="showRole"></p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">خروج
                                    </button>
                                    <input type="submit" class="btn btn-outline" value="به روزرسانی">
                                </div>
                            </div>
                        </form>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <div class="box-body">
                    <div class="content">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">سطوح دسترسی کاربران</h3>
                            </div>
                            <div class="box-header with-border">
                                <a class="btn btn-app" data-toggle="modal" data-target="#modal-add-role">
                                    <i class="fa fa-plus"></i>جدید
                                </a>
                                @include('Dashboard.toolbars.userToolbar')
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table id="keyWordDataTable"
                                           class="table table-bordered table-striped table-responsive table-hover">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>عنوان</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($roles as $key=>$r)
                                            @php
                                                $getUser=\App\Facade\OrganizationInfo::GetUserRoleAccess();
                                            @endphp
                                            @if( $r->id != 1 || $getUser->id == 1)
                                                <tr>
                                                    <td>{{ $key }}</td>
                                                    <td>{{ $r['name'] }}</td>
                                                    <td>
                                                        <div class="input-group">
                                                            <button type="button"
                                                                    class="btn bg-light-blue-active dropdown-toggle "
                                                                    data-toggle="dropdown"
                                                                    style="font-size: 9px !important ">
                                                                <span class="fa fa-caret-down"></span></button>
                                                            <ul class="dropdown-menu">
                                                                @if($r->id != 1)
                                                                <li><a class="btn bg-maroon btn-xs"
                                                                       data-id="{{ $r->id }}" title="حذف رکورد"><span
                                                                                class="fa fa-trash"></span>حذف</a></li>
                                                                @endif
                                                                <li><a class="btn bg-olive btn-xs" title="دسترسی ها "
                                                                       data-toggle="modal"
                                                                       data-target="#modal-show-permissions"
                                                                       data-id="{{ $r->id }}"><span
                                                                                class="fa fa-check-square-o"></span>دسترسی
                                                                        ها</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('JsFiles')
    <script src="{{ URL::asset('assets/login/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="/assets/login/bower_components/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function () {
            @if(Session::has('change_permission_success'))
            swal("", "سطوح دسترسی ویرایش گردید", "success");
            @endif
            @if(Session::has('add_role_success'))
            swal("", "سطح دسترسی با موفقیت ایجاد گردید", "success");
            @endif
        });
    </script>
    <script>
        $(function () {
            $('#addC').one('submit', function () {
                $(this).find('input[type="submit"]').attr('disabled', 'disabled');
            });

            $('#editC').one('submit', function () {
                $(this).find('input[type="submit"]').attr('disabled', 'disabled');
            });

            //flat CheckBox
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });


            $('body').on('click', '.bg-olive', function () {
                $('.ShowModalLoadingArea').css({'display': 'block', 'position': 'sticky',});
                var id = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/users/showPermissions',
                    data: {_token: token, RoleId: id},
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.state == 1) {
                            $('#showRole').empty();
                            $('#showRole').append(data.message);
                            $('#roleTitle').empty();
                            $('#roleTitle').append(data.Title);
                            $('#txtRoleId').val(id);
                            $('.ShowModalLoadingArea').css('display', 'none');
                        } else {
                            swal(data.message, {
                                icon: 'warning',
                                dangerMode: true
                            });
                        }
                    }
                });

            });

            $('.bg-maroon').click(function () {
                var roleId = $(this).data('id');
                var token = $('meta[name="csrf-token"]').attr('content');
                var ele = $(this).parent().parent().parent().parent().parent();
                swal({
                    title: "",
                    text: "آیا از حذف این رکورد مطمئن هستید؟",
                    icon: "warning",
                    buttons: ["خیر", "بله"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '/users/removeRole',
                            data: {_token: token, roleId: roleId},
                            type: 'POST',
                            dataType: 'JSON',
                            success: function (data) {
                                if (data.state == 1) {
                                    ele.fadeOut().remove();
                                    swal("", data.message, "success");
                                } else {
                                    swal(data.message, {
                                        icon: 'warning',
                                        dangerMode: true
                                    });
                                }
                            }
                        });
                    }
                });
            });

        })
    </script>
@endsection
