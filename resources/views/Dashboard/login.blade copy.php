<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8"/>
    <title>  ورود به سیستم مدیریت - {{ \App\Facade\DbSettings::GetDbSetting()->Title }}</title>
    <meta name="description"
          content="{{ \App\Facade\DbSettings::GetDbSetting()->Description }}">
    <meta name="keywords"
          content="{{  \App\Facade\DbSettings::GetDbSetting()->Keywords }}">
    <meta name="author" content="محمد سهیلی احرار -09180182180">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Language" content="fa">
    <link href="{{ URL::asset('assets/login/style.css') }}" rel="stylesheet" type="text/css"/>
</head>
<body id="content">
<div id="Show"></div>
<div class="Joomina">
    <div class="first">
        <a class="home" href="/"></a>
        <h1>ورود به بخش مدیریت</h1>
    </div>
    <form action="/authentication" method="POST" name="login" id="form1">
        {{ csrf_field() }}
        <input type="hidden" name="CheckCaptcha" value="1">
        <div class="inputs">
            <input name="txtUserName" type="text"
                   placeholder="نام کاربری" autofocus>
            <input name="txtPassword" type="password"
                   placeholder="کلمه عبور">
            <p>{!! captcha_img('flat') !!}</p>
            <input class="" name="Captcha" type="text"
                   placeholder="کدامنیتی">

        </div>
        <div class="second">
            <div class="right">
                <label for="remember">مرا به خاطر بسپار</label>
                <input name="remember" id="remember" type="checkbox" value="">
                <a href="#">گذرواژه خود را فراموش کرده اید؟</a>
            </div>
            @if(Session::has('authentication_failed'))
                <div class="right">
                <span style="color:Red">
                    خطا در ورود به سیستم
                </span>
                </div>
            @endif
            @if(Session::has('user_is_blocked'))
                <div class="right">
                <span style="color:Red">
                    کاربری شما غیرفعال شده است.
                </span>
                </div>
            @endif
            @if(Session::has('wrong_captcha_in_login_error'))
                <div class="right">
                <span style="color:Red">
                    اشتباه در کد امنیتی ورود به سایت
                </span>
                </div>
            @endif
            <div class="logins">
                <input name="submit" type="submit" value="&#1608;&#1585;&#1608;&#1583;">
            </div>
        </div>
    </form>
</div>
</body>
</html>