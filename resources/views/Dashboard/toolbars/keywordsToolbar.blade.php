<div class="modal modal-info fade" id="modal-keywords-filter">
    <div class="modal-dialog">
        <form action="/news/SearchInKeywords" id="search" method="post">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">فیلتر</h4>
                </div>
                <div class="modal-body">
                    <p>
                        <label for="searchKey">کلید واژه</label>
                        <input type="text" name="searchKey" id="searchKey"
                               class="form-control">
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left"
                            data-dismiss="modal">خروج
                    </button>
                    <input type="submit" class="btn btn-outline" value="جست و جو">
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<a class="btn btn-app" href="/news/keywords">
    <i class="fa fa-keyboard-o"></i>کلمات کلیدی
</a>

<a class="btn btn-app" href="/news/topKeywordsStatic">
    <i class="fa fa-area-chart"></i>نمودار برترین ها
</a>

<a class="btn btn-app" data-toggle="modal" data-target="#modal-keywords-filter">
    <i class="fa fa-filter"></i>فیلتر
</a>