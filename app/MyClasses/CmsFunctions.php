<?php

namespace App\MyClasses;

use App\DbModels\Dashboard\ExclusivePages\ExclusivePageLists;
use App\DbModels\Dashboard\Fund\UserLevel;
use App\DbModels\Dashboard\Lang;
use App\DbModels\Dashboard\Language;
use App\DbModels\Dashboard\Message\Message;
use App\DbModels\Dashboard\News\News;
use App\DbModels\Dashboard\News\NewsCategory;
use App\DbModels\Dashboard\News\NewsComment;
use App\DbModels\Dashboard\Notifications\NotificationContactUs;
use App\DbModels\Dashboard\Package\PackageUser;
use App\DbModels\Dashboard\Setting\Link;
use App\DbModels\Dashboard\Setting\Setting;
use App\DbModels\Dashboard\Setting\SocialMedia;
use App\DbModels\Dashboard\Support\Notification;
use App\DbModels\Dashboard\Users\InfoUser;
use App\DbModels\Dashboard\Users\Role;
use App\DbModels\Dashboard\Wallet\Wallet;
use App\User;
use Carbon\Carbon;
use Hekmatinasser\Verta\Verta;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use http\Env\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;

class CmsFunctions
{
    /*
    * Notice: for before date
    * Get gregorian date and convert to shamsi date for humans function
    */


    public static function GetDateForHumans($DateTimeForParse)
    {
        $now = Carbon::now();
        $label = '';
        $humanDate = Carbon::parse($DateTimeForParse)->diffForHumans($now);
        if (app()->getLocale() == 'en') {
            $humanArray = explode(' ', $humanDate, 2);
            switch ($humanArray[1]) {
                case 'second before':
                    $label = 'ثانیه قبل';
                    break;
                case 'second after':
                    $label = 'ثانیه بعد';
                    break;
                case 'seconds before':
                    $label = 'ثانیه قبل';
                    break;
                case 'seconds after':
                    $label = 'ثانیه بعد';
                    break;
                case 'minutes before':
                    $label = 'دقیقه قبل';
                    break;
                case 'minutes after':
                    $label = 'دقیقه بعد';
                    break;
                case 'minute before':
                    $label = 'دقیقه قبل';
                    break;
                case 'minute after':
                    $label = 'دقیقه بعد';
                    break;
                case 'hours before':
                    $label = 'ساعت قبل';
                    break;
                case 'hours after':
                    $label = 'ساعت بعد';
                    break;
                case 'hour before':
                    $label = 'ساعت قبل';
                    break;
                case 'hour after':
                    $label = 'ساعت بعد';
                    break;
                case 'days before':
                    $label = 'روز قبل';
                    break;
                case 'days after':
                    $label = 'روز بعد';
                    break;
                case 'day before':
                    $label = 'روز قبل';
                    break;
                case 'day after':
                    $label = 'روز بعد';
                    break;
                case 'weeks before':
                    $label = 'هفته قبل';
                    break;
                case 'weeks after':
                    $label = 'هفته بعد';
                    break;
                case 'week before':
                    $label = 'هفته قبل';
                    break;
                case 'week after':
                    $label = 'هفته بعد';
                    break;
                case 'months before':
                    $label = 'ماه قبل';
                    break;
                case 'months after':
                    $label = 'ماه بعد';
                    break;
                case 'month before':
                    $label = 'ماه قبل';
                    break;
                case 'month after':
                    $label = 'ماه بعد';
                    break;
                case 'year before':
                    $label = 'سال قبل';
                    break;
                case 'year after':
                    $label = 'سال بعد';
                    break;
                case 'years before':
                    $label = 'سال قبل';
                    break;
                case 'years after':
                    $label = 'سال بعد';
                    break;
            }
            $hd = $humanArray[0] . $label;
            return $hd;
        } else {
            return str_replace('از', ' ', $humanDate);
        }

    }

    public static function GetVariableAndConvertToHtmlTags($Parameter, $Description)
    {
        $p = explode(':', $Parameter);
        $d = explode(':', $Description);
        $result = '';
        switch ($p[1]) {
            case 'DATETIME':
                $result .= "<input type='text' data-mask='13##/##/## ##:##:##' value='13__/__/__ __:__:__' name='" . $p[0] . "' id='DATETIME-" . $p[0] . "' class='form-control' placeholder='" . $d[1] . "' pattern='([1][34][0-9][0-9])/(0[1-9]|1[012])/(0[1-9]|1[0-9]|2[0-9]|3[01]) [0-9]{2}:[0-9]{2}:[0-9]{2}' required>";
                break;
            case 'INT':
                $result .= "<input type='text'  name='" . $p[0] . "' id='" . $p[0] . "' class='form-control' placeholder='" . $d[1] . "' required>";
                break;
        }
        return $result;
    }

    public static function GetGregorianDateAndConvertToJalaliDate($gd)
    {
        $date = explode(' ', $gd);
        list($y, $m, $d) = explode('-', $date[0]);
        $jd = Verta::getJalali($y, $m, $d);
        $part2 = isset($date[1]) ? ' ' . $date[1] : ' ';
        $jalaliDate = $jd[0] . '/' . $jd[1] . '/' . $jd[2] . $part2;
        return $jalaliDate;
    }

    public static function GetJalaliDateAndConvertToGregorianDate($jd)
    {
        $date = explode(' ', $jd);
        list($y, $m, $d) = explode('-', $date[0]);
        $gd = Verta::getGregorian($y, $m, $d);
        $part2 = isset($date[1]) ? ' ' . $date[1] : ' ';
        $gergorianDate = $gd[0] . '/' . $gd[1] . '/' . $gd[2] . $part2;
        return $gergorianDate;
    }

    public static function CheckPermissionInRole($RoleID, $Permission)
    {
        $Role = Role::where('id', $RoleID)
            ->select('permissions')
            ->first();
        $p = json_decode($Role->permissions);
        unset($RoleID, $Role);
        return $p->$Permission;
    }

    public static function WhoamiURL()
    {
        $currentUrl = URL::current();
        $parseUrl = parse_url($currentUrl);
        $url = $parseUrl['scheme'] . '://' . $parseUrl['host'] . '/';
        return $url;
    }

    public static function MoveElementInArray(&$array, $to, $from)
    {
        //$out = array_splice($array, $to, 1);
        //array_splice($array, $from, 0, $out);
        $fromObj = $array[$from];
        unset($array[$from]);
        $toObj = $array[$to];
        unset($array[$to]);
        $array[$to] = $fromObj;
        $array[$from] = $toObj;
        ksort($array);
        return $array;
    }

    public static function GetVerificationStatus()
    {
        $user = Sentinel::getUser();
        return InfoUser::where('user_id', $user['id'])->select('ActiveEmail', 'ActivePhone')->first();
    }

    public static function CustomCoding($str)
    {
        return str_rot13(base64_encode($str));
    }

    public static function CustomDecoding($str)
    {
        return base64_decode(str_rot13($str));
    }

    public static function BaseSetting()
    {
        return Setting::first();
    }

    public static function SocialMedia()
    {
        return SocialMedia::first();
    }

    public static function Links()
    {
        return Link::where('Disabled', 0)->get();
    }

    public static function GetUnreadNotificationsContactUs()
    {
        return NotificationContactUs::where('State', 1)
            ->select('id', 'created_at', 'FullName', 'Subject')
            ->orderBy('id', 'DESC')
            ->get();
    }

    public static function GetLastNews()
    {
        return News::where('news_state_id', 4)
            ->select('id', 'Top_Title', 'Title', 'Content', 'View', 'Image', 'created_at')
            ->orderBy('id', 'DESC')
            ->limit(4)
            ->get();
    }

    public static function GetNewsCategories()
    {
        return NewsCategory::where([
            ['parent_id', '=', null],
            ['State', '=', 0]
        ])
            ->get();
    }

    public static function GetNewsCategoriesChild($id)
    {
        return NewsCategory::where('parent_id', $id)
            ->get();
    }

    public static function GetUnActiveNewsComment()
    {
        return NewsComment::where('Seen', 1)
            ->select('Message', 'Name', 'created_at')
            ->orderBy('id', 'DESC')
            ->get();
    }

    public static function GetUnreadMessage()
    {
        $user = Sentinel::getUser();
        $unreadMessage = Message::where([
            ['messages.to_trash', '=', 0],
            ['messages.Show', '=', 0],
            ['messages.to_user_id', '=', $user['id']],
        ])
            ->join('users', 'users.id', '=', 'messages.from_user_id')
            ->leftJoin('info_users', 'info_users.user_id', '=', 'users.id')
            ->select(
                'messages.*', 'users.first_name', 'users.last_name', 'info_users.Image'
            )
            ->get();
        return $unreadMessage;
    }

    public static function GetEnableExclusivePages()
    {
        return ExclusivePageLists::where('state', 1)
            ->select('id', 'Title', 'Content')
            ->get();
    }

    public static function SetLocalLanguage()
    {
        $defaultLanguage = Session('DefaultLanguage');
        if (is_null($defaultLanguage)) {
            App::setLocale('fa');
        } else {
            App::setLocale($defaultLanguage);
        }
    }

    public static function GetLanguageFlag()
    {
        $GL = Lang::where('Title', app()->getLocale())->select('Flag')->first();
        return $GL->Flag;
    }

    public static function RecognizeLanguageForNews($Title, $id, $Element)
    {
        $result = '';
        if (app()->getLocale() == 'fa') {
            $result = $Title;
        } elseif (app()->getLocale() == 'en') {
            $result = __('News/' . $id . '.' . $Element);
            if (!file_exists(resource_path('lang/en/News/' . $id . '.php'))) {
                $result = $Title;
            } else {
                if ($result == '') {
                    $result = $Title;
                }
            }
        }
        return $result;
    }


    public static function GetNewsCategoryByID($NewsCategoryID, $Count = -1)
    {
        if ($Count == -1) {
            $rows = News::where([
                ['news_category_id', '=', $NewsCategoryID],
                ['news_state_id', '=', 4]
            ])->paginate(10);
        } else {
            $rows = News::where([
                ['news_category_id', '=', $NewsCategoryID],
                ['news_state_id', '=', 4]
            ])->limit($Count)->get();
        }
        return $rows;
    }

    public static function UserAndLevelsCheck($CurrentUserID, $ParentUserID, $userRef, $MainReferralCode, $LID = 1)
    {
        /*
        $New = new UserLevel();
        $New->parent_user_id = $ParentUserID;
        $New->child_user_id = $CurrentUserID;
        $New->LevelID = $LID;
        $New->save();
        */

        $existRow = UserLevel::where([
            ['parent_user_id', '=', $ParentUserID],
            ['child_user_id', '=', $CurrentUserID],
            ['LevelID', '=', $LID],
        ])->select('id')->first();
        if ($ParentUserID != $CurrentUserID) {
            if (empty($existRow) && ($LID > 0)) {
                UserLevel::insert([
                    'parent_user_id' => $ParentUserID,
                    'child_user_id' => $CurrentUserID,
                    'LevelID' => $LID
                ]);
            }
        }

        if ($MainReferralCode == 0) {
            return 1;
        }
        $BeforeLevelCheck = InfoUser::where('ReferralCode', $MainReferralCode)->select('user_id', 'ReferralCode', 'HeadCode')->first();
        $UserRefCode = $BeforeLevelCheck->ReferralCode;
        $HeadRefCode = $BeforeLevelCheck->HeadCode;
        $ParentUserID = $BeforeLevelCheck->user_id;
        $LID += 1;
        return self::UserAndLevelsCheck($CurrentUserID, $ParentUserID, $UserRefCode, $HeadRefCode, $LID);
    }

    public static function EncodedText($txt)
    {
        $txt = str_rot13($txt);
        $txt = base64_encode($txt);
        return $txt;
    }

    public static function DecodedText($txt)
    {
        $txt = base64_decode($txt);
        $txt = str_rot13($txt);
        return $txt;
    }

    public static function GetUserWalletAssets($UID)
    {
        $rows = Wallet::where('user_id', $UID)->orderBy('id')->select('Price')->get();
        $Price = 0;
        foreach ($rows as $row) {
            $Price += $row->Price;
        }
        return $Price;
    }

    public static function GetWalletState(int $id)
    {
        switch ($id) {
            case 11:
                return 'increase from manual operation by administrator';
            case 10:
                return 'decrease from in progress withdraw ';
            case 9:
                return 'increase from return main price from package expire time ';
            case 8:
                return 'increase from package daily profit ';
            case 7:
                return 'increase from buy package level profit';
            case 6:
                return 'decrease from termination package';
            case 5:
                return 'decrease from buy package';
            case 4:
                return 'decrease from Withdraw';
            case 3:
                return 'decrease from Transfer';
            case 2:
                return 'increase from Transfer';
            case 1:
                return 'increase from Deposit';
            case 0:
                return 'default';
        }
    }

    public static function GetUID()
    {
        $user = Sentinel::getUser();
        return $user['id'];
    }

    public static function WalletDepositStatus(int $id)
    {
        switch ($id) {
            case 1:
                return '<span class="badge badge-light-success rounded-pill ms-auto me-1">Valid</span>';
            case 0:
                return '<span class="badge badge-light-danger rounded-pill ms-auto me-1">not Valid</span>';
        }
    }

    public static function SupportNotificationSeen(int $id)
    {
        switch ($id) {
            case 0:
                return '<span class="badge badge-light-success rounded-pill ms-auto me-1">New</span>';
            case 1:
                return '';
        }
    }

    public static function WalletWithdrawStatus(int $id)
    {
        switch ($id) {
            case 2:
                return '<span class="badge badge-light-warning rounded-pill ms-auto me-1">in progress</span>';
            case 1:
                return '<span class="badge badge-light-success rounded-pill ms-auto me-1">Valid</span>';
            case 0:
                return '<span class="badge badge-light-danger rounded-pill ms-auto me-1">not Valid</span>';
        }
    }

    public static function UserPackageStatus(int $id)
    {
        switch ($id) {
            case 1:
                return '<span class="badge badge-light-danger rounded-pill ms-auto me-1">Disable</span>';
            case 0:
                return '<span class="badge badge-light-success rounded-pill ms-auto me-1">enable</span>';
        }
    }

    public static function GetUserInfo(int $id)
    {
        return User::where('id', $id)->select('id', 'email', 'first_name', 'last_name')->first();
    }

    public static function ProfitLevels(int $id)
    {
        if ($id == 1) {
            return 5;
        } elseif ($id == 2) {
            return 2;
        } elseif ($id == 3) {
            return 1;
        } elseif ($id == 4) {
            return 0.75;
        } elseif ($id >= 5 && $id <= 8) {
            return 0.50;
        } elseif ($id >= 9 && $id <= 12) {
            return 0.25;
        } elseif ($id >= 13 && $id <= 21) {
            return 0.125;
        }
    }

    public static function SupportHeaderStatus(int $id)
    {
        switch ($id) {
            case 0:
                return '<span class="badge badge-light-warning rounded-pill ms-auto me-1">waiting for response</span>';
            case 1:
                return '<span class="badge badge-light-success rounded-pill ms-auto me-1">has been responded</span>';
            case 2:
                return '<span class="badge badge-light-danger rounded-pill ms-auto me-1">has been ended</span>';
        }
    }

    public static function CheckUserStatus(int $id)
    {
        switch ($id) {
            case 0:
                return '';
            case 1:
                return '<span class="badge badge-light-danger rounded-pill ms-auto me-1">has been blocked</span>';
        }
    }

    public static function GetUserUnseenNotification($UID)
    {
        return Notification::where([['to_user_id','=',$UID],['Seen','=',0]])->select('id')->get();
    }

    public static function TotalUserPackageProfit(int $id,int $UID)
    {
        $rows=Wallet::where([
            ['user_id','=',$UID],
            ['State','=','8'],
            ['ReferenceID','=',$id]
        ])->select('Price')->orderBy('id')->get();
        $Profit =0;
        foreach ($rows as $row) {
            $Profit += $row->Price;
        }
        return $Profit;
    }

    public static function GetTotalActiveUserPackage(int $UID)
    {
        $rows=PackageUser::where([
            ['user_id','=',$UID],
            ['Disable','=',0]
        ])->select('id')->get();
        return count($rows);
    }

    public static function GetTotalUserSubset($UID)
    {
        $rows=UserLevel::where('parent_user_id',$UID)->select('id')->get();
        return count($rows);
    }

    public static function GetTotalIncomeProfits(int $UID)
    {
        $Price=0;
        $Rows=Wallet::where('user_id',$UID)
            ->whereIn('State',[7,8])
            ->select('Price')
            ->get();
        foreach ($Rows as $row) {
            $Price+=$row->Price;
        }
        return $Price;
    }
}
