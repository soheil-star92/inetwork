<?php

use App\TestModel;
use Illuminate\Database\Seeder;

class TestTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = \Faker\Factory::create();
		for ($i = 0; $i < 50; $i++) {
			TestModel::create([
					'FirstName' => $faker->firstName,
					'LastName' => $faker->lastName,
					'Age' => $faker->numberBetween(0, 120),
					'Address' => $faker->address
			]);
		}
	}
}
