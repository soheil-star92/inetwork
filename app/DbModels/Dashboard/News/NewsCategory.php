<?php

namespace App\DbModels\Dashboard\News;

use Illuminate\Database\Eloquent\Model;

class NewsCategory extends Model
{
    public $timestamps=false;
}
